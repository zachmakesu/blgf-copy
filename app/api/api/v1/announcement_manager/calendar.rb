module API
  module V1
    module AnnouncementManager
      class Calendar < Grape::API
        resource :calendar do

          desc 'List Calendar events'
          get do
            authorize!(current_user, User::Role::ALL )
            dates = Announcement.calendar.read_by(current_user.role).order(posted_at: :desc)
            my_arrays = {}
            dates.each_with_index { |date, index|
              if my_arrays[date.posted_at[0..9]].present?
                my_arrays[date.posted_at[0..9]] << {"id": date.id, "name": date.message, "country": "PH", "date": date.posted_at, "send_to": date.send_to, "start_time_at": date.start_time_at }
              else
                my_arrays[date.posted_at[0..9]] = ["id": date.id, "name": date.message, "country": "PH", "date": date.posted_at, "send_to": date.send_to, "start_time_at": date.start_time_at]
              end
            }
            my_arrays
          end

          desc 'Add Calendar event'
          # message     (string)
          # posted_at   (string) format(yyyy-mm-dd)
          # start_time_at  (string) format(00:00:00)
          # send_to     (string) hr^employee^treasurer^all, ex: hr^treasurer or hr^employee. default value is all
          post do
            authorize!(current_user, User::Role::ADMIN )
            validate_date_format!(params[:posted_at])
            validate_time_format!(params[:start_time_at]) if params[:start_time_at].present?
            handler = AnnouncementHandler.new(params, 0).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::AnnouncementManager::Announcement::CalendarInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Calendar event'
          # id          (integer)
          # message     (string)
          # posted_at   (string) format(yyyy-mm-dd)
          # start_time_at  (string) format(00:00:00)
          # send_to     (string) hr^employee^treasurer^all, ex: hr^treasurer or hr^employee. default value is all
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            validate_date_format!(params[:posted_at])
            validate_time_format!(params[:start_time_at]) if params[:start_time_at].present?
            handler = AnnouncementHandler.new(params, 0).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::AnnouncementManager::Announcement::CalendarInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Calendar event'
          # id          (integer)
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AnnouncementHandler.new(params, 0).delete
            if handler.response[:success]
              dates = Announcement.calendar.order(posted_at: :desc)
              my_arrays = {}
              dates.each_with_index { |date, index|
                 my_arrays[date.posted_at[0..9]] = ["id": date.id, "name": date.message, "country": "PH", "date": date.posted_at]
              }
              my_arrays
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end

        resource :announcements do
  
          desc 'List Announcements'
          get do
            authorize!(current_user, User::Role::ALL )
            present Announcement, with: Entities::V1::AnnouncementManager::Announcement::Announcement, current_user: current_user
          end

          desc 'Add Announcement'
          # message     (string)
          # start_time_at  (string) format(00:00:00)
          # send_to     (string) hr^employee^treasurer^all, ex: hr^treasurer or hr^employee. default value is all
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = AnnouncementHandler.new(params, 1).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::AnnouncementManager::Announcement::AnnouncementInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Announcement'
          # id          (integer)
          # message     (string)
          # start_time_at  (string) format(00:00:00)
          # send_to     (string) hr^employee^treasurer^all, ex: hr^treasurer or hr^employee. default value is all
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AnnouncementHandler.new(params, 1).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::AnnouncementManager::Announcement::AnnouncementInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Announcement'
          # id          (integer)
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AnnouncementHandler.new(params, 1).delete
            if handler.response[:success]
              present Announcement, with: Entities::V1::AnnouncementManager::Announcement::Announcement, current_user: current_user
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end

      end

    end
  end
end
