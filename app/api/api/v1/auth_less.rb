module API
  module V1
    class AuthLess < Grape::API
      mount API::V1::Login
      mount API::V1::ForgotPassword
      #mount API::V1::Register
    end
  end
end
