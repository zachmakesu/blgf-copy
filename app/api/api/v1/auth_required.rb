module API
  module V1
    class AuthRequired < Grape::API

      helpers do
        def authenticate!
          #return if Rails.env.development?
          auth_header = headers['Authorization']
          # Check if Authorization header is present, else return 401
          error!("401 Error: Missing Authorization header", 401) if auth_header.blank?

          #generated_sig = HmacHandler.signature_from(request.path, params)
          # Authorization header is present, check if it conforms to our specs
          error!("401 Error: Invalid Authorization header", 401) unless is_header_valid?(auth_header)
        end

        def is_header_valid? auth
          if /\ABLGF ([\w]+):([\w\+\=]+)\z/ =~ auth
            employee_id = $1
            signature = $2
            token = params[:access_token]
            # puts "\n####{request.path} params : #{params}\n###"
            # puts "#{request.path} is_token_valid? : #{is_token_valid?(employee_id, token)}"
            # puts "#{request.path} is_signature_authentic? : #{is_signature_authentic?(signature)}"
            (return true if Rails.env.development? || Rails.env.test?) if !User.not_deleted.find_by(employee_id: employee_id).nil?
            is_token_valid?(employee_id, token) #&& is_signature_authentic?(signature)
          else
            false
          end
        end

        def is_token_valid?(employee_id, token)
          if !User.find_by(employee_id: employee_id).nil?
            return api_key = User.find_by(employee_id: employee_id).api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }
          else
            return false
          end
          ApiKey.secure_compare(token, api_key.encrypted_access_token)
        end

        def is_signature_authentic?(sig)
          generated_sig = HmacHandler.signature_from(request.path, params)
          Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
          Rails.logger.debug "params: #{params}" if Rails.env.staging?
          HmacHandler.secure_compare(generated_sig, sig)
        end

        def current_user
          auth = headers['Authorization']
          # puts "request: #{request.path}"
          # puts "auth header: #{auth}"
          # puts "is_header_valid? #{is_header_valid?(auth)}"
          return nil unless is_header_valid? auth
          if /\ABLGF ([\w]+):([\w\+\=]+)\z/ =~ auth
            employee_id = $1
            # puts "employee_id: #{employee_id}"
            if @current_user
              @current_user
            else
              (@current_user = User.not_deleted.find_by(employee_id: employee_id)) ? @current_user : nil
            end
          end
        end
      end

      before do
        authenticate!
      end

      # Mount all endpoints that require authentication
      mount API::V1::Logout
      mount API::V1::Notifications
      mount API::V1::ChangePassword
      mount API::V1::CsvImports

      #FOR EMPLOYEE
      mount API::V1::MyPersonalDataSheet::MyPersonalDataSheet

      mount API::V1::MyRequest::PdsRequest
      mount API::V1::MyRequest::LeaveRequests
      mount API::V1::MyRequest::FtsRequests
      mount API::V1::Payroll::MyDtr
      mount API::V1::Payroll::MyCompensation
      mount API::V1::Payroll::MyCurrentSetDeductions
      mount API::V1::Payroll::MyCurrentSetBenefits

      #FOR HR
      mount API::V1::EmployeeStatusAndRecordManager::PositionCodes
      mount API::V1::EmployeeStatusAndRecordManager::AppointmentStatuses
      mount API::V1::EmployeeStatusAndRecordManager::Courses
      mount API::V1::EmployeeStatusAndRecordManager::DutiesAndResponsibilities
      mount API::V1::EmployeeStatusAndRecordManager::EducationLevels
      mount API::V1::EmployeeStatusAndRecordManager::ExamTypes
      mount API::V1::EmployeeStatusAndRecordManager::ModeOfSeparations
      mount API::V1::EmployeeStatusAndRecordManager::Plantilles
      mount API::V1::EmployeeStatusAndRecordManager::PlantillaGroups
      mount API::V1::EmployeeStatusAndRecordManager::PlantillaDuties
      mount API::V1::EmployeeStatusAndRecordManager::ProjectCodes
      mount API::V1::EmployeeStatusAndRecordManager::Scholarships
      mount API::V1::EmployeeStatusAndRecordManager::ServiceCodes
      mount API::V1::EmployeeStatusAndRecordManager::KnowledgeBases
      mount API::V1::EmployeeStatusAndRecordManager::DmsFiles


      mount API::V1::EmployeeRequestManager::RequestTypes
      mount API::V1::EmployeeRequestManager::EmployeePdsRequests
      mount API::V1::EmployeeRequestManager::EmployeeFtsRequests

      mount API::V1::Settings::AgencyInfos
      mount API::V1::Settings::UserManager
      mount API::V1::Settings::OrganizationStructureManager
      mount API::V1::Settings::PositionDetails
      mount API::V1::Settings::Zones
      mount API::V1::Settings::DirectoryContacts
      mount API::V1::Settings::EmployeeSaln
      mount API::V1::Settings::EmployeeSalnManager
      mount API::V1::Settings::AreaManager
      mount API::V1::Settings::LguNameManager
      mount API::V1::Settings::LguTypeManager
      mount API::V1::Settings::ProvinceManager
      mount API::V1::Settings::RegionManager
      mount API::V1::Settings::MyArchive


      mount API::V1::HrInformationDatabase::EmployeeInformations
      mount API::V1::HrInformationDatabase::LegalCaseManager
      mount API::V1::HrInformationDatabase::MyLegalCases
      mount API::V1::HrInformationDatabase::ReportsManager
      mount API::V1::HrInformationDatabase::ArchiveManager

      mount API::V1::TreasurerManager::TreasurerSaln
      mount API::V1::TreasurerManager::TreasurerSalnManager
      mount API::V1::TreasurerManager::TreasurerPdfFiles
      mount API::V1::TreasurerManager::TreasurerDMSManager

      mount API::V1::Payroll::SalarySchedules
      mount API::V1::Payroll::PayrollGroups
      mount API::V1::Payroll::LeaveTypes
      mount API::V1::Payroll::LeaveManager
      mount API::V1::Payroll::PhilHealthTables
      mount API::V1::Payroll::DeductionTypes
      mount API::V1::Payroll::EmployeeDeductionsManager
      mount API::V1::Payroll::EmployeeBenefitsManager
      mount API::V1::Payroll::AttendanceSchemes
      mount API::V1::Payroll::EmployeeDailyTimeRecord
      mount API::V1::Payroll::TaxRanges
      mount API::V1::Payroll::TaxStatuses
      mount API::V1::Payroll::PayrollLists
      mount API::V1::Payroll::Benefits

      mount API::V1::AnnouncementManager::Calendar
    end
  end
end
