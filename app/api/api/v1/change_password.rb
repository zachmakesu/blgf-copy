module API
  module V1
    class ChangePassword < Grape::API

      resource :change_password do
        desc "HRMIS change password"
        params do
          requires :current_password, type: String, desc: "User's current_password"
          requires :new_password, type: String, desc: "User's new_password"
          requires :confirm_password, type: String, desc: "User's confirm_password"
        end

        post do
          if current_user.present?
            handler = ChangePasswordHandler.new(params, current_user).change_password
            if handler.response[:success]
             {messages: handler.response[:details]}
            else
              error!({messages: handler.response[:details]},400)
          end
          else
            error!({ messages: "Please log in" }, 400)
          end
        end
      end
    end
  end
end
