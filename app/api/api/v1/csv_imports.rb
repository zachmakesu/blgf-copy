module API
  module V1
    class CsvImports < Grape::API

      resource :csv_imports do
        desc 'List current importing CSVs for loading bar'
        get do
          if current_user.present?
            background_process = { data: current_user.csv_files.not_finished }
            present background_process, with: Entities::V1::CsvFile::Index 
          else
            {
              messages: "Please log in"
            }
          end
        end
        
      end
    end
  end
end
