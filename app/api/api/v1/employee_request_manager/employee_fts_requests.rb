module API
  module V1
    module EmployeeRequestManager
      class EmployeeFtsRequests < Grape::API
        include Grape::Kaminari
        resource :employee_fts_requests do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
          
          desc 'List employees Fts requests'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = FtsRequest.search(params[:search])
            fts_requests = { data: paginate(searched) }
            present fts_requests, with: Entities::V1::EmployeeRequestManager::EmployeeFtsRequest::Index, count: FtsRequest.count, searched: searched.count
          end

          desc 'View employee specific request'
          # id  (integer)
          get "/:id" do
            fts_requests = FtsRequest.find(params[:id])
            present fts_requests, with: Entities::V1::EmployeeRequestManager::EmployeeFtsRequest::ViewFtsRequest 
          end

          desc 'Update employee pending Fts Request'
          # id        (integer)
          # status    (string) values (approved, denied)
          # remarks   (string)
          post "/:id/process" do
            params[:processor_employee_id] = current_user.id
            handler = Fts::ProcessFtsHandler.new(params).process_update
            if handler.response[:success]
              handler.response[:details]
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
