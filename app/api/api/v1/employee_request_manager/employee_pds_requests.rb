module API
  module V1
    module EmployeeRequestManager
      class EmployeePdsRequests < Grape::API
        include Grape::Kaminari
        resource :employee_pds_requests do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end

          desc 'List employees, hr and treasurers pds requests'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Draft.pending.per_roles(User::Role::ALL.values).search(params[:search])
            drafts = { data: paginate(searched)}
            present drafts, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::Index, count: Draft.pending.per_roles(User::Role::ALL.values).search("").count, searched: searched.count
          end

          desc 'Bulk Approved of PDS request'
          # draft_ids   (integer)
          post "/bulk_approve/:draft_ids" do
            params[:processor_employee_id] = current_user.id
            Requests::PDS::DraftHandler.new(params).bulk_approve
            searched = Draft.pending.per_roles(User::Role::ALL.values).search(params[:search])
            drafts = { data: paginate(searched)}
            present drafts, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::Index, count: Draft.pending.per_roles(User::Role::ALL.values).search("").count, searched: searched.count
          end

          desc 'View employee or treasurer specific PDS request'
          # draft_id  (integer)
          get "/:draft_id" do
            draft = Draft.pending.per_roles(User::Role::ALL.values).find(params[:draft_id])
            present draft, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::ViewPdsDraft
          end

          desc 'Update employee or treasurer PDS Request (approved or denied) with remarks'
          # draft_id   (integer)
          # status     (string) values (approved, denied)
          # remarks    (string)
          post "/:draft_id/process" do
            params[:processor_employee_id] = current_user.id
            handler = Requests::PDS::DraftHandler.new(params).process_update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::ViewPdsDraft
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
