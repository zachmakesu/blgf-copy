module API
  module V1
    module EmployeeRequestManager
      class RequestTypes < Grape::API
        resource :request_types do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
          
          desc 'List type of request'
          get do
            present RequestType, with: Entities::V1::EmployeeRequestManager::RequestType::Index, users: User
          end

          # desc 'Add request type'
          # post do
          #   handler = RequestTypeHandler.new(params).create_request_type
          #   if handler.response[:success]
          #     present handler.response[:details], with: Entities::V1::EmployeeRequestManager::RequestType::RequestTypeInfo
          #   else
          #     error!({messages: handler.response[:details]},400)
          #   end
          # end

          desc 'Update request type'
          post "/:id/update" do
            handler = RequestTypeHandler.new(params).update_request_type
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeRequestManager::RequestType::RequestTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          # desc 'Delete request type'
          # delete "/:id/delete" do
          #   handler = RequestTypeHandler.new(params).delete_request_type
          #   if handler.response[:success]
          #     present RequestType, with: Entities::V1::EmployeeRequestManager::RequestType::Index, users: User
          #   else
          #     error!({messages: handler.response[:details]},400)
          #   end
          # end

        end
      end

    end
  end
end
