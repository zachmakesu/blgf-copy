module API
  module V1
    module EmployeeStatusAndRecordManager
      class AppointmentStatuses < Grape::API
        include Grape::Kaminari
        resource :appointment_statuses do
          desc 'List appointment statuses'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = AppointmentStatus.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::Index, count: AppointmentStatus.count, searched: searched.count
          end

          desc 'List all appointment statuses'
          get "/all" do
            searched = AppointmentStatus.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::Index, count: AppointmentStatus.count, searched: searched.count
          end

          desc 'Specific appointment statuses for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = AppointmentStatus.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::Index, count: AppointmentStatus.count, searched: searched.count
          end

          desc 'Bulk Delete appointment statusses'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AppointmentStatusHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = AppointmentStatus.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::Index, count: AppointmentStatus.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific appointment statuses'
          get "/:id" do
            datum = AppointmentStatus.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::AppointmentStatusInfo
          end

          desc 'Add appointment status'
          # appointment_code    (string)
          # appointment_status  (string)
          # leave_entitled      (boolean)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = AppointmentStatusHandler.new(params, current_user).create_appointment_status
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::AppointmentStatusInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update appointment status'
          # id                  (integer)
          # appointment_code    (string)
          # appointment_status  (string)
          # leave_entitled      (boolean)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AppointmentStatusHandler.new(params, current_user).update_appointment_status
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::AppointmentStatusInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete appointment status'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AppointmentStatusHandler.new(params, current_user).delete_appointment_status
            if handler.response[:success]
              searched = AppointmentStatus.search(params[:search])
              statuses = { data: paginate(searched) }
              present statuses, with: Entities::V1::EmployeeStatusAndRecordManager::AppointmentStatus::Index, count: AppointmentStatus.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
