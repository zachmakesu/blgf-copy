module API
  module V1
    module EmployeeStatusAndRecordManager
      class Courses < Grape::API
        include Grape::Kaminari
        resource :courses do
          desc 'List courses code'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Course.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Course::Index, count: Course.count, searched: searched.count
          end

          desc 'List all courses code'
          get "/all" do
            searched = Course.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Course::Index, count: Course.count, searched: searched.count
          end

          desc 'Import Exam Type CSV. Create or Update only'
          # courses                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:courses],current_user, "courses").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific courses for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = Course.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Course::Index, count: Course.count, searched: searched.count
          end

          desc 'Bulk Delete courses'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = CourseHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = Course.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::Course::Index, count: Course.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific course'
          get "/:id" do
            datum = Course.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::Course::CourseInfo
          end

          desc 'Add course'
          # course_code         (string)
          # course_description  (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = CourseHandler.new(params, current_user).create_course
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Course::CourseInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update course'
          # id                  (integer)
          # course_code         (string)
          # course_description  (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = CourseHandler.new(params, current_user).update_course
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Course::CourseInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete course'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = CourseHandler.new(params, current_user).delete_course
            if handler.response[:success]
              searched = Course.search(params[:search])
              courses = { data: paginate(searched) }
              present courses,with: Entities::V1::EmployeeStatusAndRecordManager::Course::Index, count: Course.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
