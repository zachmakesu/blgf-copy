module API
  module V1
    module EmployeeStatusAndRecordManager
      class DmsFiles < Grape::API
        include Grape::Kaminari
        resource :dms_file_titles do
          desc 'List dms_file_titles'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = DocketManagementFile.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::Index, count: DocketManagementFile.count, searched: searched.count
          end

          desc 'List all dms_file_titles'
          get "/all" do
            searched = DocketManagementFile.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::Index, count: DocketManagementFile.count, searched: searched.count
          end

          desc 'Specific dms_file_title for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = DocketManagementFile.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::Index, count: DocketManagementFile.count, searched: searched.count
          end

          desc 'Bulk Delete dms_file_titles'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DmsFileHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = DocketManagementFile.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::Index, count: DocketManagementFile.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific dms_file_title'
          get "/:id" do
            datum = DocketManagementFile.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::DocketManagementFileInfo
          end

          desc 'Add dms_file_title'
          # title         (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = DmsFileHandler.new(params,current_user).create_dms_file
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::DocketManagementFileInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update dms_file_title'
          # id            (integer)
          # title         (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DmsFileHandler.new(params,current_user).update_dms_file
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::DocketManagementFileInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete dms_file_title'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DmsFileHandler.new(params,current_user).delete_dms_file
            if handler.response[:success]
              searched = DocketManagementFile.search(params[:search])
              dms_file = { data: paginate(searched) }
              present dms_file,with: Entities::V1::EmployeeStatusAndRecordManager::DocketManagementFile::Index, count: DocketManagementFile.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
