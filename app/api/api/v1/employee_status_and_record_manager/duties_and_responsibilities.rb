module API
  module V1
    module EmployeeStatusAndRecordManager
      class DutiesAndResponsibilities < Grape::API
        include Grape::Kaminari
        resource :duties_and_responsibilities do
          desc 'List Duties And Responsibilities'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = DutyAndResponsibility.search(params[:search])
            duties = { data: paginate(searched) }
            present duties, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::Index, count: DutyAndResponsibility.count, searched: searched.count
          end

          desc 'List all Duties And Responsibilities'
          get "/all" do
            searched = DutyAndResponsibility.search(params[:search])
            duties = { data: searched }
            present duties, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::Index, count: DutyAndResponsibility.count, searched: searched.count
          end

          desc 'Specific Duties And Responsibilities for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = DutyAndResponsibility.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::Index, count: DutyAndResponsibility.count, searched: searched.count
          end

          desc 'Bulk Delete Duties And Responsibilities'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DutyAndResponsibilityHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = DutyAndResponsibility.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::Index, count: DutyAndResponsibility.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Duty And Responsibility'
          get "/:id" do
            datum = DutyAndResponsibility.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::DutyAndResponsibilityInfo
          end

          desc 'Add Duty And Responsibility'
          # position_code_id  (integer)
          # percent_work      (float)
          # duties            (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = DutyAndResponsibilityHandler.new(params,current_user).create_duty_and_responsibility
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::DutyAndResponsibilityInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Duty And Responsibility'
          # id                (integer)
          # position_code_id  (integer)
          # percent_work      (float)
          # duties            (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DutyAndResponsibilityHandler.new(params,current_user).update_duty_and_responsibility
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::DutyAndResponsibilityInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Duty And Responsibility'
          # id                (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DutyAndResponsibilityHandler.new(params,current_user).delete_duty_and_responsibility
            if handler.response[:success]
              searched = DutyAndResponsibility.search(params[:search])
              duties = { data: paginate(searched) }
              present duties, with: Entities::V1::EmployeeStatusAndRecordManager::DutyAndResponsibility::Index, count: DutyAndResponsibility.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
