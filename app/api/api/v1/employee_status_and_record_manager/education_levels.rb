module API
  module V1
    module EmployeeStatusAndRecordManager
      class EducationLevels < Grape::API
        include Grape::Kaminari
        resource :education_levels do
          desc 'List Education levels'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = EducationLevel.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::Index, count: EducationLevel.count, searched: searched.count
          end

          desc 'List all Education levels'
          get "/all" do
            searched = EducationLevel.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::Index, count: EducationLevel.count, searched: searched.count
          end

          desc 'Specific Education levels for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = EducationLevel.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::Index, count: EducationLevel.count, searched: searched.count
          end

          desc 'Bulk Delete Education levels'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = EducationLevelHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = EducationLevel.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::Index, count: EducationLevel.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Education level'
          get "/:id" do
            datum = EducationLevel.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::EducationLevelInfo
          end

          desc 'Add Education level'
          # level_code          (string)
          # level_description   (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = EducationLevelHandler.new(params, current_user).create_education_level
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::EducationLevelInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Education level'
          # id                  (integer)
          # level_code          (string)
          # level_description   (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = EducationLevelHandler.new(params, current_user).update_education_level
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::EducationLevelInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Education level'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = EducationLevelHandler.new(params, current_user).delete_education_level
            if handler.response[:success]
              searched = EducationLevel.search(params[:search])
              education_levels = { data: paginate(searched) }
              present education_levels, with: Entities::V1::EmployeeStatusAndRecordManager::EducationLevel::Index, count: EducationLevel.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
