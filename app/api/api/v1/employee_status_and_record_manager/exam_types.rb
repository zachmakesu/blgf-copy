module API
  module V1
    module EmployeeStatusAndRecordManager
      class ExamTypes < Grape::API
        include Grape::Kaminari
        resource :exam_types do
          desc 'List exam types'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = ExamType.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::Index, count: ExamType.count, searched: searched.count
          end

          desc 'List all exam types'
          get "/all" do
            searched = ExamType.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::Index, count: ExamType.count, searched: searched.count
          end

          desc 'Specific exam types for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = ExamType.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::Index, count: ExamType.count, searched: searched.count
          end

          desc 'Import Exam Type CSV. Create or Update only'
          # exam_types                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:exam_types],current_user, "exam_types").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Bulk Delete exam types'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ExamTypeHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = ExamType.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::Index, count: ExamType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific exam types'
          get "/:id" do
            datum = ExamType.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::ExamTypeInfo
          end
          
          desc 'Add exam type'
          # exam_code         (string)
          # exam_description  (sting)
          # csc_eligible      (boolean)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ExamTypeHandler.new(params, current_user).create_exam_type
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::ExamTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update exam type'
          # id                (integer)
          # exam_code         (string)
          # exam_description  (sting)
          # csc_eligible      (boolean)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ExamTypeHandler.new(params, current_user).update_exam_type
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::ExamTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete exam type'
          # id                (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ExamTypeHandler.new(params, current_user).delete_exam_type
            if handler.response[:success]
              searched = ExamType.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::EmployeeStatusAndRecordManager::ExamType::Index, count: ExamType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
