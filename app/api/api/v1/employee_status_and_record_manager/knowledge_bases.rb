module API
  module V1
    module EmployeeStatusAndRecordManager
      class KnowledgeBases < Grape::API
        include Grape::Kaminari
        resource :knowledge_bases do
          desc 'List knowledge base'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = KnowledgeBase.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::Index, count: KnowledgeBase.count, searched: searched.count
          end

          desc 'List all knowledge base'
          get "/all" do
            searched = KnowledgeBase.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::Index, count: KnowledgeBase.count, searched: searched.count
          end

          desc 'Specific knowledge base for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = KnowledgeBase.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::Index, count: KnowledgeBase.count, searched: searched.count
          end

          desc 'Bulk Delete knowledge base'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = KnowledgeBaseHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = KnowledgeBase.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::Index, count: KnowledgeBase.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific knowledge base'
          get "/:id" do
            datum = KnowledgeBase.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::KnowledgeBaseInfo
          end

          desc 'Add knowledge base'
          # code                (string)
          # description         (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = KnowledgeBaseHandler.new(params, current_user).create_knowledge_base
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::KnowledgeBaseInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update knowledge base'
          # id                  (integer)
          # code                (string)
          # description         (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = KnowledgeBaseHandler.new(params, current_user).update_knowledge_base
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::KnowledgeBaseInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete knowledge base'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = KnowledgeBaseHandler.new(params, current_user).delete_knowledge_base
            if handler.response[:success]
              searched = KnowledgeBase.search(params[:search])
              statuses = { data: paginate(searched) }
              present statuses, with: Entities::V1::EmployeeStatusAndRecordManager::KnowledgeBase::Index, count: KnowledgeBase.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
