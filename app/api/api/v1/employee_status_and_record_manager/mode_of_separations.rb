module API
  module V1
    module EmployeeStatusAndRecordManager
      class ModeOfSeparations < Grape::API
        include Grape::Kaminari
        resource :mode_of_separations do
          desc 'List mode of separation'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = ModeOfSeparation.search(params[:search])
            modes = { data: paginate(searched) }
            present modes, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::Index, count: ModeOfSeparation.count, searched: searched.count
          end

          desc 'List all mode of separation'
          get "/all" do
            searched = ModeOfSeparation.search(params[:search])
            modes = { data: searched }
            present modes, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::Index, count: ModeOfSeparation.count, searched: searched.count
          end

          desc 'Specific mode of separation for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = ModeOfSeparation.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::Index, count: ModeOfSeparation.count, searched: searched.count
          end

          desc 'Bulk Delete mode of separation'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ModeOfSeparationHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = ModeOfSeparation.search(params[:search])
              modes = { data: searched }
              present modes, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::Index, count: ModeOfSeparation.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific mode of separation'
          get "/:id" do
            datum = ModeOfSeparation.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::ModeOfSeparationInfo
          end

          desc 'Add mode of separation'
          # separation_mode   (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ModeOfSeparationHandler.new(params, current_user).create_mode_of_separation
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::ModeOfSeparationInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update mode of separation'
          # id                (integer)
          # separation_mode   (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ModeOfSeparationHandler.new(params, current_user).update_mode_of_separation
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::ModeOfSeparationInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete mode of separation'
          # id                (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ModeOfSeparationHandler.new(params, current_user).delete_mode_of_separation
            if handler.response[:success]
              searched = ModeOfSeparation.search(params[:search])
              modes = { data: paginate(searched) }
              present modes, with: Entities::V1::EmployeeStatusAndRecordManager::ModeOfSeparation::Index, count: ModeOfSeparation.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
