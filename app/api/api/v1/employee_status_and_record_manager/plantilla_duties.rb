module API
  module V1
    module EmployeeStatusAndRecordManager
      class PlantillaDuties < Grape::API
        include Grape::Kaminari
        resource :plantilla_duties do
          desc 'List Plantilla duties and responsibilities'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = PlantillaDuty.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::Index, count: PlantillaDuty.count, searched: searched.count
          end

          desc 'List all Plantilla duties and responsibilities'
          get "/all" do
            searched = PlantillaDuty.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::Index, count: PlantillaDuty.count, searched: searched.count
          end

          desc 'Specific Plantilla duty and responsibility for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = PlantillaDuty.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::Index, count: PlantillaDuty.count, searched: searched.count
          end

          desc 'Bulk Delete Plantilla duties and responsibilities'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaDutyHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = PlantillaDuty.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::Index, count: PlantillaDuty.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Plantilla duties and responsibilities'
          get "/:id" do
            datum = PlantillaDuty.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::PlantillaDutyInfo
          end
          
          desc 'Add Plantilla duty and responsibility'
          # plantilla_id  (integer)
          # percent_work  (float)
          # duties        (string)

          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaDutyHandler.new(params, current_user).create_plantilla_duty
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::PlantillaDutyInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Plantilla duty and responsibility'
          # id            (integer)
          # plantilla_id  (integer)
          # percent_work  (float)
          # duties        (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaDutyHandler.new(params, current_user).update_plantilla_duty
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::PlantillaDutyInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Plantilla duty and responsibility'
          # id            (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaDutyHandler.new(params, current_user).delete_plantilla_duty
            if handler.response[:success]
              searched = PlantillaDuty.search(params[:search])
              duties = { data: paginate(searched) }
              present duties, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaDuty::Index, count: PlantillaDuty.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
