module API
  module V1
    module EmployeeStatusAndRecordManager
      class PlantillaGroups < Grape::API
        include Grape::Kaminari
        resource :plantilla_groups do
          desc 'List plantilla groups'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = PlantillaGroup.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::Index, count: PlantillaGroup.count, searched: searched.count
          end

          desc 'List all plantilla groups'
          get "/all" do
            searched = PlantillaGroup.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::Index, count: PlantillaGroup.count, searched: searched.count
          end

          desc 'Import Plantilla Groups CSV. Create or Update only'
          # plantilla_groups                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:plantilla_groups],current_user, "plantilla_groups").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific plantilla groups for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = PlantillaGroup.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::Index, count: PlantillaGroup.count, searched: searched.count
          end

          desc 'Bulk Delete plantilla group'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaGroupHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = PlantillaGroup.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::Index, count: PlantillaGroup.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific plantilla group'
          get "/:id" do
            datum = PlantillaGroup.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::PlantillaGroupInfo
          end
          
          desc 'Add plantilla groups'
          # group_code  (string)
          # group_name  (string)
          # group_order (float)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaGroupHandler.new(params, current_user).create_plantilla_group
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::PlantillaGroupInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update plantilla groups'
          # id          (integer)
          # group_code  (string)
          # group_name  (string)
          # group_order (float)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaGroupHandler.new(params, current_user).update_plantilla_group
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::PlantillaGroupInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete plantilla groups'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaGroupHandler.new(params, current_user).delete_plantilla_group
            if handler.response[:success]
              searched = PlantillaGroup.search(params[:search])
              groups = { data: paginate(searched) }
              present groups, with: Entities::V1::EmployeeStatusAndRecordManager::PlantillaGroup::Index, count: PlantillaGroup.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
