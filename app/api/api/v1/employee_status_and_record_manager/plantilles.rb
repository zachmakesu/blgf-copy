module API
  module V1
    module EmployeeStatusAndRecordManager
      class Plantilles < Grape::API
        include Grape::Kaminari
        resource :plantilles do
          
          desc 'List plantilles'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          # unused      (boolean)
          get do
            unused = params[:unused].present? ? params[:unused].downcase == "true" : false
            searched =  unused ? Plantilla.includes(:position_code, :salary_schedule, :plantilla_group).search(params[:search]).except_used : Plantilla.includes(:position_code, :salary_schedule, :plantilla_group).search(params[:search])
            data = { data: paginate(searched) }
            counter = unused ? Plantilla.except_used.count : Plantilla.count
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: counter, searched: searched.count
          end

          desc 'List all plantilles'
          # unused      (boolean)
          get "/all" do
            unused = params[:unused].present? ? params[:unused].downcase == "true" : false
            searched =  unused ? Plantilla.includes(:position_code, :salary_schedule, :plantilla_group).search(params[:search]).except_used : Plantilla.includes(:position_code, :salary_schedule, :plantilla_group).search(params[:search])
            data = { data: searched }
            counter = unused ? Plantilla.except_used.count : Plantilla.count
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: counter , searched: searched.count
          end

          desc 'Import Plantilles CSV. Create or Update only'
          # plantilles                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:plantilles],current_user, "plantilles").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific plantilles for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = Plantilla.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: Plantilla.count, searched: searched.count
          end

          desc 'Bulk Delete plantilles'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = Plantilla.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: Plantilla.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific plantilla'
          get "/:id" do
            datum = Plantilla.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::PlantillaInfo
          end
          
          desc 'Add plantilla'
          # item_number             (string)
          # position_code_id        (integer)
          # plantilla_group_id      (integer)
          # salary_schedule_grade   (integer)
          # area_code               (string)
          # area_type               (string)
          # csc_eligibility         (boolean)
          # rationalized            (boolean)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaHandler.new(params,current_user).create_plantilla
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::PlantillaInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update plantilla'
          # id                      (integer)
          # item_number             (string)
          # position_code_id        (integer)
          # plantilla_group_id      (integer)
          # salary_schedule_grade   (integer)
          # area_code               (string)
          # area_type               (string)
          # csc_eligibility         (boolean)
          # rationalized            (boolean)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaHandler.new(params,current_user).update_plantilla
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::PlantillaInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete plantilla'
          # id                      (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PlantillaHandler.new(params,current_user).delete_plantilla
            if handler.response[:success]
              searched = Plantilla.search(params[:search])
              plantilles = { data: paginate(searched) }
              present plantilles, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: Plantilla.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
