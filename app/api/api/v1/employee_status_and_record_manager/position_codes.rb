module API
  module V1
    module EmployeeStatusAndRecordManager
      class PositionCodes < Grape::API
        include Grape::Kaminari
        resource :position_codes do   
          desc 'List position code'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = PositionCode.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::Index, count: PositionCode.count, searched: searched.count
          end

          desc 'List all position code'
          get "/all" do
            searched = PositionCode.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::Index, count: PositionCode.count, searched: searched.count
          end

          desc 'Import Position Code CSV. Create or Update only'
          # position_codes                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:position_codes],current_user, "position_codes").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific position code for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = PositionCode.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::Index, count: PositionCode.count, searched: searched.count
          end

          desc 'Bulk Delete position code'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PositionCodeHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = PositionCode.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::Index, count: PositionCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific position code'
          get "/:id" do
            datum = PositionCode.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::PositionCodeInfo
          end

          desc 'Add position code'
          # position_code             (string)
          # position_description      (string)
          # position_abbreviation     (string)
          # educational_requirement   (string)
          # experience_requirement    (string)
          # eligibility_requirement   (string)
          # training_requirement      (string)
          # position_classification   (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PositionCodeHandler.new(params,current_user).create_position_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::PositionCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update position code'
          # id                        (integer)
          # position_code             (string)
          # position_description      (string)
          # position_abbreviation     (string)
          # educational_requirement   (string)
          # experience_requirement    (string)
          # eligibility_requirement   (string)
          # training_requirement      (string)
          # position_classification   (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PositionCodeHandler.new(params,current_user).update_position_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::PositionCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete position code'
          # id                        (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PositionCodeHandler.new(params,current_user).delete_position_code
            if handler.response[:success]
              searched = PositionCode.search(params[:search])
              positions = { data: paginate(searched) }
              present positions, with: Entities::V1::EmployeeStatusAndRecordManager::PositionCode::Index, count: PositionCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
