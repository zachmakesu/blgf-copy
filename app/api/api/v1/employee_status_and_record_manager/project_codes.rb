module API
  module V1
    module EmployeeStatusAndRecordManager
      class ProjectCodes < Grape::API
        include Grape::Kaminari
        resource :project_codes do
          
          desc 'List Project codes'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = ProjectCode.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::Index, count: ProjectCode.count, searched: searched.count
          end

          desc 'List All Project codes'
          get "/all" do
            searched = ProjectCode.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::Index, count: ProjectCode.count, searched: searched.count
          end

          desc 'Specific Project codes for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = ProjectCode.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::Index, count: ProjectCode.count, searched: searched.count
          end

          desc 'Bulk Delete Project codes'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ProjectCodeHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = ProjectCode.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::Index, count: ProjectCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Project code'
          get "/:id" do
            datum = ProjectCode.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::ProjectCodeInfo
          end

          desc 'Add Project code'
          # project_code          (string)
          # project_description   (string)
          # project_order         (integer)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ProjectCodeHandler.new(params,current_user).create_project_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::ProjectCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Project code'
          # id                    (integer)
          # project_code          (string)
          # project_description   (string)
          # project_order         (integer)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ProjectCodeHandler.new(params,current_user).update_project_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::ProjectCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Project code'
          # id                    (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ProjectCodeHandler.new(params,current_user).delete_project_code
            if handler.response[:success]
              searched = ProjectCode.search(params[:search])
              projects = { data: paginate(searched) }
              present projects, with: Entities::V1::EmployeeStatusAndRecordManager::ProjectCode::Index, count: ProjectCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
