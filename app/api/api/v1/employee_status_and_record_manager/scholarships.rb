module API
  module V1
    module EmployeeStatusAndRecordManager
      class Scholarships < Grape::API
        include Grape::Kaminari
        resource :scholarships do

          desc 'List of scholarships'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Scholarship.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::Index, count: Scholarship.count, searched: searched.count
          end

          desc 'List all scholarships'
          get "/all" do
            searched = Scholarship.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::Index, count: Scholarship.count, searched: searched.count
          end

          desc 'Specific scholarships for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = Scholarship.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::Index, count: Scholarship.count, searched: searched.count
          end

          desc 'Bulk Delete scholarships'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ScholarshipHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = Scholarship.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::Index, count: Scholarship.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific scholarship'
          get "/:id" do
            datum = Scholarship.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::ScholarshipInfo
          end

          desc 'Add scholarship'
          # description (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ScholarshipHandler.new(params,current_user).create_scholarship
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::ScholarshipInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update scholarship'
          # id          (integer)
          # description (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ScholarshipHandler.new(params,current_user).update_scholarship
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::ScholarshipInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete scholarship'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ScholarshipHandler.new(params,current_user).delete_scholarship
            if handler.response[:success]
              searched = Scholarship.search(params[:search])
              scholarships = { data: paginate(searched) }
              present scholarships, with: Entities::V1::EmployeeStatusAndRecordManager::Scholarship::Index, count: Scholarship.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
