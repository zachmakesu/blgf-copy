module API
  module V1
    module EmployeeStatusAndRecordManager
      class ServiceCodes < Grape::API
        include Grape::Kaminari
        resource :service_codes do

          desc 'List Service codes'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = ServiceCode.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::Index, count: ServiceCode.count, searched: searched.count
          end

          desc 'List all Service codes'
          get "/all" do
            searched = ServiceCode.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::Index, count: ServiceCode.count, searched: searched.count
          end

          desc 'Specific Service codes for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = ServiceCode.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::Index, count: ServiceCode.count, searched: searched.count
          end

          desc 'Bulk Delete Service codes'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ServiceCodeHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = ServiceCode.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::Index, count: ServiceCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Service code'
          get "/:id" do
            datum = ServiceCode.find(params[:id])
            present datum, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::ServiceCodeInfo
          end

          desc 'Add Service code'
          # service_code          (string)
          # service_description   (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ServiceCodeHandler.new(params, current_user).create_service_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::ServiceCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Service code'
          # id                    (integer)
          # service_code          (string)
          # service_description   (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ServiceCodeHandler.new(params, current_user).update_service_code
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::ServiceCodeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Service code'
          # id                    (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ServiceCodeHandler.new(params, current_user).delete_service_code
            if handler.response[:success]
              searched = ServiceCode.search(params[:search])
              service_codes = { data: paginate(searched) }
              present service_codes, with: Entities::V1::EmployeeStatusAndRecordManager::ServiceCode::Index, count: ServiceCode.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
