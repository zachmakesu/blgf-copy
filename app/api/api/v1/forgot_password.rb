module API
  module V1
    class ForgotPassword < Grape::API

      resource :forgot_password do
        desc "HRMIS forgot password"
        params do
          requires :email, type: String, desc: 'User email'
          requires :employee_id, type: String, desc: 'Employee ID'
        end

        # email (string)
        post do
          handler = ForgotPasswordHandler.new(params).reset_password
          if handler.response[:success]
           {messages: handler.response[:details]}
          else
            error!({messages: handler.response[:details]},400)
          end
        end
        
      end
    end
  end
end
