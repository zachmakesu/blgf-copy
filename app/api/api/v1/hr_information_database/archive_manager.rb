module API
  module V1
    module HrInformationDatabase
      class ArchiveManager < Grape::API
        include Grape::Kaminari
        resource :archive_manager do
          before do
            authorize!(current_user, User::Role::ADMIN)
          end

          desc "List of archive dates for PDS" 
          # params on pagination ?page=1&per_page=10&search=2017-06-22
          paginate per_page: 10, offset: 0
          get "/pds" do
            all_pds = Archive.pds.distinct_date
            searched = Archive.pds.search(params[:search])
            searched = searched.distinct_date
  
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_pds.count, searched: searched.count
          end

          desc "List of archive PDS per date"
          # params on pagination ?page=1&per_page=10&employee_id=C0001&last_name=asc|desc
          # last_name default asc
          paginate per_page: 10, offset: 0
          get "/pds/:archive_date" do
            all_by_date = Archive.pds.by_date(params[:archive_date])
            searched = all_by_date.search_by_employee(params[:employee_id]).by_emp_lastname(params[:last_name])
            
            data = { data: paginate(searched) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveInfo, count: all_by_date.count, searched: searched.count
          end

          desc "List of archive dates for SALN"
          # params on pagination ?page=1&per_page=10&search=2017-06-22
          paginate per_page: 10, offset: 0
          get "/saln" do
            all_saln = Archive.saln.distinct_date
            searched = Archive.saln.search(params[:search])
            searched = searched.distinct_date
            
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_saln.count, searched: searched.count
          end

          desc "List of archive SALN per date"
          # params on pagination ?page=1&per_page=10&employee_id=C0001&last_name=asc|desc
          # last_name default asc
          paginate per_page: 10, offset: 0
          get "/saln/:archive_date" do
            all_by_date = Archive.saln.by_date(params[:archive_date])
            searched = all_by_date.search_by_employee(params[:employee_id]).by_emp_lastname(params[:last_name])
            
            data = { data: paginate(searched) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveInfo, count: all_by_date.count, searched: searched.count
          end

          desc "Generate PDS archive for all users"
          post "/pds" do
            ArchiveHandler.new(current_user).archive_pds

            all_pds = Archive.pds.distinct_date
            searched = Archive.pds.search(params[:search])
            searched = searched.distinct_date
            
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_pds.count, searched: searched.count
          end

          desc "Generate SALN archive for all users"
          post "/saln" do
            ArchiveHandler.new(current_user).archive_saln

            all_saln = Archive.saln.distinct_date
            searched = Archive.saln.search(params[:search])
            searched = searched.distinct_date
            
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_saln.count, searched: searched.count
          end
        end

      end
    end
  end
end
