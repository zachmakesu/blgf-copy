module API
  module V1
    module HrInformationDatabase
      class EmployeeInformations < Grape::API
        include Grape::Kaminari
        resource :hr_information_database do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end

          desc "List of Users for Hr information"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.by_roles(User::Role::ALL.values).except_self(current_user).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).except_self(current_user).search("").count, searched: searched.count
          end

          desc "List all Users for Hr information"
          get "/all" do
            searched = User.by_roles(User::Role::ALL.values).except_self(current_user).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).except_self(current_user).search("").count, searched: searched.count
          end

          desc "Specific employee PDS to PDF"
          get "/pds_to_pdf/:employee_id" do
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::MyPersonalDataSheet::PdfInfo::ToPdf
          end

          desc "Specific employee profile_data PDS"
          get "/profile_data/:employee_id" do
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
              present employee, with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
          end

          desc "Update Specific employee profile_data PDS"
          # email                 (string)
          # first_name            (string)
          # last_name             (string) 
          # middle_name           (string)
          # middle_initial        (string)
          # name_extension        (string)
          # birthdate             (string) format(yyyy-mm-dd)
          # birthplace            (string)
          # gender                (string) values ("male", "female")
          # civil_status          (string) values (single, living_common_law, widowed, separated, divorced, married)
          # citizenship           (string)
          # height                (float)
          # weight                (float)
          # blood_type            (string)
          # tin_number            (string)
          # gsis_policy_number    (string)
          # pagibig_id_number     (string)
          # philhealth_number     (string)
          # sss_number            (string)
          # remittance_id         (string)
          # image                 (file)
          # signature_image       (file)
          post "/profile_data/:employee_id" do
            validate_date_format!(params[:birthdate]) if params[:birthdate].present? 
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Address on specific employee profile_data PDS"
          # location        (string)
          # residency       (string) values( residential, permanent )
          # zipcode         (string)
          # contact_number  (string)
          post "/profile_data/:employee_id/addresses" do
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).add_address
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Address on specific employee profile_data PDS"
          # location        (string)
          # residency       (string) values( residential, permanent )
          # zipcode         (string)
          # contact_number  (string)
          post "/profile_data/:employee_id/addresses/:id/update" do
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).update_address
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Address on specific employee profile_data PDS"
          delete "/profile_data/:employee_id/addresses/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).delete_address
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Contact on specific employee profile_data PDS"
          # device       (string) values( mobile, telephone )
          # number       (string)
          post "/profile_data/:employee_id/contacts" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).add_contact
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Contact on specific employee profile_data PDS"
          # device       (string) values( mobile, telephone )
          # number       (string)
          post "/profile_data/:employee_id/contacts/:id/update" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).update_contact
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Contact on specific employee profile_data PDS"
          delete "/profile_data/:employee_id/contacts/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ProfileDataHandler.new(params, employee).delete_contact
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee education PDS"
          get "/education/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
              present employee, with: Entities::V1::MyPersonalDataSheet::Education::EducationInformation
          end

          desc "Add Specific employee education PDS"
          # education_level_id                (integer)    
          # school_name                       (string)
          # course_id                         (integer)
          # highest_grade_level_units_earned  (string)
          # date_of_attendance_from           (string) format (yyyy-mm-dd)
          # date_of_attendance_to             (string) format (yyyy-mm-dd)
          # honors_received                   (string)
          # year_graduated                    (string) format (yyyy)
          post "/education/:employee_id" do
            validate_date_format!(params[:date_of_attendance_from]) if params[:date_of_attendance_from].present? 
            validate_date_format!(params[:date_of_attendance_to]) if params[:date_of_attendance_to].present?
            validate_year_format!(params[:year_graduated]) if params[:year_graduated].present?
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::EducationHandler.new(params, employee).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Education::EducationInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Specific employee education PDS"
          # education_level_id                (integer) 
          # school_name                       (string)
          # course_id                         (integer)
          # highest_grade_level_units_earned  (string)
          # date_of_attendance_from           (string) format (yyyy-mm-dd)
          # date_of_attendance_to             (string) format (yyyy-mm-dd)
          # honors_received                   (string)
          # year_graduated                    (string) format (yyyy)
          post "/education/:employee_id/:id/update" do
            validate_date_format!(params[:date_of_attendance_from]) if params[:date_of_attendance_from].present? 
            validate_date_format!(params[:date_of_attendance_to]) if params[:date_of_attendance_to].present? 
            validate_year_format!(params[:year_graduated]) if params[:year_graduated].present?
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::EducationHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Education::EducationInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee education PDS"
          delete "/education/:employee_id/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::EducationHandler.new(params, employee).delete
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Education::EducationInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee examination PDS"
          get "/examination/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation
          end

          desc "Add Specific employee examination PDS"
          # exam_description    (string)
          # place_of_exam       (string)
          # rating              (float)
          # date_of_exam        (string) format(yyyy-mm-dd)
          # licence_number      (string)
          # date_of_release     (string) format(yyyy-mm-dd)
          post "/examination/:employee_id" do
            validate_date_format!(params[:date_of_exam]) if params[:date_of_exam].present? 
            validate_date_format!(params[:date_of_release]) if params[:date_of_release].present? 
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ExaminationHandler.new(params, employee).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Specific employee examination PDS"
          # exam_description    (string)
          # place_of_exam       (string)
          # rating              (float)
          # date_of_exam        (string) format(yyyy-mm-dd)
          # licence_number      (string)
          # date_of_release     (string) format(yyyy-mm-dd)
          post "/examination/:employee_id/:id/update" do
            validate_date_format!(params[:date_of_exam]) if params[:date_of_exam].present? 
            validate_date_format!(params[:date_of_release]) if params[:date_of_release].present? 
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ExaminationHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee examination PDS"
          delete "/examination/:employee_id/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::ExaminationHandler.new(params, employee).delete
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee work_experience PDS"
          get "/work_experience/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
              present employee, with: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation
          end

          desc "Add Specific employee work_experience PDS"
          # position_title            (string)
          # inclusive_date_from       (string) format(yyyy-mm-dd)
          # inclusive_date_to         (string) format(yyyy-mm-dd)
          # department_agency_office  (string)
          # monthly_salary            (string)
          # salary_grade_and_step     (string) format(grade_number-steps) example value '02-5'
          # appointment_status_id     (integer)
          # government_service        (boolean)
          post "/work_experience/:employee_id" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 

            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::WorkExperienceHandler.new(params, employee).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Specific employee work_experience PDS"
          # position_title            (string)
          # inclusive_date_from       (string) format(yyyy-mm-dd)
          # inclusive_date_to         (string) format(yyyy-mm-dd)
          # department_agency_office  (string)
          # monthly_salary            (string)
          # salary_grade_and_step     (string) format(grade_number-steps) example value '02-5'
          # appointment_status_id     (integer)
          # government_service        (boolean)
          post "/work_experience/:employee_id/:id/update" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 

            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::WorkExperienceHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Specific employee work_experience PDS"
          delete "/work_experience/:employee_id/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::WorkExperienceHandler.new(params, employee).delete
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee voluntary_work PDS"
          get "/voluntary_work/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation
          end

          desc "Add Specific employee voluntary_work PDS"
          # name_of_organization      (string)
          # address_of_organization   (string)
          # inclusive_date_from       (string) format(yyyy-mm-dd)
          # inclusive_date_to         (string) format(yyyy-mm-dd)
          # number_of_hours           (integer)
          # position_nature_of_work   (string)
          post "/voluntary_work/:employee_id" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 

            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::VoluntaryWorkHandler.new(params, employee).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Specific employee voluntary_work PDS"
          # name_of_organization      (string)
          # address_of_organization   (string)
          # inclusive_date_from       (string) format(yyyy-mm-dd)
          # inclusive_date_to         (string) format(yyyy-mm-dd)
          # number_of_hours           (integer)
          # position_nature_of_work   (string)
          post "/voluntary_work/:employee_id/:id/update" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::VoluntaryWorkHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee voluntary_work PDS"
          delete "/voluntary_work/:employee_id/:id/delete" do
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::VoluntaryWorkHandler.new(params, employee).delete
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end


          desc "Specific employee family_information PDS"
          get "/family_information/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
          end

          desc "Update Specific employee family_information PDS"
          # spouse_name                         (string)
          # spouse_occupation                   (string)
          # spouse_employer_or_business_name    (string)
          # spouse_work_or_business_address     (string)
          # spouse_contact_number               (string)
          # parent_fathers_name                 (string)
          # parent_mothers_maiden_name          (string)
          # parent_address                      (string)

          # spouse_first_name                    (string)
          # spouse_middle_name                  (string)
          # spouse_last_name                    (string)

          # parant_fathers_first_name            (string)
          # parant_fathers_middle_name          (string)
          # parant_fathers_last_name            (string)

          # parant_mothers_first_name            (string)
          # parant_mothers_middle_name          (string)
          # parant_mothers_last_name            (string)
          post "/family_information/:employee_id" do
            
            handler = HrUpdate::FamilyInfoHandler.new(params, User::Role::ALL.values).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Specific employee family_information child"
          # first_name                       (string)
          # last_name                        (string)
          # middle_name                      (string)
          # middle_initial                   (string)
          # relationship                     (string)
          # birthdate                        (string) format(yyyy-mm-dd)
          post "/family_information/:employee_id/children" do
            validate_date_format!(params[:birthdate]) if params[:birthdate].present? 
            
            handler = HrUpdate::FamilyInfoHandler.new(params, User::Role::ALL.values).add_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Specific employee family_information child"
          # first_name                       (string)
          # last_name                        (string)
          # middle_name                      (string)
          # middle_initial                   (string)
          # relationship                     (string)
          # birthdate                        (string) format(yyyy-mm-dd)
          post "/family_information/:employee_id/children/:child_id/update" do
            validate_date_format!(params[:birthdate]) if params[:birthdate].present? 
            
            handler = HrUpdate::FamilyInfoHandler.new(params, User::Role::ALL.values).update_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee family_information child"
          # child_id                       (integer)
          delete "/family_information/:employee_id/children/:child_id/delete" do
            
            handler = HrUpdate::FamilyInfoHandler.new(params, User::Role::ALL.values).delete_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee training_programs PDS"
          get "/trainings_and_seminars/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
              present employee, with: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation
          end

          desc "Add Specific employee trainings_and_seminars PDS"
          # title                 (string)
          # inclusive_date_from   (string) format(yyyy-mm-dd)
          # inclusive_date_to     (string) format(yyyy-mm-dd)
          # number_of_hours       (integer)
          # conducted_by          (string)
          post "/trainings_and_seminars/:employee_id" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
              handler = HrUpdate::TrainingHandler.new(params, employee).create
              if handler.response[:success]
                present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation
              else
                error!({messages: handler.response[:details]},400)
              end
          end

          desc "Update Specific employee trainings_and_seminars PDS"
          # title                 (string)
          # inclusive_date_from   (string) format(yyyy-mm-dd)
          # inclusive_date_to     (string) format(yyyy-mm-dd)
          # number_of_hours       (integer)
          # conducted_by          (string)
          post "/trainings_and_seminars/:employee_id/:id/update" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 
            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::TrainingHandler.new(params, employee).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee trainings_and_seminars PDS"
          # title                 (string)
          # inclusive_date_from   (string) format(yyyy-mm-dd)
          # inclusive_date_to     (string) format(yyyy-mm-dd)
          # number_of_hours       (integer)
          # conducted_by          (string)
          delete "/trainings_and_seminars/:employee_id/:id/delete" do
            validate_date_format!(params[:inclusive_date_from]) if params[:inclusive_date_from].present? 
            validate_date_format!(params[:inclusive_date_to]) if params[:inclusive_date_to].present? 

            
            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = HrUpdate::TrainingHandler.new(params, employee).delete
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific employee other_information PDS"
          get "/other_information/:employee_id" do

            employee = User.by_roles(User::Role::ALL.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
          end

          desc "Update Specific employee other_information PDS"
          # skills_and_hobbies        (string) value ex. "Surfing|Biking|Trekking"
          # non_academic_destinction  (string) value ex. "Homecoming Queen|Football letterman|Rifle team captain"
          # membership_in             (string) value ex. "GWAPOTODA|GANDATODA|BAyawYouthActionGroup"
          # third_degree_national     (boolean)
          # give_national             (string)
          # fourth_degree_local       (boolean)
          # give_local                (string)
          # formally_charge           (boolean)
          # give_charge               (string)
          # administrative_offense    (boolean)
          # give_offense              (string)
          # any_violation             (boolean)
          # give_reasons              (string)
          # canditate                 (boolean)
          # give_date_particulars     (string)
          # indigenous_member         (boolean)
          # give_group                (string)
          # differently_abled         (boolean)
          # give_disability           (string)
          # solo_parent               (boolean)
          # give_status               (string)
          # is_separated              (boolean)
          # give_details              (string)
          # signature                 (string)
          # date_accomplished         (string) format(yyyy-mm-dd)
          # ctc_number                (string)
          # issued_at                 (string)
          # issued_on                 (string)
          post "/other_information/:employee_id" do
            validate_date_format!(params[:date_accomplished]) if params[:date_accomplished].present?

            
            handler = HrUpdate::OtherInfoHandler.new(params, User::Role::ALL.values).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Specific employee other_information references"
          # name              (string)
          # address           (string)
          # contact_number    (string)
          post "/other_information/:employee_id/references" do

            handler = HrUpdate::OtherInfoHandler.new(params, User::Role::ALL.values).add_references
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Specific employee other_information references"
          # name              (string)
          # address           (string)
          # contact_number    (string)
          post "/other_information/:employee_id/references/:reference_id/update" do
            
            handler = HrUpdate::OtherInfoHandler.new(params, User::Role::ALL.values).update_references
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Specific employee other_information references"
          # reference_id      (integer)
          delete "/other_information/:employee_id/references/:reference_id/delete" do
            
            handler = HrUpdate::OtherInfoHandler.new(params, User::Role::ALL.values).delete_references
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
