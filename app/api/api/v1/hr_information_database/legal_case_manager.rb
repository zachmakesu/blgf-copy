module API
  module V1
    module HrInformationDatabase
      class LegalCaseManager < Grape::API
        include Grape::Kaminari
        resource :legal_case_manager do
          before do
            authorize!(current_user, User::Role::ADMIN)
          end

          desc "List of Legal Cases for Hr information"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/legal_cases" do
            searched = LegalCase.includes(:user).search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::HrInformationDatabase::MyLegalCases::Index, count: LegalCase.count, searched: searched.count
          end

          desc "Specific employee legal cases"
          get "/legal_cases/:employee_id" do
            searched = User.find_by!(employee_id: params[:employee_id]).legal_cases #search employee id
            data = { data: paginate(searched) }
            present data, with: Entities::V1::HrInformationDatabase::MyLegalCases::Index, count: LegalCase.count, searched: searched.count
          end

          desc 'Import Employee Pending Cases.'
          # pending_cases                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:pending_cases],current_user, "pending_cases").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add specific employee's legal case"
          # case_number  (integer)
          # charged_offenses (string)
          # title (text)
          # status (text)
          post "/legal_cases/:employee_id" do
            searched_employee = User.find_by!(employee_id: params[:employee_id]) #search employee id
            handler = LegalCasesHandler.new(params, current_user, searched_employee).create_legal_case
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end

          desc "Update specific employee's legal case"
          # case_number  (integer)
          # charged_offenses (string)
          # title (text)
          # status (text)
          post "/legal_cases/:employee_id/:legal_case_id/update" do
            searched_employee = User.find_by!(employee_id: params[:employee_id]) #search employee id
            handler = LegalCasesHandler.new(params, current_user, searched_employee).update_legal_case
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end

          desc "Delete specific employee's legal case"
          delete "/legal_cases/:employee_id/:legal_case_id/delete" do
            searched_employee = User.find_by!(employee_id: params[:employee_id]) #search employee id
            handler = LegalCasesHandler.new(params, current_user, searched_employee).delete_legal_case
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end
        end
      end
    end
  end
end
