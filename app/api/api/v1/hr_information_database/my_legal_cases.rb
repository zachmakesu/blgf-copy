module API
  module V1
    module HrInformationDatabase
      class MyLegalCases < Grape::API
        include Grape::Kaminari
        resource :my_legal_cases do
          before do
            authorize!(current_user, User::Role::ALL)
          end

          desc "List of My Legal Cases"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = current_user.legal_cases.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::HrInformationDatabase::MyLegalCases::Index, count: current_user.legal_cases.count, searched: searched.count
          end

          desc "Specific legal case"
          get "/:id" do
            datum = current_user.legal_cases.find(params[:id])
            present datum, with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
          end

          desc "Specific legal_cases for bulk edit"
          get "bulk/:ids" do
            ids = params[:ids].split('^').map(&:to_i).uniq
            searched = current_user.legal_cases.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::HrInformationDatabase::MyLegalCases::Index, count: current_user.legal_cases.count, searched: searched.count
          end

          desc "Add legal case"
          # case_number  (integer)
          # charged_offenses (string)
          # title (text)
          # status (text)
          post do
            handler = LegalCasesHandler.new(params, current_user).create_legal_case
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end

          desc "Update legal case"
          # case_number  (integer)
          # charged_offenses (string)
          # title (text)
          # status (text)
          post "/:legal_case_id/update" do
            handler = LegalCasesHandler.new(params, current_user).update_legal_case
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end

          desc "Delete legal case"
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:legal_case_id/delete" do
            handler = LegalCasesHandler.new(params, current_user).delete_legal_case
            if handler.response[:success]
              present handler.response[:details],with: Entities::V1::HrInformationDatabase::MyLegalCases::LegalCasesInfo
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end

          desc "Bulk Delete cases"
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            handler = LegalCasesHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = current_user.legal_cases.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::HrInformationDatabase::MyLegalCases::Index, count: current_user.legal_cases.count, searched: searched.count
            else
              error!({ messages: handler.response[:details] }, 400)
            end
          end
        end
      end
    end
  end
end
