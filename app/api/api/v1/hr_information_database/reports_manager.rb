module API
  module V1
    module HrInformationDatabase
      class ReportsManager < Grape::API
        include Grape::Kaminari
        resource :reports_manager do
          before do
            authorize!(current_user, User::Role::ADMIN)
          end

          desc "List of Employees for Reports Manager"
          paginate per_page: 10, offset: 0
          # page=1&per_page=10 #pagination params
          # search=text
          # paginated=false|true #if true pagination will work else not.
          # search=text
          # sort=asc|desc default "desc"
          # sort_by=age|last_name|educations_count|trainings_count|date_hired|birthdate|gender|salary_grade|service_length|retirees
          # job_category=text
          get "/all_users" do
            sort = params[:sort] == "asc"
            paginated = params[:paginated] == "true"
            job_category = params[:job_category]
            searched = User.includes(:trainings_and_seminars, treasurer_assignment: [lgu_name:[:lgu_type, :province, :region]], educations: [:education_level, :course], position_detail: [:service, plantilla: [:salary_schedule]]).search_in_reports_manager(params[:search])
            searched = job_category.present? ? searched.by_job_category(job_category) : searched
            data =  case params[:sort_by]
                    when "age"
                      sort ? searched.sort_by(&:age) : searched.sort_by(&:age).reverse
                    when "last_name"
                      sort ? searched.sort_by{ |u| u.last_name.downcase } : searched.sort_by{ |u| u.last_name.downcase }.reverse
                    when "educations_count"
                      sort ? searched.sort_by{ |u| u.educations.count } : searched.sort_by{ |u| -u.educations.count }
                    when "trainings_count"
                      sort ? searched.sort_by{ |u| u.trainings_and_seminars.count } : searched.sort_by{ |u| -u.trainings_and_seminars.count }
                    when "date_hired"
                      sort ? searched.select(&:date_hired).sort_by(&:date_hired) + searched.reject(&:date_hired) : searched.select(&:date_hired).sort_by(&:date_hired).reverse  + searched.reject(&:date_hired)
                    when "birthdate"
                      sort ? searched.select(&:birthdate).sort_by(&:birthdate) + searched.reject(&:birthdate) : searched.select(&:birthdate).sort_by(&:birthdate).reverse  + searched.reject(&:birthdate)
                    when "gender"
                      sort ? searched.sort_by(&:gender) : searched.sort_by(&:gender).reverse
                    when "salary_grade"
                      sort ? searched.select(&:salary_grade).sort_by(&:salary_grade) + searched.reject(&:salary_grade) : searched.select(&:salary_grade).sort_by(&:salary_grade).reverse  + searched.reject(&:salary_grade)
                    when "service_length"
                      sort ? searched.sort_by(&:length_of_service_in_years) : searched.sort_by(&:length_of_service_in_years).reverse
                    when "retirees"
                      retired_mode = ModeOfSeparation.find_by(separation_mode: "Retired")
                      searched = retired_mode ? User.retirees(retired_mode).includes(:trainings_and_seminars, educations: [:education_level, :course], position_detail: [:service, plantilla: [:salary_schedule]]).search(params[:search]) : {}

                      searched = job_category.present? &&  searched.present? ? searched.by_job_category(job_category) : searched

                      sort ? searched.sort_by(&:last_name) : searched.sort_by(&:last_name).reverse
                    else
                      searched
                    end

            users = paginated ? { data: paginate(Kaminari.paginate_array(data)) } : { data: data }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::LIST, count: User.count, searched: searched.count
          end


          desc 'List of Vacant Positions'
          paginate per_page: 10, offset: 0
          # page=1&per_page=10 #pagination params
          # search=text
          # paginated=false|true #if true pagination will work else not.
          # search=text
          get "/vacant_positions" do
            paginated = params[:paginated] == "true"
            searched = Plantilla.includes(:position_code, :salary_schedule, :plantilla_group).search(params[:search]).except_used
            data = paginated ? { data: paginate(searched) } : { data: searched }
            present data, with: Entities::V1::EmployeeStatusAndRecordManager::Plantilla::Index, count: Plantilla.except_used.count , searched: searched.count
          end

          desc "List Sort all Treasurers with their documents"
          # page=1&per_page=10 #pagination params
          # search=text
          # paginated=false|true #if true pagination will work else not.
          # sort=asc|desc default "desc"
          # sort_by=age|last_name|date_hired|birthdate|gender|salary_grade|service_length|pdf_files_count
          get "/treasurers" do
            sort = params[:sort] == "asc"
            paginated = params[:paginated] == "true"

            searched = User.by_roles(User::Role::TREASURER.values).includes( :pdf_files, :trainings_and_seminars, treasurer_assignment: [lgu_name:[:lgu_type, :province, :region]], educations: [:education_level, :course], position_detail: [:service, plantilla: [:salary_schedule]]).search(params[:search])
            data =  case params[:sort_by]
                    when "age"
                      sort ? searched.sort_by(&:age) : searched.sort_by(&:age).reverse
                    when "last_name"
                      sort ? searched.sort_by{ |u| u.last_name.downcase } : searched.sort_by{ |u| u.last_name.downcase }.reverse
                    when "date_hired"
                      sort ? searched.select(&:date_hired).sort_by(&:date_hired) + searched.reject(&:date_hired) : searched.select(&:date_hired).sort_by(&:date_hired).reverse  + searched.reject(&:date_hired)
                    when "birthdate"
                      sort ? searched.select(&:birthdate).sort_by(&:birthdate) + searched.reject(&:birthdate) : searched.select(&:birthdate).sort_by(&:birthdate).reverse  + searched.reject(&:birthdate)
                    when "gender"
                      sort ? searched.sort_by(&:gender) : searched.sort_by(&:gender).reverse 
                    when "salary_grade"
                      sort ? searched.select(&:salary_grade).sort_by(&:salary_grade) + searched.reject(&:salary_grade) : searched.select(&:salary_grade).sort_by(&:salary_grade).reverse  + searched.reject(&:salary_grade)
                    when "service_length"
                      sort ? searched.sort_by(&:length_of_service_in_years) : searched.sort_by(&:length_of_service_in_years).reverse
                    when "pdf_files_count"
                      sort ? searched.sort_by{ |u| u.pdf_files.count } : searched.sort_by{ |u| -u.pdf_files.count }
                    else
                      searched
                    end

            users = paginated ? { data: paginate(Kaminari.paginate_array(data)) } : { data: data }
            present users, with: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerWithPdf, count: User.by_roles(User::Role::TREASURER.values).count, searched: searched.count, dms_titles: DocketManagementFile.all
          end

        end

      end
    end
  end
end
