module API
  module V1
    class Login < Grape::API

      resource :login do
        desc "HRMIS Login"
        params do
          requires :employee_id, type: String, desc: 'User employee_id'
          requires :password, type: String, desc: 'User password'
        end

        # employee_id (string)
        # password    (string)
        post do
          handler = SessionHandler.new(params).login
          if handler.response[:success]
            handler.response[:details][:user].unlock_access!
            present handler.response[:details][:user], with: Entities::V1::Settings::UserAccount::UserInfoLogin, access_token: handler.response[:details][:access_token] 
          else
            error!({messages: handler.response[:details]},400)
          end
        end
        
      end
    end
  end
end
