module API
  module V1
    module MyPersonalDataSheet
      class MyPersonalDataSheet < Grape::API
        resource :my_pds do
          before do
            authorize!(current_user, User::Role::ALL )
          end

          desc "Retrieve my personal data"
          get "/personal_data" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation
          end

          desc "Retrieve my family info"
          get "/family" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::Family::FamilyInformation
          end

          desc "Retrieve my education info"
          get "/education" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::Education::EducationInformation
          end

          desc "Retrieve my examination info"
          get "/examination" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation
          end

          desc "Retrieve my work experience info"
          get "/work_experience" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation
          end

          desc "Retrieve my voluntary work info"
          get "/voluntary_work" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation
          end

          desc "Retrieve my trainings and seminars info"
          get "/trainings_and_seminars" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation
          end

          desc "Retrieve my other informations"
          get "/other_informations" do
            present current_user, with: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation
          end

        end
      end
    end
  end
end
