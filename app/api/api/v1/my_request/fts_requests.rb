module API
  module V1
    module MyRequest
      class FtsRequests < Grape::API
        include Grape::Kaminari
        resource :fts_requests do
          before do
            authorize!(current_user, User::Role::ALL )
          end
          
          desc 'List all of my Fts required DTRs'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&date=text
          get "/required_fts_dates"do
            searched = current_user.required_fts_dates(params[:date])
            dtrs = { data: paginate(searched)}
            present dtrs, with: Entities::V1::MyRequest::FtsRequest::RequiredFtsDtrs, count: current_user.required_fts_dates.count, searched: searched.count
          end


          desc 'List all of my Fts request(pending, approved, denied)'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = current_user.fts_requests.search(params[:search])
            fts_requests = { data: paginate(searched) }
            present fts_requests, with: Entities::V1::MyRequest::FtsRequest::Index, count: current_user.fts_requests.count, searched: searched.count
          end

          desc 'Submit Fts request'
          # dtr_date  (string) format "yyyy-mm-dd"
          # in_am     (string) format "hh:mm:ss"
          # out_am    (string) format "hh:mm:ss"
          # in_pm     (string) format "hh:mm:ss"
          # out_pm    (string) format "hh:mm:ss"

          post do
            validate_date_format!(params[:dtr_date]) if params[:dtr_date].present?
            validate_time_format!(params[:in_am]) if params[:in_am].present?
            validate_time_format!(params[:out_am]) if params[:out_am].present?
            validate_time_format!(params[:in_pm]) if params[:in_pm].present?
            validate_time_format!(params[:out_pm]) if params[:out_pm].present?

            params[:owner_id] = current_user.id
            params[:biometric_id] = current_user.biometric_id
            handler = Fts::FtsRequestHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::FtsRequest::FtsRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update pending Fts request'
          # id        (integer)
          # dtr_date  (string) format "yyyy-mm-dd"
          # in_am     (string) format "hh:mm:ss"
          # out_am    (string) format "hh:mm:ss"
          # in_pm     (string) format "hh:mm:ss"
          # out_pm    (string) format "hh:mm:ss"

          post "/:id/update" do
            validate_date_format!(params[:dtr_date]) if params[:dtr_date].present?
            validate_time_format!(params[:in_am]) if params[:in_am].present?
            validate_time_format!(params[:out_am]) if params[:out_am].present?
            validate_time_format!(params[:in_pm]) if params[:in_pm].present?
            validate_time_format!(params[:out_pm]) if params[:out_pm].present?
            
            params[:owner_id] = current_user.id
            params[:biometric_id] = current_user.biometric_id
            handler = Fts::FtsRequestHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::FtsRequest::FtsRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete pending Fts request'
          # id            (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            params[:owner_id] = current_user.id
            handler = Fts::FtsRequestHandler.new(params).delete
            if handler.response[:success]
              fts_requests = { data: paginate(current_user.fts_requests.search(params[:search])) }
              present fts_requests, with: Entities::V1::MyRequest::FtsRequest::Index, count: current_user.fts_requests.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
