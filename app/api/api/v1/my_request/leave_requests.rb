module API
  module V1
    module MyRequest
      class LeaveRequests < Grape::API
        include Grape::Kaminari
        resource :leave_requests do
          before do
            authorize!(current_user, User::Role::ALL )
          end
        
          desc 'List all of my leave request(pending, approved, denied)'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = current_user.leave_requests.search(params[:search])
            leave_requests = { data: paginate(searched) }
            present leave_requests, with: Entities::V1::MyRequest::LeaveRequest::Index, current_user: current_user, count: current_user.leave_requests.count, searched: searched.count
          end

          desc 'List Leave Credits'
          get "leave_credits" do
            present current_user, with: Entities::V1::MyRequest::LeaveRequest::LeaveCredits
          end

          desc 'Specific Leave Request'
          get "/:id" do
            leave_request = current_user.leave_requests.find(params[:id])
            present leave_request, with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
          end

          desc 'Submit Sick leave request for whole day/s'
          # date_from     (string) format(yyyy-mm-dd)
          # date_to       (string) format(yyyy-mm-dd)
          # reason        (text)
          post "sick_leave/whole_day" do
            validate_date_format!(params[:date_from]) if params[:date_from].present?
            validate_date_format!(params[:date_to]) if params[:date_to].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).create_sick_leave_whole_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Pending Sick leave request for whole day/s'
          # date_from     (string) format(yyyy-mm-dd)
          # date_to       (string) format(yyyy-mm-dd)
          # reason        (text)
          post "sick_leave/whole_day/:id/update" do
            validate_date_format!(params[:date_from]) if params[:date_from].present?
            validate_date_format!(params[:date_to]) if params[:date_to].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).update_sick_leave_whole_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit Sick leave request for half day'
          # half_date     (string)
          # reason        (text)
          post "sick_leave/half_day" do
            validate_date_format!(params[:half_date]) if params[:half_date].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).create_sick_leave_half_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Pending Sick leave request for half day'
          # half_date     (string)
          # reason        (text)
          post "sick_leave/half_day/:id/update" do
            validate_date_format!(params[:half_date]) if params[:half_date].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).update_sick_leave_half_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit Vacation leave request for whole day/s'
          # date_from     (string) format(yyyy-mm-dd)
          # date_to       (string) format(yyyy-mm-dd)
          # reason        (text)
          post "vacation_leave/whole_day" do
            validate_date_format!(params[:date_from]) if params[:date_from].present?
            validate_date_format!(params[:date_to]) if params[:date_to].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).create_vacation_leave_whole_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Pending Vacation leave request for whole day/s'
          # date_from     (string) format(yyyy-mm-dd)
          # date_to       (string) format(yyyy-mm-dd)
          # reason        (text)
          post "vacation_leave/whole_day/:id/update" do
            validate_date_format!(params[:date_from]) if params[:date_from].present?
            validate_date_format!(params[:date_to]) if params[:date_to].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).update_vacation_leave_whole_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit Vacation leave request for half day'
          # half_date     (string)
          # reason        (text)
          post "vacation_leave/half_day" do
            validate_date_format!(params[:half_date]) if params[:half_date].present?

            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).create_vacation_leave_half_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Pending Vacation leave request for half day'
          # half_date     (string)
          # reason        (text)
          post "vacation_leave/half_day/:id/update" do
            validate_date_format!(params[:half_date]) if params[:half_date].present?
            
            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).update_vacation_leave_half_day
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete pending leave request'
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            params[:owner_id] = current_user.id
            handler = Leave::LeaveRequestHandler.new(params).delete
            if handler.response[:success]
              searched = current_user.leave_requests.search(params[:search])
              leave_requests = { data: paginate(searched) }
              present leave_requests, with: Entities::V1::MyRequest::LeaveRequest::Index, current_user: current_user, count: current_user.leave_requests.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
