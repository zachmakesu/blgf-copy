module API
  module V1
    module MyRequest
      class PdsRequest < Grape::API
        include Grape::Kaminari
        resource :pds_request do
          before do
            authorize!(current_user, User::Role::ALL )
          end
          
          desc 'List all of my pds request(pending, approved, denied)'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = current_user.drafts.includes(:owner).search(params[:search])
            drafts = { data: paginate(searched) }
            present drafts, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::MyRequest, count: current_user.drafts.count, searched: searched.count
          end

          desc 'My Specific pds request'
          # draft_id  (integer)
          get "/:draft_id" do
            draft = current_user.drafts.find(params[:draft_id])
            present draft, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::ViewPdsDraft
          end

          desc 'Delete My Specific pds pending request'
          # draft_id  (integer)
          delete "/:draft_id" do
            draft = current_user.drafts.pending.find(params[:draft_id])
            draft.destroy

            searched = current_user.drafts.search(params[:search])
            drafts = { data: paginate(searched) }
            present drafts, with: Entities::V1::EmployeeRequestManager::EmployeePdsRequest::MyRequest, count: current_user.drafts.count, searched: searched.count
           
          end
# START PROFILE IMAGE
          desc 'Submit pds request to update profile image'
          # image                (file)
          post "/profile_data/update_profile_image" do
            validate_draft!(params[:image])
            validate_image_size!(params[:image]) if params[:image].present?
            
            ActiveRecord::Base.transaction do
              if params[:image].present?
                @photo = current_user.photos.employee.build()
                @photo.image = build_attachment(params[:image])
                @photo.position = 1
                @photo.save  
              end
            end
            @photo.reload
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_profile_image"
            params[:draft] = Hash.new
            params[:draft][:image_id] = @photo.id
            params[:draft][:image_url] = @photo.image.url
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
# END PROFILE IMAGE

# START SIGNATURE IMAGE
          desc 'Submit pds request to update signature image'
          # image                (file)
          post "/profile_data/update_signature_image" do
            validate_draft!(params[:image])
            validate_image_size!(params[:image]) if params[:image].present?
            
            ActiveRecord::Base.transaction do
              if params[:image].present?
                @photo = current_user.photos.signature.build()
                @photo.image = build_attachment(params[:image])
                @photo.position = 1
                @photo.save  
              end
            end
            @photo.reload
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_signature_image"
            params[:draft] = Hash.new
            params[:draft][:image_id] = @photo.id
            params[:draft][:image_url] = @photo.image.url
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
# END SIGNATURE IMAGE

# START CONTACT
          desc 'Submit pds request to add contact under Personal information'
          # draft[device]                 (string) values (mobile, telephone)
          # draft[number]                 (string)
          post "/profile_data/add_contact" do
            validate_draft!(params[:draft])
            
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_contact"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to update contact under Personal information'
          # draft[device]                 (string) values (mobile, telephone)
          # draft[number]                 (string)
          post "/profile_data/update_contact/:contact_id" do
            validate_draft!(params[:draft])

            contact = current_user.contacts.find(params[:contact_id])
            params[:draft][:contact_id] = params[:contact_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_contact"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_contact or update_contact) request'
          # draft[device]                 (string) 
          # draft[number]                 (string)
          post "/profile_data/contacts/:draft_id/update_pending" do
            validate_draft!(params[:draft])

            draft = current_user.drafts.pending_contacts.find(params[:draft_id])
            params[:draft][:contact_id] = draft.object.contact_id if draft.object.contact_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete contact under Personal information'
          post "/profile_data/delete_contact/:contact_id" do
            contact = current_user.contacts.find(params[:contact_id])
            params[:draft] = Hash.new
            params[:draft][:contact_id] = params[:contact_id]
            params[:draft][:device] = contact.device
            params[:draft][:number] = contact.number

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_contact"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
# END CONTACT

# START ADDRESS
          desc 'Submit pds request to add address under Personal information'
          # draft[residency]      (string)  values( residential, permanent )
          # draft[location]       (string)
          # draft[zipcode]        (string)
          # draft[contact_number] (string)
          post "/profile_data/add_address" do
            validate_draft!(params[:draft])

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_address"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to update address under Personal information'
          # draft[residency]      (string)  values( residential, permanent )
          # draft[location]      (string)
          # draft[zipcode]        (string)
          # draft[contact_number] (string)
          post "/profile_data/update_address/:address_id" do
            validate_draft!(params[:draft])

            address = current_user.addresses.find(params[:address_id])
            params[:draft][:address_id] = params[:address_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_address"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_address or update_address) request'
          # draft[residency]      (string)  values( residential, permanent )
          # draft[location]       (string)
          # draft[zipcode]        (string)
          # draft[contact_number] (string)
          post "/profile_data/addresses/:draft_id/update_pending" do
            validate_draft!(params[:draft])

            draft = current_user.drafts.pending_addresses.find(params[:draft_id])
            params[:draft][:address_id] = draft.object.address_id if draft.object.address_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification
            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete address under Personal information'
          post "/profile_data/delete_address/:address_id" do
            address = current_user.addresses.find(params[:address_id])
            params[:draft] = Hash.new
            params[:draft][:address_id]     = params[:address_id]
            params[:draft][:residency]      = address.residency
            params[:draft][:location]       = address.location
            params[:draft][:zipcode]        = address.zipcode
            params[:draft][:contact_number] = address.contact_number

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_address"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
# END ADDRESS

# START PERSONAL INFORMATION
          desc 'Submit pds request for Update Personal information'
          # draft[email]                      (string)
          # draft[first_name]                 (string)  
          # draft[last_name]                  (string)  
          # draft[middle_name]                (string) 
          # draft[middle_initial]             (string)
          # draft[name_extension]             (string)
          # draft[birthdate]                  (string)  format(yyyy-mm-dd)
          # draft[birthplace]                 (string)
          # draft[gender]                     (string)  values( male, female )
          # draft[civil_status]               (string)  values( single, living_common_law, widowed, separated, divorced, married)
          # draft[citizenship]                (string)
          # draft[height]                     (float)
          # draft[weight]                     (float)
          # draft[blood_type]                 (string)
          # draft[tin_number]                 (string)
          # draft[gsis_policy_number]         (string)
          # draft[pagibig_id_number]          (string)
          # draft[philhealth_number]          (string)
          # draft[sss_number]                 (string)
          # draft[remittance_id]              (string)
          post "/profile_data" do
            validate_draft!(params[:draft])

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_profile_data"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update submitted(pending) pds request for Update Personal information'
          # draft[email]                      (string) 
          # draft[first_name]                 (string) 
          # draft[last_name]                  (string) 
          # draft[middle_name]                (string)
          # draft[middle_initial]             (string)
          # draft[name_extension]             (string)
          # draft[birthdate]                  (string)  format(yyyy-mm-dd)
          # draft[birthplace]                 (string)
          # draft[gender]                     (string)  values( male, female )
          # draft[civil_status]               (string)  values( single, living_common_law, widowed, separated, divorced, married)
          # draft[citizenship]                (string)
          # draft[height]                     (float)
          # draft[weight]                     (float)
          # draft[blood_type]                 (string)
          # draft[tin_number]                 (string)
          # draft[gsis_policy_number]         (string)
          # draft[pagibig_id_number]          (string)
          # draft[philhealth_number]          (string)
          # draft[sss_number]                 (string)
          # draft[remittance_id]              (string)
          post "/profile_data/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            draft = current_user.drafts.find(params[:draft_id])
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification
            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
# END PERSONAL INFO

# START EDUCATION
          desc 'Submit pds request to Add Education'
          # draft[education_level_id]               (integer)
          # draft[school_name]                      (string)
          # draft[course_id]                        (integer)
          # draft[highest_grade_level_units_earned] (string)
          # draft[date_of_attendance_from]          (string) format (yyyy-mm-dd)
          # draft[date_of_attendance_to]            (string) format (yyyy-mm-dd)
          # draft[honors_received]                  (string)
          # draft[year_graduated]                   (string) format (yyyy)
          post "/education/add_education" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_attendance_from]) if params[:draft][:date_of_attendance_from].present?
            validate_date_format!(params[:draft][:date_of_attendance_to]) if params[:draft][:date_of_attendance_to].present?
            validate_year_format!(params[:draft][:year_graduated]) if params[:draft][:year_graduated].present?
          
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_education"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to Update Education'
          # draft[education_level_id]               (integer)
          # draft[school_name]                      (string)
          # draft[course_id]                        (integer)
          # draft[highest_grade_level_units_earned] (string)
          # draft[date_of_attendance_from]          (string) format (yyyy-mm-dd)
          # draft[date_of_attendance_to]            (string) format (yyyy-mm-dd)
          # draft[honors_received]                  (string)
          # draft[year_graduated]                   (string) format (yyyy)
          post "/education/update_education/:education_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_attendance_from]) if params[:draft][:date_of_attendance_from].present?
            validate_date_format!(params[:draft][:date_of_attendance_to]) if params[:draft][:date_of_attendance_to].present?
            validate_year_format!(params[:draft][:year_graduated]) if params[:draft][:year_graduated].present?

            education = current_user.educations.find(params[:education_id])
            params[:draft][:education_id] = params[:education_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_education"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_education or update_education) request'
          # draft[education_level_id]               (integer)
          # draft[school_name]                      (string)
          # draft[course_id]                        (integer)
          # draft[highest_grade_level_units_earned] (string)
          # draft[date_of_attendance_from]          (string) format (yyyy-mm-dd)
          # draft[date_of_attendance_to]            (string) format (yyyy-mm-dd)
          # draft[honors_received]                  (string)
          # draft[year_graduated]                   (string) format (yyyy)
          post "/education/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_attendance_from]) if params[:draft][:date_of_attendance_from].present?
            validate_date_format!(params[:draft][:date_of_attendance_to]) if params[:draft][:date_of_attendance_to].present?
            validate_year_format!(params[:draft][:year_graduated]) if params[:draft][:year_graduated].present?

            draft = current_user.drafts.pending_educations.find(params[:draft_id])
            params[:draft][:education_id] = draft.object.education_id if draft.object.education_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete Education'
          post "/education/delete_education/:education_id" do
            education = current_user.educations.find(params[:education_id])
            params[:draft] = Hash.new
            params[:draft][:education_id]                     = params[:education_id]
            params[:draft][:education_level_id]               = education.education_level_id
            params[:draft][:school_name]                      = education.school_name
            params[:draft][:course_id]                        = education.course_id
            params[:draft][:highest_grade_level_units_earned] = education.highest_grade_level_units_earned
            params[:draft][:date_of_attendance_from]          = education.date_of_attendance_from
            params[:draft][:date_of_attendance_to]            = education.date_of_attendance_to
            params[:draft][:honors_received]                  = education.honors_received
            params[:draft][:year_graduated]                   = education.year_graduated

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_education"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END EDUCATION

#START EXAMINATION
          desc 'Submit pds request to Add Examination'
          # draft[exam_description] (string)
          # draft[place_of_exam]    (string)
          # draft[rating]           (float)
          # draft[date_of_exam]     (string) format (yyyy-mm-dd)
          # draft[licence_number]   (string)
          # draft[date_of_release]  (string) format (yyyy-mm-dd)
          post "/examination/add_examination" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_exam]) if params[:draft][:date_of_exam].present?
            validate_date_format!(params[:draft][:date_of_release]) if params[:draft][:date_of_release].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_examination"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to Update Examination'
          # draft[exam_description] (string)
          # draft[place_of_exam]    (string)
          # draft[rating]           (float)
          # draft[date_of_exam]     (string) format (yyyy-mm-dd)
          # draft[licence_number]   (string)
          # draft[date_of_release]  (string) format (yyyy-mm-dd)
          post "/examination/update_examination/:examination_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_exam]) if params[:draft][:date_of_exam].present?
            validate_date_format!(params[:draft][:date_of_release]) if params[:draft][:date_of_release].present?

            examination = current_user.examinations.find(params[:examination_id])
            params[:draft][:examination_id] = params[:examination_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_examination"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_examination or update_examination) request'
          # draft[exam_description] (string)
          # draft[place_of_exam]    (string)
          # draft[rating]           (float)
          # draft[date_of_exam]     (string) format (yyyy-mm-dd)
          # draft[licence_number]   (string)
          # draft[date_of_release]  (string) format (yyyy-mm-dd)
          post "/examination/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:date_of_exam]) if params[:draft][:date_of_exam].present?
            validate_date_format!(params[:draft][:date_of_release]) if params[:draft][:date_of_release].present?

            draft = current_user.drafts.pending_examinations.find(params[:draft_id])
            params[:draft][:examination_id] = draft.object.examination_id if draft.object.examination_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete Examination'
          post "/examination/delete_examination/:examination_id" do
            examination = current_user.examinations.find(params[:examination_id])
            params[:draft] = Hash.new
            params[:draft][:examination_id]         = params[:examination_id]
            params[:draft][:exam_description]       = examination.exam_description
            params[:draft][:place_of_exam]          = examination.place_of_exam
            params[:draft][:rating]                 = examination.rating
            params[:draft][:date_of_exam]           = examination.date_of_exam
            params[:draft][:licence_number]         = examination.licence_number
            params[:draft][:date_of_release]        = examination.date_of_release
          
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_examination"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END EXAMINATION
          
#START WORK EXPERIENCE
          desc 'Submit pds request to Add Work Experience'
          # draft[position_title]            (string)
          # draft[inclusive_date_to]        (string) format (yyyy-mm-dd)
          # draft[inclusive_date_from]      (string) format (yyyy-mm-dd)
          # draft[department_agency_office]  (string)
          # draft[monthly_salary]            (float)
          # draft[salary_grade_and_step]     (string) format(grade_number-steps) example value '02-5'
          # draft[appointment_status_id]     (integer)
          # draft[government_service]        (boolean)
          post "/work_experience/add_work_experience" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_work_experience"
            params[:draft][:salary_grade_and_step] = "00-0" if params[:draft][:salary_grade_and_step].blank?
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to Update Work Experience'
          # draft[position_title]            (string)
          # draft[inclusive_date_to]        (string) format (yyyy-mm-dd)
          # draft[inclusive_date_from]      (string) format (yyyy-mm-dd)
          # draft[department_agency_office]  (string)
          # draft[monthly_salary]            (float)
          # draft[salary_grade_and_step]     (string) format(grade_number-steps) example value '02-5'
          # draft[appointment_status_id]     (integer)
          # draft[government_service]        (boolean)
          post "/work_experience/update_work_experience/:work_experience_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            work_experience = current_user.work_experiences.find(params[:work_experience_id])
            params[:draft][:work_experience_id] = params[:work_experience_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_work_experience"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_work_experience or update_work_experience) request'
          # draft[position_title]            (string)
          # draft[inclusive_date_to]        (string) format (yyyy-mm-dd)
          # draft[inclusive_date_from]      (string) format (yyyy-mm-dd)
          # draft[department_agency_office]  (string)
          # draft[monthly_salary]            (float)
          # draft[salary_grade_and_step]     (string) format(grade_number-steps) example value '02-5'
          # draft[appointment_status_id]     (integer)
          # draft[government_service]        (boolean)
          post "/work_experience/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            draft = current_user.drafts.pending_work_experiences.find(params[:draft_id])
            params[:draft][:work_experience_id] = draft.object.work_experience_id if draft.object.work_experience_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete Work Experience'
          post "/work_experience/delete_work_experience/:work_experience_id" do
            work_experience = current_user.work_experiences.find(params[:work_experience_id])
            params[:draft] = Hash.new
            params[:draft][:work_experience_id]       = params[:work_experience_id]

            params[:draft][:position_title]           = work_experience.position_title
            params[:draft][:inclusive_date_to]        = work_experience.inclusive_date_to
            params[:draft][:inclusive_date_from]      = work_experience.inclusive_date_from
            params[:draft][:department_agency_office] = work_experience.department_agency_office
            params[:draft][:monthly_salary]           = work_experience.monthly_salary
            params[:draft][:salary_grade_and_step]    = work_experience.salary_grade_and_step
            params[:draft][:appointment_status_id]    = work_experience.appointment_status_id
            params[:draft][:government_service]       = work_experience.government_service
          
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_work_experience"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END WORK EXPERIENCE

#START VOLUNTARY WORK
          desc 'Submit pds request to Add Voluntary Work'
          # draft[name_of_organization]      (string)
          # draft[address_of_organization]   (string)
          # draft[inclusive_date_from]       (string) format (yyyy-mm-dd)
          # draft[inclusive_date_to]         (string) format (yyyy-mm-dd)
          # draft[number_of_hours]           (integer)
          # draft[position_nature_of_work]   (string)
          post "/voluntary_work/add_voluntary_work" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_voluntary_work"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to Update Voluntary Work'
          # draft[name_of_organization]      (string)
          # draft[address_of_organization]   (string)
          # draft[inclusive_date_from]       (string) format (yyyy-mm-dd)
          # draft[inclusive_date_to]         (string) format (yyyy-mm-dd)
          # draft[number_of_hours]           (integer)
          # draft[position_nature_of_work]   (string)
          post "/voluntary_work/update_voluntary_work/:voluntary_work_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            voluntary_work = current_user.voluntary_works.find(params[:voluntary_work_id])
            params[:draft][:voluntary_work_id] = params[:voluntary_work_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_voluntary_work"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_voluntary_work or update_voluntary_work) request'
          # draft[name_of_organization]      (string)
          # draft[address_of_organization]   (string)
          # draft[inclusive_date_from]       (string) format (yyyy-mm-dd)
          # draft[inclusive_date_to]         (string) format (yyyy-mm-dd)
          # draft[number_of_hours]           (integer)
          # draft[position_nature_of_work]   (string)
          post "/voluntary_work/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?

            draft = current_user.drafts.pending_voluntary_works.find(params[:draft_id])
            params[:draft][:voluntary_work_id] = draft.object.voluntary_work_id if draft.object.voluntary_work_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete Voluntary Work'
          post "/voluntary_work/delete_voluntary_work/:voluntary_work_id" do
            voluntary_work = current_user.voluntary_works.find(params[:voluntary_work_id])
            params[:draft] = Hash.new
            params[:draft][:voluntary_work_id]        = params[:voluntary_work_id]
            params[:draft][:name_of_organization]     = voluntary_work.name_of_organization
            params[:draft][:address_of_organization]  = voluntary_work.address_of_organization
            params[:draft][:inclusive_date_from]      = voluntary_work.inclusive_date_from
            params[:draft][:inclusive_date_to]        = voluntary_work.inclusive_date_to
            params[:draft][:number_of_hours]          = voluntary_work.number_of_hours
            params[:draft][:position_nature_of_work]  = voluntary_work.position_nature_of_work
          
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_voluntary_work"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END VOLUNTARY WORK

#START FAMILY INFORMATION
          desc 'Submit pds request for Update Family information'
          # draft[spouse_name]                        (string)
          # draft[spouse_occupation]                  (string)
          # draft[spouse_employer_or_business_name]   (string)
          # draft[spouse_work_or_business_address]    (string)
          # draft[spouse_contact_number]              (string)
          # draft[parent_fathers_name]                (string)
          # draft[parent_mothers_maiden_name]         (string)
          # draft[parent_address]                     (string)

          # draft[spouse_first_name]           (string)
          # draft[spouse_middle_name]           (string)
          # draft[spouse_last_name]           (string)
          # draft[parant_fathers_first_name]           (string)
          # draft[parant_fathers_middle_name]           (string)
          # draft[parant_fathers_last_name]           (string)
          # draft[parant_mothers_first_name]           (string)
          # draft[parant_mothers_middle_name]           (string)
          # draft[parant_mothers_last_name]          (string)
          post "/family_information" do
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_family_information"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update submitted(pending) pds request for Update Family information'
          # draft[spouse_name]                        (string)
          # draft[spouse_occupation]                  (string)
          # draft[spouse_employer_or_business_name]   (string)
          # draft[spouse_work_or_business_address]    (string)
          # draft[spouse_contact_number]              (string)
          # draft[parent_fathers_name]                (string)
          # draft[parent_mothers_maiden_name]         (string)
          # draft[parent_address]                     (string)

          # draft[spouse_first_name]           (string)
          # draft[spouse_middle_name]           (string)
          # draft[spouse_last_name]           (string)
          # draft[parant_fathers_first_name]           (string)
          # draft[parant_fathers_middle_name]           (string)
          # draft[parant_fathers_last_name]           (string)
          # draft[parant_mothers_first_name]           (string)
          # draft[parant_mothers_middle_name]           (string)
          # draft[parant_mothers_last_name]          (string)
          post "/family_information/:draft_id/update_pending" do
            draft = current_user.drafts.find(params[:draft_id])
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification
            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END FAMILY INFORMATION

#START CHILDREN
          desc 'Submit pds request to add child under Family information'
          # draft[first_name]         (string) 
          # draft[last_name]          (string)
          # draft[middle_initial]     (string) 
          # draft[middle_name]        (string)
          # draft[relationship]       (string)
          # draft[birthdate]          (string) format (yyyy-mm-dd)
          post "/family_information/add_child" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:birthdate]) if params[:draft][:birthdate].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_child"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to update child under Family information'
          # draft[first_name]         (string) 
          # draft[last_name]          (string)
          # draft[middle_initial]     (string) 
          # draft[middle_name]        (string)
          # draft[relationship]       (string)
          # draft[birthdate]          (string) format (yyyy-mm-dd)
          post "/family_information/update_child/:child_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:birthdate]) if params[:draft][:birthdate].present?

            child = current_user.children.find(params[:child_id])
            params[:draft][:child_id] = params[:child_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_child"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_child or update_child) request'
          # draft[first_name]         (string) 
          # draft[last_name]          (string)
          # draft[middle_initial]     (string) 
          # draft[middle_name]        (string)
          # draft[relationship]       (string)
          # draft[birthdate]          (string) format (yyyy-mm-dd)
          post "/family_information/children/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:birthdate]) if params[:draft][:birthdate].present?

            draft = current_user.drafts.pending_children.find(params[:draft_id])
            params[:draft][:child_id] = draft.object.child_id if draft.object.child_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete child under Family information'
          post "/family_information/delete_child/:child_id" do
            child = current_user.children.find(params[:child_id])
            params[:draft] = Hash.new
            params[:draft][:child_id] = params[:child_id]
            params[:draft][:first_name]     = child.first_name
            params[:draft][:last_name]      = child.last_name
            params[:draft][:middle_initial] = child.middle_initial
            params[:draft][:middle_name]    = child.middle_name
            params[:draft][:relationship]   = child.relationship
            params[:draft][:birthdate]      = child.birthdate
           
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_child"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END CHILDREN
          
#START TRAINING AND SEMINAR
          desc 'Submit pds request to Add Training and Seminar'
          # draft[title]                 (string)
          # draft[inclusive_date_from]   (string) format(yyyy-mm-dd)
          # draft[inclusive_date_to]     (string) format(yyyy-mm-dd)
          # draft[number_of_hours]       (integer)
          # draft[conducted_by]          (string)
          post "/training_and_seminar/add_training_and_seminar" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_training_and_seminar"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to Update Training and Seminar'
          # draft[title]                 (string)
          # draft[inclusive_date_from]   (string) format(yyyy-mm-dd)
          # draft[inclusive_date_to]     (string) format(yyyy-mm-dd)
          # draft[number_of_hours]       (integer)
          # draft[conducted_by]          (string)
          post "/training_and_seminar/update_training_and_seminar/:training_and_seminar_id" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?

            training_and_seminar = current_user.trainings_and_seminars.find(params[:training_and_seminar_id])
            params[:draft][:training_and_seminar_id] = params[:training_and_seminar_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_training_and_seminar"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_training_and_seminar or update_training_and_seminar) request'
          # draft[title]                 (string)
          # draft[inclusive_date_from]   (string) format(yyyy-mm-dd)
          # draft[inclusive_date_to]     (string) format(yyyy-mm-dd)
          # draft[number_of_hours]       (integer)
          # draft[conducted_by]          (string)
          post "/training_and_seminar/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:inclusive_date_from]) if params[:draft][:inclusive_date_from].present?
            validate_date_format!(params[:draft][:inclusive_date_to]) if params[:draft][:inclusive_date_to].present?

            draft = current_user.drafts.pending_trainings_and_seminars.find(params[:draft_id])
            params[:draft][:training_and_seminar_id] = draft.object.training_and_seminar_id if draft.object.training_and_seminar_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete Training and Seminar'
          post "/training_and_seminar/delete_training_and_seminar/:training_and_seminar_id" do
            training_and_seminar = current_user.trainings_and_seminars.find(params[:training_and_seminar_id])
            params[:draft] = Hash.new
            params[:draft][:training_and_seminar_id]   = params[:training_and_seminar_id]
            params[:draft][:title]               = training_and_seminar.title
            params[:draft][:inclusive_date_from] = training_and_seminar.inclusive_date_from
            params[:draft][:inclusive_date_to]   = training_and_seminar.inclusive_date_to
            params[:draft][:number_of_hours]     = training_and_seminar.number_of_hours
            params[:draft][:conducted_by]        = training_and_seminar.conducted_by

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_training_and_seminar"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END TRAINING AND SEMINAR

#START REFERENCES UNDER OTHER INFORMATION
          desc 'Submit pds request to add reference under Other information'
          # draft[name]           (string)
          # draft[address]        (string)
          # draft[contact_number] (string)
          post "/other_information/add_reference" do
            validate_draft!(params[:draft])

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "add_reference"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to update reference under Other information'
          # draft[name]           (string)
          # draft[address]        (string)
          # draft[contact_number] (string)
          post "/other_information/update_reference/:reference_id" do
            validate_draft!(params[:draft])

            reference = current_user.references.find(params[:reference_id])
            params[:draft][:reference_id] = params[:reference_id]
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_reference"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update your submitted(pending) (add_reference or update_reference) request'
          # draft[name]           (string)
          # draft[address]        (string)
          # draft[contact_number] (string)
          post "/other_information/references/:draft_id/update_pending" do
            validate_draft!(params[:draft])

            draft = current_user.drafts.pending_references.find(params[:draft_id])
            params[:draft][:reference_id] = draft.object.reference_id if draft.object.reference_id.present?
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification

            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Submit pds request to delete reference under Other information'
          post "/other_information/delete_reference/:reference_id" do
            reference = current_user.references.find(params[:reference_id])
            params[:draft] = Hash.new
            params[:draft][:reference_id]   = params[:reference_id]
            params[:draft][:name]           = reference.name
            params[:draft][:address]        = reference.address
            params[:draft][:contact_number] = reference.contact_number
           
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "delete_reference"
            handler = Requests::PDS::DraftHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END REFERENCES UNDER OTHER INFORMATION

#START OTHER INFORMATION
          desc 'Submit pds request for Update Other information'
          # draft[skills_and_hobbies]         (string) value ex. "Surfing|Biking|Trekking"
          # draft[non_academic_destinction]   (string) value ex. "Homecoming Queen|Football letterman|Rifle team captain"
          # draft[membership_in]              (string) value ex. "GWAPOTODA|GANDATODA|BAyawYouthActionGroup"
          # draft[third_degree_national]      (boolean)
          # draft[give_national]              (string)
          # draft[fourth_degree_local]        (boolean) 
          # draft[give_local]                 (string)
          # draft[formally_charge]            (boolean)
          # draft[give_charge]                (string)
          # draft[administrative_offense]     (boolean)
          # draft[give_offense]               (string)
          # draft[any_violation]              (boolean)
          # draft[give_reasons]               (string)
          # draft[canditate]                  (boolean)
          # draft[give_date_particulars]      (string)
          # draft[indigenous_member]          (boolean)
          # draft[give_group]                 (string)
          # draft[differently_abled]          (boolean)
          # draft[give_disability]            (string)
          # draft[solo_parent]                (boolean)
          # draft[give_status]                (string)
          # draft[is_separated]               (boolean)
          # draft[give_details]               (string)
          # draft[signature]                  (string)
          # draft[date_accomplished]          (string)
          # draft[ctc_number]                 (string)
          # draft[issued_at]                  (string)
          # draft[issued_on]                  (string) format(yyyy-mm-dd)
          post "/other_information" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:issued_on]) if params[:draft][:issued_on].present?

            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = "update_other_information"
            handler = Requests::PDS::DraftHandler.new(params).create

            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update submitted(pending) pds request for Update Other information'
          # draft[skills_and_hobbies]         (string) value ex. "Surfing|Biking|Trekking"
          # draft[non_academic_destinction]   (string) value ex. "Homecoming Queen|Football letterman|Rifle team captain"
          # draft[membership_in]              (string) value ex. "GWAPOTODA|GANDATODA|BAyawYouthActionGroup"
          # draft[third_degree_national]      (boolean)
          # draft[give_national]              (string)
          # draft[fourth_degree_local]        (boolean) 
          # draft[give_local]                 (string)
          # draft[formally_charge]            (boolean)
          # draft[give_charge]                (string)
          # draft[administrative_offense]     (boolean)
          # draft[give_offense]               (string)
          # draft[any_violation]              (boolean)
          # draft[give_reasons]               (string)
          # draft[canditate]                  (boolean)
          # draft[give_date_particulars]      (string)
          # draft[indigenous_member]          (boolean)
          # draft[give_group]                 (string)
          # draft[differently_abled]          (boolean)
          # draft[give_disability]            (string)
          # draft[solo_parent]                (boolean)
          # draft[give_status]                (string)
          # draft[is_separated]               (boolean)
          # draft[give_details]               (string)
          # draft[signature]                  (string)
          # draft[date_accomplished]          (string)
          # draft[ctc_number]                 (string)
          # draft[issued_at]                  (string)
          # draft[issued_on]                  (string) format(yyyy-mm-dd)
          post "/other_information/:draft_id/update_pending" do
            validate_draft!(params[:draft])
            validate_date_format!(params[:draft][:issued_on]) if params[:draft][:issued_on].present?
            draft = current_user.drafts.find(params[:draft_id])
            params[:owner_employee_id] = current_user.employee_id
            params[:classification] = draft.classification
            handler = Requests::PDS::DraftHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::PdsDrafts::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end
#END OTHER INFORMATION

        end
      end
      
    end
  end
end
