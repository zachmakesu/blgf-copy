module API
  module V1
    class Notifications < Grape::API
      include Grape::Kaminari
      resource :notifications do
        before do
          authorize!(current_user, User::Role::ALL )
        end

        desc "List All Notifications"
        paginate per_page: 10, offset: 0
        get do
          searched = current_user.all_notifications(params[:search]).sort_by { |h| h[:id] * -1 }
          notifications = { data: paginate(Kaminari.paginate_array(searched)) }
          present notifications, with: Entities::V1::Notification::Index, count: current_user.all_notifications.count, searched: searched.count, current_user: current_user
        end

        desc "Bulk remove notifications that is already seen by current user"
        paginate per_page: 10, offset: 0
        post "/bulk/:ids" do
          ids = params[:ids].split('^')
          ids = ids.uniq.map {|i| i.to_i }.uniq
          ids.each do |id| 
            current_user.user_seen_notifications.find_or_create_by(notification_id: id) { |data| data.notification_id = id}
          end

          searched = current_user.all_notifications(params[:search]).sort_by { |h| h[:id] * -1 }
          notifications = { data: paginate(Kaminari.paginate_array(searched)) }
          present notifications, with: Entities::V1::Notification::Index, count: current_user.all_notifications.count, searched: searched.count, current_user: current_user
        end

        desc "Remove notifications that is already seen by current user"
        paginate per_page: 10, offset: 0
        post "/:id" do
          current_user.user_seen_notifications.find_or_create_by(notification_id: params[:id]) { |data| data.notification_id = params[:id] }

          searched = current_user.all_notifications(params[:search]).sort_by { |h| h[:id] * -1 }
          notifications = { data: paginate(Kaminari.paginate_array(searched)) }
          present notifications, with: Entities::V1::Notification::Index, count: current_user.all_notifications.count, searched: searched.count, current_user: current_user
        end
      end
    end
  end
end
