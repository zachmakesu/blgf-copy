module API
  module V1
    module Payroll
      class AttendanceSchemes < Grape::API
        include Grape::Kaminari
        resource :attendance_schemes do
   
          desc 'List Attendace Schemes'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = AttendanceScheme.search(params[:search])
            schemes = { data: paginate(searched) }
            present schemes, with: Entities::V1::Payroll::AttendanceScheme::Index, count: AttendanceScheme.count, searched: searched.count
          end

          desc 'Add Attendace Scheme'
          # scheme_code      (string)
          # scheme_name      (string)
          # am_time_in_from  (string) "07:00:00"
          # am_time_in_to    (string) "08:00:00"
          # pm_time_out_from (string) "16:00:00"
          # pm_time_out_to   (string) "17:00:00"
          # nn_time_out_from (string) "12:00:00"
          # nn_time_out_to   (string) "13:00:00"
          # nn_time_in_from  (string) "12:00:00"
          # nn_time_in_to    (string) "13:00:00"
          post do
            authorize!(current_user, User::Role::ADMIN )
            validate_time_format!(params[:am_time_in_from]) if params[:am_time_in_from].present?
            validate_time_format!(params[:am_time_in_to]) if params[:am_time_in_to].present?
            validate_time_format!(params[:pm_time_out_from]) if params[:pm_time_out_from].present?
            validate_time_format!(params[:pm_time_out_to]) if params[:pm_time_out_to].present?
            validate_time_format!(params[:nn_time_out_from]) if params[:nn_time_out_from].present?
            validate_time_format!(params[:nn_time_out_to]) if params[:nn_time_out_to].present?
            validate_time_format!(params[:nn_time_in_from]) if params[:nn_time_in_from].present?
            validate_time_format!(params[:nn_time_in_to]) if params[:nn_time_in_to].present?

            handler = AttendanceSchemeHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::AttendanceScheme::AttendanceSchemeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Attendace Scheme'
          # id               (integer)
          # scheme_code      (string)
          # scheme_name      (string)
          # am_time_in_from  (string) "07:00:00"
          # am_time_in_to    (string) "08:00:00"
          # pm_time_out_from (string) "16:00:00"
          # pm_time_out_to   (string) "17:00:00"
          # nn_time_out_from (string) "12:00:00"
          # nn_time_out_to   (string) "13:00:00"
          # nn_time_in_from  (string) "12:00:00"
          # nn_time_in_to    (string) "13:00:00"
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            validate_time_format!(params[:am_time_in_from]) if params[:am_time_in_from].present?
            validate_time_format!(params[:am_time_in_to]) if params[:am_time_in_to].present?
            validate_time_format!(params[:pm_time_out_from]) if params[:pm_time_out_from].present?
            validate_time_format!(params[:pm_time_out_to]) if params[:pm_time_out_to].present?
            validate_time_format!(params[:nn_time_out_from]) if params[:nn_time_out_from].present?
            validate_time_format!(params[:nn_time_out_to]) if params[:nn_time_out_to].present?
            validate_time_format!(params[:nn_time_in_from]) if params[:nn_time_in_from].present?
            validate_time_format!(params[:nn_time_in_to]) if params[:nn_time_in_to].present?
            
            handler = AttendanceSchemeHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::AttendanceScheme::AttendanceSchemeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Attendace Scheme'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = AttendanceSchemeHandler.new(params).delete
            if handler.response[:success]
              searched = AttendanceScheme.search(params[:search])
              schemes = { data: paginate(searched) }
              present schemes, with: Entities::V1::Payroll::AttendanceScheme::Index, count: AttendanceScheme.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
          
        end
      end
    end
  end
end
