module API
  module V1
    module Payroll
      class Benefits < Grape::API
        include Grape::Kaminari
        resource :benefits do
    
          desc 'List benefits'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Benefit.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::Benefit::Index, count: Benefit.count, searched: searched.count
          end

          desc 'List all benefits'
          get "/all" do
            searched = Benefit.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::Benefit::Index, count: Benefit.count, searched: searched.count
          end

          desc 'Import Benefits CSV. Create or Update only'
          # benefits                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:benefits],current_user, "benefits").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end


          desc 'Specific benefits for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = Benefit.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::Benefit::Index, count: Benefit.count, searched: searched.count
          end

          desc 'Bulk Delete benefits'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = BenefitHandler.new(params, current_user).bulk_delete
            if handler.response[:success]
              searched = Benefit.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::Payroll::Benefit::Index, count: Benefit.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific benefit'
          get "/:id" do
            datum = Benefit.find(params[:id])
            present datum, with: Entities::V1::Payroll::Benefit::BenefitInfo
          end

          desc 'Add benefit'
          # code   (string)
          # benefit_type (string) values (allowance, bonus, additional_income)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = BenefitHandler.new(params,current_user).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::Benefit::BenefitInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update benefit'
          # code   (string)
          # benefit_type (string) values (allowance, bonus, additional_income)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = BenefitHandler.new(params,current_user).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::Benefit::BenefitInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete benefit'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = BenefitHandler.new(params,current_user).delete
            if handler.response[:success]
              searched = Benefit.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::Payroll::Benefit::Index, count: Benefit.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      
      end
    end
  end
end
