module API
  module V1
    module Payroll
      class DeductionTypes < Grape::API
        include Grape::Kaminari
        resource :deduction_types do 
          desc 'List Deduction Types'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = DeductionType.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::DeductionType::Index, count: DeductionType.count, searched: searched.count
          end

          desc 'List all Deduction Types'
          get "/all" do
            searched = DeductionType.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::DeductionType::Index, count: DeductionType.count, searched: searched.count
          end

          desc 'Import Deduction Types CSV. Create or Update only'
          # deduction_types                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:deduction_types],current_user, "deduction_types").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'List all Deductive Base Calculations'
          get "/deductive_base_calculations" do
            { data: DeductionType::DEDUCTIVE_CALCULATIONS.map{|d| {base_calculation: d}} }
          end

          desc 'Specific Deduction Type for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = DeductionType.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::DeductionType::Index, count: DeductionType.count, searched: searched.count
          end

          desc 'Bulk Delete Deduction Types'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DeductionTypeHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = DeductionType.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::Payroll::DeductionType::Index, count: DeductionType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Deduction Type'
          get "/:id" do
            datum = DeductionType.find(params[:id])
            present datum, with: Entities::V1::Payroll::DeductionType::DeductionTypeInfo
          end

          desc 'Add Deduction type'
          # deduction_code  (string)
          # mandatory       (boolean)
          # deduction       (string)
          # deduction_base_calculations (string) values are { None WitholdingTax PAGIBIG GSISRetirement PhilHealth }
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = DeductionTypeHandler.new(params,current_user).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::DeductionType::DeductionTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Deduction type'
          # id              (integer)
          # deduction_code  (string)
          # mandatory       (boolean)
          # deduction       (string)
          # deduction_base_calculations (string) values are { None WitholdingTax PAGIBIG GSISRetirement PhilHealth }
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DeductionTypeHandler.new(params,current_user).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::DeductionType::DeductionTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Deduction type'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = DeductionTypeHandler.new(params,current_user).delete
            if handler.response[:success]
              searched = DeductionType.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::Payroll::DeductionType::Index, count: DeductionType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      
      end
    end
  end
end
