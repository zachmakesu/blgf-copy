module API
  module V1
    module Payroll
      class EmployeeBenefitsManager < Grape::API
        include Grape::Kaminari
        resource :employee_benefits_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
  
          desc 'List Users'  #this should be SG below 23 and SG above
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.includes([position_detail: :service]).by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc 'Import Employee Benefits CSV.'
          # employee_benefits      (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:employee_benefits],current_user, "employee_benefits").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'List all Users'  #this should be SG below 23 and SG above
          get "/all" do
            searched = User.includes([position_detail: :service]).by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc 'Employee current set benefits'
          # employee_id        (string)
          get "/:employee_id/current_set_benefits" do
            employee = User.includes([set_benefits: :benefit]).find_by!(employee_id: params[:employee_id])
            benefits = { data: employee.set_benefits.includes(:benefit).order(id: :asc) }
            present benefits, with: Entities::V1::Payroll::EmployeeBenefitsManager::Index
          end

          desc 'Employee update current set benefit'
          # set_benefit[amount]             (float)
          # set_benefit[overwrite]          (boolean) default false
          put "/:employee_id/current_set_benefits/:id/update" do
            handler = EmployeeBenefitsHandler.new(params).update_set_benefit
            if handler.response[:success]
              benefits = { data: handler.response[:details].set_benefits.includes(:benefit).order(id: :asc) }
              present benefits, with: Entities::V1::Payroll::EmployeeBenefitsManager::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
