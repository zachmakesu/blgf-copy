module API
  module V1
    module Payroll
      class EmployeeDailyTimeRecord < Grape::API
        include Grape::Kaminari
        resource :employee_dtr do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end

          desc "Current import_module processed"
          get "/upload_employee_dtr" do
            background_process = { data: CsvFile.not_finished }
            present background_process, with: Entities::V1::Payroll::CsvFile::Index  
          end

          desc "Upload Employee Dtr"
          params do
            requires :employee_attendances, type: File, desc: 'Employee Attendances CSV file'
          end
          post "/upload_employee_dtr" do
            handler = EmployeeDtrCsvHandler.new(params, current_user).import_attendances
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "List of Users for Employee DTR(Attendance) Daily"
          paginate per_page: 10, offset: 0
          # date (string) format(yyyy-mm-dd)
          # params on pagination ?page=1&per_page=10&search=text
          get do
            validate_date_format!(params[:date]) if params[:date].present?
            
            searched = User.includes(position_detail:[:service]).by_roles(User::Role::ALL.values).except_self(current_user).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::Payroll::MyDtr::UserDtr, count: User.by_roles(User::Role::ALL.values).except_self(current_user).count, searched: searched.count, date: params[:date]
          end

          desc "List all Users for Employee DTR(Attendance) Daily"
          # date (string) format(yyyy-mm-dd)
          get "/all" do
            validate_date_format!(params[:date]) if params[:date].present?
            searched = User.includes(position_detail:[:service]).by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::Payroll::MyDtr::UserDtr, count: User.by_roles(User::Role::ALL.values).except_self(current_user).count, searched: searched.count, date: params[:date]
          end

          desc 'Specific Employee DTR(Attendance) Monthly' 
          get "/:employee_id" do
            #month   (integer) format(1 - 12)
            #year    (integer) format(yyyy) ex. 2016
            employee = User.find_by(employee_id: params[:employee_id])
            if employee
              validate_month_format!(params[:month]) if params[:month].present?
              validate_year_format!(params[:year]) if params[:year].present?

              attendances = {}

              late_h = 0
              late_m = 0
              late_s = 0

              ut_h = 0
              ut_m = 0
              ut_s = 0

              dates_absent = []
              total_ut_late_days = 0

              date = if params[:year].present? && params[:month].present?
                    "#{params[:year]}-#{params[:month]}-01".to_date
                   else
                    Date.today
                   end
              total_number_of_working_days = (date.at_beginning_of_month..date.at_end_of_month).count {|day| !day.saturday? && !day.sunday? }

              dates = AttendanceScheme.dates(params[:year], params[:month])
              dates.each_with_index { |date, index|
                attendances[date] = employee.attendances(date).present? ? employee.attendances(date) : nil

                attendance = employee.attendances(date).present? ? employee.attendances(date).last : nil

                dates_absent << date.to_date.day if attendances[date].blank? && date.to_time.is_weekday? && date.to_time.is_weekday?

                total_ut_late_days += 1 if attendance.present? && (attendance.late != "00:00:00" || attendance.ut != "00:00:00")

                late = attendance.present? ? attendance.late : "00:00:00"
                ut = attendance.present? ? attendance.ut : "00:00:00"
                
                t = late.split(":")
                late_h += t[0].to_i
                late_m += t[1].to_i
                late_s += t[2].to_i

                t = ut.split(":")
                ut_h += t[0].to_i
                ut_m += t[1].to_i
                ut_s += t[2].to_i
              }

              seconds = Time.now.minus_with_coercion((late_h.hours + late_m.minutes + late_s.seconds).ago).round
              mm, ss = seconds.divmod(60)
              hh, mm = mm.divmod(60)
              dd, hh = hh.divmod(24)

              total_late = "%d:%d:%d:%d" % [dd, hh, mm, ss]

              seconds = Time.now.minus_with_coercion((ut_h.hours + ut_m.minutes + ut_s.seconds).ago).round
              mm, ss = seconds.divmod(60)
              hh, mm = mm.divmod(60)
              dd, hh = hh.divmod(24)

              total_ut = "%d:%d:%d:%d" % [dd, hh, mm, ss]

              total_days_absent = dates_absent.count
              dates_absent = dates_absent.join(" ")
              
              {data: { attendance: attendances, total_number_of_working_days: total_number_of_working_days, total_days_absent: total_days_absent, dates_absent: dates_absent, total_late: total_late, total_ut: total_ut, total_ut_late_days: total_ut_late_days} }
              
            else
              error!({messages: "Invalid employee_id"},400)
            end

          end

          desc "GET Employee DTR(Attendance) Specefic Date"
          get "/:employee_id/:dtr_date" do
            validate_date_format!(params[:dtr_date])
            handler = EmployeeDtrHandler.new(params).get_dtr
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::MyDtr::SpecificDtr
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Employee DTR(Attendance) Specific Date"
          # in_am     (string) format("hh:mm:ss") ex. "12:23:44"
          # out_am    (string) format("hh:mm:ss") ex. "09:24:34"
          # in_pm     (string) format("hh:mm:ss") ex. "13:24:35"
          # out_pm    (string) format("hh:mm:ss") ex. "15:24:35"
          post "/:employee_id/:dtr_date/update" do
            validate_date_format!(params[:dtr_date])
            validate_time_format!(params[:in_am])
            validate_time_format!(params[:out_am])
            validate_time_format!(params[:in_pm])
            validate_time_format!(params[:out_pm])

            params[:processed_by] = current_user.id
            handler = EmployeeDtrHandler.new(params).update_dtr
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::MyDtr::SpecificDtr
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Employee DTR(Attendance) Specific Date"
          # in_am     (string) format("hh:mm:ss") ex. "12:23:44"
          # out_am    (string) format("hh:mm:ss") ex. "09:24:34"
          # in_pm     (string) format("hh:mm:ss") ex. "13:24:35"
          # out_pm    (string) format("hh:mm:ss") ex. "15:24:35"
          post "/:employee_id/:dtr_date" do
            validate_date_format!(params[:dtr_date])
            validate_time_format!(params[:in_am])
            validate_time_format!(params[:out_am])
            validate_time_format!(params[:in_pm])
            validate_time_format!(params[:out_pm])

            params[:processed_by] = current_user.id
            handler = EmployeeDtrHandler.new(params).add_dtr
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::MyDtr::SpecificDtr
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Employee DTR(Attendance) Specific Date"
          delete "/:employee_id/:dtr_date" do
            validate_date_format!(params[:dtr_date])

            params[:processed_by] = current_user.id
            handler = EmployeeDtrHandler.new(params).delete_dtr
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::MyDtr::SpecificDtr
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      end
    end
  end
end
