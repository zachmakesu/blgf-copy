module API
  module V1
    module Payroll
      class EmployeeDeductionsManager < Grape::API
        include Grape::Kaminari
        resource :employee_deductions_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
  
          desc 'List Users'  #this should be SG below 23 and SG above
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.includes([position_detail: :service]).by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc 'Import Employee Deductions CSV.'
          # employee_deductions      (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:employee_deductions],current_user, "employee_deductions").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Employees deductions to Excel'
          #month   (integer) format(1 - 12)
          #year    (integer) format(yyyy) ex. 2016
          get '/deductions_to_excel' do
            validate_month_format!(params[:month]) if params[:month].present?
            validate_year_format!(params[:year]) if params[:year].present?

            if params[:year].present? && params[:month].present?
              year = params[:year]
              month = '%02d' % params[:month]
            else
              date_today = Date.today - 1.month
              year = date_today.year.to_s
              month = '%02d' % date_today.month
            end
            
            users = User.includes(:monthly_salary_computations).by_roles(User::Role::ALL.values)
            users = { data: users }
            present users, with: Entities::V1::Payroll::EmployeeDeductionsManager::DeductionsToExcel, year: year, month: month
          end

          desc 'List all Users'  #this should be SG below 23 and SG above
          get "/all" do
            searched = User.by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc 'Set Default Value as global deduction for all Employees.'
          # amount             (float)
          # deduction_type_id  (integer)
          put "set_global_deduction" do
            handler = GlobalDeductionHandler.new(params).set_global_deduction
            if handler.response[:success]
              {messages: handler.response[:details]}
            else
              error!({messages: "Invalid Deduction Type ID"},400)
            end
          end

          desc 'Employee current set deductives'
          # employee_id        (string)
          get "/:employee_id/current_set_deductives" do
            employee = User.includes([deductives: :deduction_type]).find_by!(employee_id: params[:employee_id])
            present employee, with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductives
          end

          desc 'Employee update current set deductive'
          # deductive[amount]             (float)
          # deductive[overwrite]          (boolean) default false
          put "/:employee_id/current_set_deductives/:id/update" do
            handler = EmployeeDeductivesHandler.new(params).update_deductives
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductives
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          # desc 'List Employee deduction history base on date yyyy-mm'
          # # date        (string) values "yyyy-mm"
          # get "/:employee_id/history" do
          #   employee = User.find_by(employee_id: params[:employee_id])
          #   params[:date] = ensure_date(params[:date]) #api_helper
          #   if employee
          #     present employee, with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductionHistory, date: params[:date]
          #   else
          #     error!({messages: "Invalid employee_id"},400)
          #   end
          # end

          # desc 'Create employee deduction history base on date yyyy-mm'
          # # date        (string) values "yyyy-mm"
          # post "/:employee_id/history" do
          #   params[:date] = ensure_date(params[:date])
          #   handler = EmployeeDeductionsHandler.new(params).create_history
          #   if handler.response[:success]
          #     present handler.response[:details], with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductionHistory, date: params[:date]
          #   else
          #     error!({messages: handler.response[:details]},400)
          #   end
          # end

          # desc 'Delete employee deductions history'
          # # history_id  (integer)
          # delete "/:employee_id/history/:history_id/delete" do
          #   handler = EmployeeDeductionsHandler.new(params).delete_history
          #   if handler.response[:success]
          #     present handler.response[:details], with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductionHistory, date: params[:date]
          #   else
          #     error!({messages: handler.response[:details]},400)
          #   end
          # end

        end
      end
    end
  end
end
