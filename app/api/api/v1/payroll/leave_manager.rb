module API
  module V1
    module Payroll
      class LeaveManager < Grape::API
        include Grape::Kaminari
        resource :leave_management_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
        
          desc 'List all pending leave request'  #this should be SG below 23 and SG above
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = LeaveRequest.pending.search(params[:search])
            leave_requests = { data: paginate(searched) }
            present leave_requests, with: Entities::V1::Payroll::LeaveManager::Index, count: LeaveRequest.pending.count, searched: searched.count
          end

          desc 'Import Leave Credits.'
          # leave_credits                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:leave_credits],current_user, "leave_credits").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Calculate Leave Credits of all Employees for a specific past month.'
          #month   (integer) format(1 - 12)
          #year    (integer) format(yyyy) ex. 2016

          post "/calculate_leave_credits" do
            authorize!(current_user, User::Role::ADMIN )
            validate_month_format!(params[:month]) if params[:month].present?
            validate_year_format!(params[:year]) if params[:year].present?
            
            handler = CalculateLeaveCreditsHandler.new(params,current_user).calculate_leave_credits
            if handler.response[:success]
              { messages: handler.response[:details] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Leave Request'
          get "/:id" do
            leave_request = LeaveRequest.pending.find(params[:id])
            present leave_request, with: Entities::V1::MyRequest::LeaveRequest::LeaveRequestInfo
          end

          desc 'Update employee Leave Request (approved or denied) with remarks'
          # id         (integer)
          # status     (string) values (approved, denied)
          # remarks    (string)
          post "/:id/process" do
            params[:processor_employee_id] = current_user.id
            handler = Leave::ProcessLeaveHandler.new(params).process_update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::LeaveManager::LeaveRequestInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Get user's leave credits"
          get ":employee_id/leave_credits" do
            employee = User.not_deleted.find_by(employee_id: params[:employee_id])
            if employee
              present employee, with: Entities::V1::MyRequest::LeaveRequest::LeaveCredits
            else
              error!({messages: "Invalid employee_id"},400)
            end
          end

          desc 'Add or Subtract leave credits (Sick or Vacation or CTO leave credits)'
          # deductible      (boolean) true if subtract, false if add
          # credit_type     (string) values( sick_leaves, vacation_leaves, cto_leaves)
          # credits         (float)
          post ":employee_id/add_or_sub_credits" do
            employee = User.not_deleted.find_by!(employee_id: params[:employee_id])
            params[:created_by] = current_user.id
            handler = Leave::LeaveCreditsHandler.new(params).add_or_remove_leave_credits
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::MyRequest::LeaveRequest::LeaveCredits
            else
              error!({messages: handler.response[:details]},400)
            end
          end


        end
      end
    end
  end
end
