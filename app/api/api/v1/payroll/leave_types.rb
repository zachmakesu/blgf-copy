module API
  module V1
    module Payroll
      class LeaveTypes < Grape::API
        include Grape::Kaminari
        resource :leave_types do
          desc 'List Leave Types'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = LeaveType.no_specific_leaves.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::LeaveType::AllLeaveTypes, count: LeaveType.no_specific_leaves.count, searched: searched.count
          end

          desc 'List all Leave Types'
          get "/all" do
            searched = LeaveType.no_specific_leaves.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::LeaveType::AllLeaveTypes, count: LeaveType.no_specific_leaves.count, searched: searched.count
          end

          desc 'Specific leave_types for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = LeaveType.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::LeaveType::AllLeaveTypes, count: LeaveType.no_specific_leaves.count, searched: searched.count
          end

          desc 'Bulk Delete leave_types'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = LeaveTypeHandler.new(params, true).bulk_delete
            if handler.response[:success]
              searched = LeaveType.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::Payroll::LeaveType::AllLeaveTypes, count: LeaveType.no_specific_leaves.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific leave_type'
          get "/:id" do
            datum = LeaveType.find(params[:id])
            present datum, with: Entities::V1::Payroll::LeaveType::LeaveTypeInfo
          end

          desc 'Add leave_type'
          # leave_code  (string)
          # leave_type  (string)
          # no_of_days  (integer)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = LeaveTypeHandler.new(params, true).create_leave_type
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::LeaveType::LeaveTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update leave_type'
          # id          (integer)
          # leave_code  (string)
          # leave_type  (string)
          # no_of_days  (integer)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = LeaveTypeHandler.new(params, true).update_leave_type
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::LeaveType::LeaveTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete leave_type'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = LeaveTypeHandler.new(params, true).delete_leave_type
            if handler.response[:success]
              searched = LeaveType.no_specific_leaves.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::Payroll::LeaveType::AllLeaveTypes, count: LeaveType.no_specific_leaves.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end

        # resource :specific_leaves do
        #   before do
        #     authorize!(current_user, User::Role::ADMIN )
        #   end
          
        #   desc 'List Specific Leave'
        #   get do
        #     present LeaveType, with: Entities::V1::Payroll::LeaveType::AllSpecificLeaves, leave_types: LeaveType
        #   end

        #   desc 'Add Specific Leave'
        #   post do
        #     handler = LeaveTypeHandler.new(params, false).create_specific_leave
        #     if handler.response[:success]
        #       present handler.response[:details], with: Entities::V1::Payroll::LeaveType::SpecificLeaveInfo
        #     else
        #       error!({messages: handler.response[:details]},400)
        #     end
        #   end

        #   desc 'Update Specific Leave'
        #   post "/:id/update" do
        #     handler = LeaveTypeHandler.new(params, false).update_specific_leave
        #     if handler.response[:success]
        #       present handler.response[:details], with: Entities::V1::Payroll::LeaveType::SpecificLeaveInfo
        #     else
        #       error!({messages: handler.response[:details]},400)
        #     end
        #   end

        #   desc 'Delete Specific Leave'
        #   delete "/:id/delete" do
        #     handler = LeaveTypeHandler.new(params, false).delete_specific_leave
        #     if handler.response[:success]
        #       present LeaveType, with: Entities::V1::Payroll::LeaveType::AllSpecificLeaves, leave_types: LeaveType.no_specific_leaves.order(leave_code: :asc)
        #     else
        #       error!({messages: handler.response[:details]},400)
        #     end
        #   end
        # end

      end
    end
  end
end
