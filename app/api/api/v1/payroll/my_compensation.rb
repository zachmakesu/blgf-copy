module API
  module V1
    module Payroll
      class MyCompensation < Grape::API
        include Grape::Kaminari
        resource :my_compensation do
          before do
            authorize!(current_user, User::Role::ALL )
          end
  
          desc 'My Compensation(Payslip) per month(excluding current month onwards)' 
          #month   (integer) format(1 - 12)
          #year    (integer) format(yyyy) ex. 2016
          get do
            validate_month_format!(params[:month]) if params[:month].present? 
            validate_year_format!(params[:year]) if params[:year].present? 

            date = if params[:year].blank? || params[:month].blank?
              Date.today.beginning_of_month - 1.days
            else
              "#{params[:year]}-#{params[:month]}-01".to_date
            end
            validate_compensation_date!(date)
            compensation = MonthlySalaryComputation.includes(:user).included_in_dtr.find_by(user_id: current_user.id, this_month: date.to_s[0..6])
            if compensation
              present compensation, with: Entities::V1::Payroll::PayrollList::Payslip
            else
              present current_user, with: Entities::V1::Payroll::PayrollList::DummyPayslip, this_month: date.to_s[0..6]
            end
          end

          desc 'My Compensation(Payslip) History' 
          # params on pagination ?page=1&per_page=10
          # search ex. "2017-05"
          get "/history" do
            payslip_history = current_user.monthly_salary_computations
            searched = payslip_history.by_month(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::PayrollList::PayslipDates, count: payslip_history.count, searched: searched.count
          end
        end
      end
    end
  end
end
