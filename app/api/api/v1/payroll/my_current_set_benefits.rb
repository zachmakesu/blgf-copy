module API
  module V1
    module Payroll
      class MyCurrentSetBenefits < Grape::API
        include Grape::Kaminari
        resource :my_current_set_benefits do
          before do
            authorize!(current_user, User::Role::ALL )
          end
          
          desc 'My current set benefits allowances' 
          get "/allowances" do
            benefits = { data: current_user.set_benefits.allowances.includes(:benefit).order(id: :asc) }
            present benefits, with: Entities::V1::Payroll::EmployeeBenefitsManager::Index
          end

          desc 'My current set benefits bonuses' 
          get "/bonuses" do
            benefits = { data: current_user.set_benefits.bonuses.includes(:benefit).order(id: :asc) }
            present benefits, with: Entities::V1::Payroll::EmployeeBenefitsManager::Index
          end

          desc 'My current set benefits additional_incomes' 
          get "/additional_incomes" do
            benefits = { data: current_user.set_benefits.additional_incomes.includes(:benefit).order(id: :asc) }
            present benefits, with: Entities::V1::Payroll::EmployeeBenefitsManager::Index
          end


        end
      end
    end
  end
end
