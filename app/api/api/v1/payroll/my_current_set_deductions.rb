module API
  module V1
    module Payroll
      class MyCurrentSetDeductions < Grape::API
        resource :my_current_set_deductions do
          before do
            authorize!(current_user, User::Role::ALL )
          end
          
          desc 'My current set deductions' 
          get do
            present current_user, with: Entities::V1::Payroll::EmployeeDeductionsManager::EmployeeDeductives
          end


        end
      end
    end
  end
end
