module API
  module V1
    module Payroll
      class MyDtr < Grape::API
        resource :my_dtr do
          before do
            authorize!(current_user, User::Role::ALL )
          end
          
          desc 'My DTR(Attendance) Monthly' 
          get do
            #month   (integer) format(1 - 12)
            #year    (integer) format(yyyy) ex. 2016
            validate_month_format!(params[:month]) if params[:month].present?
            validate_year_format!(params[:year]) if params[:year].present?

            attendances = {}

            late_h = 0
            late_m = 0
            late_s = 0

            ut_h = 0
            ut_m = 0
            ut_s = 0

            dates_absent = []
            total_ut_late_days = 0

            date = if params[:year].present? && params[:month].present?
                    "#{params[:year]}-#{params[:month]}-01".to_date
                   else
                    Date.today
                   end
            total_number_of_working_days = (date.at_beginning_of_month..date.at_end_of_month).count {|day| !day.saturday? && !day.sunday? }

            dates = AttendanceScheme.dates(params[:year], params[:month])
            dates.each_with_index { |date, index|
              attendances[date] = current_user.attendances(date).present? ? current_user.attendances(date) : nil

              attendance = current_user.attendances(date).present? ? current_user.attendances(date).last : nil

              dates_absent << date.to_date.day if attendances[date].blank? && date.to_time.is_weekday? && date.to_time.is_weekday?

              total_ut_late_days += 1 if attendance.present? && (attendance.late != "00:00:00" || attendance.ut != "00:00:00")

              late = attendance.present? ? attendance.late : "00:00:00"
              ut = attendance.present? ? attendance.ut : "00:00:00"
              
              t = late.split(":")
              late_h += t[0].to_i
              late_m += t[1].to_i
              late_s += t[2].to_i

              t = ut.split(":")
              ut_h += t[0].to_i
              ut_m += t[1].to_i
              ut_s += t[2].to_i
            }

            seconds = Time.now.minus_with_coercion((late_h.hours + late_m.minutes + late_s.seconds).ago).round
            mm, ss = seconds.divmod(60)
            hh, mm = mm.divmod(60)
            dd, hh = hh.divmod(24)

            total_late = "%d:%d:%d:%d" % [dd, hh, mm, ss]

            seconds = Time.now.minus_with_coercion((ut_h.hours + ut_m.minutes + ut_s.seconds).ago).round
            mm, ss = seconds.divmod(60)
            hh, mm = mm.divmod(60)
            dd, hh = hh.divmod(24)

            total_ut = "%d:%d:%d:%d" % [dd, hh, mm, ss]

            total_days_absent = dates_absent.count
            dates_absent = dates_absent.join(" ")
            
            {data: { attendance: attendances, total_number_of_working_days: total_number_of_working_days, total_days_absent: total_days_absent, dates_absent: dates_absent, total_late: total_late, total_ut: total_ut, total_ut_late_days: total_ut_late_days} }
            
          end


        end
      end
    end
  end
end
