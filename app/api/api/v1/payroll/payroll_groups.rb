module API
  module V1
    module Payroll
      class PayrollGroups < Grape::API
        include Grape::Kaminari
        resource :payroll_groups do

          desc 'List Payroll Groups'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = PayrollGroup.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::PayrollGroup::Index, count: PayrollGroup.count, searched: searched.count
          end

          desc 'List all Payroll Groups'
          get "/all" do
            searched = PayrollGroup.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::PayrollGroup::Index, count: PayrollGroup.count, searched: searched.count
          end

          desc 'Specific Payroll Groups for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = PayrollGroup.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::PayrollGroup::Index, count: PayrollGroup.count, searched: searched.count
          end

          desc 'Bulk Delete Payroll Groups'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PayrollGroupHandler.new(params,current_user).bulk_delete
            if handler.response[:success]
              searched = PayrollGroup.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::Payroll::PayrollGroup::Index, count: PayrollGroup.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Payroll Group'
          get "/:id" do
            datum = PayrollGroup.find(params[:id])
            present datum, with: Entities::V1::Payroll::PayrollGroup::PayrollGroupInfo
          end

          desc 'Add Payroll Group'
          # project_code_id     (integer)
          # payroll_group_code  (string)
          # payroll_group_name  (string)
          # payroll_group_order (integer)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PayrollGroupHandler.new(params,current_user).create_payroll_group
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::PayrollGroup::PayrollGroupInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Payroll Group'
          # id                  (integer)
          # project_code_id     (integer)
          # payroll_group_code  (string)
          # payroll_group_name  (string)
          # payroll_group_order (integer)

          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PayrollGroupHandler.new(params,current_user).update_payroll_group
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::PayrollGroup::PayrollGroupInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Payroll Group'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = PayrollGroupHandler.new(params,current_user).delete_payroll_group
            if handler.response[:success]
              searched = PayrollGroup.search(params[:search])
              groups = { data: paginate(searched) }
              present groups, with: Entities::V1::Payroll::PayrollGroup::Index, count: PayrollGroup.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
