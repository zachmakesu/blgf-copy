module API
  module V1
    module Payroll
      class PayrollLists < Grape::API
        include Grape::Kaminari
        resource :payroll_lists do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
  
          desc 'List all User with compensation summary per month (excluding current month onwards)' 
          # month   (integer) format(1 - 12)
          # year    (integer) format(yyyy) ex. 2016
          # page=1&per_page=10 #pagination params
          # search=text
          # paginated=false|true #if true pagination will work else not.
          # search=text
          # sort=asc|desc default "desc"
          # sort_by=last_name|first_name
          get do
            sort = params[:sort] == "asc"
            paginated = params[:paginated] == "true"

            validate_month_format!(params[:month]) if params[:month].present? 
            validate_year_format!(params[:year]) if params[:year].present? 

            date = if params[:year].blank? || params[:month].blank?
              Date.today.beginning_of_month - 1.days
            else
              "#{params[:year]}-#{params[:month]}-01".to_date
            end
            validate_compensation_date!(date)

            searched = MonthlySalaryComputation.includes(:user).included_in_dtr.with_in_this_date(date.to_s[0..6]).search(params[:search])
            data =  case params[:sort_by]
                    when "last_name"
                      sort ? searched.sort_by{ |u| u.user.last_name.downcase } : searched.sort_by{ |u| u.user.last_name.downcase }.reverse
                    when "first_name"
                      sort ? searched.sort_by{ |u| u.user.first_name.downcase } : searched.sort_by{ |u| u.user.first_name.downcase }.reverse
                    else
                      searched.sort_by{ |u| u.user.first_name.downcase }.reverse
                    end

            users = paginated ? { data: paginate(Kaminari.paginate_array(data)) } : { data: data }
            present users, with: Entities::V1::Payroll::PayrollList::Index, count: data.count, searched: searched.count
          end

          desc "Specific user's compensation (Payslip)"
          get "/:id" do
            compensation = MonthlySalaryComputation.includes(:user).included_in_dtr.find(params[:id])
            present compensation, with: Entities::V1::Payroll::PayrollList::Payslip
          end

          desc "Generate Payslip for all user With specific month"
          # month   (integer) format(1 - 12)
          # year    (integer) format(yyyy) ex. 2016

          post "/generate_paylsip" do
            validate_month_format!(params[:month]) if params[:month].present?
            validate_year_format!(params[:year]) if params[:year].present? 
            
            handler = GeneratePayslipHandler.new(params, current_user).generate
            if handler.response[:success]
              { messages: handler.response[:details] }
            else
              error!({messages: handler.response[:details]},400)
            end
            
          end


          desc "Delete Specific user's compensation (Payslip)"
          delete "/:id" do
            MonthlySalaryComputation.find(params[:id]).delete
            
            sort = params[:sort] == "asc"
            paginated = params[:paginated] == "true"

            validate_month_format!(params[:month]) if params[:month].present? 
            validate_year_format!(params[:year]) if params[:year].present? 

            date = if params[:year].blank? || params[:month].blank?
              Date.today.beginning_of_month - 1.days
            else
              "#{params[:year]}-#{params[:month]}-01".to_date
            end
            validate_compensation_date!(date)

            searched = MonthlySalaryComputation.includes(:user).included_in_dtr.with_in_this_date(date.to_s[0..6]).search(params[:search])
            data =  case params[:sort_by]
                    when "last_name"
                      sort ? searched.sort_by{ |u| u.user.last_name.downcase } : searched.sort_by{ |u| u.user.last_name.downcase }.reverse
                    when "first_name"
                      sort ? searched.sort_by{ |u| u.user.first_name.downcase } : searched.sort_by{ |u| u.user.first_name.downcase }.reverse
                    else
                      searched.sort_by{ |u| u.user.first_name.downcase }.reverse
                    end

            users = paginated ? { data: paginate(Kaminari.paginate_array(data)) } : { data: data }
            present users, with: Entities::V1::Payroll::PayrollList::Index, count: data.count, searched: searched.count
          end

        end
      end
    end
  end
end
