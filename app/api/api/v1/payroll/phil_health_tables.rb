module API
  module V1
    module Payroll
      class PhilHealthTables < Grape::API
        resource :phil_health_table do
          get do
            data = { data: PhilHealthTable.all }
            present  data, with: Entities::V1::Payroll::PhilHealth::Index
          end

          desc "Import csv PhilHealth Table"
          params do
            requires :phil_health_table, type: File, desc: 'PhilHealth Table CSV file'
          end

          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = PhilHealthTableHandler.new(params).import_csv_file
            if handler.response[:success]
              { messages: handler.response[:details] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end
          
        end
      end
    end
  end
end
