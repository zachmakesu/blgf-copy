module API
  module V1
    module Payroll
      class SalarySchedules < Grape::API
        include Grape::Kaminari
        resource :salary_schedules do
          
          desc "Get Salary schedule and grades"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = SalarySchedule.search(params[:search])
            salary_schedules = { data: paginate(searched)}
            present salary_schedules, with: Entities::V1::Payroll::SalarySchedule::Index, count: SalarySchedule.count, searched: searched.count
          end

          desc "Get All Salary schedule and grades"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/all" do
            searched = SalarySchedule.search(params[:search])
            salary_schedules = { data: searched }
            present salary_schedules, with: Entities::V1::Payroll::SalarySchedule::Index, count: SalarySchedule.count, searched: searched.count
          end

          desc "Import csv Salary Schedule"
          #salary_schedule (file)
          params do
            requires :salary_schedule, type: File, desc: 'Salary Schedule CSV file'
          end

          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = SalaryScheduleHandler.new(params).import_csv_file
            if handler.response[:success]
              { messages: handler.response[:details] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end
          
        end
      end
    end
  end
end
