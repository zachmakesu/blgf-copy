module API
  module V1
    module Payroll
      class TaxRanges < Grape::API
        include Grape::Kaminari
        resource :tax_ranges do    
          desc 'List tax ranges'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = TaxRange.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::TaxRange::Index, count: TaxRange.count, searched: searched.count
          end

          desc 'List all tax ranges'
          get "/all" do
            searched = TaxRange.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::TaxRange::Index, count: TaxRange.count, searched: searched.count
          end

          desc 'Specific tax ranges for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = TaxRange.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::TaxRange::Index, count: TaxRange.count, searched: searched.count
          end

          desc 'Bulk Delete tax ranges'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxRangeHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = TaxRange.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::Payroll::TaxRange::Index, count: TaxRange.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific tax range'
          get "/:id" do
            datum = TaxRange.find(params[:id])
            present datum, with: Entities::V1::Payroll::TaxRange::TaxRangeInfo
          end

          desc 'Add tax range'
          # tax_base        (float)
          # tax_deduction   (float)
          # factor          (float)
          # taxable_from    (float)
          # taxable_to      (float)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxRangeHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::TaxRange::TaxRangeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update tax range'
          # id              (integer)
          # tax_base        (float)
          # tax_deduction   (float)
          # factor          (float)
          # taxable_from    (float)
          # taxable_to      (float)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxRangeHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::TaxRange::TaxRangeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete tax range'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxRangeHandler.new(params).delete
            if handler.response[:success]
              searched = TaxRange.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::Payroll::TaxRange::Index, count: TaxRange.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      
      end
    end
  end
end
