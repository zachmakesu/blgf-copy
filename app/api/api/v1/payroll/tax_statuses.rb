module API
  module V1
    module Payroll
      class TaxStatuses < Grape::API
        include Grape::Kaminari
        resource :tax_statuses do
  
          desc 'List tax statuses'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = TaxStatus.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Payroll::TaxStatus::Index, count: TaxStatus.count, searched: searched.count
          end

          desc 'List all tax statuses'
          get "/all" do
            searched = TaxStatus.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Payroll::TaxStatus::Index, count: TaxStatus.count, searched: searched.count
          end

          desc 'Specific tax statuses for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = TaxStatus.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Payroll::TaxStatus::Index, count: TaxStatus.count, searched: searched.count
          end

          desc 'Bulk Delete tax statuses'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxStatusHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = TaxStatus.search(params[:search])
              data = { data: paginate(searched) }
              present data, with: Entities::V1::Payroll::TaxStatus::Index, count: TaxStatus.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific tax status'
          get "/:id" do
            datum = TaxStatus.find(params[:id])
            present datum, with: Entities::V1::Payroll::TaxStatus::TaxStatusInfo
          end

          desc 'Add tax status'
          # tax_status         (string)
          # exemption_amount   (float)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxStatusHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::TaxStatus::TaxStatusInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update tax status'
          # id                 (integer)
          # tax_status         (string)
          # exemption_amount   (float)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxStatusHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Payroll::TaxStatus::TaxStatusInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete tax status'
          # id          (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = TaxStatusHandler.new(params).delete
            if handler.response[:success]
              searched = TaxStatus.search(params[:search])
              types = { data: paginate(searched) }
              present types, with: Entities::V1::Payroll::TaxStatus::Index, count: TaxStatus.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      
      end
    end
  end
end
