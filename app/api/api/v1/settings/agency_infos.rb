module API
  module V1
    module Settings
      class AgencyInfos < Grape::API
        resource :agency_infos do
          before do
            @model_fields = [ 
            :agency_name, 
            :agency_code, 
            :region, 
            :tin_number, 
            :address,
            :zip_code,
            :telephone_number,
            :facsimile,
            :email,
            :website,
            :salary_schedule,
            :gsis_number,
            :gsis_employee_percent_share,
            :gsis_employer_percent_share,    
            :pagibig_number,
            :pagibig_employee_percent_share,
            :pagibig_employer_percent_share,
            :privident_employee_percent_share,
            :privident_employer_percent_share,
            :philhealth_employee_percent_share,
            :philhealth_employer_percent_share,
            :philhealth_percentage,
            :philhealth_number,
            :mission,
            :vision,
            :mandate,
            :bank_account_number
            ]
            @model = :agency_info
          end
    
          desc "Get Agency Info"
          get do
            present AgencyInfo.first, with: Entities::V1::Settings::AgencyInfo::Index
          end

          desc "Update partially Agency Info"
          # params required
          # agency_info[agency_name]
          # agency_info[agency_code]
          # agency_info[region]
          # agency_info[tin_number]
          # agency_info[address]
          # agency_info[zip_code]
          # agency_info[telephone_number]
          # agency_info[facsimile]
          # agency_info[email]
          # agency_info[website]
          # agency_info[salary_schedule]
          # agency_info[gsis_number]
          # agency_info[gsis_employee_percent_share]
          # agency_info[gsis_employer_percent_share]
          # agency_info[pagibig_number]
          # agency_info[pagibig_employee_percent_share]
          # agency_info[pagibig_employer_percent_share]
          # agency_info[privident_employee_percent_share]
          # agency_info[privident_employer_percent_share]
          # agency_info[philhealth_employee_percent_share]
          # agency_info[philhealth_employer_percent_share]
          # agency_info[philhealth_percentage]
          # agency_info[philhealth_number]
          # agency_info[mission]
          # agency_info[vision]
          # agency_info[mandate]
          # agency_info[bank_account_number]
          put do
            authorize!(current_user, User::Role::ADMIN )
            agency_info = AgencyInfo.first
            if agency_info.update permited_params(@model, @model_fields)
              message = "#{@current_user.capitalize_name} updated Agency Information."
              NotificationHandler.new(agency_info, message, "hr", "agency_infos").create
            end
            present AgencyInfo.first, with: Entities::V1::Settings::AgencyInfo::Index
          end

        end
      end
    end
  end
end
