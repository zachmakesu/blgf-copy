module API
  module V1
    module Settings
      class AreaManager < Grape::API
        resource :area_manager do

          desc 'Import Area CSV. Create or Update only'
          # areas                  (file) CSV
          post "/import" do
            # authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:areas],current_user, "areas").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
