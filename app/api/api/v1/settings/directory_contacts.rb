module API
  module V1
    module Settings
      class DirectoryContacts < Grape::API
        include Grape::Kaminari
        resource :directory_contacts do
          desc 'List directory_contacts'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = DirectoryContact.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Settings::DirectoryContact::Index, count: DirectoryContact.count, searched: searched.count
          end

          desc 'List all directory_contacts'
          get "/all" do
            searched = DirectoryContact.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Settings::DirectoryContact::Index, count: DirectoryContact.count, searched: searched.count
          end

          desc 'Bulk Delete directory_contact'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )

            BulkHandler.bulk_delete(params, "DirectoryContact")

            searched = DirectoryContact.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Settings::DirectoryContact::Index, count: DirectoryContact.count, searched: searched.count
          end

          desc 'Specific directory_contact'
          get "/:id" do
            datum = DirectoryContact.find(params[:id])
            present datum, with: Entities::V1::Settings::DirectoryContact::DirectoryContactInfo
          end

          desc 'Add directory_contact'
          #employee_id    (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            user = User.find_by(employee_id: params[:employee_id])
            if user
              datum = DirectoryContact.find_or_create_by(user_id: user.id)
              present datum, with: Entities::V1::Settings::DirectoryContact::DirectoryContactInfo
            else
              error!({messages: "Invalid employee_id"},400)
            end
          end

          desc 'Delete directory_contact'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            directory_contact = DirectoryContact.find(params[:id])
            directory_contact.destroy

            searched = DirectoryContact.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Settings::DirectoryContact::Index, count: DirectoryContact.count, searched: searched.count
          end

        end
      end

    end
  end
end
