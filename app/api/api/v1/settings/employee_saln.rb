module API
  module V1
    module Settings
      class EmployeeSaln < Grape::API
        include Grape::Kaminari
        resource :employee_saln do
          before do
            authorize!(current_user, User::Role::ALL )
          end

          desc 'Employee Saln record'
          get do
            present current_user.saln, with: Entities::V1::Treasurer::SalnRecord::Index
          end

          desc 'Update Saln basic and spouse information'
          # saln[statement_filing_status] (string) values (joint, separate, not_applicable)
          # saln[last_name]               (string)
          # saln[first_name]              (string)
          # saln[middle_initial]          (string)
          # saln[address]                 (string)
          # saln[position]                (string)
          # saln[office]                  (string)
          # saln[office_address]          (string)
          # saln[spouse_first_name]       (string)
          # saln[spouse_last_name]        (string)
          # saln[spouse_middle_initial]   (string)
          # saln[spouse_position]         (string)
          # saln[spouse_office]           (string)
          # saln[spouse_office_address]   (string)

          put "/basic" do
            validate_draft!(params[:saln])

            handler = EmployeeSalnHandler.new(current_user, params).update_basic
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end

          end

          desc 'Add Saln unmarried children'
          # saln_child[name]       (string)
          # saln_child[birthdate]  (string) value (yyyy-mm-dd)
          # saln_child[age]        (string)
          post "/children" do
            validate_draft!(params[:saln_child])
            validate_date_format!(params[:saln_child][:birthdate]) if params[:saln_child][:birthdate].present?

            handler = EmployeeSalnHandler.new(current_user, params).add_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln unmarried children'
          # id                     (int)
          # saln_child[name]       (string)
          # saln_child[birthdate]  (string) value (yyyy-mm-dd)
          # saln_child[age]        (string)
          put "/children/:id/update" do
            validate_draft!(params[:saln_child])
            validate_date_format!(params[:saln_child][:birthdate]) if params[:saln_child][:birthdate].present?

            handler = EmployeeSalnHandler.new(current_user, params).update_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln unmarried children'
          # id                     (int)
          delete "/children/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln real asset'
          # saln_asset[description]           (string)
          # saln_asset[kind]                  (string)
          # saln_asset[exact_location]        (string)
          # saln_asset[assesed_value]         (float)
          # saln_asset[current_market_value]  (float)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_mode]      (string)
          # saln_asset[acquisition_cost]      (float)
          post "/real_asset" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])

            params[:saln_asset].asset_type = "real"
            handler = EmployeeSalnHandler.new(current_user, params).add_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln real asset'
          # id                                (int)
          # saln_asset[description]           (string)
          # saln_asset[kind]                  (string)
          # saln_asset[exact_location]        (string)
          # saln_asset[assesed_value]         (float)
          # saln_asset[current_market_value]  (float)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_mode]      (string)
          # saln_asset[acquisition_cost]      (float)
          put "/real_asset/:id/update" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])

            params[:saln_asset].asset_type = "real"
            handler = EmployeeSalnHandler.new(current_user, params).update_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln real asset'
          # id                     (int)
          delete "/real_asset/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end


          desc 'Add Saln personal asset'
          # saln_asset[description]           (string)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_cost]      (float)
          post "/personal_asset" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])

            params[:saln_asset].asset_type = "personal"
            handler = EmployeeSalnHandler.new(current_user, params).add_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln personal asset'
          # id                                (int)
          # saln_asset[description]           (string)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_cost]      (float)
          put "/personal_asset/:id/update" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])

            params[:saln_asset].asset_type = "personal"
            handler = EmployeeSalnHandler.new(current_user, params).update_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln personal asset'
          # id                     (int)
          delete "/personal_asset/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln liability'
          # saln_liability[nature]               (string)
          # saln_liability[creditors_name]       (string)
          # saln_liability[outstanding_balance]  (float)
          post "/liabilities" do
            validate_draft!(params[:saln_liability])

            handler = EmployeeSalnHandler.new(current_user, params).add_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln liability'
          # id                     (int)
          # saln_liability[nature]               (string)
          # saln_liability[creditors_name]       (string)
          # saln_liability[outstanding_balance]  (float)
          put "/liabilities/:id/update" do
            validate_draft!(params[:saln_liability])

            handler = EmployeeSalnHandler.new(current_user, params).update_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln liability'
          # id                     (int)
          delete "/liabilities/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln business interests and financial connections'
          # saln_bi_and_fc[acquisition_date]            (string) value (yyyy-mm-dd)
          # saln_bi_and_fc[business_address]            (string)
          # saln_bi_and_fc[business_enterprise]         (string)
          # saln_bi_and_fc[business_financial_nature]   (string)
          post "/bi_and_fcs" do
            validate_draft!(params[:saln_bi_and_fc])
            validate_date_format!(params[:saln_bi_and_fc][:acquisition_date]) if params[:saln_bi_and_fc][:acquisition_date].present?

            handler = EmployeeSalnHandler.new(current_user, params).add_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln business interests and financial connections'
          # id                     (int)
          # saln_bi_and_fc[acquisition_date]            (string) value (yyyy-mm-dd)
          # saln_bi_and_fc[business_address]            (string)
          # saln_bi_and_fc[business_enterprise]         (string)
          # saln_bi_and_fc[business_financial_nature]   (string)
          put "/bi_and_fcs/:id/update" do
            validate_draft!(params[:saln_bi_and_fc])
            validate_date_format!(params[:saln_bi_and_fc][:acquisition_date]) if params[:saln_bi_and_fc][:acquisition_date].present?

            handler = EmployeeSalnHandler.new(current_user, params).update_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln business interests and financial connections'
          # id                     (int)
          delete "/bi_and_fcs/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln relatives in the government service'
          # saln_government_relative[name]           (string)
          # saln_government_relative[office_address] (string)
          # saln_government_relative[position]       (string)
          # saln_government_relative[relationship]   (string)
          post "/government_relatives" do
            validate_draft!(params[:saln_government_relative])

            handler = EmployeeSalnHandler.new(current_user, params).add_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln relatives in the government service'
          # id                     (int)
          # saln_government_relative[name]           (string)
          # saln_government_relative[office_address] (string)
          # saln_government_relative[position]       (string)
          # saln_government_relative[relationship]   (string)
          put "/government_relatives/:id/update" do
            validate_draft!(params[:saln_government_relative])

            handler = EmployeeSalnHandler.new(current_user, params).update_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln relatives in the government service'
          # id                     (int)
          delete "/government_relatives/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln signature and government issued id'
          # saln_issued_id[date_issued]   (string) value (yyyy-mm-dd)
          # saln_issued_id[id_no]         (string)
          # saln_issued_id[id_type]       (string) values (declarant, spouse)
  
          post "/issued_ids" do
            validate_draft!(params[:saln_issued_id])
            validate_date_format!(params[:saln_issued_id][:date_issued]) if params[:saln_issued_id][:date_issued].present?

            handler = EmployeeSalnHandler.new(current_user, params).add_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln signature and government issued id'
          # id                     (int)
          # saln_issued_id[date_issued]   (string) value (yyyy-mm-dd)
          # saln_issued_id[id_no]         (string)
          # saln_issued_id[id_type]       (string) values (declarant, spouse)
          put "/issued_ids/:id/update" do
            validate_draft!(params[:saln_issued_id])
            validate_date_format!(params[:saln_issued_id][:date_issued]) if params[:saln_issued_id][:date_issued].present?

            handler = EmployeeSalnHandler.new(current_user, params).update_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln signature and government issued id'
          # id                     (int)
          delete "/issued_ids/:id/delete" do
            handler = EmployeeSalnHandler.new(current_user, params).delete_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
