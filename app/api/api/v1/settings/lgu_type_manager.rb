module API
  module V1
    module Settings
      class LguTypeManager < Grape::API
        include Grape::Kaminari
        resource :lgu_types do
          desc 'List LGU types'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = LguType.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::Settings::LguType::Index, count: LguType.count, searched: searched.count
          end

          desc 'List All LGU types'
          get "/all" do
            searched = LguType.all.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::Settings::LguType::Index, count: LguType.count, searched: searched.count
          end

          desc 'Bulk Delete LGU types'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            handler = LguTypeHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = LguType.all.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::Settings::LguType::Index, count: LguType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific LGU type'
          # id                     (integer)
          get "/:id" do
            lgu_type = LguType.find(params[:id])
            present lgu_type, with: Entities::V1::Settings::LguType::LguTypeInfo
          end

          desc 'Admin - Add LGU type'
          # lgu_type[name]         (string)
          post do
            authorize!(current_user, User::Role::ADMIN)
            handler = LguTypeHandler.new(params).create
            if handler.response[:success]
              lgu_type = LguType.find(handler.response[:details].id)
              present lgu_type, with: Entities::V1::Settings::LguType::LguTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Update LGU type'
          # id                     (integer)
          # lgu_type[name]         (string)
          put "/:id/update" do
            authorize!(current_user, User::Role::ADMIN)
            handler = LguTypeHandler.new(params).update
            if handler.response[:success]
              lgu_type = LguType.find(handler.response[:details].id)
              present lgu_type, with: Entities::V1::Settings::LguType::LguTypeInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Delete LGU type'
          # id                     (integer)
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN)
            handler = LguTypeHandler.new(params).delete
            if handler.response[:success]
              searched = LguType.search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::Settings::LguType::Index, count: LguType.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
