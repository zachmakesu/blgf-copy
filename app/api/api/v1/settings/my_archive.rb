module API
  module V1
    module Settings
      class MyArchive < Grape::API
        include Grape::Kaminari
        resource :my_archive do
          before do
            authorize!(current_user, User::Role::ALL )
          end

          desc "List of my archive dates for PDS" 
          # params on pagination ?page=1&per_page=10&search=2017-06-22
          paginate per_page: 10, offset: 0
          get "/pds" do
            all_pds = current_user.archives.pds.distinct_date
            searched = current_user.archives.pds.search(params[:search])
            searched = searched.distinct_date
  
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_pds.count, searched: searched.count
          end

          desc "List of my archive PDS per date"
          # params on pagination ?page=1&per_page=10
          paginate per_page: 10, offset: 0
          get "/pds/:archive_date" do
            all_by_date = current_user.archives.pds.by_date(params[:archive_date])
            data = { data: paginate(all_by_date) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveInfo, count: all_by_date.count, searched: all_by_date.count
          end

          desc "List of my archive dates for SALN"
          # params on pagination ?page=1&per_page=10&search=2017-06-22
          paginate per_page: 10, offset: 0
          get "/saln" do
            all_saln = current_user.archives.saln.distinct_date
            searched = current_user.archives.saln.search(params[:search])
            searched = searched.distinct_date
            
            data = { data: paginate(Kaminari.paginate_array(searched)) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveDates, count: all_saln.count, searched: searched.count
          end

          desc "List of my archive SALN per date"
          # params on pagination ?page=1&per_page=10
          paginate per_page: 10, offset: 0
          get "/saln/:archive_date" do
            all_by_date = current_user.archives.saln.by_date(params[:archive_date])
            data = { data: paginate(all_by_date) }
            present data, with: Entities::V1::HrInformationDatabase::Archive::ArchiveInfo, count: all_by_date.count, searched: all_by_date.count
          end

        end
      end
    end
  end
end
