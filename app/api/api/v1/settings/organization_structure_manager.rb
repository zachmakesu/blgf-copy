module API
  module V1
    module Settings
      class OrganizationStructureManager < Grape::API
        include Grape::Kaminari
        resource :organizations do
          desc "List of Main Organization Stucture"
          get do
            searched = Organization.search(params[:search])
            organizations = { data: searched }
            present organizations, with: Entities::V1::Settings::Organization::Index, count: Organization.count, searched: searched.count
          end

          desc 'Import Executive Offices CSV. Create or Update only'
          # executive_offices                  (file) CSV
          post "/executive_offices/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:executive_offices],current_user, "executive_offices").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Import Services CSV. Create or Update only'
          # services                  (file) CSV
          post "/services/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:services],current_user, "services").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Import Divisions CSV. Create or Update only'
          # divisions                  (file) CSV
          post "/divisions/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:divisions],current_user, "divisions").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Import Sections CSV. Create or Update only'
          # sections                  (file) CSV
          post "/sections/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:sections],current_user, "sections").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end


          desc "List of Organization Level's children"
          # organization_id (integer)
          # params on pagination ?page=1&per_page=10&search=text
          get "/:organization_id/children" do
            organization = Organization.find(params[:organization_id])
            if organization
              children = { data: paginate(organization.organization_children.search(params[:search])) } 
              present children, with: Entities::V1::Settings::OrganizationChild::Index, count: organization.organization_children.count
            else
              error!({messages: "Invalid origanization_id"},400)
            end
          end

          desc "List of Organization Level's all children"
          # organization_id (integer)
          get "/:organization_id/children/all" do
            organization = Organization.find(params[:organization_id])
            if organization
              children = { data: organization.organization_children.search(params[:search]) } 
              present children, with: Entities::V1::Settings::OrganizationChild::Index, count: organization.organization_children.count
            else
              error!({messages: "Invalid origanization_id"},400)
            end
          end

          desc "Create Organization child"
          # organization_id (integer)
          # level_1_id      (integer)
          # level_2_id      (integer)
          # level_3_id      (integer)
          # employee_id     (string)
          # head_title      (string)
          # code            (string)
          # name            (string)
          post "/:organization_id/children" do
            authorize!(current_user, User::Role::ADMIN )
            handler = OrganizationChildHandler.new(params).create_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::OrganizationChild::Child
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update Organization child"
          # organization_id (integer)
          # id              (integer)
          # level_1_id      (integer)
          # level_2_id      (integer)
          # level_3_id      (integer)
          # employee_id     (string)
          # head_title      (string)
          # code            (string)
          # name            (string)
          post ":organization_id/children/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = OrganizationChildHandler.new(params).update_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::OrganizationChild::Child
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete Organization child"
          # organization_id (integer)
          # id              (integer)
          delete ":organization_id/children/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = OrganizationChildHandler.new(params).delete_child
            if handler.response[:success]
              organization = Organization.find(params[:organization_id])
              if organization
                children = { data: organization.organization_children.search(params[:search]) } 
                present children, with: Entities::V1::Settings::OrganizationChild::Index, count: organization.organization_children.count
              else
                error!({messages: "Invalid origanization_id"},400)
              end
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Add Organization child custodian"
          # organization_id (integer)
          # id              (integer)
          # employee_id     (string)
          post ":organization_id/children/:id/add_custodian" do
            authorize!(current_user, User::Role::ADMIN )
            handler = OrganizationChildHandler.new(params).add_custodian
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::OrganizationChild::Child
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Remove Organization child custodian"
          # organization_id (integer)
          # id              (integer)
          # employee_id     (string)
          delete ":organization_id/children/:id/remove_custodian" do
            authorize!(current_user, User::Role::ADMIN )
            handler = OrganizationChildHandler.new(params).remove_custodian
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::OrganizationChild::Child
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
