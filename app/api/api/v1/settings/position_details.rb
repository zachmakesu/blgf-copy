module API
  module V1
    module Settings
      class PositionDetails < Grape::API
        include Grape::Kaminari
        resource :position_details do
          rescue_from ArgumentError do |e|
            rack_response('{ "status": 422, "message": "Invalid values on option fields " }', 422)
          end

          desc 'List Users for position_details'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            authorize!(current_user, User::Role::ADMIN )
            searched = User.by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::Settings::UserAccount::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc 'List of job category for position_details'
          get "/job_categories" do
            { data: { job_categories: PositionDetail::JOB_CATEGORIES } }
          end

          desc 'Import Employee Position Details CSV.'
          # position_details                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:position_details],current_user, "position_details").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'List all Users for position_details'
          get "/all" do
            authorize!(current_user, User::Role::ADMIN )
            searched = User.by_roles(User::Role::ALL.values).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::Settings::UserAccount::Index, count: User.by_roles(User::Role::ALL.values).count, searched: searched.count
          end

          desc "Current User's Position Details"
          get "/current" do
            if current_user
              present current_user.position_detail, with: Entities::V1::Settings::PositionDetails::Index
            else
              error!({messages: "Invalid employee ID"},400)
            end
          end

          desc "Employee's Position Details"
          # employee_id           (string)
          get "/:employee_id" do
            authorize!(current_user, User::Role::ADMIN )
            employee = User.by_roles(User::Role::ALL.values).find_by(employee_id: params[:employee_id])
            if employee
              present employee.position_detail, with: Entities::V1::Settings::PositionDetails::Index
            else
              error!({messages: "Invalid employee ID"},400)
            end
          end
         
          desc "Update Specific employee's Position Details"
          # service_code_id       (integer)
          # first_day_gov         (string) format(yyyy-mm-dd)
          # first_day_agency      (string) format(yyyy-mm-dd)
          # mode_of_separation_id (integer)
          # separation_date       (string) format(yyyy-mm-dd)
          # appointment_status_id (integer)
          # job_category          (string)
          # executive_office_id   (integer)
          # service_id            (integer)
          # division_id           (integer)
          # section_id            (integer)
          # place_of_assignment   (string)
          # salary_effective_date (string) format(yyyy-mm-dd)
          # employment_basis      (string) option (full_time, part_time)
          # category_service      (string) option (career, non_career)
          # tax_status            (string) values( "Z", "S / ME", "ME1 / S1", "ME2 / S2", "ME3 / S3", "ME4 / S4" )
          # dependents_number     (integer)
          # personnel_action      (string)
          # payroll_group_id      (integer)
          # include_dtr           (boolean) values (true, false)
          # attendance_scheme_id  (integer)
          # health_insurance_exception (boolean) values (true, false)
          # include_in_payroll    (boolean) values (true, false)
          # hazard_pay_factor     (float)          
          # plantilla_id          (integer)
          # position_date         (string) format(yyyy-mm-dd)
          # date_increment        (string) format(yyyy-mm-dd) 
          # head_of_the_agency    (boolean) values (true, false)
          # step_number           (integer)
          post "/:employee_id/update" do
            authorize!(current_user, User::Role::ADMIN )
            validate_date_format!(params[:first_day_gov]) if params[:first_day_gov].present?
            validate_date_format!(params[:first_day_agency]) if params[:first_day_agency].present?
            validate_date_format!(params[:separation_date]) if params[:separation_date].present?
            validate_date_format!(params[:salary_effective_date]) if params[:salary_effective_date].present?
            validate_date_format!(params[:position_date]) if params[:position_date].present?
            validate_date_format!(params[:date_increment]) if params[:date_increment].present?

            handler = HrUpdate::PositionDetailHandler.new(params, User::Role::ALL.values).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::PositionDetails::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
