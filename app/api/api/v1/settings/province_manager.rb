module API
  module V1
    module Settings
      class ProvinceManager < Grape::API
        include Grape::Kaminari

        resource :provinces do

          desc 'List Provinces'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Province.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::Settings::Province::Index, count: Province.count, searched: searched.count
          end

          desc 'List All Provinces'
          get "/all" do
            searched = Province.all.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::Settings::Province::Index, count: Province.count, searched: searched.count
          end

          desc 'Admin - Bulk Delete Provinces'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN)
            handler = ProvinceHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = Province.all.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::Settings::Province::Index, count: Province.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Specific Province'
          # id                     (integer)
          get "/:id" do
            province = Province.find(params[:id])
            present province, with: Entities::V1::Settings::Province::ProvinceInfo
          end

          desc 'Admin - Add Province'
          # province[name]         (string)
          # province[geocode]      (string)
          post do
            authorize!(current_user, User::Role::ADMIN)
            handler = ProvinceHandler.new(params).create
            if handler.response[:success]
              province = Province.find(handler.response[:details].id)
              present province, with: Entities::V1::Settings::Province::ProvinceInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Update Province'
          # id                   (integer)
          # province[name]       (string)
          # province[geocode]      (string)
          put "/:id/update" do
            authorize!(current_user, User::Role::ADMIN)
            handler = ProvinceHandler.new(params).update
            if handler.response[:success]
              province = Province.find(handler.response[:details].id)
              present province, with: Entities::V1::Settings::Province::ProvinceInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Delete Province'
          # id                     (integer)
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN)
            handler = ProvinceHandler.new(params).delete
            if handler.response[:success]
              searched = Province.search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::Settings::Province::Index, count: Province.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
