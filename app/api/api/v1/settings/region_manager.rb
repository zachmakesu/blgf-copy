module API
  module V1
    module Settings
      class RegionManager < Grape::API
        include Grape::Kaminari

        resource :regions do
          desc 'List Regions'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Region.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::Settings::Region::Index, count: Region.count, searched: searched.count
          end

          desc 'List All Regions'
          get "/all" do
            searched = Region.all.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::Settings::Region::Index, count: Region.count, searched: searched.count
          end

          desc 'Admin - Bulk Delete Regions'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN)
            handler = RegionHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = Region.all.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::Settings::Region::Index, count: Region.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific Region'
          # id                     (integer)
          get "/:id" do
            region = Region.find(params[:id])
            present region, with: Entities::V1::Settings::Region::RegionInfo
          end

          desc 'Admin - Add Region'
          # region[name]         (string)
          post do
            authorize!(current_user, User::Role::ADMIN)
            handler = RegionHandler.new(params).create
            if handler.response[:success]
              region = Region.find(handler.response[:details].id)
              present region, with: Entities::V1::Settings::Region::RegionInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Update Region'
          # id                   (integer)
          # region[name]         (string)
          put "/:id/update" do
            authorize!(current_user, User::Role::ADMIN)
            handler = RegionHandler.new(params).update
            if handler.response[:success]
              region = Region.find(handler.response[:details].id)
              present region, with: Entities::V1::Settings::Region::RegionInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Admin - Delete Region'
          # id                     (integer)
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN)
            handler = RegionHandler.new(params).delete
            if handler.response[:success]
              all_data = { data: paginate(Region.search(params[:search]).order(name: :asc)) }
              present all_data, with: Entities::V1::Settings::Region::Index, count: Region.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
