module API
  module V1
    module Settings
      class UserManager < Grape::API
        include Grape::Kaminari
        resource :user_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end

          desc 'List Users for User Manager'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.except_self(current_user).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::Settings::UserAccount::Index, current_user: current_user, count: User.except_self(current_user).count, searched: searched.count
          end

          desc 'List all Users for User Manager'
          get "/all" do
            searched = User.except_self(current_user).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::Settings::UserAccount::Index, current_user: current_user, count: User.except_self(current_user).count, searched: searched.count
          end

          desc 'Import Users with Personal Data. Create or Update only.'
          # users                  (file) CSV
          post "/import" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ImportCsvHandler.new(params[:users],current_user, "users").import_csv
            if handler.response[:success]
              { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Bulk Delete Users'
          # employee_ids=C0004^C0005^C0004
          delete "/bulk/:employee_ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = UserManagerHandler.new(params, current_user).bulk_delete_users
            if handler.response[:success]
              searched = User.except_self(current_user).search(params[:search])
              users = { data: paginate(searched) }
              present users, with: Entities::V1::Settings::UserAccount::Index, current_user: current_user, count: User.except_self(current_user).count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Get a user for User Manager'
          get "/:employee_id" do
            user = User.not_deleted.find_by!(employee_id: params[:employee_id])
            present user , with: Entities::V1::Settings::UserAccount::UserInfo, current_user: current_user
          end

          desc "Add A User"
          # email               (string)
          # first_name          (string)
          # middle_name         (string)
          # last_name           (string
          # gender              (string)  values("male", "female")
          # role                (string)  values("employee", "hr", "treasurer")
          # office_designation  (string)  values("central", "regional")
          # biometric_id        (string)
          # default password value is "password123"
          # image               (file)    extensions(jpg|jpeg|png)
          # signature_image     (file)    extensions(jpg|jpeg|png)

          # add to params if user role is treasurer
          # treasurer_subrole   (string)
          # lgu_code            (string)
          post do
            params[:position] = 0
            params[:default_type] = 0
            handler = UserManagerHandler.new(params, current_user).create_user
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::UserAccount::UserInfo, current_user: current_user
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update A User"
          # employee_id         (string)
          # password            (string)
          # email               (string)
          # first_name          (string)
          # middle_name         (string)
          # last_name           (string)
          # gender              (string)  values("gender_not_specified", "male", "female")
          # role                (string)  values("employee", "hr", "treasurer")
          # office_designation  (string)  values("central", "regional")
          # biometric_id        (string)
          # image               (file)    extensions(jpg|jpeg|png)
          # signature_image     (file)    extensions(jpg|jpeg|png)

          # add to params if user role is treasurer
          # treasurer_subrole   (string)
          # lgu_code            (string)
          post "/:employee_id/update" do
            params[:position] = 0
            params[:default_type] = 0
            handler = UserManagerHandler.new(params, current_user).update_user
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::UserAccount::UserInfo, current_user: current_user
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Unblock User'
          post "/:employee_id/unblock" do
            authorize!(current_user, User::Role::ADMIN )
            user = User.find_by!(employee_id: params[:employee_id])
            user.unlock_access!
            {messages: "User Unblock!"}
          end

          desc 'Delete User'
          delete "/:employee_id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = UserManagerHandler.new(params, current_user).delete_user
            if handler.response[:success]
              searched = User.except_self(current_user).search(params[:search])
              users = { data: paginate(searched) }
              present users, with: Entities::V1::Settings::UserAccount::Index, current_user: current_user, count: User.except_self(current_user).count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end
    end
  end
end
