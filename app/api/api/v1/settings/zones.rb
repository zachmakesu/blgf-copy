module API
  module V1
    module Settings
      class Zones < Grape::API
        include Grape::Kaminari
        resource :zones do
          desc 'List zones'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = Zone.search(params[:search])
            data = { data: paginate(searched) }
            present data, with: Entities::V1::Settings::Zone::Index, count: Zone.count, searched: searched.count
          end

          desc 'List all zones'
          get "/all" do
            searched = Zone.search(params[:search])
            data = { data: searched }
            present data, with: Entities::V1::Settings::Zone::Index, count: Zone.count, searched: searched.count
          end

          desc 'Specific zones for bulk edit'
          get "bulk/:ids" do
            ids = params[:ids].split('^')
            ids = ids.uniq.map {|i| i.to_i }.uniq
            searched = Zone.where(id: ids)
            data = { data: searched }
            present data, with: Entities::V1::Settings::Zone::Index, count: Zone.count, searched: searched.count
          end

          desc 'Bulk Delete zone'
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ZoneHandler.new(params).bulk_delete
            if handler.response[:success]
              searched = Zone.search(params[:search])
              data = { data: searched }
              present data, with: Entities::V1::Settings::Zone::Index, count: Zone.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Specific zone'
          get "/:id" do
            datum = Zone.find(params[:id])
            present datum, with: Entities::V1::Settings::Zone::ZoneInfo
          end

          desc 'Add zone'
          # zone_code    (string)
          # description  (text)
          # server_name  (string)
          # username     (string)
          # password     (string)
          # database     (string)
          post do
            authorize!(current_user, User::Role::ADMIN )
            handler = ZoneHandler.new(params).create
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::Zone::ZoneInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update zone'
          # id           (integer)
          # zone_code    (string)
          # description  (text)
          # server_name  (string)
          # username     (string)
          # password     (string)
          # database     (string)
          post "/:id/update" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ZoneHandler.new(params).update
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Settings::Zone::ZoneInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete zone'
          # id                  (integer)
          paginate per_page: 10, offset: 0
          delete "/:id/delete" do
            authorize!(current_user, User::Role::ADMIN )
            handler = ZoneHandler.new(params).delete
            if handler.response[:success]
              searched = Zone.search(params[:search])
              statuses = { data: paginate(searched) }
              present statuses, with: Entities::V1::Settings::Zone::Index, count: Zone.count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
