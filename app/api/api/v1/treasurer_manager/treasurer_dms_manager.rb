module API
  module V1
    module TreasurerManager
      class TreasurerDMSManager < Grape::API
        include Grape::Kaminari
        resource :treasurer_dms_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end
          
          desc "List of Users for treasurers DMS"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.by_roles(User::Role::TREASURER.values).except_self(current_user).includes(:treasurer_assignment, :pdf_files, position_detail: [:service]).search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerWithPdf, count: User.by_roles(User::Role::TREASURER.values).except_self(current_user).count, searched: searched.count, dms_titles: DocketManagementFile.all
          end

          desc "List all Users for treasurers DMS"   
          get "/all" do
            searched = User.by_roles(User::Role::TREASURER.values).except_self(current_user).includes(:treasurer_assignment, :pdf_files, position_detail: [:service]).search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerWithPdf, count: User.by_roles(User::Role::TREASURER.values).except_self(current_user).count, searched: searched.count, dms_titles: DocketManagementFile.all
          end

          desc "DMS List treasurer's pdf files"
          paginate per_page: 10, offset: 0
          # employee_id (string)
          # params on pagination ?page=1&per_page=10&search=text
          get "/:employee_id" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            searched = employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
            pdf_files = { data: searched }
            present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).count, searched: searched.count
          end

          desc "DMS Bulk Delete Specific treasurer's pdf file"
          # ids=1^2^3                  (integer)
          delete ":employee_id/bulk/:ids" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = PdfFileHandler.new(employee, params, current_user).bulk_delete
            if handler.response[:success]
              searched = employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
              pdf_files = { data: searched }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific treasurer's pdf files"
          get "/:employee_id/:id" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            datum = { data: employee.pdf_files.includes(:docket_management_file).find(params[:id]) }
            present datum, with: Entities::V1::Treasurer::TreasurerPdfFile::PdfFileInfo
          end

          desc "DMS Add treasurer's pdf file"
          # employee_id (string)
          # pdf         (file)
          # docket_management_file_id (integer)
          post "/:employee_id" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = PdfFileHandler.new(employee,params, current_user).create_file
            if handler.response[:success]
              pdf_files = { data: [ handler.response[:details] ] }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index
            else
              error!({messages: handler.response[:details]},400)
            end
            
          end

          desc "DMS Update treasurer's pdf file"
          # employee_id (string)
          # id              (integer)
          # pdf         (file)
          # docket_management_file_id (integer)
          post "/:employee_id/:id/update" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = PdfFileHandler.new(employee,params, current_user).update_file
            if handler.response[:success]
              pdf_files = { data: [ handler.response[:details] ] }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "DMS Delete treasurer's pdf file"
          # employee_id (string)
          # id  (integer)
          delete "/:employee_id/:id/delete" do
            employee = User.by_roles(User::Role::TREASURER.values).except_self(current_user).find_by!(employee_id: params[:employee_id])
            handler = PdfFileHandler.new(employee,params, current_user).delete_file
            if handler.response[:success]
              searched = employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
              pdf_files = { data: searched }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: employee.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      end

    end
  end
end
