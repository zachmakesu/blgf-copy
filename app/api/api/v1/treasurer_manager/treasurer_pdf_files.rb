module API
  module V1
    module TreasurerManager
      class TreasurerPdfFiles < Grape::API
        include Grape::Kaminari
        resource :treasurer_pdf_files do
          before do
            authorize!(current_user, User::Role::TREASURER )
          end
          
          desc "List treasurer's pdf files"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = current_user.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
            pdf_files = { data: searched }
            present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: current_user.pdf_files.where.not(docket_management_file_id: nil).search("").count, searched: searched.count
          end

          desc "Bulk Delete treasurer's pdf file"
          # ids=1^2^3                  (integer)
          delete "/bulk/:ids" do
            handler = PdfFileHandler.new(current_user, params).bulk_delete
            if handler.response[:success]
              searched = current_user.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
              pdf_files = { data: searched }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: current_user.pdf_files.where.not(docket_management_file_id: nil).search("").count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Specific treasurer's pdf files"
          get "/:id" do
            datum = { data: current_user.pdf_files.includes(:docket_management_file).find(params[:id]) }
            present datum, with: Entities::V1::Treasurer::TreasurerPdfFile::PdfFileInfo
          end

          desc "Add treasurer's pdf file"
          # pdf  (file)
          # docket_management_file_id (integer)
          post do
            handler = PdfFileHandler.new(current_user,params).create_file
            if handler.response[:success]
              pdf_files = { data: [ handler.response[:details] ] }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::PdfFileInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Update treasurer's pdf file"
          # pdf             (file)
          # docket_management_file_id (integer)
          post ":id/update" do
            handler = PdfFileHandler.new(current_user,params).update_file
            if handler.response[:success]
              pdf_files = { data: [ handler.response[:details] ] }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::PdfFileInfo
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc "Delete treasurer's pdf file"
          # id  (integer)
          delete ":id/delete" do
            handler = PdfFileHandler.new(current_user,params).delete_file
            if handler.response[:success]
              searched = current_user.pdf_files.includes(:docket_management_file).where.not(docket_management_file_id: nil).search(params[:search])
              pdf_files = { data: searched }
              present pdf_files, with: Entities::V1::Treasurer::TreasurerPdfFile::Index, count: current_user.pdf_files.where.not(docket_management_file_id: nil).search("").count, searched: searched.count
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      end

    end
  end
end
