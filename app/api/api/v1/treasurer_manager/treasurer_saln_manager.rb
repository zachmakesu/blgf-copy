module API
  module V1
    module TreasurerManager
      class TreasurerSalnManager < Grape::API
        include Grape::Kaminari
        resource :treasurer_saln_manager do
          before do
            authorize!(current_user, User::Role::ADMIN )
          end

          desc "List of Treasurers for TreasurerSalnManager paginated"
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get do
            searched = User.treasurer.search(params[:search])
            users = { data: paginate(searched) }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.treasurer.count, searched: searched.count
          end

          desc "List all Treasurers for TreasurerSalnManager"   
          get "/all" do
            searched = User.treasurer.search(params[:search])
            users = { data: searched }
            present users, with: Entities::V1::HrInformationDatabase::EmployeeInformation::Index, count: User.treasurer.count, searched: searched.count
          end

          
          desc 'Treasurer Saln record'
          get "/:employee_id" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            present employee.saln, with: Entities::V1::Treasurer::SalnRecord::Index
          end

          desc 'Update Saln basic and spouse information'
          # saln[statement_filing_status] (string) values (joint, separate, not_applicable)
          # saln[last_name]               (string)
          # saln[first_name]              (string)
          # saln[middle_initial]          (string)
          # saln[address]                 (string)
          # saln[position]                (string)
          # saln[office]                  (string)
          # saln[office_address]          (string)
          # saln[spouse_first_name]       (string)
          # saln[spouse_last_name]        (string)
          # saln[spouse_middle_initial]   (string)
          # saln[spouse_position]         (string)
          # saln[spouse_office]           (string)
          # saln[spouse_office_address]   (string)

          put "/:employee_id/basic" do
            validate_draft!(params[:saln])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_basic
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln unmarried children'
          # saln_child[name]       (string)
          # saln_child[birthdate]  (string) value (yyyy-mm-dd)
          # saln_child[age]        (string)
          post "/:employee_id/children" do
            validate_draft!(params[:saln_child])
            validate_date_format!(params[:saln_child][:birthdate]) if params[:saln_child][:birthdate].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).add_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln unmarried children'
          # id                     (int)
          # saln_child[name]       (string)
          # saln_child[birthdate]  (string) value (yyyy-mm-dd)
          # saln_child[age]        (string)
          put "/:employee_id/children/:id/update" do
            validate_draft!(params[:saln_child])
            validate_date_format!(params[:saln_child][:birthdate]) if params[:saln_child][:birthdate].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln unmarried children'
          # id                     (int)
          delete "/:employee_id/children/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_child
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln real asset'
          # saln_asset[description]           (string)
          # saln_asset[kind]                  (string)
          # saln_asset[exact_location]        (string)
          # saln_asset[assesed_value]         (float)
          # saln_asset[current_market_value]  (float)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_mode]      (string)
          # saln_asset[acquisition_cost]      (float)
          post "/:employee_id/real_asset" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])

            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            params[:saln_asset].asset_type = "real"
            handler = SalnHandler.new(employee, params, current_user).add_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln real asset'
          # id                                (int)
          # saln_asset[description]           (string)
          # saln_asset[kind]                  (string)
          # saln_asset[exact_location]        (string)
          # saln_asset[assesed_value]         (float)
          # saln_asset[current_market_value]  (float)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_mode]      (string)
          # saln_asset[acquisition_cost]      (float)
          put "/:employee_id/real_asset/:id/update" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            params[:saln_asset].asset_type = "real"
            handler = SalnHandler.new(employee, params, current_user).update_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln real asset'
          # id                     (int)
          delete "/:employee_id/real_asset/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_real_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln personal asset'
          # saln_asset[description]           (string)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_cost]      (float)
          post "/:employee_id/personal_asset" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            params[:saln_asset].asset_type = "personal"
            handler = SalnHandler.new(employee, params, current_user).add_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln personal asset'
          # id                                (int)
          # saln_asset[description]           (string)
          # saln_asset[acquisition_year]      (string) value (yyyy)
          # saln_asset[acquisition_cost]      (float)
          put "/:employee_id/personal_asset/:id/update" do
            validate_draft!(params[:saln_asset])
            validate_year_format!(params[:saln_asset][:acquisition_year])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            params[:saln_asset].asset_type = "personal"
            handler = SalnHandler.new(employee, params, current_user).update_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln personal asset'
          # id                     (int)
          delete "/:employee_id/personal_asset/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_personal_asset
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln liability'
          # saln_liability[nature]               (string)
          # saln_liability[creditors_name]       (string)
          # saln_liability[outstanding_balance]  (float)
          post "/:employee_id/liabilities" do
            validate_draft!(params[:saln_liability])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).add_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln liability'
          # id                     (int)
          # saln_liability[nature]               (string)
          # saln_liability[creditors_name]       (string)
          # saln_liability[outstanding_balance]  (float)
          put "/:employee_id/liabilities/:id/update" do
            validate_draft!(params[:saln_liability])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln liability'
          # id                     (int)
          delete "/:employee_id/liabilities/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_liability
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln business interests and financial connections'
          # saln_bi_and_fc[acquisition_date]            (string) value (yyyy-mm-dd)
          # saln_bi_and_fc[business_address]            (string)
          # saln_bi_and_fc[business_enterprise]         (string)
          # saln_bi_and_fc[business_financial_nature]   (string)
          post "/:employee_id/bi_and_fcs" do
            validate_draft!(params[:saln_bi_and_fc])
            validate_date_format!(params[:saln_bi_and_fc][:acquisition_date]) if params[:saln_bi_and_fc][:acquisition_date].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).add_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln business interests and financial connections'
          # id                     (int)
          # saln_bi_and_fc[acquisition_date]            (string) value (yyyy-mm-dd)
          # saln_bi_and_fc[business_address]            (string)
          # saln_bi_and_fc[business_enterprise]         (string)
          # saln_bi_and_fc[business_financial_nature]   (string)
          put "/:employee_id/bi_and_fcs/:id/update" do
            validate_draft!(params[:saln_bi_and_fc])
            validate_date_format!(params[:saln_bi_and_fc][:acquisition_date]) if params[:saln_bi_and_fc][:acquisition_date].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln business interests and financial connections'
          # id                     (int)
          delete "/:employee_id/bi_and_fcs/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_bi_and_fc
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln relatives in the government service'
          # saln_government_relative[name]           (string)
          # saln_government_relative[office_address] (string)
          # saln_government_relative[position]       (string)
          # saln_government_relative[relationship]   (string)
          post "/:employee_id/government_relatives" do
            validate_draft!(params[:saln_government_relative])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).add_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln relatives in the government service'
          # id                     (int)
          # saln_government_relative[name]           (string)
          # saln_government_relative[office_address] (string)
          # saln_government_relative[position]       (string)
          # saln_government_relative[relationship]   (string)
          put "/:employee_id/government_relatives/:id/update" do
            validate_draft!(params[:saln_government_relative])
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln relatives in the government service'
          # id                     (int)
          delete "/:employee_id/government_relatives/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_government_relative
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Add Saln signature and government issued id'
          # saln_issued_id[date_issued]   (string) value (yyyy-mm-dd)
          # saln_issued_id[id_no]         (string)
          # saln_issued_id[id_type]       (string) values (declarant, spouse)
  
          post "/:employee_id/issued_ids" do
            validate_draft!(params[:saln_issued_id])
            validate_date_format!(params[:saln_issued_id][:date_issued]) if params[:saln_issued_id][:date_issued].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).add_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Update Saln signature and government issued id'
          # id                     (int)
          # saln_issued_id[date_issued]   (string) value (yyyy-mm-dd)
          # saln_issued_id[id_no]         (string)
          # saln_issued_id[id_type]       (string) values (declarant, spouse)
          put "/:employee_id/issued_ids/:id/update" do
            validate_draft!(params[:saln_issued_id])
            validate_date_format!(params[:saln_issued_id][:date_issued]) if params[:saln_issued_id][:date_issued].present?
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).update_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

          desc 'Delete Saln signature and government issued id'
          # id                     (int)
          delete "/:employee_id/issued_ids/:id/delete" do
            employee = User.treasurer.find_by!(employee_id: params[:employee_id])
            handler = SalnHandler.new(employee, params, current_user).delete_issued_id
            if handler.response[:success]
              present handler.response[:details], with: Entities::V1::Treasurer::SalnRecord::Index
            else
              error!({messages: handler.response[:details]},400)
            end
          end

        end
      end

    end
  end
end
