module Entities
  module V1
    module Address
      class Index < Grape::Entity
        expose :id, :residency, :location, :zipcode, format_with: :format_if_nil
      end
    end
  end
end