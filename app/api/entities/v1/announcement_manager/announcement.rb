module Entities
  module V1
    module AnnouncementManager
      module Announcement

        class CalendarAnnouncementListing < Grape::Entity
          expose :id, :message, :posted_at, :start_time_at, :send_to

        end

        class AnnouncementListing < Grape::Entity
          expose :id, :message, :start_time_at, :send_to

        end

        class Calendar < Grape::Entity
          expose :data, using: CalendarAnnouncementListing

          private
          def data
            object.calendar.order(created_at: :asc)
          end
        end


        class Announcement < Grape::Entity
          expose :data, using: AnnouncementListing

          private
          def data
            object.web.read_by(options[:current_user].role).order(created_at: :asc)
          end
        end


        class CalendarInfo < Grape::Entity
          expose :data, using: CalendarAnnouncementListing

          private
          def data
            object
          end
        end

        class AnnouncementInfo < Grape::Entity
          expose :data, using: AnnouncementListing

          private
          def data
            object
          end
        end

      end
    end
  end
end