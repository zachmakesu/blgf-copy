module Entities
  module V1
    module Contact
      class Index < Grape::Entity
        expose :id, :device, :number, format_with: :format_if_nil
      end
    end
  end
end