module Entities
  module V1
    module EmployeeRequestManager
      module EmployeeFtsRequest

        class ViewFtsRequest < Grape::Entity
          expose :id
          expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
          expose :processed_by, :dtr_date, :in_am, :in_pm, :out_am, :out_pm, :status, :remarks

          private
          def employee
            object.owner
          end

          def processed_by
            object.processor.present? ? "#{object.processor.first_name} #{object.processor.last_name}" : nil
          end
        end

        class FtsRequestListing < Grape::Entity
          expose :id
          expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
          expose :processed_by, :dtr_date, :in_am, :in_pm, :out_am, :out_pm, :status, :remarks

          private
          def employee
            object.owner
          end

          def processed_by
            object.processor.present? ? "#{object.processor.first_name} #{object.processor.last_name}" : nil
          end
        end

        class Index < Grape::Entity
          expose :data, using: FtsRequestListing 
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

      end
    end
  end
end