module Entities
  module V1
    module EmployeeRequestManager
      module EmployeePdsRequest

        class ViewPdsDraft < Grape::Entity
          expose :data do
            expose :id, :request_type
            expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
            expose :draft
            expose :request_date, format_with: :mdy
            expose :processed_at, format_with: :mdy
            expose :status, :remarks, format_with: :format_if_nil
          end

          private
          def request_type
            object.classification
          end

          def employee
            object.owner
          end

          def request_date
            object.created_at
          end

          def draft
            # hash = Hash.new
            # object.object.each{ |k, v| hash[k.to_sym] = correct_value(v) }
            # hash
            object.object
          end
        end

        class PdsRequestListing < Grape::Entity
          expose :id, :request_type
          expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
          expose :request_date, format_with: :mdy
          expose :processed_at, format_with: :mdy
          expose :status, :remarks, format_with: :format_if_nil

          private
          def request_type
            object.classification
          end

          def employee
            object.owner
          end

          def request_date
            object.created_at
          end
        end

        class Index < Grape::Entity
          expose :data, using: PdsRequestListing
          expose :total_rows, :total_results, :total_searched
          
          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class MyRequest < Grape::Entity
          expose :data, using: PdsRequestListing
          expose :total_rows, :total_results, :total_searched
          
          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end
      end
    end
  end
end