module Entities
  module V1
    module EmployeeRequestManager
      module RequestType

        class RequestTypeListing < Grape::Entity
          expose  :id, :type_of_request
          expose  :first_signatory, :second_signatory, :third_signatory, :fourth_signatory, using: Entities::V1::Settings::UserAccount::UserFullNames
        end

        class Index < Grape::Entity
          expose :data do
            expose :requests ,using: RequestTypeListing
            expose :applicants
            expose :employees, using: Entities::V1::Settings::UserAccount::UserFullNames 
          end
          
          private
          def requests
            data = object.order(type_of_request: :asc)
            data.present? ? data : nil
          end

          def applicants
            object.applicants.map{|applicant| {id: applicant[1], applicant: applicant[0] } }
          end

          def employees
            data = options[:users].not_deleted.not_regular_employee.order(first_name: :asc)
            data.present? ? data : nil
          end
        end

        class RequestTypeInfo < Grape::Entity
          expose :data, using: RequestTypeListing
          private
          def data
            object
          end
        end

        class RequestTypeForOthers < Grape::Entity
          expose  :id, :type_of_request
        end

      end
    end
  end
end