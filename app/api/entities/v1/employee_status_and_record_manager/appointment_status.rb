module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module AppointmentStatus

        class AppointmentStatusListing < Grape::Entity
          expose  :id, :appointment_code, :appointment_status, :leave_entitled, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: AppointmentStatusListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class AppointmentStatusInfo < Grape::Entity
          expose :data, using: AppointmentStatusListing
          private
          def data
            object
          end
        end

      end
    end
  end
end