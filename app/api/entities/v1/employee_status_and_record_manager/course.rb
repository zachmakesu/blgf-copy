module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module Course

        class CourseListing < Grape::Entity
          expose  :id, :course_code, :course_description, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: CourseListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class CourseInfo < Grape::Entity
          expose :data, using: CourseListing
          private
          def data
            object
          end
        end

      end
    end
  end
end