module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module DocketManagementFile

        class DocketManagementFileListing < Grape::Entity
          expose  :id, :title, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: DocketManagementFileListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class DocketManagementFileInfo < Grape::Entity
          expose :data, using: DocketManagementFileListing
          private
          def data
            object
          end
        end

      end
    end
  end
end