module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module DutyAndResponsibility

        class DutyAndResponsibilityListing < Grape::Entity
          expose  :id, :position_code_id, :position, :percent_work, :duties, format_with: :format_if_nil

          private
          def position
            object.position_code.present? ? object.position_code.position_description : nil
          end
        end

        class Index < Grape::Entity
          expose :data, using: DutyAndResponsibilityListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class DutyAndResponsibilityInfo < Grape::Entity
          expose :data, using: DutyAndResponsibilityListing
          
          private
          def data
            object
          end
        end

      end
    end
  end
end