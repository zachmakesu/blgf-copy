module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module EducationLevel

        class EducationLevelListing < Grape::Entity
          expose  :id, :level_code, :level_description, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: EducationLevelListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class EducationLevelInfo < Grape::Entity
          expose :data, using: EducationLevelListing
          private
          def data
            object
          end
        end

      end
    end
  end
end