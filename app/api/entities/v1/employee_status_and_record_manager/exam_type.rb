module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module ExamType

        class ExamTypeListing < Grape::Entity
          expose  :id, :exam_code, :exam_description, :csc_eligible, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: ExamTypeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class ExamTypeInfo < Grape::Entity
          expose :data, using: ExamTypeListing
          private
          def data
            object
          end
        end

      end
    end
  end
end