module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module ModeOfSeparation

        class ModeOfSeparationListing < Grape::Entity
          expose  :id, :separation_mode, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: ModeOfSeparationListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class ModeOfSeparationInfo < Grape::Entity
          expose :data, using: ModeOfSeparationListing
          private
          def data
            object
          end
        end

      end
    end
  end
end