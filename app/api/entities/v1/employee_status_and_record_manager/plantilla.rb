module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module Plantilla

        class PlantillaListing < Grape::Entity
          expose  :id, :item_number, :position_code_id, :position_description, :authorize_salary_month, :authorize_salary_year, :salary_grade_id, :salary_grade, :plantilla_group_id, :plantilla_group, :area_code, :area_type, :csc_eligibility, :rationalized, format_with: :format_if_nil

          private
          
          def position_code_id
            object.position_code.present? ? object.position_code.id : nil
          end

          def position_description
            object.position_code.present? ? object.position_code.position_description : nil
          end

          def authorize_salary_month
            #this is not official
            if object.salary_schedule.present?
              object.salary_schedule.step_1.present? ? object.salary_schedule.step_1 : nil
            else
              nil
            end
          end

          def authorize_salary_year
            #this is not official
            if object.salary_schedule.present?
              object.salary_schedule.step_1.present? ? object.salary_schedule.step_1 * 12  : nil
            else
              nil
            end
          end

          def salary_grade_id
            object.salary_schedule.present? ? object.salary_schedule.id : nil
          end

          def salary_grade
            object.salary_schedule.present? ? object.salary_schedule.salary_grade : nil
          end

          def plantilla_group_id
            object.plantilla_group.present? ? object.plantilla_group.id : nil
          end

          def plantilla_group
            object.plantilla_group.present? ? object.plantilla_group.group_name : nil
          end

        end

        class Index < Grape::Entity
          #expose :salary_grades, using: Entities::V1::Payroll::SalarySchedule::SalaryScheduleForOthers
          expose :data, using: PlantillaListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class PlantillaInfo < Grape::Entity
          expose :data, using: PlantillaListing
          private
          def data
            object
          end
        end

        class PlantillaForOthers < Grape::Entity
          expose  :id, :item_number
        end

      end
    end
  end
end