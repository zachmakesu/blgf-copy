module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module PlantillaDuty

        class PlantillaDutyisting < Grape::Entity
          expose  :id, :plantilla_id, :position, :percent_work, :duties, format_with: :format_if_nil

          private
          def position
            object.plantilla.present? ? object.plantilla.item_number : nil
          end
        end

        class Index < Grape::Entity
          expose :data, using: PlantillaDutyisting
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class PlantillaDutyInfo < Grape::Entity
          expose :data, using: PlantillaDutyisting
          
          private
          def data
            object
          end
        end

      end
    end
  end
end