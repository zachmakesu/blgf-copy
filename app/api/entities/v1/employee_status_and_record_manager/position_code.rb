module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module PositionCode

        class PositionCodeListing < Grape::Entity
          expose  :id, :position_code, :position_description, :position_abbreviation, :educational_requirement, :experience_requirement, :eligibility_requirement, :training_requirement, :position_classification, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: PositionCodeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class PositionCodeInfo < Grape::Entity
          expose :data, using: PositionCodeListing
          private
          def data
            object
          end
        end

        class PositionCodeForOthers < Grape::Entity
          expose  :id, :position_code, :position_description
        end


      end
    end
  end
end