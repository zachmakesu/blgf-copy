module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module ProjectCode

        class ProjectCodeListing < Grape::Entity
          expose  :id, :project_code, :project_description, :project_order, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: ProjectCodeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class ProjectCodeInfo < Grape::Entity
          expose :data, using: ProjectCodeListing
          private
          def data
            object
          end
        end

      end
    end
  end
end