module Entities
  module V1
    module EmployeeStatusAndRecordManager
      module Scholarship

        class ScholarshipListing < Grape::Entity
          expose  :id, :description, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: ScholarshipListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class ScholarshipInfo < Grape::Entity
          expose :data, using: ScholarshipListing
          private
          def data
            object
          end
        end

      end
    end
  end
end