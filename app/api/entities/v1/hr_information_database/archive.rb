module Entities
  module V1
    module HrInformationDatabase
      module Archive

        class ArchiveListing < Grape::Entity
          expose :id, :archive_date, :field
          expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
          expose :info
          private

          def employee
            object.user
          end
        end

        class ArchiveInfo < Grape::Entity
          expose :data, using: ArchiveListing
          expose :total_rows, :total_results, :total_searched
          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class Dates < Grape::Entity
          expose :date, format_with: :mdy
          private
          def date
            object
          end
        end

        class ArchiveDates < Grape::Entity
          expose :data, using: Dates
          expose :total_rows, :total_results, :total_searched
          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

      end
    end
  end
end