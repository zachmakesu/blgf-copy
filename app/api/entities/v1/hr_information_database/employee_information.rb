module Entities
  module V1
    module HrInformationDatabase
      module EmployeeInformation

        class UserForHrDatabase < Grape::Entity
          expose :employee_id, :first_name, :last_name, :service_department, :job_category, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo
          expose :educations, using: Entities::V1::MyPersonalDataSheet::Education::EducationList
          expose :date_hired, format_with: :format_if_nil
          expose :age, format_with: :format_if_nil
          expose :birthdate, format_with: :format_if_nil
          expose :gender, format_with: :format_if_nil
          expose :length_of_service_in_years, format_with: :format_if_nil
          expose :salary_grade, format_with: :format_if_nil
          expose :trainings_and_seminars, using: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsList

          private
          def service_department
            object.position_detail.service.present? ? object.position_detail.service.name : nil
          end

          def profile_image
            object.avatar
          end
          
          def date_hired
            object.date_hired.present? ? object.date_hired : ""
          end

          def birthdate
            object.birthdate.present? ? object.birthdate : ""
          end

          def salary_grade
            object.salary_grade.present? ? object.salary_grade : ""
          end

          def job_category
            object.position_detail.job_category
          end
        end

        class Index < Grape::Entity
          expose :data, using: Entities::V1::Settings::UserAccount::UserForHrDatabase
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class LIST < Grape::Entity
          expose :data, using: UserForHrDatabase
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end
       
      end
    end
  end
end