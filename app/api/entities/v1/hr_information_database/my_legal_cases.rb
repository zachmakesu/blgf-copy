module Entities
  module V1
    module HrInformationDatabase
      module MyLegalCases

        class LegalCaseListing < Grape::Entity
          expose  :id, :employee_id, :case_number, :title, :charged_offenses, :status, format_with: :format_if_nil
          expose :employee, using: Entities::V1::Settings::UserAccount::UserFullNames
          private
          def employee_id
            object.user.employee_id
          end

          def employee
            object.user
          end
        end

        class Index < Grape::Entity
          expose :data, using: LegalCaseListing
          expose :total_rows, :total_results, :total_searched
          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end

        end

        class LegalCasesInfo < Grape::Entity
          expose :data, using: LegalCaseListing
          private
          def data
            object
          end
        end

      end
    end
  end
end