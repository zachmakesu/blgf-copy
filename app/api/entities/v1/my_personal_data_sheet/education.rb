module Entities
  module V1
    module MyPersonalDataSheet
      module Education

        class EducationList < Grape::Entity
          expose :id, :education_level_id, :education_level, :school_name, :course_id, :course, :highest_grade_level_units_earned, :honors_received, :year_graduated, format_with: :format_if_nil
          expose :date_of_attendance_from, :date_of_attendance_to, format_with: :format_if_nil

          def course
            object.course.present? ? object.course.course_description : nil
          end

          def education_level
            object.education_level.present? ? object.education_level.level_description : nil
          end
        end

        class EducationInformation < Grape::Entity
          expose :data, using: EducationList
          private
          def data
            object.educations
          end
        end

      end
    end
  end
end