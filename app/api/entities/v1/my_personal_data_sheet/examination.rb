module Entities
  module V1
    module MyPersonalDataSheet
      module Examination

        class ExaminationList < Grape::Entity
          expose :id, :exam_description, :rating, :date_of_exam, :place_of_exam, :licence_number, :date_of_release, format_with: :format_if_nil
        end

        class ExaminationInfomation < Grape::Entity
          expose :data, using: ExaminationList
          private
          def data
            object.examinations
          end
        end

      end
    end
  end
end