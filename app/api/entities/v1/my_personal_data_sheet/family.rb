module Entities
  module V1
    module MyPersonalDataSheet
      module Family
  
        class Children < Grape::Entity
            expose :id, :first_name, :last_name, :middle_name, :middle_initial, :relationship, format_with: :format_if_nil
            expose :birthdate, format_with: :format_if_nil
        end

        class FamilyInformation < Grape::Entity
          expose :data do
            expose :family_information do
              expose :spouse_information do 
                expose :spouse_name, :spouse_occupation, :spouse_employer_or_business_name, :spouse_work_or_business_address, :spouse_contact_number, :spouse_first_name, :spouse_middle_name, :spouse_last_name, format_with: :format_if_nil
              end
              expose :parents_information do
                expose :parent_fathers_name, :parent_mothers_maiden_name, :parent_address, :parant_fathers_first_name, :parant_fathers_middle_name, :parant_fathers_last_name, :parant_mothers_first_name, :parant_mothers_middle_name, :parant_mothers_last_name, format_with: :format_if_nil
              end
            end

            expose :children_information do
              expose :children, using: Children
            end      
            
          end

          private
          def children
            object.children.present?  ? object.children : []
          end
        end

      end
    end
  end
end