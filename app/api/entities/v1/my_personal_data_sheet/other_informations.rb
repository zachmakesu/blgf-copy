module Entities
  module V1
    module MyPersonalDataSheet
      module OtherInformations

        class ReferencesList < Grape::Entity
          expose :id, :name, :address, :contact_number, format_with: :format_if_nil
        end

        class OtherInformationList < Grape::Entity
          expose :skills_and_hobbies, :non_academic_destinction, :membership_in, :third_degree_national, :give_national, :fourth_degree_local, :give_local, :formally_charge, :give_charge, :administrative_offense, :give_offense, :any_violation, :give_reasons, :canditate,  :give_date_particulars, :indigenous_member, :give_group, :differently_abled, :give_disability, :solo_parent, :give_status, :is_separated, :give_details, format_with: :format_if_nil
          expose :signature, format_with: :format_if_nil
          expose :date_accomplished, format_with: :format_if_nil
          expose :ctc_number, format_with: :format_if_nil
          expose :issued_at, :issued_on, format_with: :format_if_nil
          expose :signature_image, format_with: :original_photo_url

          private
          def references
            object.user.references.present? ? object.references : []
          end

          def signature_image
            object.user.signature_image
          end

          def skills_and_hobbies
            if object.skills_and_hobbies.present?
              object.skills_and_hobbies.split("|").map{|s| s.rstrip.lstrip}.reject { |e| e.to_s.empty? }
            else
              []
            end
          end

          def non_academic_destinction
            if object.non_academic_destinction.present?
              object.non_academic_destinction.split("|").map{|s| s.rstrip.lstrip}.reject { |e| e.to_s.empty? }
            else
              []
            end
          end

          def membership_in
            if object.membership_in.present?
              object.membership_in.split("|").map{|s| s.rstrip.lstrip}.reject { |e| e.to_s.empty? }
            else
              []
            end
          end
        end

        class OtherInformation < Grape::Entity
          expose :data do 
            expose :other_informations, using: OtherInformationList
            expose :references, using: ReferencesList
          end

          private
          def other_informations
            object.other_information
          end
          def references
            object.references.present? ? object.references : []
          end
        end

      end
    end
  end
end
