module Entities
  module V1
    module MyPersonalDataSheet
      module PdfInfo
  
        class ToPdf < Grape::Entity
          expose :data do
              expose :first_name,
                     :middle_name,
                     :middle_initial,
                     :last_name,
                     :name_extension,
                     :birthdate,
                     :birthplace,
                     :gender,
                     :civil_status,
                     :citizenship,
                     :blood_type,
                     :height, :weight,
                     :agency_employee_number,
                     :tin_number,
                     :gsis_policy_number,
                     :pagibig_id_number,
                     :philhealth_number,
                     :sss_number,
                     :remittance_id,
                     :email, format_with: :format_if_nil
              expose :addresses, using: Entities::V1::Address::Index
              expose :contacts, using: Entities::V1::Contact::Index
              expose :profile_image, format_with: :original_photo_url
              expose :signature_image, format_with: :original_photo_url

              expose :family_background do
                expose  :spouse_first_name,
                        :spouse_middle_name,
                        :spouse_last_name,
                        :spouse_contact_number,
                        :spouse_employer_or_business_name,
                        :spouse_name,
                        :spouse_occupation,
                        :spouse_work_or_business_address,
                        :parant_fathers_first_name,
                        :parant_fathers_middle_name,
                        :parant_fathers_last_name,
                        :parant_mothers_first_name,
                        :parant_mothers_middle_name,
                        :parant_mothers_last_name, format_with: :format_if_nil

                expose  :children, using: Entities::V1::MyPersonalDataSheet::Family::Children
              end

              expose :educational_background, using: Entities::V1::MyPersonalDataSheet::Education::EducationList
              expose :civil_service_eligibilty, using: Entities::V1::MyPersonalDataSheet::Examination::ExaminationList
              expose :work_experience, using: Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceList
              expose :voluntary_work, using: Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkList
              expose :training_programs, using: Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsList
              expose :other_info, using: Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformationList
          end

          private
          def addresses
            object.addresses.present? ? object.addresses : []
          end

          def contacts
            object.contacts.present? ? object.contacts : []
          end

          def children
            object.children.present? ? object.children : []
          end

          def educational_background
            object.educations.present? ? object.educations : []
          end

          def civil_service_eligibilty
            object.examinations.present? ? object.examinations : []
          end

          def work_experience
            object.work_experiences.present? ? object.work_experiences.order(inclusive_date_from: :desc) : []
          end

          def voluntary_work
            object.voluntary_works.present? ? object.voluntary_works.order(inclusive_date_from: :desc) : []
          end

          def other_info
            object.other_information
          end

          def training_programs
            object.trainings_and_seminars.present? ? object.trainings_and_seminars.order(inclusive_date_from: :desc) : []
          end

          def agency_employee_number
            object.biometric_id
          end

          def profile_image
            object.avatar.present? ? object.avatar : nil
          end
        end
        
      end
    end
  end
end