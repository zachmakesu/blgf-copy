module Entities
  module V1
    module MyPersonalDataSheet
      module PersonalInfo
  
        class PersonalInformation < Grape::Entity
          expose :data do
              expose :first_name, :middle_name, :middle_initial, :last_name, :name_extension, :birthdate, :birthplace, :gender, :civil_status, :citizenship, :blood_type, :height, :weight, :agency_employee_number, :tin_number, :gsis_policy_number, :pagibig_id_number, :philhealth_number, :sss_number, :remittance_id, :email, format_with: :format_if_nil
              expose :addresses, using: Entities::V1::Address::Index
              expose :contacts, using: Entities::V1::Contact::Index
              expose :profile_image, format_with: :original_photo_url
              expose :signature_image, format_with: :original_photo_url
          end

          private
          def addresses
            object.addresses.present? ? object.addresses : []
          end

          def contacts
            object.contacts.present? ? object.contacts : []
          end

          def agency_employee_number
            object.biometric_id
          end

          def profile_image
            object.avatar.present? ? object.avatar : nil
          end
        end
        
      end
    end
  end
end