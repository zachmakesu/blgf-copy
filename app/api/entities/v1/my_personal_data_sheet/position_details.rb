module Entities
  module V1
    module MyPersonalDataSheet
      module PositionDetails

        class PositionDetailsList < Grape::Entity
          expose :service_code, :appointment_status, :executive_office, :service, :division, :section, :payroll_group, :attendance_scheme, :plantilla, format_with: :format_if_nil

          private
          def service_code
            object.service_code.present? ? object.service_code.service_code : nil
          end

          def appointment_status
            object.appointment_status.present? ? object.appointment_status.appointment_status : nil
          end

          def executive_office
            object.executive_office.present? ? object.executive_office.name : nil
          end

          def service
            object.service.present? ? object.service.name : nil
          end

          def division
            object.division.present? ? object.division.name : nil
          end

          def section
            object.section.present? ? object.section.name : nil
          end

          def payroll_group
            object.payroll_group.present? ? object.payroll_group.payroll_group_name : nil
          end

          def attendance_scheme
            object.attendance_scheme.present? ? object.attendance_scheme.scheme_code : nil
          end

          def plantilla
            object.plantilla.present? ? object.plantilla.item_number : nil
          end
        end
        
        class PositionDetails < Grape::Entity
          expose :data, using: PositionDetailsList

          private
          def data
            object.position_detail
          end
        end

      end
    end
  end
end