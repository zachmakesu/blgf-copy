module Entities
  module V1
    module MyPersonalDataSheet
      module TrainingsAndSeminars

        class TrainingsAndSeminarsList < Grape::Entity
          expose :id, :title, format_with: :format_if_nil
          expose :inclusive_date_from, :inclusive_date_to, format_with: :format_if_nil
          expose :number_of_hours, :conducted_by, format_with: :format_if_nil
        end

        class TrainingsAndSeminarsInformation < Grape::Entity
          expose :data, using: TrainingsAndSeminarsList
          private
          def data
            object.trainings_and_seminars
          end
        end

      end
    end
  end
end