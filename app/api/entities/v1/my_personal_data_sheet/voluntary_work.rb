module Entities
  module V1
    module MyPersonalDataSheet
      module VoluntaryWork

        class VoluntaryWorkList < Grape::Entity
          expose :id, :name_of_organization, :address_of_organization, :inclusive_date_from, :inclusive_date_to, :number_of_hours, :position_nature_of_work, format_with: :format_if_nil
        end

        class VoluntaryWorkInformation < Grape::Entity
          expose :data, using: VoluntaryWorkList
          private
          def data
            object.voluntary_works
          end
        end

      end
    end
  end
end