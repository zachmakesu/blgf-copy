module Entities
  module V1
    module MyPersonalDataSheet
      module WorkExperience

        class WorkExperienceList < Grape::Entity
          expose :id, format_with: :format_if_nil
          expose :inclusive_date_from, :inclusive_date_to, format_with: :format_if_nil
          expose :position_title, :department_agency_office, :monthly_salary, :appointment_status_id, :appointment_status, :government_service, :salary_grade_and_step, format_with: :format_if_nil

          private
          def appointment_status
            object.appointment_status.present? ? object.appointment_status.appointment_status : nil
          end
        end

        class WorkExperienceInformation < Grape::Entity
          expose :data, using: WorkExperienceList
          private
          def data
            object.work_experiences
          end
        end

      end
    end
  end
end