module Entities
  module V1
    module MyRequest
      module FtsRequest

        class FtsRequestListing < Grape::Entity
          expose :id, :dtr_date, :in_am, :in_pm, :out_am, :out_pm, :status, :processed_by, :remarks, format_with: :format_if_nil

          private
          def processed_by
            object.processor.present? ? "#{object.processor.first_name} #{object.processor.last_name}" : nil
          end
        end


        class FtsRequestInfo < Grape::Entity
          expose :data, using: FtsRequestListing

          private
          def data
            object
          end
        end

        class Index < Grape::Entity
          expose :data, using: FtsRequestListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class RequiredFtsDtrs < Grape::Entity
          expose :data, using: Entities::V1::Payroll::MyDtr::DtrListings
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
          
        end


      end
    end
  end
end