module Entities
  module V1
    module MyRequest
      module LeaveRequest

        class LeaveRequestListing < Grape::Entity
          expose :id, :leave_type, :day_type, :date_from, :date_to, :half_date, :deductible_credits, :reason, :processed_by, :status, :remarks, format_with: :format_if_nil

          private
          def processed_by
            object.processor.present? ? "#{object.processor.first_name} #{object.processor.last_name}" : nil
          end
        end

        class LeaveRequestInfo < Grape::Entity
          expose :data, using: LeaveRequestListing

          private
          def data
            object
          end
        end

        class LeaveCredits < Grape::Entity
          expose :data do
            expose :leave_credits do
              expose :leave_credit_certified_date_month, :vacation_leave_credits, :sick_leave_credits, :total_leave_credits, :cto_leave_credits
              expose :last_updated_by_at
              expose :updated_by, using: Entities::V1::Settings::UserAccount::UserInfo
            end
          end

          private
          def leave_credit_certified_date_month
            object.leave_credit_certified_date_month ? object.leave_credit_certified_date_month.strftime("%B, %Y") : Date.today.strftime("%B, %Y")
          end

          def vacation_leave_credits
            object.vacation_leave_credits_left_count
          end

          def sick_leave_credits
            object.sick_leave_credits_left_count
          end

          def total_leave_credits
            object.total_leave_credits
          end

          def cto_leave_credits
            object.cto_leave_credits_left_count
          end

          def updated_by
            object.last_updated_by
          end

        end

        class Index < Grape::Entity
          expose :data do 
            expose :leave_requests, using: LeaveRequestListing
            expose :total_rows, :total_results, :total_searched
            
          end
          
          private
          def leave_requests
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end
          
          def total_searched
            options[:searched]
          end
        end
      end
    end
  end
end