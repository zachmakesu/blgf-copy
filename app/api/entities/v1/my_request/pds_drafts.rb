module Entities
  module V1
    module MyRequest
      module PdsDrafts
        class Index < Grape::Entity
          expose :id, :request_type, :drafts, :status, :processor_id, format_with: :format_if_nil
          expose :processed_at, format_with: :mdy
          
          private
          def request_type
            object.classification
          end

          def drafts
            object.object
          end
        end
      end
    end
  end
end