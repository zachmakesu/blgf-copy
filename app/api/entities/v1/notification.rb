module Entities
  module V1
    module Notification

      class NotificationListing < Grape::Entity
        expose :id, :message, :api_slug, :notificationable_type, :seen, format_with: :format_if_nil
        expose :created_at, format_with: :utc

        def seen
          seen = options[:current_user].user_seen_notifications.map(&:notification_id)
          seen.include?(object.id)
        end
      end

      class Index < Grape::Entity
        expose :data, using: NotificationListing
        expose :total_rows, :total_results, :total_unseen

        private
        def data
          object[:data]
        end

        def total_rows
          options[:count]
        end

        def total_results
          object[:data].count
        end

        def total_searched
          options[:searched]
        end

        def total_unseen
          options[:current_user].unseen_notifications.count
        end

      end
    end
  end
end