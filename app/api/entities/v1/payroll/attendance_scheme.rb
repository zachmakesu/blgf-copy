module Entities
  module V1
    module Payroll
      module AttendanceScheme
        
        class AttendanceSchemeListing < Grape::Entity
          expose :id, :scheme_code, :scheme_name
          expose :am_time_in_from, :am_time_in_to, :pm_time_out_from, :pm_time_out_to, :nn_time_out_from, :nn_time_out_to, :nn_time_in_from, :nn_time_in_to#, format_with: :utc_time
        end

        class Index < Grape::Entity
          expose :data, using: AttendanceSchemeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class AttendanceSchemeInfo < Grape::Entity
          expose :data, using: AttendanceSchemeListing
          private
          def data
            object
          end
        end

      end
    end
  end
end