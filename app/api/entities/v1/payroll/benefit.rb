module Entities
  module V1
    module Payroll
      module Benefit
        
        class BenefitListing < Grape::Entity
          expose  :id, :code, :benefit_type, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: BenefitListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class BenefitInfo < Grape::Entity
          expose :data, using: BenefitListing
          private
          def data
            object
          end
        end

      end
    end
  end
end