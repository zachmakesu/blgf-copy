module Entities
  module V1
    module Payroll
      module DeductionType
        
        class DeductionTypeListing < Grape::Entity
          expose  :id, :deduction_code, :deduction, :mandatory, :deduction_base_calculations, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: DeductionTypeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class DeductionTypeInfo < Grape::Entity
          expose :data, using: DeductionTypeListing
          private
          def data
            object
          end
        end

      end
    end
  end
end