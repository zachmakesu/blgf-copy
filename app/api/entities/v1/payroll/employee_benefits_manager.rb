module Entities
  module V1
    module Payroll
      module EmployeeBenefitsManager
        
        class BenefitListings < Grape::Entity
          expose :id, :amount, :benefit_id, :benefit, :benefit_type, :overwrite, format_with: :format_if_nil

          private
          def benefit
            object.benefit.present? ? object.benefit.code : nil
          end

          def benefit_type
            object.benefit.present? ? object.benefit.benefit_type : nil
          end
        end

   
        class EmployeeBenefits < Grape::Entity
          expose :data, using: BenefitListings
          private
          def data
            object
          end
        end

        class Index < Grape::Entity
          expose :data, using: BenefitListings

          private
          def data
            object[:data]
          end

        end


      end
    end
  end
end