module Entities
  module V1
    module Payroll
      module EmployeeDeductionsManager
        
        class DeductiveListings < Grape::Entity
          expose :id, :deduction_type, :deduction_type_id, :amount, :mandatory, :overwrite, format_with: :format_if_nil

          private
          def deduction_type
            object.deduction_type.present? ? object.deduction_type.deduction : nil
          end

          def mandatory
            object.deduction_type.present? ? object.deduction_type.mandatory : nil
          end

        end

        class DeductionsInfo < Grape::Entity
          expose :data, using: DeductiveListings
          private
          def data
            object[:data]
          end
        end

        class EmployeeDeductives < Grape::Entity
          expose :data, using: DeductiveListings
          private
          def data
            object.deductives.order(id: :asc)
          end
        end

        class EmployeeDeductionHistoryListing < Grape::Entity
          expose :history_id, :date_applied
          expose :deductions, using: DeductiveListings

          private
          def history_id
            object.id
          end

          def deductions
            object.deduction_history_children
          end
          
        end


        class EmployeeDeductionHistory < Grape::Entity
          expose :data, using: EmployeeDeductionHistoryListing
          private
          def data
            object.deduction_histories.includes(deduction_history_children: [:deduction_type]).date_applied(options[:date]).order(id: :asc)
          end
        end

        class DeductionsToExcelListings < Grape::Entity
          expose :employee, using: Entities::V1::Treasurer::TreasurerPdfFile::UserBasicInfo
          expose :deductions
          private
          def employee
            object
          end

          def deductions
            object.deductions_to_excel("#{options[:year]}-#{options[:month]}")
          end
        end

        class DeductionsToExcel < Grape::Entity
          expose :data do
            expose :this_month
            expose :employees, using: DeductionsToExcelListings
          end
          
          private
          def employees
            object[:data]
          end
          def this_month
            "#{options[:year]}-#{options[:month]}"
          end
        end

      end
    end
  end
end