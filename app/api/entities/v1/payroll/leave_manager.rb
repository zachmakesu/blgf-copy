module Entities
  module V1
    module Payroll
      module LeaveManager
        class LeaveRequestListing < Grape::Entity
          
          expose :id, :employee, :leave_type, :day_type, :date_from, :date_to, :half_date, :deductible_credits, :reason, :status, :remarks, format_with: :format_if_nil
          private
          def employee
            object.owner.present? ? "#{object.owner.first_name} #{object.owner.last_name}" : nil
          end
        end

        class Index < Grape::Entity
          expose :data, using: LeaveRequestListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class LeaveRequestInfo < Grape::Entity
          expose :data, using: LeaveRequestListing

          private
          def data
            object
          end
        end

      end
    end
  end
end