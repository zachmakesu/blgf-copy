module Entities
  module V1
    module Payroll
      module LeaveType
        class LeaveTypeForOthers< Grape::Entity
          expose  :id, :leave_code, :leave_type, format_with: :format_if_nil
        end
        
        class SpecificLeaveListing < Grape::Entity
          expose  :id, :leave_code, :specific_leave, format_with: :format_if_nil

          def leave_code
            object.parent_leave_type.present? ? object.parent_leave_type.leave_code : nil
          end
        end

        class LeaveTypeListing < Grape::Entity
          expose  :id, :leave_code, :leave_type, :no_of_days, format_with: :format_if_nil
        end

        class AllLeaveTypes < Grape::Entity
          expose :data, using: LeaveTypeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class AllSpecificLeaves < Grape::Entity
          expose :data do
            expose :specific_leaves, using: SpecificLeaveListing
            expose :leave_types, using: LeaveTypeForOthers
          end
          private
          def specific_leaves
            data = object.specific_leaves.order(leave_type_id: :asc)
            data.present? ? data : nil
          end

          def leave_types
            data = options[:leave_types].no_specific_leaves.order(leave_code: :asc)
            data.present? ? data : nil
          end
        end

        class LeaveTypeInfo < Grape::Entity
          expose :data, using: LeaveTypeListing
          private
          def data
            object
          end
        end

        class SpecificLeaveInfo < Grape::Entity
          expose :data, using: SpecificLeaveListing
          private
          def data
            object
          end
        end

      end
    end
  end
end