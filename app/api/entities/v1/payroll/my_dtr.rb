module Entities
  module V1
    module Payroll
      module MyDtr
        
        class DtrListings < Grape::Entity
          expose :id, :dtr_date, :in_am, :out_am, :in_pm, :out_pm, :in_ot, :out_ot, :remarks, :name, :ot, :late, :ut, :old_value, :is_edited, format_with: :format_if_nil
          expose :processed_by, using: Entities::V1::Settings::UserAccount::UserForHrDatabase
         
          private
          def processed_by
            object.user.present? ? object.user : nil
          end
        end

        class SpecificDtr < Grape::Entity
          expose :data, using: DtrListings

          private
          def data
            object
          end
        end

        class Index < Grape::Entity
          expose :data, using: DtrListings

          private
          def data
            object.attendances(options[:date])
          end
        end

        class UserDtr < Grape::Entity
          expose :data, using: Entities::V1::Settings::UserAccount::UserForDTr
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end
          
          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

       
      end
    end
  end
end