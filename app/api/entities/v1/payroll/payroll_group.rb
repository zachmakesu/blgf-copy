module Entities
  module V1
    module Payroll
      module PayrollGroup

        class PayrollGroupListing < Grape::Entity
          expose  :id, :project_code_id, :project, :payroll_group_order, :payroll_group_code, :payroll_group_name, format_with: :format_if_nil

          private
          def project
            object.project_code.present? ? object.project_code.project_description : nil
          end

        end

        class Index < Grape::Entity
          expose :data, using: PayrollGroupListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class PayrollGroupInfo < Grape::Entity
          expose :data, using: PayrollGroupListing
          private
          def data
            object
          end
        end

      end
    end
  end
end