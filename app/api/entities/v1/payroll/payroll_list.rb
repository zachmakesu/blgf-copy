module Entities
  module V1
    module Payroll
      module PayrollList

        class AllowanceWithBenefit < Grape::Entity
          expose :id, :benefit_id, :benefit_code,:benefit_type, :amount, format_with: :format_if_nil
          private
          def id
            object["id"]
          end

          def benefit_id
            object["benefit_id"]
          end

          def benefit_code
            object["benefit_code"]
          end

          def benefit_type
            object["benefit_type"]
          end

          def amount
            object["amount"]
          end
        end

        class DeductionsWithDeductionType < Grape::Entity
          expose :id, :deduction_type_id, :deduction_code, :deduction, :mandatory, :amount, format_with: :format_if_nil
          private
          def id
            object["id"]
          end

          def deduction_type_id
            object["deduction_type_id"]
          end

          def deduction_code
            object["deduction_code"]
          end

          def deduction
            object["deduction"]
          end

          def mandatory
            object["mandatory"]
          end

          def amount
            object["amount"]
          end
        end

        class PayrollListing < Grape::Entity
          expose :id, :employee, :first_name, :last_name, :this_month, :allowances_amount_total_this_month, :deductions_amount_total_this_month, :authorized_salary_this_month, :gross_salary, :net_salary_this_month, format_with: :format_if_nil
          private
          def employee
            "#{object.user.first_name} #{object.user.last_name}"
          end

          def first_name
            object.user.first_name
          end

          def last_name
            object.user.last_name
          end

          def gross_salary
            object.authorized_salary_this_month + object.allowances_amount_total_this_month
          end
        end

        class Payslip < Grape::Entity 
          expose :data do
            expose :id, :employee, :this_month, :sg_this_month, :plantilla_position_this_month
            expose :leave_credits_this_month
            expose :late_this_month, :under_time_this_month, format_with: :format_if_nil
            expose :allowances_amount_total_this_month
            expose :allowances_this_month, using: AllowanceWithBenefit
            expose :deductions_amount_total_this_month
            expose :deductions_this_month, using: DeductionsWithDeductionType
            expose :authorized_salary_this_month, :gross_salary, :net_salary_this_month
            expose :payslip 
          end
          
          private
          def employee
            "#{object.user.first_name} #{object.user.last_name}"
          end

          def allowances_this_month
            object.benefits_this_month
          end

          def deductions_this_month
            object.deductions_this_month
          end

          def gross_salary
            object.authorized_salary_this_month + object.allowances_amount_total_this_month
          end

          def payslip
            object.get_payslip_form
          end
        end

        class Index < Grape::Entity
          expose :data, using: PayrollListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class DummyPayslip < Grape::Entity 
          expose :data do
            expose :employee, :this_month
            expose :allowances_amount_total_this_month
            expose :allowances_this_month, using: AllowanceWithBenefit
            expose :deductions_amount_total_this_month
            expose :deductions_this_month, using: DeductionsWithDeductionType
            expose :authorized_salary_this_month, :gross_salary, :net_salary_this_month
            expose :payslip 
          end
          
          private
          def employee
            "#{object.first_name} #{object.last_name}"
          end

          def this_month
            options[:this_month]
          end

          def allowances_amount_total_this_month
            0
          end

          def allowances_this_month
            []
          end

          def deductions_amount_total_this_month
            0
          end

          def deductions_this_month
            []
          end

          def authorized_salary_this_month
            0
          end

          def gross_salary
            0
          end

          def net_salary_this_month
            0
          end
          
          def payslip
            [
              {
                column: "Week 1",
                period_pay: 0,
                benefits: 0,
                deductions: 0,
                net_pay: 0
              },
              {
                column: "Week 2",
                period_pay: 0,
                benefits: 0,
                deductions: 0,
                net_pay: 0
              },
              {
                column: "Week 3",
                period_pay: 0,
                benefits: 0,
                deductions: 0,
                net_pay: 0
              },
              {
                column: "Week 4",
                period_pay: 0,
                benefits: 0,
                deductions: 0,
                net_pay: 0
              },
              {
                column: "Total",
                period_pay: 0,
                benefits: 0,
                deductions: 0,
                net_pay: 0
              }
            ]
          end
        end

        class PayslipDatesListing < Grape::Entity
          expose :id, :this_month
        end

        class PayslipDates < Grape::Entity
          expose :data, using: PayslipDatesListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

      end
    end
  end
end