module Entities
  module V1
    module Payroll
      module PhilHealth

        class PhilHealthListing < Grape::Entity
          expose  :id, :salary_bracket, :salary_range, :salary_base, :total_monthly_premium, :employee_share, :employer_share, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: PhilHealthListing

          private
          def data
            object[:data]
          end

        end

      end
    end
  end
end