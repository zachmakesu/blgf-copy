module Entities
  module V1
    module Payroll
      module SalarySchedule

        class SalaryScheduleForOthers < Grape::Entity
          expose  :salary_grade, :salary, :step_1 #step_1 here is used by them in prev site
        end

        class SalaryScheduleListing < Grape::Entity
          expose  :id, :salary_grade, :salary, :step_1, :step_2, :step_3, :step_4, :step_5, :step_6, :step_7, :step_8, :step_9, :step_10
        end

        class Index < Grape::Entity
          expose :data, using: SalaryScheduleListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

      end
    end
  end
end