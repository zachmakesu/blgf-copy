module Entities
  module V1
    module Payroll
      module TaxRange
        
        class TaxRangeListing < Grape::Entity
          expose  :id, :tax_base, :tax_deduction, :factor, :taxable_from, :taxable_to, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: TaxRangeListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class TaxRangeInfo < Grape::Entity
          expose :data, using: TaxRangeListing
          private
          def data
            object
          end
        end

      end
    end
  end
end