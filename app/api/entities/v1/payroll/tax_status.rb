module Entities
  module V1
    module Payroll
      module TaxStatus
        
        class TaxStatusListing < Grape::Entity
          expose  :id, :tax_status, :exemption_amount, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: TaxStatusListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class TaxStatusInfo < Grape::Entity
          expose :data, using: TaxStatusListing
          private
          def data
            object
          end
        end

      end
    end
  end
end