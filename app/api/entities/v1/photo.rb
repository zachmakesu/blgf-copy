module Entities
  module V1
    module Photo
      class Attr < Grape::Entity
        expose :id
        expose :photo, format_with: :original_photo_url
        def photo
          object
        end
      end
    end
  end
end