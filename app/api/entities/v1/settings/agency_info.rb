module Entities
  module V1
    module Settings
      module AgencyInfo

        class Index < Grape::Entity
          expose :data do
            expose  :agency_name, 
                    :agency_code, 
                    :region, 
                    :tin_number, 
                    :address,
                    :zip_code,
                    :telephone_number,
                    :facsimile,
                    :email,
                    :website,
                    :salary_schedule,
                    :gsis_number,
                    :gsis_employee_percent_share,
                    :gsis_employer_percent_share,    
                    :pagibig_number,
                    :pagibig_employee_percent_share,
                    :pagibig_employer_percent_share,
                    :privident_employee_percent_share,
                    :privident_employer_percent_share,
                    :philhealth_employee_percent_share,
                    :philhealth_employer_percent_share,
                    :philhealth_percentage,
                    :philhealth_number,
                    :mission,
                    :vision,
                    :mandate,
                    :bank_account_number, format_with: :format_if_nil
          end

          private
          def data
            object
          end

        end
      end
    end
  end
end