module Entities
  module V1
    module Settings
      module DirectoryContact
        class UserContacts < Grape::Entity
          expose :employee_id, :first_name, :last_name, :email, format_with: :format_if_nil
          expose :addresses, using: Entities::V1::Address::Index
          expose :contacts, using: Entities::V1::Contact::Index

          private
          def addresses
            object.addresses
          end

          def contacts
            object.contacts
          end
        end

        class DirectoryContactListing < Grape::Entity
          expose  :id
          expose  :contact, using: UserContacts

          private
          def contact
            object.user
          end
        end

        class Index < Grape::Entity
          expose :data, using: DirectoryContactListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class DirectoryContactInfo < Grape::Entity
          expose :data, using: DirectoryContactListing
          private
          def data
            object
          end
        end

      end
    end
  end
end