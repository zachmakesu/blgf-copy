module Entities
  module V1
    module Settings
      module LguName

        class LguNameListing < Grape::Entity
          expose  :id, :region, :province, :lgu_type, :name, :lgu_code, format_with: :format_if_nil
          # expose :lgu_seal, :lgu_background, format_with: :original_photo_url
          # expose  :coords do
          #   expose :latitude, :longitude
          # end

          private
            def region
              object.region.try(:name)
            end

            def province
              object.province.try(:name)
            end

            def lgu_type
              object.lgu_type.try(:name)
            end

            # def lgu_seal
            #   object.seal
            # end
            #
            # def lgu_background
            #   object.background
            # end
        end

        class Index < Grape::Entity
          expose :data, using: LguNameListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class LguNameInfo < Grape::Entity
          expose :data, using: LguNameListing
          private
          def data
            object
          end
        end

      end
    end
  end
end
