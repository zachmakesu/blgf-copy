module Entities
  module V1
    module Settings
      module LguType

          class LguTypeListing < Grape::Entity
            expose  :id, :name, format_with: :format_if_nil
          end

          class Index < Grape::Entity
            expose :data, using: LguTypeListing
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end

          end

          class LguTypeInfo < Grape::Entity
            expose :data, using: LguTypeListing
            private
            def data
              object
            end
          end

        end
    end
  end
end
