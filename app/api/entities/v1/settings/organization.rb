module Entities
  module V1
    module Settings
      module Organization

        class OrganizationListing < Grape::Entity
          expose :id, :name, :level, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: OrganizationListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end
      end
    end
  end
end