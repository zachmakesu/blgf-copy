module Entities
  module V1
    module Settings
      module OrganizationChild

        class OrganizationChildListing < Grape::Entity
          expose :id, :organization_id, :level_1, :level_2, :level_3, :code, :name, :head_title, :head, format_with: :format_if_nil
          expose :custodian, using: Entities::V1::Settings::UserAccount::UserFullNames

          private
          def custodian
            object.employee_custodians.present? ? object.employee_custodians : nil
          end

          def head
            if object.user.present?
              first_name = object.user.first_name.present? ? object.user.first_name.upcase : ""
              middle_initial = object.user.middle_initial.present? ? object.user.middle_initial.upcase : ""
              last_name = object.user.last_name.present? ? object.user.last_name.upcase : ""
              "#{first_name} #{middle_initial}. #{last_name}"
            else
              ""
            end
            
          end

          def level_1
            object.executive_office.try(:code)
          end

          def level_2
            object.service.try(:code)
          end

          def level_3
            object.division.try(:code)
          end
        end

        class Child < Grape::Entity
          expose :data, using: OrganizationChildListing
          
          private
          def data
            object
          end
        end

        class Index < Grape::Entity
          expose :data, using: OrganizationChildListing
          expose :total_rows, :total_results

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end
        end

      end
    end
  end
end