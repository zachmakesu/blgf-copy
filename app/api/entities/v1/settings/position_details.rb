module Entities
  module V1
    module Settings
      module PositionDetails
        
        class Index < Grape::Entity
          expose :data do
            expose :position_details do
              expose :service_code_id, :service_code, :first_day_gov, :first_day_agency, :mode_of_separation_id, :mode_of_separation, :separation_date, :appointment_status_id, :appointment_status, :job_category, :executive_office_id, :executive_office, :service_id, :service, :division_id, :division, :section_id, :section, :place_of_assignment, :salary_effective_date, :employment_basis, :category_service, :tax_status, :dependents_number, :personnel_action, format_with: :format_if_nil
            end
            
            expose :payroll do
              expose :payroll_group_id, :payroll_group, :include_dtr, :attendance_scheme_id, :attendance_scheme_name, :attendance_scheme_code, :health_insurance_exception, :include_in_payroll, :hazard_pay_factor, format_with: :format_if_nil
            end
            expose :plantilla_position do
              expose :item_number, :actual_salary, :plantilla_id, :plantilla_position_description, :position_date, :step_number, :salary_grade, :date_increment, :head_of_the_agency, format_with: :format_if_nil
            end
          end

          private
          #position details
          def service_code
            object.service_code.present? ? object.service_code.service_code : ""
          end

          def mode_of_separation
            object.mode_of_separation.present? ? object.mode_of_separation.separation_mode : ""
          end

          def appointment_status
            object.appointment_status.present? ? object.appointment_status.appointment_status : ""
          end

          def executive_office
            object.executive_office.present? ? object.executive_office.name : ""
          end

          def service
            object.service.present? ? object.service.name : ""
          end

          def division
            object.division.present? ? object.division.name : ""
          end

          def section
            object.section.present? ? object.section.name : ""
          end

          #payroll
          def payroll_group
            object.payroll_group.present? ? object.payroll_group.payroll_group_name : ""
          end

          def attendance_scheme_name
            object.attendance_scheme.present? ? object.attendance_scheme.scheme_name : ""
          end

          def attendance_scheme_code
            object.attendance_scheme.present? ? object.attendance_scheme.scheme_code : ""
          end

          #plantilla position
          def item_number
            object.plantilla.present? ? object.plantilla.item_number : ""
          end

          def salary_grade
            object.plantilla.present? ? object.plantilla.salary_schedule_grade : ""
          end

          def plantilla_position_description
            object.plantilla.present? ? (object.plantilla.position_code.present? ? object.plantilla.position_code.position_description : "") : ""
          end
        end
      end
    end
  end
end