module Entities
  module V1
    module Settings
      module Region

          class RegionListing < Grape::Entity
            expose  :id, :name, format_with: :format_if_nil
          end

          class Index < Grape::Entity
            expose :data, using: RegionListing
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end

          end

          class RegionInfo < Grape::Entity
            expose :data, using: RegionListing
            private
            def data
              object
            end
          end

        end
    end
  end
end
