module Entities
  module V1
    module Settings
      module UserAccount
        
        class UserFullNames < Grape::Entity
          expose :employee_id, :name, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo

          private
          def name
            "#{object.first_name} #{object.middle_initial} #{object.last_name}".split.map(&:capitalize).join(' ')
          end

          def profile_image
            object.avatar
          end
        end

        class UserAccountList < Grape::Entity
          expose :employee_id, :email, :first_name, :middle_name, :last_name, :gender, :role, :office_designation, :biometric_id, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo

          private
          def profile_image
            object.avatar
          end
        end

        class UserInfo < Grape::Entity
          expose :data, using: UserAccountList
          private
          def data
            object.present? ? object : nil
          end
        end

        class Index < Grape::Entity
          expose :data, using: UserAccountList
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class UserForHrDatabase < Grape::Entity
          expose :employee_id, :first_name, :last_name, :service_department, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo

          private
          def service_department
            object.position_detail.service.present? ? object.position_detail.service.name : nil
          end

          def profile_image
            object.avatar
          end
        end

        class UserForDTr < Grape::Entity
          expose :employee_id, :first_name, :last_name, :service_department, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo
          expose :dtr_date

          private
          def service_department
            object.position_detail.service.present? ? object.position_detail.service.name : nil
          end

          def profile_image
            object.avatar
          end

          def dtr_date
            object.attendance(options[:date]).present? ? object.attendance(options[:date]) : nil
          end
        end

        class UserInfoLogin < Grape::Entity
          expose :data do 
            expose :access_token, :employee_id, :email, :first_name, :middle_name, :last_name, :gender, :role, :office_designation, :biometric_id, format_with: :format_if_nil
            expose :profile_image, format_with: :original_photo_url
            expose :signature_image, format_with: :original_photo_url
            expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo
          end            

          private
          def profile_image
            object.avatar
          end

          def access_token
            options[:access_token]
          end
        end


      end
    end
  end
end