module Entities
  module V1
    module Settings
      module Zone

        class ZoneListing < Grape::Entity
          expose  :id, :zone_code, :description, :server_name, :username, :password, :database, format_with: :format_if_nil
        end

        class Index < Grape::Entity
          expose :data, using: ZoneListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class ZoneInfo < Grape::Entity
          expose :data, using: ZoneListing
          private
          def data
            object
          end
        end

      end
    end
  end
end