module Entities
  module V1
    module Treasurer
      module SalnRecord

        class SalnChild < Grape::Entity
          expose :id, :name, :birthdate, :age, format_with: :format_if_nil
        end

        class AssetsReal < Grape::Entity
          expose :id, :description, :kind, :exact_location, :assesed_value, :current_market_value, :acquisition_year, :acquisition_mode, :acquisition_cost, format_with: :format_if_nil
        end

        class AssetsPersonal < Grape::Entity
          expose :id, :description, :acquisition_year, :acquisition_cost, format_with: :format_if_nil
        end

        class Liability < Grape::Entity
          expose :id, :nature, :creditors_name, :outstanding_balance, format_with: :format_if_nil
        end

        class BiAndFc < Grape::Entity
          expose :id, :business_enterprise, :business_address, :business_financial_nature, :acquisition_date, format_with: :format_if_nil
        end

        class SalnGovernmentRelative < Grape::Entity
          expose :id, :name, :relationship, :position, :office_address, format_with: :format_if_nil
        end

        class SalnIssuedId < Grape::Entity
          expose :id, :id_type, :id_no, :date_issued, format_with: :format_if_nil
        end
        
        class Index < Grape::Entity
          expose :data do
            expose :treasurer_assignment, using: Entities::V1::Treasurer::TreasurerPdfFile::TreasurerAssignmentInfo
            expose :statement_filing_status, format_with: :format_if_nil
            expose :last_name, :first_name , :middle_initial, :address, :position, :office, :office_address, format_with: :format_if_nil
            expose :spouse_first_name, :spouse_last_name , :spouse_middle_initial, :spouse_position, :spouse_office, :spouse_office_address, format_with: :format_if_nil
            
            expose :saln_children, using: SalnChild

            expose :assets do 
              expose :assets_real, using: AssetsReal
              expose :assets_real_sub_total

              expose :assets_personal, using: AssetsPersonal
              expose :assets_personal_sub_total
            end
            expose :total_assets

            expose :liabilities, using: Liability
          
            expose :liabilities_sub_total

            expose :net_worth

            expose :saln_bi_and_fcs, using: BiAndFc

            expose :saln_government_relatives, using: SalnGovernmentRelative

            expose :saln_issued_ids, using: SalnIssuedId

          end
          
          def treasurer_assignment
            object.user.treasurer_assignment
          end

          def assets_real
            object.saln_assets.real
          end

          def assets_personal
            object.saln_assets.personal
          end

          def total_assets
            object.total_assets
          end

          def liabilities
            object.saln_liabilities
          end

          def saln_bi_and_fcs
            object.saln_bi_and_fcs
          end

        end

      end
    end
  end
end