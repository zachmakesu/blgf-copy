module Entities
  module V1
    module Treasurer
      module TreasurerPdfFile
        class TreasurerAssignmentInfo < Grape::Entity
          expose :treasurer_subrole, format_with: :format_if_nil
          expose :lgu_name, using: Entities::V1::Settings::LguName::LguNameListing
          private
          def lgu_name
            object.lgu_name.present? ? object.lgu_name : []
          end
        end
        
        class UserBasicInfo< Grape::Entity
          expose :employee_id, :biometric_id, :first_name, :last_name, :middle_name, :service_department, format_with: :format_if_nil
          expose :profile_image, format_with: :original_photo_url
          expose :signature_image, format_with: :original_photo_url
          expose :treasurer_assignment, using: TreasurerAssignmentInfo
          expose :pdf_files
          expose :date_hired, format_with: :format_if_nil
          expose :age, format_with: :format_if_nil
          expose :birthdate, format_with: :format_if_nil
          expose :gender, format_with: :format_if_nil
          expose :length_of_service_in_years, format_with: :format_if_nil
          expose :salary_grade, format_with: :format_if_nil
          expose :tin_number, format_with: :format_if_nil
          expose :gsis_policy_number, format_with: :format_if_nil
          expose :pagibig_id_number, format_with: :format_if_nil
          expose :philhealth_number, format_with: :format_if_nil
          expose :sss_number, format_with: :format_if_nil
          expose :remittance_id, format_with: :format_if_nil

          private
          def service_department
            object.position_detail.service.present? ? object.position_detail.service.name : nil
          end

          def profile_image
            object.avatar
          end

          def date_hired
            object.date_hired.present? ? object.date_hired : ""
          end

          def birthdate
            object.birthdate.present? ? object.birthdate : ""
          end

          def salary_grade
            object.salary_grade.present? ? object.salary_grade : ""
          end
          
        end

        class FileListng < Grape::Entity
          expose :id, :docket_management_file_id, :docket_management_file_title, :pdf_file_name, :pdf_download_link, format_with: :format_if_nil

          private

          def docket_management_file_title
            object.docket_management_file.present? ? object.docket_management_file.title : nil
          end
          def pdf_download_link
            object.pdf.url
          end
        end
        
        class Index < Grape::Entity
          expose :data, using: FileListng
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class PdfFileInfo < Grape::Entity
          expose :data, using: FileListng
          private
          def data
            object[:data]
          end
        end

        class TreasurerWithPdf < Grape::Entity
          expose :data, using: UserBasicInfo
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
            object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

      end
    end
  end
end