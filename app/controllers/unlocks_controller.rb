class UnlocksController < Devise::UnlocksController 
  skip_before_filter :authenticate_user! 

  def after_unlock_path_for(resource)
    reset_path
  end
end