module ApiHelper
  extend Grape::API::Helpers

  # TODO: Change references to utc
  # Why name format :utc if it returns :iso8601?
  Grape::Entity.format_with :utc do |date|
    date.to_datetime if date.present?
  end

  Grape::Entity.format_with :mdy do |date|
    date.to_datetime.strftime("%B %d, %Y") if date.present?
  end

  Grape::Entity.format_with :utc_time do |time|
    time.to_s(:time) if time.present?
  end

  Grape::Entity.format_with :date_announcement do |date|
    date.to_date if date.present?
  end

  Grape::Entity.format_with :convert_to_empty_string_if_nil do |obj|
    obj.present? ? obj : ""
  end

  Grape::Entity.format_with :format_if_nil do |datum|
    datum.nil? ?  "" : datum
  end

  ['original','thumb','medium','large','xlarge'].each do |size|
    Grape::Entity.format_with "#{size}_photo_url".to_sym do |photo|
      photo.present? ? photo.image.url(size).to_s : ""
    end
  end
 
  def authorize!(current_user, permission)
    error!({messages: '401 Unauthorized permission' },401) unless permission.key?(current_user.role)
  end

  def permited_params(model, model_fields)
    ActionController::Parameters.new(params).require(model).permit(model_fields)
  end

  def build_attachment(image)
    attachment =  {
      :filename => image[:filename],
      :type => image[:type],
      :headers => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end

  def build_attachment_file(file)
    attachment =  {
      :filename => file[:filename],
      :type => file[:type],
      :headers => file[:head],
      :tempfile => file[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end

  def ensure_date(date = nil) #yyyy-mm
    date = "#{Time.now.to_s[0,7]}" if date.blank? 
    y, m = date.split '-'
    d = 1
    if Date.valid_date? y.to_i, m.to_i, d.to_i
      Time.parse("#{date[0,7]}-01") >= Time.now ? "" : date[0,7]
    else
      ""
    end
  end
  
  def validate_year_format!(yyyy) #yyyy
    error!({messages: 'Invalid year format' },401) unless Date.strptime("#{yyyy.to_i}", "%Y").gregorian?
  end

  def validate_month_format!(mm) #yyyy
    error!({messages: 'Invalid month format' },401) unless (1..12).include?(mm.to_i)
  end

  def validate_date_format!(date) #yyyy-mm-dd
    y, m, d = date.split '-'
    error!({messages: 'Invalid date format it should be a valid yyyy-mm-dd' },401) unless Date.valid_date? y.to_i, m.to_i, d.to_i
  end

  def validate_time_format!(time) #hh:mm:ss
    error!({messages: 'Incomplete parameter' },401) if time.blank?

    date = (time =~ /^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/ )
    error!({messages: 'Invalid time format' },401) unless date
  end

  def validate_compensation_date!(date)
    error!({messages: 'Date should be less than current month' },401) unless date < Date.today.beginning_of_month
  end

  def validate_draft!(draft)
    error!({messages: 'Missing/Invalid parameters' },401) unless draft.present?
  end

  def validate_image_size!(image)
    error!({messages: 'Image should have a maximum of 1MB' },401) unless image["tempfile"].size <= 1000000
  end

end
