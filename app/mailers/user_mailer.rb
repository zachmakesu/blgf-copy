class UserMailer < ApplicationMailer
  default from: 'hrmis@blgf.com'

  def password_reset(user, password)
    @user = user
    @password = password
    mail(to: '#{@user.email}', subject: 'Password Reset')
  end

  def announcement_email(user, announcement)
    @user = user
    @announcement = announcement
    mail(to: '#{@user.email}', subject: 'HRMIS Announcement')
  end
end
