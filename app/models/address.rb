# == Schema Information
#
# Table name: addresses
#
#  contact_number :string
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  location       :text
#  residency      :integer
#  updated_at     :datetime         not null
#  user_id        :integer
#  zipcode        :string
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_48c9e0c5a2  (user_id => users.id)
#

class Address < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  #2nd scopes

  #3rd enums and others
  enum residency: { residential: 0, permanent: 1 }
  #4th callbacks

  #5th validations
  validates :user_id, :location, presence: true
end
