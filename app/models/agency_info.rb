# == Schema Information
#
# Table name: agency_infos
#
#  address                           :text
#  agency_code                       :string
#  agency_name                       :string
#  bank_account_number               :string
#  created_at                        :datetime         not null
#  email                             :string
#  facsimile                         :string
#  gsis_employee_percent_share       :float
#  gsis_employer_percent_share       :float
#  gsis_number                       :string
#  id                                :integer          not null, primary key
#  mandate                           :text
#  mission                           :text
#  pagibig_employee_percent_share    :float
#  pagibig_employer_percent_share    :float
#  pagibig_number                    :string
#  philhealth_employee_percent_share :float
#  philhealth_employer_percent_share :float
#  philhealth_number                 :string
#  philhealth_percentage             :float
#  privident_employee_percent_share  :float
#  privident_employer_percent_share  :float
#  region                            :string
#  salary_schedule                   :string
#  telephone_number                  :string
#  tin_number                        :string
#  updated_at                        :datetime         not null
#  vision                            :text
#  website                           :string
#  zip_code                          :string
#

class AgencyInfo < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
end
