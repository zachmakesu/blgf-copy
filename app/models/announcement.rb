# == Schema Information
#
# Table name: announcements
#
#  announcement_type :integer
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  message           :text
#  posted_at         :string
#  send_to           :string           default("all")
#  start_time_at     :string           default("00:00:00")
#  updated_at        :datetime         not null
#

class Announcement < ActiveRecord::Base
  #1st assotiations
  
  #2nd scopes

  #3rd enums and others

  enum announcement_type: { calendar: 0, web: 1}
  
  #4th callbacks

  #5th validations
  validates :announcement_type, :message, :posted_at, presence: true
    
   def self.read_by(role)
    if role == 'hr'
      all
    else
      where("send_to iLIKE :role or send_to iLIKE '%all%'", role: "%#{role}%")
    end
  end
end
