# == Schema Information
#
# Table name: appointment_statuses
#
#  appointment_code   :string
#  appointment_status :string
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  leave_entitled     :boolean          default(FALSE)
#  updated_at         :datetime         not null
#

class AppointmentStatus < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :appointment_code, :appointment_status
  validates_uniqueness_of :appointment_code

  def self.search(search)
    where('appointment_code iLIKE :search or appointment_status iLIKE :search', search: "%#{search}%").order(appointment_code: :asc)  
  end
end
