# == Schema Information
#
# Table name: archives
#
#  archive_date :string
#  created_at   :datetime         not null
#  field        :integer          default(0)
#  id           :integer          not null, primary key
#  info         :json
#  updated_at   :datetime         not null
#  user_id      :integer
#
# Indexes
#
#  index_archives_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_8503645e3f  (user_id => users.id)
#

class Archive < ActiveRecord::Base
  #1st assotiations
  belongs_to :user

  #2nd scopes

  #3rd enums and others
  enum field: {pds: 0, saln: 1 }

  #4th callbacks

  #5th validations

  def self.search(search = nil)
    date = Date.parse(search) rescue nil
    if date
      where(archive_date: date.to_date.to_s)
    else
      all
    end
  end

  def self.by_date(date = nil)
    date = Date.parse(date) rescue nil
    if date
      where(archive_date: date.to_date.to_s)
    else
      where(archive_date: nil)
    end
  end

  def self.search_by_employee(employee_id = nil)
    if employee_id
      includes(:user).where('users.employee_id iLIKE :employee_id', employee_id: "%#{employee_id}%")
    else
      all
    end
  end

  def self.distinct_date
    select("DISTINCT archive_date, archive_date").collect(&:archive_date)
  end

  def self.by_emp_lastname(sort = "asc")
    sort != "asc" ? order('users.last_name desc') : order('users.last_name asc')
  end
end
