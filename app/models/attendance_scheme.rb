# == Schema Information
#
# Table name: attendance_schemes
#
#  am_time_in_from   :string           default("00:00:00")
#  am_time_in_to     :string           default("00:00:00")
#  created_at        :datetime         not null
#  gp_late           :boolean          default(FALSE)
#  gp_leave_creadits :boolean          default(FALSE)
#  grace_period      :string           default("00:00:00")
#  hlf_late_und      :boolean          default(FALSE)
#  id                :integer          not null, primary key
#  nn_time_in_from   :string           default("00:00:00")
#  nn_time_in_to     :string           default("00:00:00")
#  nn_time_out_from  :string           default("00:00:00")
#  nn_time_out_to    :string           default("00:00:00")
#  over_time_ends    :string           default("00:00:00")
#  over_time_starts  :string           default("00:00:00")
#  pm_time_out_from  :string           default("00:00:00")
#  pm_time_out_to    :string           default("00:00:00")
#  scheme_code       :string           not null
#  scheme_name       :string           not null
#  scheme_type       :integer          default(0), not null
#  updated_at        :datetime         not null
#  wrkhr_leave       :string           default("00:00:00")
#

class AttendanceScheme < ActiveRecord::Base
  #1st assotiations
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks
  
  #5th validations
  validates :scheme_code, :scheme_name, :am_time_in_from, :am_time_in_to, :pm_time_out_from, :pm_time_out_to, :nn_time_out_from, :nn_time_out_to, :nn_time_in_from, :nn_time_in_to, presence: true
  validates :scheme_code, :scheme_name, uniqueness: true
  validate :correct_time_attr

  def self.search(search)
    where('scheme_code iLIKE :search or scheme_name iLIKE :search', search: "%#{search}%").order(scheme_code: :asc)  
  end

  def calculate_late(in_am, in_pm)
    #morning
    Time.parse(in_am) > Time.parse(self.am_time_in_to) ? morning = Time.parse(in_am) - Time.parse(self.am_time_in_to) : morning = 0
    
    #afternoon
    Time.parse(in_pm) > Time.parse(self.nn_time_out_to) ? afternoon = Time.parse(in_pm) - Time.parse(self.nn_time_out_to) : afternoon = 0 
    
    time_diff =  morning + afternoon
    if time_diff > 0 
      Time.at(time_diff.round.abs).utc.strftime "%H:%M:%S"
    else
      "00:00:00"
    end
  end

  def calculate_ut(in_am, out_am, out_pm)
    #morning
    Time.parse(out_am) < Time.parse(self.nn_time_in_from) ? morning = Time.parse(self.nn_time_in_from) - Time.parse(out_am) : morning = 0
    
    #afternoon
    if Time.parse(in_am) <= Time.parse(self.am_time_in_from)
      should_out_pm = Time.parse(self.am_time_in_from) + 9.hours
    elsif Time.parse(in_am) >= Time.parse(self.am_time_in_to) 
      should_out_pm = Time.parse(self.am_time_in_to) + 9.hours
    else
      should_out_pm = Time.parse(in_am) + 9.hours
    end

    Time.parse(out_pm) < should_out_pm ? afternoon = should_out_pm - Time.parse(out_pm) : afternoon = 0

    time_diff =  morning + afternoon
    if time_diff > 0
      Time.at(time_diff.round.abs).utc.strftime "%H:%M:%S"
    else
      "00:00:00"
    end
  end

  def correct_time_attr
    am_time_in_from  = Time.parse(self.am_time_in_from) rescue nil
    am_time_in_to    = Time.parse(self.am_time_in_to) rescue nil
    pm_time_out_to   = Time.parse(self.pm_time_out_to) rescue nil
    nn_time_out_from = Time.parse(self.nn_time_out_from) rescue nil
    nn_time_out_to   = Time.parse(self.nn_time_out_to) rescue nil
    nn_time_in_from  = Time.parse(self.nn_time_in_from) rescue nil
    nn_time_in_to    = Time.parse(self.nn_time_in_to) rescue nil

    self.errors.add(:base, "Invalid time input") if am_time_in_from.blank?
    self.errors.add(:base, "Invalid time am_time_in_to") if am_time_in_to.blank?
    self.errors.add(:base, "Invalid time pm_time_out_to") if pm_time_out_to.blank?
    self.errors.add(:base, "Invalid time nn_time_out_from") if nn_time_out_from.blank?
    self.errors.add(:base, "Invalid time nn_time_out_to") if nn_time_out_to.blank?
    self.errors.add(:base, "Invalid time nn_time_in_from") if nn_time_in_from.blank?
    self.errors.add(:base, "Invalid time nn_time_in_to") if nn_time_in_to.blank?
  end

  def self.dates(year=nil, month=nil)
    year ||= Date.today.year
    month ||= Date.today.month
    
    year = year.to_i
    month = month.to_i

    (Date.new(year, month)..Date.new(year, month).end_of_month).map {|d| d}
  end
end
