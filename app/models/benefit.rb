# == Schema Information
#
# Table name: benefits
#
#  benefit_type :integer          default(0), not null
#  code         :string           not null
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  updated_at   :datetime         not null
#

class Benefit < ActiveRecord::Base
  include BenefitAssociationGenerator
  #1st assotiations
  has_many :set_benefits, dependent: :destroy

  #2nd scopes

  #3rd enums and others
  enum benefit_type: {allowance: 0, bonus: 1, additional_income: 2 }
  #4th callbacks

  #5th validations
  validates :code, :benefit_type, presence: true
  validates :code, uniqueness: true
  
  def self.search(search)
    where('code iLIKE :search', search: "%#{search}%").order(code: :asc)   
  end
end
