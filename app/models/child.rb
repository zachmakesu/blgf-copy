# == Schema Information
#
# Table name: children
#
#  birthdate      :string
#  created_at     :datetime         not null
#  first_name     :string
#  id             :integer          not null, primary key
#  last_name      :string
#  middle_initial :string
#  middle_name    :string
#  relationship   :string
#  updated_at     :datetime         not null
#  user_id        :integer
#
# Indexes
#
#  index_children_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_a51d7cfb22  (user_id => users.id)
#

class Child < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :first_name, presence: true
  
end
