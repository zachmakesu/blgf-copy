module AssociationGenerator
  extend ActiveSupport::Concern

  included do
    after_create :create_dependencies
  end

  protected

  def create_dependencies
    OtherInformation.create(user: self)
    self.create_position_detail
    self.create_treasurer_assignment
    Saln.create(user: self, first_name: self.first_name, middle_initial: self.middle_initial, last_name: self.last_name)
    DeductionType.all.each { |deduction_type| self.deductives.create(deduction_type_id: deduction_type.id, amount: 0) }
    Benefit.all.each { |benefit| self.set_benefits.create(benefit_id: benefit.id, amount: 0) }
  end
end
