module BenefitAssociationGenerator
  extend ActiveSupport::Concern

  included do
    after_create :create_dependencies
  end

  protected

  def create_dependencies
    User.all.each { |user| UserSetBenefitsWorker.perform_async(user.id, self.id) }
  end
end
