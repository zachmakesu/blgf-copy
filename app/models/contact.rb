# == Schema Information
#
# Table name: contacts
#
#  created_at :datetime         not null
#  device     :integer
#  id         :integer          not null, primary key
#  number     :string
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_contacts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_8d2134e55e  (user_id => users.id)
#

class Contact < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  #2nd scopes

  #3rd enums and others
  enum device: { mobile: 0, telephone: 1 }
  
  #4th callbacks

  #5th validations
  validates :user_id, :number, presence: true
end
