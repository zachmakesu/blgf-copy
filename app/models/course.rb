# == Schema Information
#
# Table name: courses
#
#  course_code        :string
#  course_description :string
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  updated_at         :datetime         not null
#

class Course < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :course_code, :course_description
  validates_uniqueness_of :course_code
  def self.search(search)
    where('course_code iLIKE :search or course_description iLIKE :search', search: "%#{search}%").order(course_code: :asc)  
  end
end
