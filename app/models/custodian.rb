# == Schema Information
#
# Table name: custodians
#
#  created_at            :datetime         not null
#  id                    :integer          not null, primary key
#  organization_child_id :integer
#  updated_at            :datetime         not null
#  user_id               :integer
#

class Custodian < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :organization_child, class_name: "OrganizationChild", foreign_key: "organization_child_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :organization_child_id, presence: true
  validates  :user_id, uniqueness: { scope: :organization_child_id }
end
