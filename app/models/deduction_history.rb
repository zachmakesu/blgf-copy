# == Schema Information
#
# Table name: deduction_histories
#
#  created_at   :datetime         not null
#  date_applied :string           not null
#  id           :integer          not null, primary key
#  updated_at   :datetime         not null
#  user_id      :integer          not null
#

class DeductionHistory < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  has_many :deduction_history_children, dependent: :destroy

  #2nd scopes
  scope :date_applied, -> (date){ where('date_applied iLike :search', search: "%#{date}%") }

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :user_id, :date_applied, presence: true
  validates :date_applied, uniqueness: { scope: :user_id }
end
