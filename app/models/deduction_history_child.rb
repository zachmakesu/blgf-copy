# == Schema Information
#
# Table name: deduction_history_children
#
#  amount               :integer          default(0), not null
#  created_at           :datetime         not null
#  deduction_history_id :integer
#  deduction_type_id    :integer          not null
#  id                   :integer          not null, primary key
#  updated_at           :datetime         not null
#

class DeductionHistoryChild < ActiveRecord::Base
	#1st assotiations
  belongs_to :deduction_history, class_name: "DeductionHistory", foreign_key: "deduction_history_id"
  belongs_to :deduction_type, class_name: "DeductionType", foreign_key: "deduction_type_id"
  
  #2nd scopes

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :deduction_history_id, :deduction_type_id, :amount, presence: true
  validates :deduction_type_id, uniqueness: { scope: :deduction_history_id }
end
