# == Schema Information
#
# Table name: deduction_types
#
#  created_at                  :datetime         not null
#  deduction                   :string           not null
#  deduction_base_calculations :string           default("None"), not null
#  deduction_code              :string           not null
#  id                          :integer          not null, primary key
#  mandatory                   :boolean          default(FALSE)
#  updated_at                  :datetime         not null
#

class DeductionType < ActiveRecord::Base
  DEDUCTIVE_CALCULATIONS = %w{ None WitholdingTax PAGIBIG GSISRetirement PhilHealth }
  include DeductiveTypeAssociationGenerator
  #1st assotiations
  has_many :deductives, dependent: :destroy

  #2nd scopes

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :deduction_code, :deduction, presence: true
  validates :deduction_code, uniqueness: true
  validates :deduction_base_calculations, inclusion: { in: DEDUCTIVE_CALCULATIONS , message: "%{value} is not a valid base calculation." }

  def self.search(search)
    where('deduction_code iLIKE :search or deduction iLIKE :search', search: "%#{search}%").order(mandatory: :desc, deduction_code: :asc)  
  end
end
