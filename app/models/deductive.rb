# == Schema Information
#
# Table name: deductives
#
#  amount            :float            default(0.0)
#  created_at        :datetime         not null
#  deduction_type_id :integer          not null
#  id                :integer          not null, primary key
#  overwrite         :boolean          default(FALSE)
#  updated_at        :datetime         not null
#  user_id           :integer          not null
#

class Deductive < ActiveRecord::Base
  #1st assotiations
  GSIS_RETIREMENT_PECENTAGE = 0.07
  MAXIMUM_PAGIBIG_MONTHLY_COMPENSATION = 5000
  PAGIBIG_PERCENTAGE = [0.01, 0.02]
  BIR_TAX_TABLE = []
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :deduction_type, class_name: "DeductionType", foreign_key: "deduction_type_id"

  #2nd scopes
  
  #3rd enums and others
  
  #4th callbacks
  before_save :ensure_amount
  #5th validations
  validates :user_id, :deduction_type_id, :amount, presence: true
  validates :deduction_type_id, uniqueness: { scope: :user_id }

  def generate_calculations
    case self.deduction_type.deduction_base_calculations
      when "None" then 0
      when "WitholdingTax" then self.calculate_tax_monthly
      when "PAGIBIG" then self.calculate_pagibig
      when "GSISRetirement" then self.calculate_gsis_retirement
      when "PhilHealth" then self.calculate_philhealth
      else 0
    end
  end

  def calculate_tax_monthly
    tax_status = self.user.position_detail.tax_status
    actual_salary = self.user.position_detail.actual_salary.to_f

    if actual_salary <= 0
      0
    else
      taxable_income = actual_salary + self.user.set_benefits.sum(:amount) - self.user.deductives.where.not(deduction_type_id: self.deduction_type_id).sum(:amount)
      TaxGeneratorHandler.new(taxable_income, tax_status, "monthly").generate_tax
    end
  end

  def calculate_pagibig
    actual_salary = self.user.position_detail.actual_salary.to_f
    actual_salary > 0 && actual_salary < MAXIMUM_PAGIBIG_MONTHLY_COMPENSATION ? actual_salary * PAGIBIG_PERCENTAGE[0] : actual_salary * PAGIBIG_PERCENTAGE[1]
  end

  def calculate_philhealth
    PhilHealthTable.get_employee_share(self.user.position_detail.actual_salary)
  end

  def calculate_gsis_retirement
    self.user.position_detail.actual_salary.to_f * GSIS_RETIREMENT_PECENTAGE
  end

  private
  def ensure_amount
    self.amount = self.generate_calculations unless self.overwrite 
  end
end
