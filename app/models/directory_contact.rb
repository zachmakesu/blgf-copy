# == Schema Information
#
# Table name: directory_contacts
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_directory_contacts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_8bb7af016c  (user_id => users.id)
#

class DirectoryContact < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  #2nd scopes

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :user_id, presence: true, uniqueness: true

  def self.search(search)
    includes({user: [:addresses, :contacts]}).where('users.first_name iLIKE :search OR users.last_name iLIKE :search', search: "%#{search}%").order("users.first_name ASC") 
  end

end
