# == Schema Information
#
# Table name: docket_management_files
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  title      :string           not null
#  updated_at :datetime         not null
#

class DocketManagementFile < ActiveRecord::Base
  #1st assotiations
  has_many :pdf_files

  #2nd scopes
  
  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :title, presence: true
  validates :title, uniqueness: true
  def self.search(search)
    where('title iLIKE :search', search: "%#{search}%").order(title: :asc)  
  end
end
