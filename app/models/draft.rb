# == Schema Information
#
# Table name: drafts
#
#  classification :string
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  object         :text
#  owner_id       :integer
#  processed_at   :datetime
#  processor_id   :integer
#  remarks        :text
#  status         :integer          default(0)
#  updated_at     :datetime         not null
#

class Draft < ActiveRecord::Base
  #1st assotiations
  belongs_to :owner, class_name: "User"
  belongs_to :processor, class_name: "User"
  
  #2nd scopes
  scope :except_self, -> (user) { where.not(owner_id: user.id ) }
  scope :for_specific_processor, -> (user) { where(processor_id: user.id ) }
  scope :per_roles, -> (roles) { joins(:owner).where(users: {role: roles}) }

  scope :pending_contacts, -> { where(status: 0, classification: ["update_contact","add_contact"]) }
  scope :pending_update_delete_contacts, -> { where(status: 0, classification: ["update_contact", "delete_contact"]) }

  scope :pending_addresses, -> { where(status: 0, classification: ["update_address","add_address"]) }
  scope :pending_update_delete_addresses, -> { where(status: 0, classification: ["update_address", "delete_address"]) }

  scope :pending_educations, -> { where(status: 0, classification: ["update_education","add_education"]) }
  scope :pending_update_delete_educations, -> { where(status: 0, classification: ["update_education", "delete_education"]) }

  scope :pending_examinations, -> { where(status: 0, classification: ["update_examination","add_examination"]) }
  scope :pending_update_delete_examinations, -> { where(status: 0, classification: ["update_examination", "delete_examination"]) }

  scope :pending_work_experiences, -> { where(status: 0, classification: ["update_work_experience","add_work_experience"]) }
  scope :pending_update_delete_experiences, -> { where(status: 0, classification: ["update_work_experience", "delete_work_experience"]) }

  scope :pending_voluntary_works, -> { where(status: 0, classification: ["update_voluntary_work","add_voluntary_work"]) }
  scope :pending_update_delete_voluntary_works, -> { where(status: 0, classification: ["update_voluntary_work", "delete_voluntary_work"]) }

  scope :pending_trainings_and_seminars, -> { where(status: 0, classification: ["update_training_and_seminar","add_training_and_seminar"]) }
  scope :pending_update_delete_trainings_and_seminars, -> { where(status: 0, classification: ["update_training_and_seminar", "delete_training_and_seminar"]) }

  scope :pending_children, -> { where(status: 0, classification: ["update_child","add_child"]) }
  scope :pending_update_delete_children, -> { where(status: 0, classification: ["update_child", "delete_child"]) }

  scope :pending_references, -> { where(status: 0, classification: ["update_reference","add_reference"]) }
  scope :pending_update_delete_references, -> { where(status: 0, classification: ["update_reference", "delete_reference"]) }


  #3rd enums and others
  serialize :object
  CLASSIFICATIONS = %w( update_profile_data update_profile_image update_signature_image add_contact update_contact delete_contact add_address update_address delete_address add_education update_education delete_education add_examination update_examination delete_examination add_work_experience update_work_experience delete_work_experience add_voluntary_work update_voluntary_work delete_voluntary_work update_family_information add_child update_child delete_child add_training_and_seminar update_training_and_seminar delete_training_and_seminar update_other_information add_reference update_reference delete_reference)
  RESIDENCIES = %w( residential permanent)
  DEVICES = %w( mobile telephone)
  enum status: { pending: 0, approved: 1, denied: 2 }

  #4th callbacks
  after_destroy :destroy_draft_image
  #5th validations
  validates :classification, inclusion: { in: CLASSIFICATIONS }
  validates :owner, :object, :status, presence: true

  after_validation(on: :create) do
    unless self.owner.nil? || self.owner.drafts.pending.blank?
      if self.classification == "add_contact" || self.classification == "add_address" || self.classification == "add_education" || self.classification == "add_examination" || self.classification == "add_work_experience" || self.classification == "add_voluntary_work" || self.classification == "add_child" || self.classification == "add_training_and_seminar"  || self.classification == "add_reference"
        #do nothing
      elsif self.classification == "update_contact" || self.classification == "delete_contact"
        existing = self.owner.drafts.pending_update_delete_contacts.select {|draft| draft.classification == self.classification && draft.object.contact_id == self.object.contact_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.contact_id}.") if existing.present?
      
      elsif self.classification == "update_address" || self.classification == "delete_address"
        existing = self.owner.drafts.pending_update_delete_addresses.select {|draft| draft.classification == self.classification && draft.object.address_id == self.object.address_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.address_id}.") if existing.present?

      elsif self.classification == "update_education" || self.classification == "delete_education"
        existing = self.owner.drafts.pending_update_delete_educations.select {|draft| draft.classification == self.classification && draft.object.education_id == self.object.education_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.education_id}.") if existing.present?

      elsif self.classification == "update_examination" || self.classification == "delete_examination"
        existing = self.owner.drafts.pending_update_delete_examinations.select {|draft| draft.classification == self.classification && draft.object.examination_id == self.object.examination_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.examination_id}.") if existing.present?

      elsif self.classification == "update_work_experience" || self.classification == "delete_work_experience"
        existing = self.owner.drafts.pending_update_delete_experiences.select {|draft| draft.classification == self.classification && draft.object.work_experience_id == self.object.work_experience_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.work_experience_id}.") if existing.present?

      elsif self.classification == "update_voluntary_work" || self.classification == "delete_voluntary_work"
        existing = self.owner.drafts.pending_update_delete_voluntary_works.select {|draft| draft.classification == self.classification && draft.object.voluntary_work_id == self.object.voluntary_work_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.voluntary_work_id}.") if existing.present?

      elsif self.classification == "update_training_and_seminar" || self.classification == "delete_training_and_seminar"
        existing = self.owner.drafts.pending_update_delete_trainings_and_seminars.select {|draft| draft.classification == self.classification && draft.object.training_and_seminar_id == self.object.training_and_seminar_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.training_and_seminar_id}.") if existing.present?

      elsif self.classification == "update_child" || self.classification == "delete_child"
        existing = self.owner.drafts.pending_update_delete_children.select {|draft| draft.classification == self.classification && draft.object.child_id == self.object.child_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.child_id}.") if existing.present?

      elsif self.classification == "update_reference" || self.classification == "delete_reference"
        existing = self.owner.drafts.pending_update_delete_references.select {|draft| draft.classification == self.classification && draft.object.reference_id == self.object.reference_id}
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")} id #{self.object.reference_id}.") if existing.present?

      else
        self.errors.add(:base, "Already have pending draft for #{self.classification.gsub("_"," ")}.") if self.owner.drafts.pending.map(&:classification).uniq.include?(self.classification)
      end
    end  
  end

  before_validation do
    if !self.object.nil? && self.object.is_a?(Hash) && self.object.keys.uniq.map(&:to_s).sort != send("#{self.classification}_valid_object_params").uniq.sort
      self.errors.add(:base, "Invalid object for specific classification")
    end

    if self.classification == "update_profile_data"
      self.errors.add(:base, "missing first_name") if object[:first_name].blank?
      self.errors.add(:base, "missing last_name") if object[:last_name].blank?
      self.errors.add(:base, "missing email") if object[:email].blank?

      if object[:email].present?
        self.errors.add(:base, "invalid email") if !is_a_valid_email?(object[:email])
      end

    elsif self.classification == "update_profile_image"

    elsif self.classification == "update_signature_image"

    elsif self.classification == "add_contact" || self.classification == "update_contact"
      self.errors.add(:base, "missing device") if object[:device].blank?
      self.errors.add(:base, "missing number") if object[:number].blank?
      if object[:device].present?
        self.errors.add(:base, "invalid device") if !DEVICES.include?(object[:device])
      end

    elsif self.classification == "add_address" || self.classification == "update_address"
      self.errors.add(:base, "missing residency") if object[:residency].blank?
      self.errors.add(:base, "missing location") if object[:location].blank?

      if object[:device].present?
        self.errors.add(:base, "invalid residency") if !RESIDENCIES.include?(object[:residency])
      end

    elsif self.classification == "add_education" || self.classification == "update_education"
      self.errors.add(:base, "missing education_level_id") if object[:education_level_id].blank?

    elsif self.classification == "add_examination" || self.classification == "update_examination"
      self.errors.add(:base, "missing exam_description") if object[:exam_description].blank?

    elsif self.classification == "add_work_experience" || self.classification == "update_work_experience"
      self.errors.add(:base, "missing appointment_status_id") if object[:appointment_status_id].blank?
      self.errors.add(:base, "missing position_title") if object[:position_title].blank?

    elsif self.classification == "add_voluntary_work" || self.classification == "update_voluntary_work"
      self.errors.add(:base, "missing name_of_organization") if object[:name_of_organization].blank?
      self.errors.add(:base, "missing number_of_hours") if object[:number_of_hours].blank?
      self.errors.add(:base, "missing position_nature_of_work") if object[:position_nature_of_work].blank?

    elsif self.classification == "update_family_information"
      #do nothing
      
    elsif self.classification == "add_child" || self.classification == "update_child"
      self.errors.add(:base, "missing first_name") if object[:first_name].blank?
    
    elsif self.classification == "add_reference" || self.classification == "update_reference"
      self.errors.add(:base, "missing name") if object[:name].blank?
      self.errors.add(:base, "missing address") if object[:address].blank?

    elsif self.classification == "add_training_and_seminar" || self.classification == "update_training_and_seminar"
      self.errors.add(:base, "missing title") if object[:title].blank?
      self.errors.add(:base, "missing number_of_hours") if object[:number_of_hours].blank?
      self.errors.add(:base, "missing conducted_by") if object[:conducted_by].blank?

    elsif self.classification == "update_other_information"
      object[:third_degree_national] == "true" ? (self.errors.add(:base, "Please give particulars for third degree") if object.give_national.blank?) : nil
      object[:fourth_degree_local] == "true" ? (self.errors.add(:base, "Please give particulars for fourth degree") if object.give_local.blank?) : nil
      object[:formally_charge] == "true" ? (self.errors.add(:base, "Please give formally charge offense") if object.give_charge.blank?) : nil
      object[:administrative_offense] == "true" ? (self.errors.add(:base, "Please give details of offense") if object.give_offense.blank?) : nil
      object[:any_violation] == "true" ? (self.errors.add(:base, "Please give reasons") if object.give_reasons.blank?) : nil
      object[:canditate] == "true" ? (self.errors.add(:base, "Please give date and particulars") if object.give_date_particulars.blank?) : nil
      object[:indigenous_member] == "true" ? (self.errors.add(:base, "Please give group") if object.give_group.blank?) : nil
      object[:differently_abled] == "true" ? (self.errors.add(:base, "Please give disability") if object.give_disability.blank?) : nil
      object[:solo_parent] == "true" ? (self.errors.add(:base, "Please give status for solo parent") if object.give_status.blank?) : nil
      object[:is_separated] == "true" ? (self.errors.add(:base, "Please give details on separation") if object.give_details.blank?) : nil
    else
      # do nothing
    end
  end

  def is_a_valid_email?(email)
    (email =~ /\A[^@\s]+@([^@\s\.]+\.)+[^@\s\.]+\z/)
  end

  def destroy_draft_image
    if self.classification == "update_profile_image" || self.classification == "update_signature_image"
      photo_id = self.object["image_id"]
      Photo.find(photo_id).destroy
    end
  end

  def notification_classification
    self.classification.gsub(/_/," ")
  end

  private
  def update_profile_data_valid_object_params
    %w( first_name last_name middle_name middle_initial name_extension birthdate gender civil_status citizenship height weight blood_type email tin_number gsis_policy_number pagibig_id_number philhealth_number sss_number remittance_id )
  end

  def update_profile_image_valid_object_params
    %w( image_id image_url )
  end

  def update_signature_image_valid_object_params
    %w( image_id image_url )
  end

  #CONTACTS
  def add_contact_valid_object_params
    %w( device number )
  end
  def update_contact_valid_object_params
    %w( contact_id device number )
  end
  def delete_contact_valid_object_params
    %w( contact_id device number )
  end

  #ADDRESS
  def add_address_valid_object_params
    %w( residency location zipcode contact_number )
  end
  def update_address_valid_object_params
    %w( address_id residency location zipcode contact_number )
  end
  def delete_address_valid_object_params
    %w( address_id residency location zipcode contact_number )
  end

  #EDUCATION
  def add_education_valid_object_params
    %w( education_level_id school_name course_id highest_grade_level_units_earned date_of_attendance_from date_of_attendance_to honors_received year_graduated )
  end
  def update_education_valid_object_params
    %w( education_id education_level_id school_name course_id highest_grade_level_units_earned date_of_attendance_from date_of_attendance_to honors_received year_graduated )
  end  
  def delete_education_valid_object_params
    %w( education_id education_level_id school_name course_id highest_grade_level_units_earned date_of_attendance_from date_of_attendance_to honors_received year_graduated )
  end

  #EXAMINATION
  def add_examination_valid_object_params
    %w( exam_description place_of_exam rating date_of_exam licence_number date_of_release )
  end
  def update_examination_valid_object_params
    %w( examination_id exam_description place_of_exam rating date_of_exam licence_number date_of_release )
  end  
  def delete_examination_valid_object_params
    %w( examination_id exam_description place_of_exam rating date_of_exam licence_number date_of_release )
  end

  #WORK EXPERIENCE
  def add_work_experience_valid_object_params
    %w( position_title inclusive_date_from inclusive_date_to department_agency_office monthly_salary salary_grade_and_step appointment_status_id government_service )
  end
  def update_work_experience_valid_object_params
    %w( work_experience_id position_title inclusive_date_from inclusive_date_to department_agency_office monthly_salary salary_grade_and_step appointment_status_id government_service )
  end
  def delete_work_experience_valid_object_params
    %w( work_experience_id position_title inclusive_date_from inclusive_date_to department_agency_office monthly_salary salary_grade_and_step appointment_status_id government_service )
  end

  #VOLUNTARY WORK
  def add_voluntary_work_valid_object_params
    %w( name_of_organization address_of_organization inclusive_date_from inclusive_date_to number_of_hours position_nature_of_work )
  end
  def update_voluntary_work_valid_object_params
    %w( voluntary_work_id name_of_organization address_of_organization inclusive_date_from inclusive_date_to number_of_hours position_nature_of_work )
  end
  def delete_voluntary_work_valid_object_params
    %w( voluntary_work_id name_of_organization address_of_organization inclusive_date_from inclusive_date_to number_of_hours position_nature_of_work )
  end

  #FAMILY INFO
  def update_family_information_valid_object_params
    %w( spouse_name spouse_occupation spouse_employer_or_business_name spouse_work_or_business_address spouse_contact_number parent_fathers_name parent_mothers_maiden_name parent_address spouse_first_name spouse_middle_name spouse_last_name parant_fathers_first_name parant_fathers_middle_name parant_fathers_last_name parant_mothers_first_name parant_mothers_middle_name parant_mothers_last_name )
  end

  #CHILD
  def add_child_valid_object_params
    %w( first_name last_name middle_name middle_initial relationship birthdate )
  end
  def update_child_valid_object_params
    %w( child_id first_name last_name middle_name middle_initial relationship birthdate )
  end
  def delete_child_valid_object_params
    %w( child_id first_name last_name middle_name middle_initial relationship birthdate )
  end

  #TRAINING AND SEMINAR
  def add_training_and_seminar_valid_object_params
    %w( title inclusive_date_from inclusive_date_to number_of_hours conducted_by )
  end
  def update_training_and_seminar_valid_object_params
    %w( training_and_seminar_id title inclusive_date_from inclusive_date_to number_of_hours conducted_by )
  end
  def delete_training_and_seminar_valid_object_params
    %w( training_and_seminar_id title inclusive_date_from inclusive_date_to number_of_hours conducted_by )
  end

  #OTHER INFO
  def update_other_information_valid_object_params
    %w( skills_and_hobbies non_academic_destinction membership_in third_degree_national give_national fourth_degree_local give_local formally_charge give_charge administrative_offense give_offense any_violation give_reasons canditate give_date_particulars indigenous_member give_group differently_abled give_disability solo_parent give_status is_separated give_details signature date_accomplished ctc_number issued_at issued_on )
  end

  #REFERENCE
  def add_reference_valid_object_params
    %w( name address contact_number )
  end
  def update_reference_valid_object_params
    %w( reference_id name address contact_number )
  end
  def delete_reference_valid_object_params
    %w( reference_id name address contact_number )
  end

  def self.search(search)
    where('classification iLIKE :search', search: "%#{search}%").order(classification: :asc)
  end
end
