# == Schema Information
#
# Table name: duty_and_responsibilities
#
#  created_at       :datetime         not null
#  duties           :string
#  id               :integer          not null, primary key
#  percent_work     :float
#  position_code_id :integer          not null
#  updated_at       :datetime         not null
#

class DutyAndResponsibility < ActiveRecord::Base
  #1st assotiations
  belongs_to :position_code

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :position_code_id, :percent_work, :duties

  def self.search(search)
    includes(:position_code).where('duties iLIKE :search', search: "%#{search}%").order("position_codes.position_description ASC") 
  end
end
