# == Schema Information
#
# Table name: educations
#
#  course_id                        :integer
#  created_at                       :datetime         not null
#  date_of_attendance_from          :string
#  date_of_attendance_to            :string
#  education_level_id               :integer
#  highest_grade_level_units_earned :string
#  honors_received                  :text
#  id                               :integer          not null, primary key
#  school_name                      :string
#  updated_at                       :datetime         not null
#  user_id                          :integer
#  year_graduated                   :string
#
# Indexes
#
#  index_educations_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_f18eeff57c  (user_id => users.id)
#

#
#  index_educations_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_f18eeff57c  (user_id => users.id)
#

class Education < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  belongs_to :education_level, class_name: "EducationLevel", foreign_key: "education_level_id"
  belongs_to :course, class_name: "Course", foreign_key: "course_id"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user, :education_level_id, presence: true
end
