# == Schema Information
#
# Table name: education_levels
#
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  level_code        :string
#  level_description :string
#  updated_at        :datetime         not null
#

class EducationLevel < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :level_code, :level_description
  validates_uniqueness_of :level_code
  
  def self.search(search)
    where('level_code iLIKE :search or level_description iLIKE :search', search: "%#{search}%").order(level_code: :asc)  
  end
end
