# == Schema Information
#
# Table name: employee_dtrs
#
#  attendance_scheme_id_used :integer          default(0), not null
#  biometric_id              :string           not null
#  created_at                :datetime         not null
#  dtr_date                  :string           default("0000-00-00"), not null
#  editdate                  :string
#  id                        :integer          not null, primary key
#  in_am                     :string           default("00:00:00"), not null
#  in_ot                     :string           default("00:00:00"), not null
#  in_pm                     :string           default("00:00:00"), not null
#  ip                        :string
#  is_edited                 :boolean          default(FALSE)
#  late                      :string           default("00:00:00"), not null
#  name                      :string
#  old_value                 :string
#  ot                        :string           default("00:00:00"), not null
#  other_info                :string
#  out_am                    :string           default("00:00:00"), not null
#  out_ot                    :string           default("00:00:00"), not null
#  out_pm                    :string           default("00:00:00"), not null
#  perdiem                   :string
#  processed_by              :integer
#  remarks                   :string
#  updated_at                :datetime         not null
#  ut                        :string           default("00:00:00"), not null
#

class EmployeeDtr < ActiveRecord::Base
  #1st assotiations
  serialize :old_value
  belongs_to :user, class_name: "User", foreign_key: "processed_by"

  #2nd scopes

  #3rd enums and others

  #4th callbacks
  before_save :ensure_other_attrs

  #5th validations
  validates :dtr_date, uniqueness: { scope: :biometric_id }

  def ensure_other_attrs # to ensure attrs, User must be created first before saving its DTR
    employee = User.find_by(biometric_id: self.biometric_id)
    if employee
      attendance_scheme = AttendanceScheme.find(employee.position_detail.attendance_scheme_id)
      self.attendance_scheme_id_used = attendance_scheme.id
       
      #self.ot = inprogress
      self.late = attendance_scheme.calculate_late(self.in_am, self.in_pm)
      self.ut = attendance_scheme.calculate_ut(self.in_am, self.out_am, self.out_pm)
    end
  end

  private

  def self.create_dummy_dtr(biometric_id, date)
    dtr = {
    biometric_id: biometric_id,
    dtr_date: date,
    in_am: "07:00:00",
    out_am: "12:00:10",
    in_pm: "12:00:30",
    out_pm: "16:00:00"
    }
    EmployeeDtr.create(dtr)
  end

  def self.days_of(month, year) #month int, year int 
    Time.days_in_month(month, year)
  end

  def is_weekday?
    [0, 6, 7].include?(wday)
  end

  def self.generate_attendances_monthly(biometric_id,date)
    date = date.to_date
    dates = (date.at_beginning_of_month..date.at_end_of_month).select { |day| !day.saturday? && !day.sunday? }

    dates.each { |date| EmployeeDtr.create_dummy_dtr(biometric_id, date) }
  end
end
