# == Schema Information
#
# Table name: exam_types
#
#  created_at       :datetime         not null
#  csc_eligible     :boolean          default(FALSE)
#  exam_code        :string
#  exam_description :string
#  id               :integer          not null, primary key
#  updated_at       :datetime         not null
#

class ExamType < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :exam_code, :exam_description
  validates_uniqueness_of :exam_code

  def self.search(search)
    where('exam_code iLIKE :search or exam_description iLIKE :search', search: "%#{search}%").order(exam_code: :asc)  
  end
end
