# == Schema Information
#
# Table name: examinations
#
#  created_at       :datetime         not null
#  date_of_exam     :string
#  date_of_release  :string
#  exam_description :string
#  exam_type_id     :integer
#  id               :integer          not null, primary key
#  licence_number   :string
#  place_of_exam    :string
#  rating           :float
#  updated_at       :datetime         not null
#  user_id          :integer
#

class Examination < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :exam_description, :place_of_exam, presence: true

end
