# == Schema Information
#
# Table name: fts_requests
#
#  biometric_id :string           not null
#  created_at   :datetime         not null
#  dtr_date     :string           default("0000-00-00"), not null
#  id           :integer          not null, primary key
#  in_am        :string           default("00:00:00"), not null
#  in_ot        :string           default("00:00:00"), not null
#  in_pm        :string           default("00:00:00"), not null
#  out_am       :string           default("00:00:00"), not null
#  out_ot       :string           default("00:00:00"), not null
#  out_pm       :string           default("00:00:00"), not null
#  owner_id     :integer          not null
#  processor_id :integer
#  remarks      :string
#  status       :integer          default(0), not null
#  updated_at   :datetime         not null
#

class FtsRequest < ActiveRecord::Base
	#1st assotiations
  belongs_to :owner, class_name: "User", foreign_key: "owner_id"
  belongs_to :processor, class_name: "User", foreign_key: "processor_id"

  #2nd scopes

  #3rd enums and others
  enum status: { pending: 0, approved: 1, denied: 2 }

  #4th callbacks
  
  #5th validations
  validates :owner_id, :biometric_id, :dtr_date, :in_am, :in_pm, :out_am, :out_pm, presence: true 
 
  def self.search(search)
    where('dtr_date iLIKE :search or remarks iLIKE :search', search: "%#{search}%").order(created_at: :desc)  
  end

  def employee_dtr
    EmployeeDtr.where("biometric_id = ? AND dtr_date = ?", self.biometric_id, self.dtr_date)
  end
end
