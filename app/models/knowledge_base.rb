# == Schema Information
#
# Table name: knowledge_bases
#
#  code        :string           not null
#  created_at  :datetime         not null
#  description :string
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#

class KnowledgeBase < ActiveRecord::Base

  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :code, :description
  validates_uniqueness_of :code

  def self.search(search)
    where('code iLIKE :search or description iLIKE :search', search: "%#{search}%").order(code: :asc)  
  end

end
