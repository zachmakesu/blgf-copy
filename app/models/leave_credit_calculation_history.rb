# == Schema Information
#
# Table name: leave_credit_calculation_histories
#
#  calculation_date :string           not null
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  proccessed_by    :integer          not null
#  updated_at       :datetime         not null
#

class LeaveCreditCalculationHistory < ActiveRecord::Base
  #1st assotiations

  belongs_to :user, class_name: "User", foreign_key: "processed_by"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :calculation_date, :proccessed_by, presence: true
end
