# == Schema Information
#
# Table name: leave_credit_histories
#
#  created_at       :datetime         not null
#  created_by       :integer
#  credit_type      :integer          not null
#  credits          :float            default(0.0)
#  deductible       :boolean          not null
#  id               :integer          not null, primary key
#  leave_request_id :integer
#  updated_at       :datetime         not null
#  user_id          :integer          not null
#

class LeaveCreditHistory < ActiveRecord::Base
  MINUTES_TO_LEAVE_CREDITS = [[1,	0.002],	[2,	0.004],	[3,	0.006],	[4,	0.008],	[5,	0.010],	[6,	0.012],	[7,	0.015],	[8,	0.017],	[9,	0.019],	[10,	0.021],	[11,	0.023],	[12,	0.025],	[13,	0.027],	[14,	0.029],	[15,	0.031],	[16,	0.033],	[17,	0.035],	[18,	0.037],	[19,	0.040],	[20,	0.042],	[21,	0.044],	[22,	0.046],	[23,	0.048],	[24,	0.050],	[25,	0.052],	[26,	0.054],	[27,	0.056],	[28,	0.058],	[29,	0.060],	[30,	0.062],	[31,	0.065],[32,	0.067],[33,	0.069],[34,	0.071],[35,	0.073],[36,	0.075],[37,	0.077],[38,	0.079],[39,	0.081],[40,	0.083],[41,	0.085],[42,	0.087],[43,	0.090],[44,	0.092],[45,	0.094],[46,	0.096],[47,	0.098],[48,	0.100],[49,	0.102],[50,	0.104],[51,	0.106],[52,	0.108],[53,	0.110],[54,	0.112],[55,	0.115],[56,	0.117],[57,	0.119],[58,	0.121],[59,	0.123],[60,	0.125]]

  HOURS_TO_LEAVE_CREDITS = [[1,	0.125], [2,	0.250], [3,	0.375], [4,	0.500], [5,	0.625], [6,	0.750], [7,	0.875], [8,	1.000]]
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :admin_user, class_name: "User", foreign_key: "created_by"
  belongs_to :leave_request, class_name: "LeaveRequest", foreign_key: "leave_request_id"
  
  #2nd scopes
  scope :deductibles, -> { where(deductible: true) }
  scope :to_be_deducted, -> { where(deductible: false) }

  #3rd enums and others
  enum credit_type: { sick_leaves: 0, vacation_leaves: 1, cto_leaves: 2}

  #4th callbacks
  before_save :ensure_remove_credits_when_exceeds_limit
  #5th validations
  validates :deductible, inclusion: { in: [ true, false ] }
  validates :user_id, presence: true

  def ensure_remove_credits_when_exceeds_limit
    owner = self.user

    if self.deductible
      if self.credit_type == "sick_leaves"
        self.credits = owner.sick_leave_credits_left_count if owner.sick_leave_credits_left_count <= self.credits
      elsif self.credit_type == "vacation_leaves"
        self.credits = owner.vacation_leave_credits_left_count if owner.vacation_leave_credits_left_count <= self.credits
      elsif self.credit_type == "cto_leaves"
        self.credits = owner.cto_leave_credits_left_count if owner.cto_leave_credits_left_count <= self.credits
      else
        #do nothing
      end
    end

  end

end
