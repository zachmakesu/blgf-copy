# == Schema Information
#
# Table name: leave_requests
#
#  applied_days       :float
#  created_at         :datetime         not null
#  date_from          :string
#  date_to            :string
#  day_type           :integer
#  deductible_credits :float            default(0.0)
#  half_date          :string
#  id                 :integer          not null, primary key
#  leave_type         :integer
#  owner_id           :integer
#  processor_id       :integer
#  reason             :text
#  remarks            :string
#  status             :integer          default(0)
#  updated_at         :datetime         not null
#

class LeaveRequest < ActiveRecord::Base
  #1st assotiations
  belongs_to :owner, class_name: "User", foreign_key: "owner_id"
  belongs_to :processor, class_name: "User", foreign_key: "processor_id"

  has_one   :leave_credit_history

  has_many :notifications, as: :notificationable, dependent: :destroy
  
  #2nd scopes
  scope :except_self, -> (user) { where.not(owner_id: user.id ) }

  #3rd enums and others
  enum status: { pending: 0, approved: 1, denied: 2 }
  enum day_type: { whole_day: 0, half_day: 1 }
  enum leave_type: { sick_leave: 0, vacation_leave: 1 }

  #4th callbacks
  after_initialize :ensure_deductible_credits
  before_save :ensure_deductible_credits

  #5th validations
  validates :owner_id, :status, presence: true
  validate :validate_dates

  after_validation(on: :create) do
    self.errors.add(:base, "Already have pending leave request") if self.owner.leave_requests.pending.present?
  end

  def self.search(search)
    where('date_from iLIKE :search or date_to iLIKE :search or half_date iLIKE :search or remarks iLIKE :search', search: "%#{search}%").order(created_at: :desc)  
  end

  def validate_dates
    if self.day_type == "whole_day"
      self.errors.add(:base, "Invalid date from") if self.date_from.blank?
      self.errors.add(:base, "Invalid date to") if self.date_to.blank?
      
      if self.date_from && self.date_to
        self.errors.add(:base, "Date from should be less than date to") if self.date_from.to_date > self.date_to.to_date
      end
    else
      self.errors.add(:base, "Invalid half day date") if self.half_date.blank?
    end
  end

  def ensure_deductible_credits
    if self.half_date.present?
      self.deductible_credits = 0.5
    else
      self.deductible_credits = ((self.date_to.to_date.mjd - self.date_from.to_date.mjd) + 1)
    end
  end

end
