# == Schema Information
#
# Table name: leave_types
#
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  leave_code     :string
#  leave_type     :string
#  leave_type_id  :integer
#  no_of_days     :integer          default(0)
#  parent         :boolean          not null
#  specific_leave :string
#  updated_at     :datetime         not null
#

class LeaveType < ActiveRecord::Base
  #1st assotiations
  belongs_to :parent_leave_type, class_name: "LeaveType", foreign_key: "leave_type_id"

  #2nd scopes
  scope :specific_leaves, -> { where(parent: false) }
  scope :no_specific_leaves, -> { where(parent: true) }

  #3rd enums and others

  #4th callbacks
  before_save :ensure_attr

  #5th validations
  validates :parent, inclusion: { in: [ true, false ] }
  validates :leave_code, :leave_type, presence: true, if: lambda { self.parent? }
  validates :specific_leave, :leave_type_id, presence: true, if: lambda { !self.parent? }

  validates :leave_code, uniqueness: true, if: lambda { self.parent? }

  def ensure_attr
    if self.parent?
      self.specific_leave = nil
      self.leave_type_id = nil
      self.no_of_days = 0 if self.no_of_days.blank?
    else
      self.leave_code = nil
      self.leave_type = nil
      self.leave_type = 0
      self.no_of_days = 0 if self.no_of_days.blank?
    end
  end

  def self.search(search)
    where('leave_code iLIKE :search or leave_type iLIKE :search', search: "%#{search}%").order(leave_code: :asc)  
  end
end
