# == Schema Information
#
# Table name: legal_cases
#
#  case_number      :integer          not null
#  charged_offenses :string
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  status           :text
#  title            :text
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_legal_cases_on_case_number  (case_number)
#

class LegalCase < ActiveRecord::Base
  #Associations
  belongs_to :user
  
  #Validations
  validates :case_number, presence: true, uniqueness: true
  validates :user_id, presence: true
  
  def self.of_user(user_id)
    #query users legal cases using inner join
    joins(:user).where("legal_cases.user_id = ?", user_id)
  end

  def self.search(search)
    where('title iLIKE :search or case_number::text iLIKE :search', search: "%#{search}%").order(title: :asc)  
  end
end
