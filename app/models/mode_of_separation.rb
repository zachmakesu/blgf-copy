# == Schema Information
#
# Table name: mode_of_separations
#
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  separation_mode :string
#  updated_at      :datetime         not null
#

class ModeOfSeparation < ActiveRecord::Base
  #1st assotiations
  has_many  :position_details
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :separation_mode
  validates_uniqueness_of :separation_mode

  def self.search(search)
    where('separation_mode iLIKE :search', search: "%#{search}%").order(separation_mode: :asc)  
  end
end
