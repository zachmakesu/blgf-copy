# == Schema Information
#
# Table name: monthly_salary_computations
#
#  authorized_salary_this_month  :float            not null
#  benefits_this_month           :text             not null
#  created_at                    :datetime         not null
#  deductions_this_month         :text             not null
#  id                            :integer          not null, primary key
#  include_in_dtr_this_month     :boolean          default(FALSE), not null
#  include_in_payroll_this_month :boolean          default(FALSE), not null
#  late_this_month               :string
#  leave_credits_this_month      :text             not null
#  over_time_this_month          :string
#  plantilla_position_this_month :string
#  sg_this_month                 :integer
#  this_month                    :string           not null
#  under_time_this_month         :string
#  updated_at                    :datetime         not null
#  user_id                       :integer
#

class MonthlySalaryComputation < ActiveRecord::Base
  #1st assotiations
  belongs_to :user

  #2nd scopes
  scope :included_in_dtr, -> { where(include_in_payroll_this_month: true) }
  scope :with_in_this_date, -> (date){ where(this_month: date) }
  #3rd enums and others
  serialize :deductions_this_month, JSON
  serialize :benefits_this_month, JSON
  serialize :leave_credits_this_month, JSON

  #4th callbacks

  #5th validations
  validates_presence_of :user_id, :sg_this_month, :this_month, :leave_credits_this_month, :authorized_salary_this_month

  validates  :user_id, uniqueness: { scope: :this_month }

  def self.search(search)
    joins(:user).where('users.employee_id iLIKE :search or users.biometric_id iLIKE :search or users.first_name iLIKE :search or users.last_name iLIKE :search or users.employee_id iLIKE :search', search: "%#{search}%").order("users.first_name ASC")
  end

  def self.by_month(month = nil)
    if month
      where('this_month iLIKE :month', month: "%#{month}%")
    else
      all
    end
  end

  def deductions_amount_total_this_month
    self.deductions_this_month.map {|s| s['amount']}.reduce(0, :+)
  end

  def allowances_amount_total_this_month #allowances only
    self.benefits_this_month.map {|s| s['amount']}.reduce(0, :+)
  end

  def net_salary_this_month
    self.authorized_salary_this_month + self.allowances_amount_total_this_month - self.deductions_amount_total_this_month
  end

  def get_payslip_form
    #Authorized salary = Authorized Salary of employee base on its Salary Grade.
    #Days = Last month total Days
    #
    #Total deductible leave credits = Late/ Absent/ UT that is converted to deductible Leave Credits(float)
    #Check employee if its lc is enough or not. 
    #total deductible leave cretits

    #weekly_tdlc check if user has no LC and has absent UT and late
    weekly_compensation(weekly_authorized, weekly_deductions, net_pay)
  end

  def weekly_authorized
    (self.authorized_salary_this_month + self.allowances_amount_total_this_month) / 4  
  end

  def weekly_deductions
    (self.deductions_amount_total_this_month / 4) + weekly_tdlc
  end

  def net_pay
    weekly_authorized - weekly_deductions
  end

  def weekly_tdlc
    y, m = self.this_month.split '-'
    days = EmployeeDtr.days_of(m.to_i,y.to_i)
    tdlc = 0 #it should be employee.tdlc.present? tldc = employee.tdlc : tldc = 0
    ((authorized_salary_this_month / days) * tdlc) / 4
  end


  def weekly_compensation(weekly_authorized, weekly_deductions, net_pay)
    [
      {
        column: "Week 1",
        period_pay: weekly_authorized,
        benefits: 0,
        deductions: weekly_deductions,
        net_pay: net_pay
      },
      {
        column: "Week 2",
        period_pay: weekly_authorized,
        benefits: 0,
        deductions: weekly_deductions,
        net_pay: net_pay
      },
      {
        column: "Week 3",
        period_pay: weekly_authorized,
        benefits: 0,
        deductions: weekly_deductions,
        net_pay: net_pay
      },
      {
        column: "Week 4",
        period_pay: weekly_authorized,
        benefits: allowances_amount_total_this_month,
        deductions: weekly_deductions,
        net_pay: net_pay + allowances_amount_total_this_month
      },
      {
        column: "Total",
        period_pay: authorized_salary_this_month,
        benefits: allowances_amount_total_this_month,
        deductions: weekly_deductions,
        net_pay: net_salary_this_month
      }
    ]
  end

  def update_monthly_benefits_and_deductions
    generated_deductives = self.user.generate_deductions_this_month
    generated_benefits = self.user.generate_benefits_this_month
    self.update(deductions_this_month: generated_deductives, benefits_this_month: generated_benefits)
  end
end
