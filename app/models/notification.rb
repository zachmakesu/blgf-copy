# == Schema Information
#
# Table name: notifications
#
#  api_slug              :string
#  created_at            :datetime         not null
#  id                    :integer          not null, primary key
#  message               :text
#  notificationable_id   :integer
#  notificationable_type :string
#  notified_by_id        :integer
#  read                  :boolean          default(FALSE)
#  read_by               :integer          not null
#  updated_at            :datetime         not null
#  user_id               :integer
#
# Indexes
#
#  index_notifications_on_notified_by_id  (notified_by_id)
#  index_notifications_on_user_id         (user_id)
#

class Notification < ActiveRecord::Base
  # #1st assotiations
  belongs_to :user
  # #2nd scopes
  scope :after_user_created, -> (user) { where(created_at: user.created_at..DateTime.now.end_of_day) }
  # #3rd enums and others
  enum read_by: { all_user: 0, hr: 1, employee: 2, treasurer: 3, can_recieve_all: 4 }

  # #4th callbacks

  # #5th validations

  def self.search(search)
    where('message iLIKE :search or api_slug iLIKE :search', search: "%#{search}%")
  end
end
