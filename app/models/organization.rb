# == Schema Information
#
# Table name: organizations
#
#  created_at :datetime         not null
#  deleted_at :string
#  id         :integer          not null, primary key
#  level      :integer
#  name       :string
#  updated_at :datetime         not null
#

class Organization < ActiveRecord::Base
  #1st assotiations
  has_many :organization_children

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :name, :level
  validates_uniqueness_of :level

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(level: :asc)  
  end
  
end
