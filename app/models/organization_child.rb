# == Schema Information
#
# Table name: organization_children
#
#  code                :string
#  created_at          :datetime         not null
#  custodian           :text
#  division_id         :integer
#  executive_office_id :integer
#  head_title          :string
#  id                  :integer          not null, primary key
#  name                :string
#  organization_id     :integer
#  service_id          :integer
#  updated_at          :datetime         not null
#  user_id             :integer
#
# Indexes
#
#  index_organization_children_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_d8608c53e3  (organization_id => organizations.id)
#

class OrganizationChild < ActiveRecord::Base 
  #1st assotiations
  belongs_to :organization
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :executive_office, class_name: "OrganizationChild", foreign_key: "executive_office_id"
  belongs_to :service, class_name: "OrganizationChild", foreign_key: "service_id"
  belongs_to :division, class_name: "OrganizationChild", foreign_key: "division_id"
  has_many :custodians
  has_many :employee_custodians, through: :custodians, source: :user

  #2nd scopes

  #3rd enums and others

  #4th callbacks
  before_save :ensure_child_attr

  #5th validations
  validates_presence_of :organization_id, :code, :name
  validates :code, uniqueness: true
  validate :presence_of_child_attr

  def self.search(search)
    where('name iLIKE :search or code iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end

  private
  def presence_of_child_attr
    if self.organization_id == 4
      self.errors.add(:base, "Invalid executive_office_id, service_id or division_id") if (self.executive_office_id.blank? || self.service_id.blank? || self.division_id.blank?)
    elsif self.organization_id == 3
      self.errors.add(:base, "Invalid executive_office_id or service_id") if (self.executive_office_id.blank? || self.service_id.blank?)
    elsif self.organization_id == 2
      self.errors.add(:base, "Invalid executive_office_id") if self.executive_office_id.blank?
    else
      #do nothing.
    end

  end

  def ensure_child_attr
    if self.organization_id == 1
      self.executive_office_id = nil
      self.service_id = nil
      self.division_id = nil
    elsif self.organization_id == 2
      self.service_id = nil
      self.division_id = nil
    elsif self.organization_id == 3
      self.division_id = nil
    else
      #do nothing.
    end
  end
end
