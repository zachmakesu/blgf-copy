# == Schema Information
#
# Table name: other_informations
#
#  administrative_offense   :boolean          default(FALSE)
#  any_violation            :boolean          default(FALSE)
#  canditate                :boolean          default(FALSE)
#  created_at               :datetime         not null
#  ctc_number               :string
#  date_accomplished        :string
#  differently_abled        :boolean          default(FALSE)
#  formally_charge          :boolean          default(FALSE)
#  fourth_degree_local      :boolean          default(FALSE)
#  give_charge              :text
#  give_date_particulars    :text
#  give_details             :text
#  give_disability          :text
#  give_group               :text
#  give_local               :text
#  give_national            :text
#  give_offense             :text
#  give_reasons             :text
#  give_status              :text
#  id                       :integer          not null, primary key
#  indigenous_member        :boolean          default(FALSE)
#  is_separated             :boolean          default(FALSE)
#  issued_at                :string
#  issued_on                :string
#  membership_in            :string
#  non_academic_destinction :text
#  signature                :string
#  skills_and_hobbies       :text
#  solo_parent              :boolean          default(FALSE)
#  third_degree_national    :boolean          default(FALSE)
#  updated_at               :datetime         not null
#  user_id                  :integer
#

class OtherInformation < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks
  before_save :ensure_attr

  #5th validations
  validates :user_id, presence: true
  validate :presence_of_attr

  def presence_of_attr
    self.third_degree_national ? (self.errors.add(:base, "Please give particulars for third degree") if self.give_national.blank?) : nil
    self.fourth_degree_local ? (self.errors.add(:base, "Please give particulars for fourth degree") if self.give_local.blank?) : nil
    self.formally_charge ? (self.errors.add(:base, "Please give formally charge offense") if self.give_charge.blank?) : nil
    self.administrative_offense ? (self.errors.add(:base, "Please give details of offense") if self.give_offense.blank?) : nil
    self.any_violation ? (self.errors.add(:base, "Please give reasons") if self.give_reasons.blank?) : nil
    self.canditate ? (self.errors.add(:base, "Please give date and particulars") if self.give_date_particulars.blank?) : nil
    self.indigenous_member ? (self.errors.add(:base, "Please give group") if self.give_group.blank?) : nil
    self.differently_abled ? (self.errors.add(:base, "Please give disability") if self.give_disability.blank?) : nil
    self.solo_parent ? (self.errors.add(:base, "Please give status for solo parent") if self.give_status.blank?) : nil
    self.is_separated ? (self.errors.add(:base, "Please give details on separation") if self.give_details.blank?) : nil
  end

  def ensure_attr
    self.give_national = nil if !self.third_degree_national
    self.give_local = nil if !self.fourth_degree_local
    self.give_charge = nil if !self.formally_charge
    self.give_offense = nil if !self.administrative_offense
    self.give_reasons = nil if !self.any_violation
    self.give_date_particulars = nil if !self.canditate
    self.give_group = nil if !self.indigenous_member
    self.give_disability = nil if !self.differently_abled
    self.give_status = nil if !self.solo_parent
    self.give_details = nil if !self.is_separated
  end
end
