# == Schema Information
#
# Table name: payroll_groups
#
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  payroll_group_code  :string
#  payroll_group_name  :string
#  payroll_group_order :integer
#  project_code_id     :integer
#  updated_at          :datetime         not null
#

class PayrollGroup < ActiveRecord::Base
  #1st assotiations
  belongs_to :project_code, class_name:"ProjectCode", foreign_key: :project_code_id

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :project_code_id, :payroll_group_code, :payroll_group_name, presence: true
  validates :payroll_group_code, :payroll_group_order, uniqueness: true

  def self.search(search)
    where('payroll_group_code iLIKE :search or payroll_group_name iLIKE :search', search: "%#{search}%").order(payroll_group_code: :asc)  
  end
  
end
