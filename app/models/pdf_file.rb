# == Schema Information
#
# Table name: pdf_files
#
#  created_at                :datetime         not null
#  docket_management_file_id :integer
#  id                        :integer          not null, primary key
#  pdf_content_type          :string
#  pdf_file_name             :string
#  pdf_file_size             :integer
#  pdf_updated_at            :datetime
#  pdfable_id                :integer
#  pdfable_type              :string
#  updated_at                :datetime         not null
#

class PdfFile < ActiveRecord::Base
	#1st assotiations
  belongs_to :pdfable, polymorphic: true
  belongs_to :docket_management_file, class_name: "DocketManagementFile", foreign_key: "docket_management_file_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  has_attached_file :pdf
  validates_attachment_content_type :pdf, :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document), :message => 'not allowed.'

  validates :pdf, presence: {:on => :create} 
  validates :docket_management_file_id, uniqueness: { scope: :pdfable_id }, allow_nil: true


  def self.search(search)
    joins(:docket_management_file).where('docket_management_files.title iLIKE :search ', search: "%#{search}%").order("docket_management_files.title ASC")  
  end
end
