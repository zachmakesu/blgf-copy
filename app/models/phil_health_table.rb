# == Schema Information
#
# Table name: phil_health_tables
#
#  created_at            :datetime         not null
#  employee_share        :float            not null
#  employer_share        :float            not null
#  id                    :integer          not null, primary key
#  salary_base           :float
#  salary_bracket        :integer          not null
#  salary_range          :string
#  total_monthly_premium :float            not null
#  updated_at            :datetime         not null
#

class PhilHealthTable < ActiveRecord::Base
	#1st assotiations

  #2nd scopes

  #3rd enums and others
  
  #4th callbacks
  before_save :ensure_total_month_premium
  before_save :ensure_attributes

  #5th validations
  validates :salary_bracket, :salary_range, :salary_base, :employee_share, :employer_share, :total_monthly_premium, presence: true
  validates :salary_bracket, uniqueness: true

  def ensure_total_month_premium
  	self.total_monthly_premium = self.employee_share + self.employer_share
  end

  def ensure_attributes
    self.employee_share = 0 if self.employee_share.blank?
    self.employer_share = 0 if self.employer_share.blank?
    self.salary_base = 0 if self.salary_base.blank?
  end

  def self.get_employee_share(salary_base)
    philhealth = self.find_by(salary_base: salary_base.round(-3))
    if philhealth.blank?
      philhealth = salary_base.round(-3) > self.maximum("salary_base") ? self.find_by(salary_base: self.maximum("salary_base")) : self.find_by(salary_base: self.minimum("salary_base"))
    end
    philhealth.employee_share
  end
end
