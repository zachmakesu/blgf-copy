# == Schema Information
#
# Table name: photos
#
#  created_at         :datetime         not null
#  default            :boolean          default(FALSE), not null
#  default_type       :integer          not null
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  position           :integer          not null
#  updated_at         :datetime         not null
#

class Photo < ActiveRecord::Base
  #1st assotiations
  belongs_to :imageable, polymorphic: true
  
  #2nd scopes
  scope :default, ->{ where(default: true, default_type: 0).first }

  #3rd enums and others
  enum default_type: { employee: 0, signature: 1, lgu_name: 2}

  #4th callbacks

  #5th validations
  has_attached_file :image, styles: { xlarge: "1000x1000>" , large: "800x800>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :image,:content_type => /^image\/(jpg|jpeg|png)$/, :message => 'not allowed.'

  validates :image, presence: {:on => :create} 
end
