# == Schema Information
#
# Table name: plantillas
#
#  area_code             :string
#  area_type             :string
#  created_at            :datetime         not null
#  csc_eligibility       :boolean          default(FALSE)
#  id                    :integer          not null, primary key
#  item_number           :string
#  plantilla_group_id    :integer
#  position_code_id      :integer
#  rationalized          :boolean          default(FALSE)
#  salary_schedule_grade :integer
#  updated_at            :datetime         not null
#

class Plantilla < ActiveRecord::Base
  #1st assotiations
  belongs_to :position_code, class_name: "PositionCode", foreign_key: "position_code_id"
  belongs_to :plantilla_group, class_name: "PlantillaGroup", foreign_key: "plantilla_group_id"
  belongs_to :salary_schedule, class_name: "SalarySchedule", foreign_key: "salary_schedule_grade", primary_key: "salary_grade"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :item_number, :salary_schedule_grade, :position_code_id, :plantilla_group_id,  presence: true
  validates :item_number, uniqueness: true

  def self.except_used
    where.not(id: PositionDetail.all.map(&:plantilla_id).compact)
  end

  def self.search(search)
    where('item_number iLIKE :search', search: "%#{search}%").order(item_number: :asc)  
  end
end
