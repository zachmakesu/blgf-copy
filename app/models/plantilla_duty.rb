# == Schema Information
#
# Table name: plantilla_duties
#
#  created_at   :datetime         not null
#  duties       :string
#  id           :integer          not null, primary key
#  percent_work :float
#  plantilla_id :integer
#  updated_at   :datetime         not null
#

class PlantillaDuty < ActiveRecord::Base
  #1st assotiations
  belongs_to :plantilla, class_name: "Plantilla", foreign_key: "plantilla_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :plantilla_id, :duties, :percent_work, presence: true

  def self.search(search)
    includes(:plantilla).where('plantillas.item_number iLIKE :search', search: "%#{search}%").order("plantillas.item_number ASC")
  end
end
