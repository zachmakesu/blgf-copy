# == Schema Information
#
# Table name: plantilla_groups
#
#  created_at  :datetime         not null
#  group_code  :string
#  group_name  :string
#  group_order :float
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#

class PlantillaGroup < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :group_code, :group_order, :group_name
  validates_uniqueness_of :group_code, :group_order
  validates :group_order, numericality: { greater_than: 0 }
  
  def self.search(search)
    where('group_code iLIKE :search or group_name iLIKE :search', search: "%#{search}%").order(group_code: :asc)  
  end
end
