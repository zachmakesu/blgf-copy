# == Schema Information
#
# Table name: position_codes
#
#  created_at              :datetime         not null
#  educational_requirement :text
#  eligibility_requirement :text
#  experience_requirement  :text
#  id                      :integer          not null, primary key
#  position_abbreviation   :string
#  position_classification :string
#  position_code           :string
#  position_description    :text
#  training_requirement    :text
#  updated_at              :datetime         not null
#

class PositionCode < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :position_code, :position_description
  validates_uniqueness_of :position_code

  def self.search(search)
    where('position_code iLIKE :search or position_description iLIKE :search', search: "%#{search}%").order(position_code: :asc)  
  end


  def self.test_import
    require "csv"
    require "pry"
    @csv_file = File.open(File.join(Rails.root, "/db/test_imports/test_csv_import.csv"))
    total_rows = @csv_file.readlines.size

    
    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @group_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @group_name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @group_order = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.to_i : nil

      # binding.pry
      # process_row if @group_code && @group_name && @group_order

      # @file.update(total_processed_rows: $.)
      # @file.reload
      # @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
  end
end
