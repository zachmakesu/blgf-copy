# == Schema Information
#
# Table name: position_details
#
#  appointment_status_id      :integer
#  attendance_scheme_id       :integer          default(1)
#  category_service           :integer          default(0)
#  created_at                 :datetime         not null
#  date_increment             :string
#  dependents_number          :integer          default(0)
#  division_id                :integer
#  employment_basis           :integer          default(0)
#  executive_office_id        :integer
#  first_day_agency           :string
#  first_day_gov              :string
#  hazard_pay_factor          :float            default(0.0)
#  head_of_the_agency         :boolean          default(FALSE)
#  health_insurance_exception :boolean          default(FALSE)
#  id                         :integer          not null, primary key
#  include_dtr                :boolean          default(FALSE)
#  include_in_payroll         :boolean          default(FALSE)
#  job_category               :string
#  mode_of_separation_id      :integer
#  payroll_group_id           :integer
#  personnel_action           :string
#  place_of_assignment        :string
#  plantilla_id               :integer
#  position_date              :string
#  salary_effective_date      :string
#  section_id                 :integer
#  separation_date            :string
#  service_code_id            :integer
#  service_id                 :integer
#  step_number                :integer          default(1)
#  tax_status                 :string           default("Z")
#  updated_at                 :datetime         not null
#  user_id                    :integer
#

class PositionDetail < ActiveRecord::Base
  JOB_CATEGORIES = ["Audit & Taxation","Banking/Financial","Corporate Finance/Investment","General/Cost Accounting","Clerical/Administrative","Human Resources","Secretarial","Top Management","Advertising","Arts/Creative Design","Entertainment","Public Relations","Architect/Interior Design","Civil Engineering/Construction","Property/Real Estate","Quantity Surveying","IT - Hardware","IT - Network/Sys/DB Admin","IT - Software","Education","Training & Dev","Chemical Engineering","Electrical Engineering","Electronics Engineering","Environmental Engineering","Industrial Engineering","Mechanical/Automotive Engineering","Oil/Gas Engineering","Other Engineering","Doctor/Diagnosis","Pharmacy","Nurse/Medical Support","Food/Beverage/Restaurant","Hotel/Tourism","Maintenance","Manufacturing","Process Design & Control","Purchasing/Material Mgmt","Quality Assurance","Sales - Corporate","Marketing/Business Dev","Merchandising","Retail Sales","Sales - Eng/Tech/IT","Sales - Financial Services","Telesales/Telemarketing","Actuarial/Statistics","Agriculture","Aviation","Biotechnology","Chemistry","Food Tech/Nutritionist","Geology/Geophysics","Science & Technology","Security/Armed Forces","Customer Service","Logistics/Supply Chain","Law/Legal Services","Personal Care","Social Services","Tech & Helpdesk Support","General Work","Journalist/Editors","Publishing","Others"]
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  #position details
  belongs_to :service_code, class_name: "ServiceCode", foreign_key: "service_code_id"
  belongs_to :mode_of_separation, class_name: "ModeOfSeparation", foreign_key: "mode_of_separation_id"
  belongs_to :appointment_status, class_name: "AppointmentStatus", foreign_key: "appointment_status_id"
  belongs_to :executive_office, class_name: "OrganizationChild", foreign_key: "executive_office_id"
  belongs_to :service, class_name: "OrganizationChild", foreign_key: "service_id"
  belongs_to :division, class_name: "OrganizationChild", foreign_key: "division_id"
  belongs_to :section, class_name: "OrganizationChild", foreign_key: "section_id"

  #payroll group
  belongs_to :payroll_group, class_name: "PayrollGroup", foreign_key: "payroll_group_id"

  #plantilla_position
  belongs_to :plantilla, class_name: "Plantilla", foreign_key: "plantilla_id"

  #attendance_scheme
  belongs_to :attendance_scheme, class_name: "AttendanceScheme", foreign_key: "attendance_scheme_id"
  #2nd scopes

  #3rd enums and others
  enum category_service: { career: 0, non_career: 1 }
  enum employment_basis: { full_time: 0, part_time: 1 }

  #4th callbacks

  #5th validations
  validates :head_of_the_agency, inclusion: { in: [ true, false ] }
  validates :health_insurance_exception, inclusion: { in: [ true, false ] }
  validates :include_dtr, inclusion: { in: [ true, false ] }
  validates :include_in_payroll, inclusion: { in: [ true, false ] }

  validates :user_id, presence: true
  validates :user_id, uniqueness: true
  validates :plantilla_id, uniqueness: true, allow_nil: true

  def actual_salary
    self.plantilla.present? && self.step_number.present? ? self.plantilla.salary_schedule.send("step_#{self.step_number}") : 0
  end
end
