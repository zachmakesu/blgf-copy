# == Schema Information
#
# Table name: project_codes
#
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  project_code        :string
#  project_description :string
#  project_order       :integer
#  updated_at          :datetime         not null
#

class ProjectCode < ActiveRecord::Base
  #1st assotiations
  has_many :payroll_groups
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :project_code, :project_order, :project_description
  validates_uniqueness_of :project_code, :project_order
  validates :project_order, numericality: { only_integer: true, greater_than: 0 }

  def self.search(search)
    where('project_code iLIKE :search or project_description iLIKE :search', search: "%#{search}%").order(project_code: :asc)  
  end
end
