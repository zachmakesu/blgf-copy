# == Schema Information
#
# Table name: references
#
#  address        :text
#  contact_number :string
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  name           :string           not null
#  updated_at     :datetime         not null
#  user_id        :integer
#

class Reference < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :name, :contact_number, :user_id, presence: true
end
