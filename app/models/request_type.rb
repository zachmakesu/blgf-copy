# == Schema Information
#
# Table name: request_types
#
#  applicants          :integer
#  created_at          :datetime         not null
#  first_signatory_id  :integer
#  fourth_signatory_id :integer
#  id                  :integer          not null, primary key
#  second_signatory_id :integer
#  third_signatory_id  :integer
#  type_of_request     :string           not null
#  updated_at          :datetime         not null
#

class RequestType < ActiveRecord::Base
  #1st assotiations
  belongs_to :first_signatory, class_name: "User", foreign_key: "first_signatory_id"
  belongs_to :second_signatory, class_name: "User", foreign_key: "second_signatory_id"
  belongs_to :third_signatory, class_name: "User", foreign_key: "third_signatory_id"
  belongs_to :fourth_signatory, class_name: "User", foreign_key: "fourth_signatory_id"

  #2nd scopes

  #3rd enums and others
  enum applicants: { all_employees: 0, under_executive: 1, under_division: 2, under_section: 3, under_service: 4  }
  #4th callbacks

  #5th validations
  validates :type_of_request, :applicants, presence: true
  validates :type_of_request, uniqueness: true
end
