# == Schema Information
#
# Table name: salary_schedules
#
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  salary       :float
#  salary_grade :integer          not null
#  step_1       :float
#  step_10      :float
#  step_2       :float
#  step_3       :float
#  step_4       :float
#  step_5       :float
#  step_6       :float
#  step_7       :float
#  step_8       :float
#  step_9       :float
#  updated_at   :datetime         not null
#

class SalarySchedule < ActiveRecord::Base
  #1st assotiations
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :salary, :step_1, :step_2, :step_3, :step_4, :step_5, :step_6, :step_7, :step_8, :step_9, :step_10, numericality: true, allow_nil: true 
  validates :salary_grade, presence: true, numericality: true

  def self.search(search)
    where('salary_grade::text iLIKE :search', search: "%#{search}%").order(salary_grade: :asc)  
  end
end
