# == Schema Information
#
# Table name: salns
#
#  address                 :text
#  created_at              :datetime         not null
#  first_name              :string
#  id                      :integer          not null, primary key
#  last_name               :string
#  middle_initial          :string
#  office                  :string
#  office_address          :text
#  position                :string
#  spouse_first_name       :string
#  spouse_last_name        :string
#  spouse_middle_initial   :string
#  spouse_office           :string
#  spouse_office_address   :text
#  spouse_position         :string
#  statement_filing_status :integer          default(0), not null
#  updated_at              :datetime         not null
#  user_id                 :integer
#
# Indexes
#
#  index_salns_on_user_id  (user_id)
#

class Saln < ActiveRecord::Base
  #1st assotiations
  belongs_to :user
  has_many :saln_children, dependent: :destroy
  has_many :saln_assets, dependent: :destroy
  has_many :saln_liabilities, dependent: :destroy
  has_many :saln_bi_and_fcs, dependent: :destroy
  has_many :saln_government_relatives, dependent: :destroy
  has_many :saln_issued_ids, dependent: :destroy
  
  #2nd scopes

  #3rd enums and others
  enum statement_filing_status: {joint: 0, separate: 1, not_applicable: 2}

  #4th callbacks

  #5th validations
  validates :user_id, :statement_filing_status, :first_name, :last_name, presence: true
  validates :statement_filing_status, inclusion: { in: statement_filing_statuses.keys }

  def assets_real_sub_total
    self.saln_assets.real.sum(:acquisition_cost)
  end

  def assets_personal_sub_total
    self.saln_assets.personal.sum(:acquisition_cost)
  end

  def total_assets
    assets_real_sub_total + assets_personal_sub_total
  end

  def liabilities_sub_total
    self.saln_liabilities.sum(:outstanding_balance)
  end

  def net_worth
    total_assets - liabilities_sub_total
  end  


end
