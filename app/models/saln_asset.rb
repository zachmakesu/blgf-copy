# == Schema Information
#
# Table name: saln_assets
#
#  acquisition_cost     :float
#  acquisition_mode     :string
#  acquisition_year     :string
#  assesed_value        :float
#  asset_type           :integer          not null
#  created_at           :datetime         not null
#  current_market_value :float
#  description          :text
#  exact_location       :text
#  id                   :integer          not null, primary key
#  kind                 :string
#  saln_id              :integer
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_saln_assets_on_saln_id  (saln_id)
#

class SalnAsset < ActiveRecord::Base
  #1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others
  enum asset_type: {real: 0, personal: 1 }

  #4th callbacks

  #5th validations
  validates :saln_id, :asset_type, :description, :acquisition_cost, presence: true
  validates :assesed_value, :current_market_value, numericality: true, allow_nil: true
  validates :acquisition_cost, numericality: true
end
