# == Schema Information
#
# Table name: saln_bi_and_fcs
#
#  acquisition_date          :string
#  business_address          :text
#  business_enterprise       :string           not null
#  business_financial_nature :text
#  created_at                :datetime         not null
#  id                        :integer          not null, primary key
#  saln_id                   :integer
#  updated_at                :datetime         not null
#
# Indexes
#
#  index_saln_bi_and_fcs_on_saln_id  (saln_id)
#

class SalnBiAndFc < ActiveRecord::Base
  #1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :saln_id, :business_enterprise, :acquisition_date, presence: true
end
