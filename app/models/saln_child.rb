# == Schema Information
#
# Table name: saln_children
#
#  age        :integer
#  birthdate  :string
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  saln_id    :integer
#  updated_at :datetime         not null
#
# Indexes
#
#  index_saln_children_on_saln_id  (saln_id)
#

class SalnChild < ActiveRecord::Base
  #1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :saln_id, :name, :birthdate, :age, presence: true
end
