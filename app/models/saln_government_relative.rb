# == Schema Information
#
# Table name: saln_government_relatives
#
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  name           :string           not null
#  office_address :text
#  position       :string
#  relationship   :string
#  saln_id        :integer
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_saln_government_relatives_on_saln_id  (saln_id)
#

class SalnGovernmentRelative < ActiveRecord::Base
	#1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :saln_id, :name, :position, :relationship, presence: true
end
