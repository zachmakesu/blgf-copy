# == Schema Information
#
# Table name: saln_issued_ids
#
#  created_at  :datetime         not null
#  date_issued :string           not null
#  id          :integer          not null, primary key
#  id_no       :string           not null
#  id_type     :integer          not null
#  saln_id     :integer
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_saln_issued_ids_on_saln_id  (saln_id)
#

class SalnIssuedId < ActiveRecord::Base
  #1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others
  enum id_type: {declarant: 0, spouse: 1 }

  #4th callbacks

  #5th validations
  validates :saln_id, :date_issued, :id_no, presence: true
end
