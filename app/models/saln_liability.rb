# == Schema Information
#
# Table name: saln_liabilities
#
#  created_at          :datetime         not null
#  creditors_name      :text
#  id                  :integer          not null, primary key
#  nature              :string           not null
#  outstanding_balance :float            not null
#  saln_id             :integer
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_saln_liabilities_on_saln_id  (saln_id)
#

class SalnLiability < ActiveRecord::Base
  #1st assotiations
  belongs_to :saln

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :saln_id, :nature, :outstanding_balance, presence: true
  validates :outstanding_balance, numericality: true
end
