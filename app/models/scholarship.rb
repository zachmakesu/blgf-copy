# == Schema Information
#
# Table name: scholarships
#
#  created_at  :datetime         not null
#  description :string
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#

class Scholarship < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :description
  validates_uniqueness_of :description
  def self.search(search)
    where('description iLIKE :search', search: "%#{search}%").order(description: :asc)  
  end
end
