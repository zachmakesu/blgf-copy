# == Schema Information
#
# Table name: service_codes
#
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  service_code        :string
#  service_description :string
#  updated_at          :datetime         not null
#

class ServiceCode < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :service_code, :service_description
  validates_uniqueness_of :service_code

  def self.search(search)
    where('service_code iLIKE :search or service_description iLIKE :search', search: "%#{search}%").order(service_code: :asc)  
  end
end
