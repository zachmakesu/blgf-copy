# == Schema Information
#
# Table name: tax_ranges
#
#  created_at    :datetime         not null
#  factor        :float            default(0.0)
#  id            :integer          not null, primary key
#  tax_base      :float            default(0.0), not null
#  tax_deduction :float            default(0.0), not null
#  taxable_from  :float            default(0.0)
#  taxable_to    :float            default(0.0)
#  updated_at    :datetime         not null
#

class TaxRange < ActiveRecord::Base
	#1st assotiations

  #2nd scopes

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :tax_base, :tax_deduction, :factor, :taxable_from, :taxable_to, presence: true

  def self.search(search)
    where('tax_base::text iLIKE :search', search: "%#{search}%").order(tax_base: :asc)   
  end
end
