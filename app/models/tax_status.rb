# == Schema Information
#
# Table name: tax_statuses
#
#  created_at       :datetime         not null
#  exemption_amount :float            default(0.0), not null
#  id               :integer          not null, primary key
#  tax_status       :string           not null
#  updated_at       :datetime         not null
#

class TaxStatus < ActiveRecord::Base
	#1st assotiations

  #2nd scopes

  #3rd enums and others
  
  #4th callbacks

  #5th validations
  validates :tax_status, :exemption_amount, presence: true
  validates :tax_status, uniqueness: true

  def self.search(search)
    where('tax_status iLIKE :search', search: "%#{search}%").order(tax_status: :asc)   
  end
end
