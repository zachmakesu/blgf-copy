# == Schema Information
#
# Table name: trainings_and_seminars
#
#  conducted_by        :string
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  inclusive_date_from :string
#  inclusive_date_to   :string
#  number_of_hours     :integer          not null
#  title               :string
#  updated_at          :datetime         not null
#  user_id             :integer
#

class TrainingsAndSeminar < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :title, :number_of_hours, :conducted_by, presence: true
end
