# == Schema Information
#
# Table name: treasurer_assignments
#
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  lgu_name_id       :integer
#  treasurer_subrole :string
#  updated_at        :datetime         not null
#  user_id           :integer
#
# Indexes
#
#  index_treasurer_assignments_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_0809cc98f8  (user_id => users.id)
#

class TreasurerAssignment < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :lgu_name, class_name: "LguName", foreign_key: "lgu_name_id"
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, presence: true
  validates :user_id, uniqueness: true
end
