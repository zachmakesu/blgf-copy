# == Schema Information
#
# Table name: users
#
#  biometric_id                      :string
#  birthdate                         :string
#  birthplace                        :string
#  blood_type                        :string
#  citizenship                       :string
#  civil_status                      :integer          default(0), not null
#  created_at                        :datetime         not null
#  current_sign_in_at                :datetime
#  current_sign_in_ip                :inet
#  deleted_at                        :string
#  department_id                     :integer
#  division_id                       :integer
#  email                             :string           default(""), not null
#  employed_date_at                  :integer
#  employed_status                   :boolean
#  employee_id                       :string           not null
#  encrypted_password                :string           default(""), not null
#  failed_attempts                   :integer          default(0), not null
#  first_name                        :string
#  gender                            :integer          default(0), not null
#  gsis_policy_number                :string
#  height                            :float
#  id                                :integer          not null, primary key
#  last_name                         :string
#  last_sign_in_at                   :datetime
#  last_sign_in_ip                   :inet
#  lc_deduction_to_payroll           :float            default(0.0)
#  leave_credit_certified_date_month :date
#  leave_credit_certified_date_year  :date
#  locked_at                         :datetime
#  middle_initial                    :string
#  middle_name                       :string
#  name_extension                    :string
#  office_designation                :integer          default(0), not null
#  pagibig_id_number                 :string
#  parant_fathers_first_name         :string
#  parant_fathers_last_name          :string
#  parant_fathers_middle_name        :string
#  parant_mothers_first_name         :string
#  parant_mothers_last_name          :string
#  parant_mothers_middle_name        :string
#  parent_address                    :text
#  parent_fathers_name               :string
#  parent_mothers_maiden_name        :string
#  philhealth_number                 :string
#  position_id                       :integer
#  remember_created_at               :datetime
#  remittance_id                     :string
#  reset_password_sent_at            :datetime
#  reset_password_token              :string
#  role                              :integer          default(0), not null
#  sign_in_count                     :integer          default(0), not null
#  spouse_contact_number             :string
#  spouse_employer_or_business_name  :string
#  spouse_first_name                 :string
#  spouse_last_name                  :string
#  spouse_middle_name                :string
#  spouse_name                       :string
#  spouse_occupation                 :string
#  spouse_work_or_business_address   :string
#  sss_number                        :string
#  tin_number                        :string
#  treasurer_subrole                 :string
#  unlock_token                      :string
#  updated_at                        :datetime         not null
#  weight                            :float
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

class User < ActiveRecord::Base
  include AssociationGenerator
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable, :lockable

  #1st assotiations  
  has_many :contacts, dependent: :destroy
  has_many :addresses, dependent: :destroy
  has_many :children, dependent: :destroy
  has_many :educations, dependent: :destroy
  has_many :examinations, dependent: :destroy
  has_many :work_experiences, dependent: :destroy
  has_many :voluntary_works, dependent: :destroy
  has_many :trainings_and_seminars, dependent: :destroy
  has_one  :other_information, dependent: :destroy
  has_many :references, dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :pdf_files, as: :pdfable, dependent: :destroy

  has_many :api_keys, dependent: :destroy
  has_many :drafts, foreign_key: :owner_id
  has_many :leave_requests, foreign_key: :owner_id
  has_many :fts_requests, foreign_key: :owner_id
  has_many :leave_credit_histories
  has_many :monthly_salary_computations, dependent: :destroy

  has_one  :position_detail, dependent: :destroy
  has_one  :treasurer_assignment, dependent: :destroy
  has_one  :saln, dependent: :destroy

  has_many :deduction_histories, dependent: :destroy
  has_many :deductives, dependent: :destroy
  has_many :set_benefits, dependent: :destroy

  has_many :notifications, dependent: :destroy
  has_many :user_seen_notifications, dependent: :destroy
  has_many :csv_files, as: :csvable, dependent: :destroy
  
  has_one  :directory_contact, dependent: :destroy

  has_many :legal_cases, dependent: :destroy
  has_many :archives, dependent: :destroy
  #2nd scopes
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :except_self, -> (user) { where.not(id: user.id ) }
  scope :not_regular_employee, -> { where.not(role: 0) }
  scope :by_roles, -> (roles) { where(role: roles) }
  scope :retirees, -> (retired_mode) { joins(:position_detail).where(position_details: {mode_of_separation_id: retired_mode.id}) }

  #3rd enums and others
  enum gender: {male: 0, female: 1 }
  enum civil_status: { single: 0, married: 1, separated: 2, annuled: 3, widowed: 4, others: 5 }
  enum role: { employee: 0, hr: 1, treasurer: 2 , officer: 3, execom: 4}
  enum office_designation: { central: 0, regional: 1 }

  DAYUNITS = %w{hours minutes seconds}

  #4th callbacks
  #5th validations 
  validates_presence_of :employee_id, :email, :first_name, :last_name
  validates_uniqueness_of :employee_id , :email
  validates_uniqueness_of :biometric_id , allow_nil: true

  after_validation do
    if self.birthdate.present?
      birthdate = self.birthdate.to_date
      self.errors.add(:base, "Invalid date range.") if ((Date.today - 10.years)..Date.today).include?(birthdate) || birthdate >= Date.today
    end
  end

  def delete_previous_api_keys
    api_keys.first.delete if api_keys.count > 0
  end

  def avatar
    self.photos.employee.find_by(position: 0)
  end

  def signature_image
    self.photos.signature.find_by(position: 0)
  end

  def age
    if self.birthdate.present?
      dob = self.birthdate.to_date rescue nil
      now = Time.now.utc.to_date
      if dob.present? 
        now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
      else
        0
      end
    else
      0
    end
  end

  def length_of_service_in_years
    if self.position_detail.position_date.present?
      losy = self.position_detail.position_date.to_date rescue nil
      now = Time.now.utc.to_date
      if losy.present? 
        now.year - losy.year - ((now.month > losy.month || (now.month == losy.month && now.day >= losy.day)) ? 0 : 1)
      else
        0
      end
    else
      0
    end
  end

  def date_hired
    if self.position_detail.position_date.present?
      dob = self.position_detail.position_date.to_date rescue nil
    else
      nil
    end
  end

  def job_category
    self.position_detail.job_category
  end

  #For Leave Credits
  def cto_leave_credits_left_count
    to_be_deducted = self.leave_credit_histories.cto_leaves.to_be_deducted.sum(:credits)
    deductions = self.leave_credit_histories.cto_leaves.deductibles.sum(:credits)
    to_be_deducted - deductions
  end

  def sick_leave_credits_left_count
    to_be_deducted = self.leave_credit_histories.sick_leaves.to_be_deducted.sum(:credits)
    deductions = self.leave_credit_histories.sick_leaves.deductibles.sum(:credits)
    to_be_deducted - deductions
  end

  def vacation_leave_credits_left_count
    to_be_deducted = self.leave_credit_histories.vacation_leaves.to_be_deducted.sum(:credits)
    deductions = self.leave_credit_histories.vacation_leaves.deductibles.sum(:credits)
    to_be_deducted - deductions
  end

  def total_leave_credits
    vacation_leave_credits_left_count + sick_leave_credits_left_count
  end

  def leave_credits
    {
    cto_leave_credits: self.cto_leave_credits_left_count,
    sick_leave_credits: self.sick_leave_credits_left_count,
    vacation_leave_credits: self.vacation_leave_credits_left_count,
    total_leave_credits: self.total_leave_credits
    }
  end

  def last_updated_by
    if self.leave_credit_histories.present?
      updated_by = self.leave_credit_histories.where.not(created_by: nil).order(created_at: :desc)
      updated_by.first.admin_user if updated_by.first
    end
  end

  def last_updated_by_at
    if self.leave_credit_histories.present?
      updated_by = self.leave_credit_histories.where.not(created_by: nil).order(created_at: :desc)
      updated_by.first.created_at if updated_by.first
    end
  end

  #For Payslip
  def total_current_set_deductions_amount
    self.deductives.sum(:amount)
  end

  def total_current_set_benefits_amount #allowances only
    self.set_benefits.allowances.sum(:amount)
  end

  def add_credits_at_month_end
    if self.leave_credit_certified_date_month != Date.today
      self.leave_credit_histories.create(credit_type: 0, credits: 1.25, deductible: false)
      self.leave_credit_histories.create(credit_type: 1, credits: 1.25, deductible: false)
      self.update(leave_credit_certified_date_month: Date.today)

      #update position detail include in DTR if
      self.position_detail.update(include_in_payroll: (self.total_leave_credits > 10))
    end
  end

  def force_leave_at_year_end
    if self.leave_credit_certified_date_year != Date.today
      vl = vacation_leave_credits_left_count
      sl = sick_leave_credits_left_count
      total = sl + vl 
      if total > 10
        if sl > vl 
          lowest = [vl, 1]
          highest = [sl, 0]
        else
          lowest = [sl, 0]
          highest = [vl, 1]
        end

        if lowest[0] < 2.5
          highest[0] = 5 - lowest[0]
          self.leave_credit_histories.create(credit_type: lowest[1], credits:lowest[0], deductible: true)
          self.leave_credit_histories.create(credit_type: highest[1], credits: highest[0], deductible: true)
          self.update(leave_credit_certified_date_year: Date.today)

          #update position detail include in DTR if
          self.position_detail.update(include_in_payroll: (self.total_leave_credits > 10))
        else
          self.leave_credit_histories.create(credit_type: 0, credits: 2.5, deductible: true)
          self.leave_credit_histories.create(credit_type: 1, credits: 2.5, deductible: true)
          self.update(leave_credit_certified_date_year: Date.today)

          #update position detail include in DTR if
          self.position_detail.update(include_in_payroll: (self.total_leave_credits > 10))
        end
      end
      
    end
  end

  def salary_grade
    self.position_detail.plantilla.present? ? self.position_detail.plantilla.salary_schedule.salary_grade : nil
  end

  def position_description
    self.position_detail.plantilla.present? ? self.position_detail.plantilla.position_code.position_description : nil
  end

  def authorized_salary
    self.position_detail.actual_salary
  end

  def self.with_10_above_and_included_to_payroll_list
    select {|user| user.total_leave_credits > 10 && user.position_detail.include_in_payroll? }
  end

  def late_this_month(date)
    attendances = {}
    late_h = 0
    late_m = 0
    late_s = 0
    
    dates = AttendanceScheme.dates(date.to_date.year, date.to_date.month)
    dates.each_with_index { |date, index|
      attendances[date] = self.attendances(date).present? ? self.attendances(date) : nil
      attendance = self.attendances(date).present? ? self.attendances(date).last : nil
      late = attendance.present? ? attendance.late : "00:00:00"
      
      t = late.split(":")
      late_h += t[0].to_i
      late_m += t[1].to_i
      late_s += t[2].to_i
    }

    seconds = Time.now.minus_with_coercion((late_h.hours + late_m.minutes + late_s.seconds).ago).round
    mm, ss = seconds.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)

    total_late = "%d:%d:%d:%d" % [dd, hh, mm, ss]
  end

  def under_time_this_month(date)
    attendances = {}

    ut_h = 0
    ut_m = 0
    ut_s = 0

    dates = AttendanceScheme.dates(date.to_date.year, date.to_date.month)
    dates.each_with_index { |date, index|
      attendances[date] = self.attendances(date).present? ? self.attendances(date) : nil
      attendance = self.attendances(date).present? ? self.attendances(date).last : nil
      ut = attendance.present? ? attendance.ut : "00:00:00"
      
      t = ut.split(":")
      ut_h += t[0].to_i
      ut_m += t[1].to_i
      ut_s += t[2].to_i
    }

    seconds = Time.now.minus_with_coercion((ut_h.hours + ut_m.minutes + ut_s.seconds).ago).round
    mm, ss = seconds.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)

    total_ut = "%d:%d:%d:%d" % [dd, hh, mm, ss]
  end
  
  def late_and_ut_this_month(date)
    late = self.late_this_month("#{date}-01")
    ut   = self.under_time_this_month("#{date}-01")

    t = late.split(":")
    late_d = t[0].to_i * 24
    late_h = t[1].to_i
    late_m = t[2].to_i
    late_s = t[3].to_i

    t = ut.split(":")
    ut_d = t[0].to_i * 24
    ut_h = t[1].to_i
    ut_m = t[2].to_i
    ut_s = t[3].to_i

    late_seconds = (late_d.hours + late_h.hours + late_m.minutes + late_s.seconds)
    ut_seconds   = (ut_d.hours + ut_h.hours + ut_m.minutes + ut_s.seconds)

    seconds = Time.now.minus_with_coercion((late_seconds + ut_seconds).ago).round
    mm, ss = seconds.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)

    total_late_and_ut = "%d:%d:%d:%d" % [dd, hh, mm, ss]
  end

  def calculate_leave_credits_this_month(date)
    late_and_ut = self.late_and_ut_this_month("#{date}-01")
    t = late_and_ut.split(":")
    days    = t[0].to_i
    hours   = t[1].to_i
    minutes = t[2].to_i
    
    total_hours = (days * 24) + hours # get total_hours
    whole = (total_hours / 8)         # whole number leave credit to be deducted
    hour_fraction = total_hours % 8   # get remaining hours on fraction
    
    if hour_fraction > 0
      hour_to_lc_fraction = LeaveCreditHistory::HOURS_TO_LEAVE_CREDITS.select{ |c| c[0] ==  hour_fraction } # get equivalent of fraction to lc
      minute_to_lc_fraction = LeaveCreditHistory::MINUTES_TO_LEAVE_CREDITS.select{ |c| c[0] ==  minutes }   # get equivalent of fraction to lc
      
      hour_to_lc_equivalent = hour_to_lc_fraction.blank? ? 0 : hour_to_lc_fraction[0][1]
      minute_to_lc_equivalent = minute_to_lc_fraction.blank? ? 0 : minute_to_lc_fraction[0][1]

      deductible = whole  + hour_to_lc_equivalent + minute_to_lc_equivalent
      
      cto = self.cto_leave_credits_left_count
      sl  = self.sick_leave_credits_left_count
      deduction = 0

      if cto >= deductible
        self.leave_credit_histories.create(credit_type: "cto_leaves", credits: deductible, deductible: true)
      else
        self.leave_credit_histories.create(credit_type: "cto_leaves", credits: cto, deductible: true)
        sl_deductible = (deductible - cto).round(3)
        if sl >= sl_deductible
          self.leave_credit_histories.create(credit_type: "sick_leaves", credits: sl_deductible, deductible: true)
        else
          self.leave_credit_histories.create(credit_type: "sick_leaves", credits: sl, deductible: true)
          deduction =  (sl_deductible - sl).round(3)
        end
      end

      if deduction > 0
        self.update(lc_deduction_to_payroll: deduction)
      else
        self.update(lc_deduction_to_payroll: 0.0)
      end
      # calculate this then apply the ramaining to payroll deductions
    else
      self.update(lc_deduction_to_payroll: 0.0)
    end
  end

  def generate_payslip(date) # "yyyy-mm"
    lc = self.cto_leave_credits_left_count + self.sick_leave_credits_left_count

    if self.position_detail.include_in_payroll && lc >= 0
      payslip_params = { 
        plantilla_position_this_month: self.position_description,
        sg_this_month: self.salary_grade,
        this_month: date,
        authorized_salary_this_month: self.authorized_salary,
        benefits_this_month: generate_benefits_this_month,
        include_in_payroll_this_month: self.position_detail.include_in_payroll,

        late_this_month: self.late_this_month("#{date}-01"),
        under_time_this_month: self.under_time_this_month("#{date}-01"),

        deductions_this_month: generate_deductions_this_month,
        leave_credits_this_month: self.leave_credits
      }

      self.monthly_salary_computations.new(payslip_params).save
    end
  end

  def generate_benefits_this_month
    self.set_benefits.allowances.map do |benefit|
      data = Hash.new
      data[:id] = benefit.id
      data[:benefit_id] = benefit.benefit_id
      data[:benefit_code] = benefit.benefit.code
      data[:benefit_type] = benefit.benefit.benefit_type
      data[:amount] = benefit.amount
      data
    end
  end

  def generate_deductions_this_month
    deductives = self.deductives.map do |deductive|
      data = Hash.new
      data[:id] = deductive.id
      data[:deduction_type_id] = deductive.deduction_type_id
      data[:deduction_code] = deductive.deduction_type.deduction_code
      data[:deduction] = deductive.deduction_type.deduction
      data[:mandatory] = deductive.deduction_type.mandatory
      data[:amount] = deductive.amount
      data
    end

    lc_deduction = self.lc_deduction_to_payroll

    # deductives << { id: "", deduction_type_id: "late_and_ut", deduction_code: "late_and_ut", deduction: "Late and Undertime", mandatory: "true", amount: 0}
  end

  def self.search(search)
    where('employee_id iLIKE :search or biometric_id iLIKE :search or first_name iLIKE :search or last_name iLIKE :search or employee_id iLIKE :search', search: "%#{search}%").order(last_name: :asc)  
  end

  def self.search_in_reports_manager(search)
    where('employee_id iLIKE :search or biometric_id iLIKE :search or first_name iLIKE :search or last_name iLIKE :search or employee_id iLIKE :search', search: "%#{search}%")  
  end

  def self.by_job_category(job_category)
    joins(:position_detail).where('position_details.job_category iLIKE :job_category', job_category: "%#{job_category}%")
  end

  def attendances(date = nil) #yyyy-mm
    date = "#{(Time.now).to_s[0,7]}" if date.blank?
    EmployeeDtr.where('biometric_id = :biometric_id and dtr_date LIKE :date', date: "%#{date}%", biometric_id: self.biometric_id).order(dtr_date: :desc)
  end

  def attendance(date = nil) #yyyy-mm-dd
    date = "#{(Time.now).to_s[0,10]}" if date.blank?
    EmployeeDtr.where('biometric_id = :biometric_id and dtr_date LIKE :date', date: "%#{date}%", biometric_id: self.biometric_id).order(dtr_date: :desc)
  end

  def total_late(date = nil)
    late_in_seconds = 0
    self.attendances(date).each do |dtr|
      late_in_seconds = late_in_seconds + dtr.late.split(":").map.with_index{|x,i| x.to_i.send(DAYUNITS[i])}.reduce(:+).to_i
    end
    process_time(late_in_seconds)
  end

  def total_ut(date = nil)
    ut_in_seconds = 0
    self.attendances(date).each do |dtr|
      ut_in_seconds = ut_in_seconds + dtr.ut.split(":").map.with_index{|x,i| x.to_i.send(DAYUNITS[i])}.reduce(:+).to_i #convert "hh:mm:ss" to seconds and sum it all
    end
    process_time(ut_in_seconds) #process integer seconds
  end

  def process_time(seconds)
    mm, ss = seconds.divmod(60)        
    hh, mm = mm.divmod(60)         
    dd, hh = hh.divmod(24)        
    "%02d:%02d:%02d:%02d" % [dd, hh, mm, ss] #returns "dd:hh:mm:ss"
  end

  def required_fts_dates(date = nil)
    EmployeeDtr.where('biometric_id = :biometric_id and dtr_date iLIKE :search and (in_am LIKE :fts_date or out_am LIKE :fts_date  or in_pm LIKE :fts_date  or out_pm LIKE :fts_date )', search: "%#{date}%", biometric_id: self.biometric_id, fts_date: "00:00:00").order(dtr_date: :desc)
  end

  def unseen_notifications
    self.all_notifications.select{ |notification| notification if self.user_seen_notifications.map(&:notification_id).exclude? notification.id }
  end

  def all_notifications(search=nil)
    can_recieve_all = Notification.search(search).after_user_created(self).can_recieve_all
    
    specific_role = case self.role
      when "hr"           then Notification.search(search).after_user_created(self).hr.where(user_id: nil)
      when "employee"     then Notification.search(search).after_user_created(self).employee.where(user_id: nil)
      when "treasurer"    then Notification.search(search).after_user_created(self).treasurer.where(user_id: nil)
      end

    specific_role = specific_role ? specific_role : []

    self.notifications + can_recieve_all + specific_role
  end

  def dms_files
    DocketManagementFile.all.map do |dms_title|
      {
        docket_management_file_id: dms_title.id,
        docket_management_file_title: dms_title.title,
        uploaded: self.pdf_files.map(&:docket_management_file_id).include?(dms_title.id)
      }
    end
  end

  def role_message
    if self.hr?
      "your"
    else
     "his/her"
    end
  end

  def capitalize_name
    "#{self.first_name.titleize} #{self.last_name.titleize}"
  end

  def deductions_to_excel(date)
    compensation = self.monthly_salary_computations.find_by(this_month: date)
    for_dummy = MonthlySalaryComputation.where(this_month: date).maximum(:deductions_this_month)
    
    for_dummy_deductions = if for_dummy
      eval(for_dummy).map do |d|
        {
            "deduction_type_id": d[:deduction_type_id],
            "deduction_code": d[:deduction_code],
            "deduction": d[:deduction],
            "amount": 0
        }
      end
    else
      []
    end

    if compensation
      total = compensation.authorized_salary_this_month.to_f - compensation.deductions_this_month.map { |h| h["amount"] }.sum
      deductions_this_month = compensation.deductions_this_month.map do |d|
        {
            "deduction_type_id": d["deduction_type_id"],
            "deduction_code": d["deduction_code"],
            "deduction": d["deduction"],
            "amount": d["amount"]
        }
      end
      
      { 
        plantilla_position_this_month: compensation.plantilla_position_this_month,
        sg_this_month: compensation.sg_this_month,
        authorized_salary_this_month: compensation.authorized_salary_this_month,
        deductions_this_month: deductions_this_month,
        total: total
      }
      
    else
      { 
        plantilla_position_this_month: "",
        sg_this_month: "",
        authorized_salary_this_month: 0,
        deductions_this_month: for_dummy_deductions,
        total: 0
      }
    end
  end
end
