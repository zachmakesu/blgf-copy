class User
  class Role
    ADMIN              = User.roles.select{ |k,v| ["hr", "officer", "execom"].include?(k) }
    ALL                = User.roles
    TREASURER          = User.roles.select{ |k,v| ["treasurer"].include?(k) }
  end
end