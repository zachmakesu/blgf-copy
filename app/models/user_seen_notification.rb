# == Schema Information
#
# Table name: user_seen_notifications
#
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  notification_id :integer
#  updated_at      :datetime         not null
#  user_id         :integer
#
# Indexes
#
#  index_user_seen_notifications_on_user_id  (user_id)
#

class UserSeenNotification < ActiveRecord::Base
  # #1st assotiations
  belongs_to :user

  # #2nd scopes

  # #3rd enums and others

  # #4th callbacks

  # #5th validations
end
