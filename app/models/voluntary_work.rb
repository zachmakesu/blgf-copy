# == Schema Information
#
# Table name: voluntary_works
#
#  address_of_organization :text
#  created_at              :datetime         not null
#  id                      :integer          not null, primary key
#  inclusive_date_from     :string
#  inclusive_date_to       :string
#  name_of_organization    :string
#  number_of_hours         :integer          not null
#  position_nature_of_work :string
#  updated_at              :datetime         not null
#  user_id                 :integer
#

class VoluntaryWork < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :name_of_organization, :number_of_hours, :position_nature_of_work, presence: true

end
