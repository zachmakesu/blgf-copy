# == Schema Information
#
# Table name: work_experiences
#
#  appointment_status_id    :integer
#  branch                   :integer
#  created_at               :datetime         not null
#  department_agency_office :string
#  government_service       :boolean          default(FALSE)
#  id                       :integer          not null, primary key
#  inclusive_date_from      :string
#  inclusive_date_to        :string
#  mode_of_separation_id    :integer
#  monthly_salary           :string
#  position_title           :string
#  salary_grade_and_step    :string
#  separation_date          :string
#  updated_at               :datetime         not null
#  user_id                  :integer
#

class WorkExperience < ActiveRecord::Base
  #1st assotiations
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :mode_of_separation, class_name: "ModeOfSeparation", foreign_key: "mode_of_separation_id"
  belongs_to :appointment_status, class_name: "AppointmentStatus", foreign_key: "appointment_status_id"
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :user_id, :appointment_status_id, :position_title, presence: true
end
