# == Schema Information
#
# Table name: zones
#
#  created_at  :datetime         not null
#  database    :string
#  description :text
#  id          :integer          not null, primary key
#  password    :string
#  server_name :string
#  updated_at  :datetime         not null
#  username    :string
#  zone_code   :string           not null
#

class Zone < ActiveRecord::Base
  #1st assotiations

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates_presence_of :zone_code
  validates_uniqueness_of :zone_code

  def self.search(search)
    where('zone_code iLIKE :search', search: "%#{search}%").order(zone_code: :asc)  
  end
end
