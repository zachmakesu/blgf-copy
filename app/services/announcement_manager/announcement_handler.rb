AnnouncementHandlerError = Class.new(StandardError)

class AnnouncementHandler
  CALENDAR_SLUG = "calendar"
  ANNOUNCEMENT_SLUG = "announcements"
  attr_accessor :params, :type, :response

  def initialize(params, type)
    @params   = params
    @type     = type
    @response = Hash.new
  end

  def notif_message_all
    "#{@announcement.message}"
  end

  def slug
    @announcement.calendar? ? CALENDAR_SLUG : ANNOUNCEMENT_SLUG
  end

  def create
    ActiveRecord::Base.transaction do
      @announcement  = Announcement.create(post_params)
      if @announcement.errors.blank?
        get_send_to.split('^').uniq.each { |role| proccess_notification(role) }
        AnnouncementWorker.perform_async(@announcement.id)
      end
    end

    create_response
    self
  end

  def update
    @announcement = Announcement.find_by(id: params[:id])
    return invalid_id if @announcement.blank? || Announcement.where(announcement_type: type).count == 0

    ActiveRecord::Base.transaction do
      if @announcement.update_attributes(post_params)
        @announcement.reload
      else
        @errors = @announcement.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @announcement = Announcement.find_by(id: params[:id], announcement_type: type)
    return invalid_id if @announcement.blank? || Announcement.where(announcement_type: type).count == 0
    @announcement.destroy
    create_response
    self
  end


  private
  def post_params
    type == 1 ? posted_at = DateTime.now : posted_at = params[:posted_at]
    { 
      announcement_type: type,
      message: params[:message],
      posted_at: posted_at,
      start_time_at: params[:start_time_at],
      send_to: get_send_to
    }
  end

  def get_send_to
    str = ""
    if params[:send_to].present?
      roles = params[:send_to].split('^').uniq
      if params[:send_to].include?("all") 
        "all"
      else
        roles.each { |role| (str << "#{role}^") if User.roles.keys.include?(role) }
        str[0..-2]
      end
    else
      "all"
    end
  end

  def proccess_notification(role)
    case role
    when "hr" then NotificationHandler.new(@announcement, notif_message_all, "hr", slug).create
    when "employee" then NotificationHandler.new(@announcement, notif_message_all, "employee", slug).create
    when "treasurer" then NotificationHandler.new(@announcement, notif_message_all, "treasurer", slug).create
    when "all" then NotificationHandler.new(@announcement, notif_message_all, "can_recieve_all", slug).create
    else
      # do nothing
    end
    
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid id"
    self
  end
  
  def create_response(error=nil)
    if !@announcement.blank? && @announcement.errors.blank?
      response[:success] = true
      response[:details] = @announcement
    else
      response[:success] = false
      response[:details] = error || @announcement.errors.full_messages.join(', ')
    end
  end

end
