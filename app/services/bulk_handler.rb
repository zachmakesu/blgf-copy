BulkHandlerError = Class.new(StandardError)

class BulkHandler
  def self.bulk_delete(params, model)
    model = model.constantize
    ids = params[:ids].split('^').uniq.map {|i| i.to_i }
    ids.each do |id| 
      datum = model.find_by(id: id) 
      datum.delete if datum.present?
    end
  end
end
