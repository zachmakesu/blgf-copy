ChangePasswordHandlerError = Class.new(StandardError)

class ChangePasswordHandler
  attr_accessor :user, :params, :response

  def initialize(params, user)
    @params   = params
    @user     = user
    @response = Hash.new
  end

  def change_password
    return error_message if params[:new_password] != params[:confirm_password] || !@user.valid_password?(params[:current_password]) || params[:new_password].length < 6
    check = @user.update(password: params[:new_password])
    
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Password must be 6 above / Invalid current password / Password and Confirm password not equal"
    self
  end
  def create_response
    response[:success] = true
    response[:details] = "Password change successfully!" 
  end
end
