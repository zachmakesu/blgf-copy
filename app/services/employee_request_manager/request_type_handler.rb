RequestTypeHandlerError = Class.new(StandardError)

class RequestTypeHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  # def create_request_type
  #   ActiveRecord::Base.transaction do
  #     @request_type  = RequestType.create(post_params)
  #   end

  #   create_response
  #   self
  # end

  def update_request_type
    @request_type = RequestType.find(params[:id])
    return invalid_id if @request_type.blank? || RequestType.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      #update_params.delete(:type_of_request) if @request_type.type_of_request == params[:type_of_request]

      if @request_type.update_attributes(update_params)
        @request_type.reload
      else
        @errors = @request_type.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  # def delete_request_type
  #   @request_type = RequestType.find_by(id: params[:id])
  #   return invalid_id if @request_type.blank? || RequestType.count == 0
  #   @request_type.destroy
  #   create_response
  #   self
  # end

  private
  def post_params
    first_signatory  = User.not_deleted.not_regular_employee.find_by(employee_id: params[:first_signatory_id])
    second_signatory = User.not_deleted.not_regular_employee.find_by(employee_id: params[:second_signatory_id])
    third_signatory  = User.not_deleted.not_regular_employee.find_by(employee_id: params[:third_signatory_id])
    fourth_signatory = User.not_deleted.not_regular_employee.find_by(employee_id: params[:fourth_signatory_id])
    { 
      #type_of_request: params[:type_of_request],
      applicants: params[:applicants].to_i, 
      first_signatory_id: first_signatory,
      second_signatory_id: second_signatory,
      third_signatory_id: third_signatory,
      fourth_signatory_id: fourth_signatory
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Request Type id"
    self
  end
  
  def create_response(error=nil)
    if !@request_type.blank? && @request_type.errors.blank?
      response[:success] = true
      response[:details] = @request_type
    else
      response[:success] = false
      response[:details] = error || @request_type.errors.full_messages.join(', ')
    end
  end

end
