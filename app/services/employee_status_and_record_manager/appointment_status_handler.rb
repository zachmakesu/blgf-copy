AppointmentStatusHandlerError = Class.new(StandardError)

class AppointmentStatusHandler
  ADMIN_SLUG = "appointment_statuses"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_appointment_status
    ActiveRecord::Base.transaction do
      @appointment_status  = AppointmentStatus.create(post_params)
      message = "#{@current_user.capitalize_name} created Appointment Status \"#{@appointment_status.appointment_status}\"."
      NotificationHandler.new(@appointment_status, message, "hr", "#{ADMIN_SLUG}/#{@appointment_status.id}").create if @appointment_status.errors.blank?
    end

    create_response
    self
  end

  def update_appointment_status
    @appointment_status = AppointmentStatus.find(params[:id])
    return invalid_id if @appointment_status.blank? || AppointmentStatus.count == 0
    message = "#{@current_user.capitalize_name} updated Appointment Status \"#{@appointment_status.appointment_status}\"."
    ActiveRecord::Base.transaction do
      params[:leave_entitled].blank? ? leave_entitled = false : leave_entitled = params[:leave_entitled]
      
      update_params = post_params
      update_params.delete(:appointment_code) if @appointment_status.appointment_code == params[:appointment_code]

      if @appointment_status.update_attributes(update_params)
        @appointment_status.reload
        NotificationHandler.new(@appointment_status, message, "hr", "#{ADMIN_SLUG}/#{@appointment_status.id}").create
      else
        @errors = @appointment_status.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_appointment_status
    @appointment_status = AppointmentStatus.find_by(id: params[:id])
    return invalid_id if @appointment_status.blank? || AppointmentStatus.count == 0
    @passed_object = @appointment_status
    @appointment_status.destroy
    message = "#{@current_user.capitalize_name} deleted Appointment Status \"#{@passed_object.appointment_status}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || AppointmentStatus.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = AppointmentStatus.find_by(id: id) 
      if datum.present?
        @appointment_status = datum
        @passed_object = @appointment_status
        @appointment_status.destroy
        message = "#{@current_user.capitalize_name} deleted Appointment Status|\"#{@passed_object.appointment_status}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

      end
    end
    (@appointment_status = AppointmentStatus.new) if @appointment_status.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    params[:leave_entitled].blank? ? leave_entitled = false : leave_entitled = params[:leave_entitled]
    { 
      appointment_code: params[:appointment_code],
      appointment_status: params[:appointment_status], 
      leave_entitled: leave_entitled
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid appointment status id"
    self
  end
  
  def create_response(error=nil)
    if !@appointment_status.blank? && @appointment_status.errors.blank?
      response[:success] = true
      response[:details] = @appointment_status
    else
      response[:success] = false
      response[:details] = error || @appointment_status.errors.full_messages.join(', ')
    end
  end

end
