CourseHandlerError = Class.new(StandardError)

class CourseHandler
  ADMIN_SLUG = "courses"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_course
    ActiveRecord::Base.transaction do
      @course  = Course.create(post_params)
      message = "#{@current_user.capitalize_name} created Course \"#{@course.course_code}\"."
      NotificationHandler.new(@course, message, "hr", "#{ADMIN_SLUG}/#{@course.id}").create if @course.errors.blank?
    end

    create_response
    self
  end

  def update_course
    @course = Course.find(params[:id])
    message = "#{@current_user.capitalize_name} updated Course \"#{@course.course_code}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:course_code) if @course.course_code == params[:course_code]

      if @course.update_attributes(update_params)
        @course.reload
        NotificationHandler.new(@course, message, "hr", "#{ADMIN_SLUG}/#{@course.id}").create
      else
        @errors = @course.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_course
    @course = Course.find(params[:id])
    @passed_object = @course
    @course.destroy
    message = "#{@current_user.capitalize_name} deleted Course \"#{@passed_object.course_code}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || Course.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Course.find_by(id: id) 
      if datum.present?
        @course = datum
        @passed_object = @course
        @course.destroy
        message = "#{@current_user.capitalize_name} deleted Course \"#{@passed_object.course_code}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@course = Course.new) if @course.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      course_code: params[:course_code],
      course_description: params[:course_description]
    }
  end

  def create_response(error=nil)
    if !@course.blank? && @course.errors.blank?
      response[:success] = true
      response[:details] = @course
    else
      response[:success] = false
      response[:details] = error || @course.errors.full_messages.join(', ')
    end
  end

end
