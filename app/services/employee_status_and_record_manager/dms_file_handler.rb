DmsFileHandlerError = Class.new(StandardError)

class DmsFileHandler
  ADMIN_SLUG = "dms_file_titles"
  attr_accessor :params, :response, :current_user

  def initialize(params,current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def notif_message_admin
    "#{@current_user.capitalize_name} #{@message}"
  end

  def create_dms_file
    ActiveRecord::Base.transaction do
      @dms_file  = DocketManagementFile.create(post_params)
      @passed_object = @dms_file
      @message = "created DMS File Title \"#{@passed_object.title}\"."
      NotificationHandler.new(@passed_object, notif_message_admin, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create if @dms_file.errors.blank?
    end

    create_response
    self
  end

  def update_dms_file
    @dms_file = DocketManagementFile.find(params[:id])
    ActiveRecord::Base.transaction do
      update_params = post_params
      @message = "updated DMS File Title \"#{@dms_file.title}\"."
      if @dms_file.update_attributes(update_params)
        @dms_file.reload
        @passed_object = @dms_file
        NotificationHandler.new(@passed_object, notif_message_admin, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      else
        @errors = @dms_file.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_dms_file
    @dms_file = DocketManagementFile.find(params[:id])
    @object_raw = @dms_file
    @dms_file.destroy
    @message = "deleted DMS File Title \".#{@object_raw.title}\"."
    NotificationHandler.new(@object_raw, notif_message_admin, "hr", "#{ADMIN_SLUG}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || DocketManagementFile.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = DocketManagementFile.find_by(id: id) 
      if datum.present?
        @dms_file = datum
        @object_raw = @dms_file
        @dms_file.destroy
        @message = "deleted DMS File Title \"#{@object_raw.title}\"."
        NotificationHandler.new(@object_raw, notif_message_admin, "hr", "#{ADMIN_SLUG}").create
      end
    end

    (@dms_file = DocketManagementFile.new) if @dms_file.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      title: params[:title]
    }
  end

  def create_response(error=nil)
    if !@dms_file.blank? && @dms_file.errors.blank?
      response[:success] = true
      response[:details] = @dms_file
    else
      response[:success] = false
      response[:details] = error || @dms_file.errors.full_messages.join(', ')
    end
  end

end
