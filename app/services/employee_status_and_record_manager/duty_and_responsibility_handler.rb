DutyAndResponsibilityHandlerError = Class.new(StandardError)

class DutyAndResponsibilityHandler
  ADMIN_SLUG = "duties_and_responsibilities"
  attr_accessor :position_code, :params, :response, :current_user

  def initialize(params, current_user)
    @params         = params
    @position_code  = PositionCode.find_by(id: params[:position_code_id])
    @response       = Hash.new
    @current_user   = current_user
  end

  def create_duty_and_responsibility
    return invalid_position_code_id if position_code.blank?
    ActiveRecord::Base.transaction do
      @duty_and_responsibility  = DutyAndResponsibility.create(post_params)
      message = "#{@current_user.capitalize_name} created Duty and Responsibility \"#{@duty_and_responsibility.duties}\"."
      NotificationHandler.new(@duty_and_responsibility, message, "hr", "#{ADMIN_SLUG}/#{@duty_and_responsibility.id}").create if @duty_and_responsibility.errors.blank?
    end
    
    create_response
    self
  end

  def update_duty_and_responsibility
    @duty_and_responsibility = DutyAndResponsibility.find(params[:id])
    return invalid_id if @duty_and_responsibility.blank? || DutyAndResponsibility.count == 0
    return invalid_position_code_id if position_code.blank?
    message = "#{@current_user.capitalize_name} updated Duty and Responsibility \"#{@duty_and_responsibility.duties}\"."
    ActiveRecord::Base.transaction do
      if @duty_and_responsibility.update_attributes(post_params)
        @duty_and_responsibility.reload
        NotificationHandler.new(@duty_and_responsibility, message, "hr", "#{ADMIN_SLUG}/#{@duty_and_responsibility.id}").create
      else
        @errors = @duty_and_responsibility.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_duty_and_responsibility
    @duty_and_responsibility = DutyAndResponsibility.find_by(id: params[:id])
    return invalid_id if @duty_and_responsibility.blank? || DutyAndResponsibility.count == 0
    @passed_object = @duty_and_responsibility
    @duty_and_responsibility.destroy
    message = "#{@current_user.capitalize_name} deleted Duty and Responsibility \"#{@passed_object.duties}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || DutyAndResponsibility.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = DutyAndResponsibility.find_by(id: id) 
      if datum.present?
        @duty_and_responsibility = datum
        @passed_object = @duty_and_responsibility
        @duty_and_responsibility.destroy
        message = "#{@current_user.capitalize_name} deleted Duty and Responsibility \"#{@passed_object.duties}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@duty_and_responsibility = DutyAndResponsibility.new) if @duty_and_responsibility.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      position_code_id: position_code.id,
      percent_work: params[:percent_work],
      duties: params[:duties]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid duty and responsibility id"
    self
  end

  def invalid_position_code_id
    response[:success] = false
    response[:details] = "Invalid position code id"
    self
  end
  
  def create_response(error=nil)
    if !@duty_and_responsibility.blank? && @duty_and_responsibility.errors.blank?
      response[:success] = true
      response[:details] = @duty_and_responsibility
    else
      response[:success] = false
      response[:details] = error || @duty_and_responsibility.errors.full_messages.join(', ')
    end
  end

end
