EducationLevelHandlerError = Class.new(StandardError)

class EducationLevelHandler
  ADMIN_SLUG = "education_levels"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_education_level
    ActiveRecord::Base.transaction do
      @education_level  = EducationLevel.create(post_params)
      message = "#{@current_user.capitalize_name} created Education Level \"#{@education_level.level_description}\"."
      NotificationHandler.new(@education_level, message, "hr", "#{ADMIN_SLUG}/#{@education_level.id}").create if @education_level.errors.blank?
    end

    create_response
    self
  end

  def update_education_level
    @education_level = EducationLevel.find(params[:id])
    return invalid_id if @education_level.blank? || EducationLevel.count == 0
    message = "#{@current_user.capitalize_name} updated Education Level \"#{@education_level.level_description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:level_code) if @education_level.level_code == params[:level_code]

      if @education_level.update_attributes(update_params)
        @education_level.reload
        NotificationHandler.new(@education_level, message, "hr", "#{ADMIN_SLUG}/#{@education_level.id}").create
      else
        @errors = @education_level.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_education_level
    @education_level = EducationLevel.find_by(id: params[:id])
    return invalid_id if @education_level.blank? || EducationLevel.count == 0

    @passed_object = @education_level
    @education_level.destroy
    message = "#{@current_user.capitalize_name} deleted Education Level \"#{@passed_object.level_description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || EducationLevel.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = EducationLevel.find_by(id: id) 
      if datum.present?
        @education_level = datum
        @passed_object = @education_level
        @education_level.destroy
        message = "#{@current_user.capitalize_name} deleted Education Level \"#{@passed_object.level_description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@education_level = EducationLevel.new) if @education_level.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      level_code: params[:level_code],
      level_description: params[:level_description]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Education level id"
    self
  end
  
  def create_response(error=nil)
    if !@education_level.blank? && @education_level.errors.blank?
      response[:success] = true
      response[:details] = @education_level
    else
      response[:success] = false
      response[:details] = error || @education_level.errors.full_messages.join(', ')
    end
  end

end
