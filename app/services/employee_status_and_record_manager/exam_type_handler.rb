ExamTypeHandlerError = Class.new(StandardError)

class ExamTypeHandler
  ADMIN_SLUG = "exam_types"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_exam_type
    ActiveRecord::Base.transaction do
      @exam_type  = ExamType.create(post_params)
      message = "#{@current_user.capitalize_name} created Exam Type \"#{@exam_type.exam_description}\"."
      NotificationHandler.new(@exam_type, message, "hr", "#{ADMIN_SLUG}/#{@exam_type.id}").create if @exam_type.errors.blank?
    end

    create_response
    self
  end

  def update_exam_type
    @exam_type = ExamType.find(params[:id])
    return invalid_id if @exam_type.blank? || ExamType.count == 0
    message = "#{@current_user.capitalize_name} updated Exam Type \"#{@exam_type.exam_description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:exam_code) if @exam_type.exam_code == params[:exam_code]

      if @exam_type.update_attributes(update_params)
        @exam_type.reload
        NotificationHandler.new(@exam_type, message, "hr", "#{ADMIN_SLUG}/#{@exam_type.id}").create
      else
        @errors = @exam_type.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_exam_type
    @exam_type = ExamType.find_by(id: params[:id])
    return invalid_id if @exam_type.blank? || ExamType.count == 0

    @passed_object = @exam_type
    @exam_type.destroy
    message = "#{@current_user.capitalize_name} deleted Exam Type \"#{@passed_object.exam_code}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || ExamType.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = ExamType.find_by(id: id) 
      if datum.present?
        @exam_type = datum
        @passed_object = @exam_type
        @exam_type.destroy
        message = "#{@current_user.capitalize_name} deleted Exam Type \"#{@passed_object.exam_code}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@exam_type = ExamType.new) if @exam_type.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    params[:csc_eligible].blank? ? csc_eligible = false : csc_eligible = params[:csc_eligible]
    { 
      exam_code: params[:exam_code],
      exam_description: params[:exam_description], 
      csc_eligible: csc_eligible
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Exam Type id"
    self
  end
  
  def create_response(error=nil)
    if !@exam_type.blank? && @exam_type.errors.blank?
      response[:success] = true
      response[:details] = @exam_type
    else
      response[:success] = false
      response[:details] = error || @exam_type.errors.full_messages.join(', ')
    end
  end

end
