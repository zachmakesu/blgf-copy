KnowledgeBaseHandlerError = Class.new(StandardError)

class KnowledgeBaseHandler
  ADMIN_SLUG = "knowledge_bases"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_knowledge_base
    ActiveRecord::Base.transaction do
      @knowledge_base = KnowledgeBase.create(post_params)
      message = "#{@current_user.capitalize_name} created Knowledge Base \"#{@knowledge_base.description}\"."
      NotificationHandler.new(@knowledge_base, message, "hr", "#{ADMIN_SLUG}/#{@knowledge_base.id}").create if @knowledge_base.errors.blank?
    end

    create_response
    self
  end

  def update_knowledge_base
    @knowledge_base = KnowledgeBase.find(params[:id])
    return invalid_id if @knowledge_base.blank? || KnowledgeBase.count == 0
    message = "#{@current_user.capitalize_name} updated Knowledge Base \"#{@knowledge_base.description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:code) if @knowledge_base.code == params[:code]

      if @knowledge_base.update_attributes(update_params)
        @knowledge_base.reload
        NotificationHandler.new(@knowledge_base, message, "hr", "#{ADMIN_SLUG}/#{@knowledge_base.id}").create
      else
        @errors = @knowledge_base.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_knowledge_base
    @knowledge_base = KnowledgeBase.find_by(id: params[:id])
    return invalid_id if @knowledge_base.blank? || KnowledgeBase.count == 0

    @passed_object = @knowledge_base
    @knowledge_base.destroy
    message = "#{@current_user.capitalize_name} deleted Knowledge Base \"#{@passed_object.description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || KnowledgeBase.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = KnowledgeBase.find_by(id: id) 
      if datum.present?
        @knowledge_base = datum
        @passed_object = @knowledge_base
        @knowledge_base.destroy
        message = "#{@current_user.capitalize_name} deleted Knowledge Base \"#{@passed_object.description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

      end
    end
    
    (@knowledge_base = KnowledgeBase.new) if @knowledge_base.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      code: params[:code],
      description: params[:description]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Knowledge Base id"
    self
  end
  
  def create_response(error=nil)
    if !@knowledge_base.blank? && @knowledge_base.errors.blank?
      response[:success] = true
      response[:details] = @knowledge_base
    else
      response[:success] = false
      response[:details] = error || @knowledge_base.errors.full_messages.join(', ')
    end
  end

end
