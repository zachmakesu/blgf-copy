ModeOfSeparationHandlerError = Class.new(StandardError)

class ModeOfSeparationHandler
  ADMIN_SLUG = "mode_of_separations"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_mode_of_separation
    ActiveRecord::Base.transaction do
      @mode_of_separation  = ModeOfSeparation.create(post_params)
      message = "#{@current_user.capitalize_name} created Separation Mode \"#{@mode_of_separation.separation_mode}\"."
      NotificationHandler.new(@mode_of_separation, message, "hr", "#{ADMIN_SLUG}/#{@mode_of_separation.id}").create if @mode_of_separation.errors.blank?
    end

    create_response
    self
  end

  def update_mode_of_separation
    @mode_of_separation = ModeOfSeparation.find(params[:id])
    return invalid_id if @mode_of_separation.blank? || ModeOfSeparation.count == 0
    message = "#{@current_user.capitalize_name} updated Separation Mode \"#{@mode_of_separation.separation_mode}\"."
    ActiveRecord::Base.transaction do          
      if @mode_of_separation.update_attributes(post_params)
        @mode_of_separation.reload
        NotificationHandler.new(@mode_of_separation, message, "hr", "#{ADMIN_SLUG}/#{@mode_of_separation.id}").create
      else
        @errors = @mode_of_separation.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_mode_of_separation
    @mode_of_separation = ModeOfSeparation.find_by(id: params[:id])
    return invalid_id if @mode_of_separation.blank? || ModeOfSeparation.count == 0

    @passed_object = @mode_of_separation
    @mode_of_separation.destroy
    message = "#{@current_user.capitalize_name} deleted Separation Mode \"#{@passed_object.separation_mode}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || ModeOfSeparation.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = ModeOfSeparation.find_by(id: id) 
      if datum.present?
        @mode_of_separation = datum
        @passed_object = @mode_of_separation
        @mode_of_separation.destroy
        message = "#{@current_user.capitalize_name} deleted Separation Mode \"#{@passed_object.separation_mode}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@mode_of_separation = ModeOfSeparation.new) if @mode_of_separation.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      separation_mode: params[:separation_mode]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Mode of Separation id"
    self
  end
  
  def create_response(error=nil)
    if !@mode_of_separation.blank? && @mode_of_separation.errors.blank?
      response[:success] = true
      response[:details] = @mode_of_separation
    else
      response[:success] = false
      response[:details] = error || @mode_of_separation.errors.full_messages.join(', ')
    end
  end

end
