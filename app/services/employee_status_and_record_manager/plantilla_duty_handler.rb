PlantillaDutyHandlerError = Class.new(StandardError)

class PlantillaDutyHandler
  ADMIN_SLUG = "plantilla_duties"
  attr_accessor :plantilla, :params, :response, :current_user

  def initialize(params, current_user)
    @params        = params
    @plantilla     = Plantilla.find_by(id: params[:plantilla_id])
    @response      = Hash.new
    @current_user  = current_user
  end

  def create_plantilla_duty
    return invalid_plantilla_id if plantilla.blank?
    ActiveRecord::Base.transaction do
      @plantilla_duty  = PlantillaDuty.create(post_params)
      message = "#{@current_user.capitalize_name} created Plantilla Duty \"#{@plantilla_duty.duties}\"."
      NotificationHandler.new(@plantilla_duty, message, "hr", "#{ADMIN_SLUG}/#{@plantilla_duty.id}").create if @plantilla_duty.errors.blank?
    end
    create_response
    self
  end

  def update_plantilla_duty
    @plantilla_duty = PlantillaDuty.find(params[:id])
    return invalid_id if @plantilla_duty.blank? || PlantillaDuty.count == 0
    return invalid_plantilla_id if plantilla.blank?
    message = "#{@current_user.capitalize_name} updated Plantilla Duty \"#{@plantilla_duty.duties}\"."
    ActiveRecord::Base.transaction do
      if @plantilla_duty.update_attributes(post_params)
        @plantilla_duty.reload
        NotificationHandler.new(@plantilla_duty, message, "hr", "#{ADMIN_SLUG}/#{@plantilla_duty.id}").create
      else
        @errors = @plantilla_duty.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_plantilla_duty
    @plantilla_duty = PlantillaDuty.find_by(id: params[:id])
    return invalid_id if @plantilla_duty.blank? || PlantillaDuty.count == 0
    @passed_object = @plantilla_duty
    @plantilla_duty.destroy
    message = "#{@current_user.capitalize_name} deleted Plantilla Duty \"#{@passed_object.duties}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || PlantillaDuty.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = PlantillaDuty.find_by(id: id) 
      if datum.present?
        @plantilla_duty = datum
        @passed_object = @plantilla_duty
        @plantilla_duty.destroy
        message = "#{@current_user.capitalize_name} deleted Plantilla Duty \"#{@passed_object.duties}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@plantilla_duty = PlantillaDuty.new) if @plantilla_duty.blank?
    create_response
    self
  end

  private

  def post_params
    { 
      plantilla_id: plantilla.id,
      percent_work: params[:percent_work],
      duties: params[:duties]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid duty and responsibility id"
    self
  end

  def invalid_plantilla_id
    response[:success] = false
    response[:details] = "Invalid plantilla id"
    self
  end
  
  def create_response(error=nil)
    if !@plantilla_duty.blank? && @plantilla_duty.errors.blank?
      response[:success] = true
      response[:details] = @plantilla_duty
    else
      response[:success] = false
      response[:details] = error || @plantilla_duty.errors.full_messages.join(', ')
    end
  end

end
