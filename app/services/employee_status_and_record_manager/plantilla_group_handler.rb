PlantillaGroupHandlerError = Class.new(StandardError)

class PlantillaGroupHandler
  ADMIN_SLUG = "plantilla_groups"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_plantilla_group
    ActiveRecord::Base.transaction do
      @plantilla_group  = PlantillaGroup.create(post_params)
      message = "#{@current_user.capitalize_name} created Plantilla Group \"#{@plantilla_group.group_name}\"."
      NotificationHandler.new(@plantilla_group, message, "hr", "#{ADMIN_SLUG}/#{@plantilla_group.id}").create if @plantilla_group.errors.blank?
    end

    create_response
    self
  end

  def update_plantilla_group
    @plantilla_group = PlantillaGroup.find(params[:id])
    return invalid_id if @plantilla_group.blank? || PlantillaGroup.count == 0
    message = "#{@current_user.capitalize_name} updated Plantilla Group \"#{@plantilla_group.group_name}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:group_code) if @plantilla_group.group_code == params[:group_code]
      update_params.delete(:group_order) if @plantilla_group.group_order == params[:group_order]

      if @plantilla_group.update_attributes(update_params)
        @plantilla_group.reload
        NotificationHandler.new(@plantilla_group, message, "hr", "#{ADMIN_SLUG}/#{@plantilla_group.id}").create
      else
        @errors = @plantilla_group.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_plantilla_group
    @plantilla_group = PlantillaGroup.find_by(id: params[:id])
    return invalid_id if @plantilla_group.blank? || PlantillaGroup.count == 0

    @passed_object = @plantilla_group
    @plantilla_group.destroy
    message = "#{@current_user.capitalize_name} deleted Plantilla Group \"#{@passed_object.group_name}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || PlantillaGroup.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = PlantillaGroup.find_by(id: id) 
      if datum.present?
        @plantilla_group = datum
        @passed_object = @plantilla_group
        @plantilla_group.destroy
        message = "#{@current_user.capitalize_name} deleted Plantilla Group \"#{@passed_object.group_name}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@plantilla_group = PlantillaGroup.new) if @plantilla_group.blank?
    create_response
    self
  end

  private

  def post_params
    { 
      group_code: params[:group_code],
      group_name: params[:group_name],
      group_order: params[:group_order]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid plantilla_group id"
    self
  end
  
  def create_response(error=nil)
    if !@plantilla_group.blank? && @plantilla_group.errors.blank?
      response[:success] = true
      response[:details] = @plantilla_group
    else
      response[:success] = false
      response[:details] = error || @plantilla_group.errors.full_messages.join(', ')
    end
  end

end
