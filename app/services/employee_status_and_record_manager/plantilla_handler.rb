PlantillaHandlerError = Class.new(StandardError)

class PlantillaHandler
  ADMIN_SLUG = "plantilles"
  attr_accessor :params, :response, :current_user

  def initialize(params,current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_plantilla
    return invalid_parameters if params[:salary_schedule_grade].blank? 
    ActiveRecord::Base.transaction do
      @plantilla  = Plantilla.create(post_params)
      message = "#{@current_user.capitalize_name} created Plantilla \"#{@plantilla.item_number}\"."
      NotificationHandler.new(@plantilla, message, "hr", "#{ADMIN_SLUG}/#{@plantilla.id}").create if @plantilla.errors.blank?
    end

    create_response
    self
  end

  def update_plantilla
    @plantilla = Plantilla.find(params[:id])
    return invalid_parameters if @plantilla.blank? || Plantilla.count == 0 || params[:salary_schedule_grade].blank?
    message = "#{@current_user.capitalize_name} updated Plantilla \"#{@plantilla.item_number}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:item_number) if @plantilla.item_number == params[:item_number]

      if @plantilla.update_attributes(update_params)
        @plantilla.reload
        NotificationHandler.new(@plantilla, message, "hr", "#{ADMIN_SLUG}/#{@plantilla.id}").create
      else
        @errors = @plantilla.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_plantilla
    @plantilla = Plantilla.find_by(id: params[:id])
    return invalid_parameters if @plantilla.blank? || Plantilla.count == 0

    @passed_object = @plantilla
    @plantilla.destroy
    message = "#{@current_user.capitalize_name} deleted Plantilla \"#{@passed_object.item_number}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || Plantilla.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Plantilla.find_by(id: id) 
      if datum.present?
        @plantilla = datum
        @passed_object = @plantilla
        @plantilla.destroy
        message = "#{@current_user.capitalize_name} deleted Plantilla \"#{@passed_object.item_number}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@plantilla = Plantilla.new) if @plantilla.blank?
    create_response
    self
  end

  private


  def post_params
    position_code    = PositionCode.find(params[:position_code_id])
    salary_grade     = SalarySchedule.find_by(salary_grade: params[:salary_schedule_grade])
    plantilla_group  = PlantillaGroup.find(params[:plantilla_group_id])
    { 
      item_number: params[:item_number],
      position_code: position_code,
      plantilla_group: plantilla_group,
      salary_schedule: salary_grade,
      area_code: params[:area_code],
      area_type: params[:area_type],
      csc_eligibility: params[:csc_eligibility],
      rationalized: params[:rationalized]
    }
  end

  def invalid_parameters
    response[:success] = false
    response[:details] = "Invalid Parameters"
    self
  end
  
  def create_response(error=nil)
    if !@plantilla.blank? && @plantilla.errors.blank?
      response[:success] = true
      response[:details] = @plantilla
    else
      response[:success] = false
      response[:details] = error || @plantilla.errors.full_messages.join(', ')
    end
  end

end
