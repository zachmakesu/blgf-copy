PositionCodeHandlerError = Class.new(StandardError)

class PositionCodeHandler
  ADMIN_SLUG = "courses"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_position_code
    ActiveRecord::Base.transaction do
      @position_code  = PositionCode.create(post_params)
      message = "#{@current_user.capitalize_name} created Position Code \"#{@position_code.position_description}\"."
      NotificationHandler.new(@position_code, message, "hr", "#{ADMIN_SLUG}/#{@position_code.id}").create if @position_code.errors.blank?
    end

    create_response
    self
  end

  def update_position_code
    @position_code = PositionCode.find(params[:id])
    return invalid_id if @position_code.blank? || PositionCode.count == 0
    message = "#{@current_user.capitalize_name} updated Position Code \"#{@position_code.position_description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:position_code) if @position_code.position_code == params[:position_code]
      
      if @position_code.update_attributes(update_params)
        @position_code.reload
        NotificationHandler.new(@position_code, message, "hr", "#{ADMIN_SLUG}/#{@position_code.id}").create
      else
        @errors = @position_code.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_position_code
    @position_code = PositionCode.find(params[:id])
    return invalid_id if @position_code.blank? || PositionCode.count == 0
    
    @passed_object = @position_code
    @position_code.destroy
    message = "#{@current_user.capitalize_name} deleted Position Code \"#{@passed_object.position_description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create

    @position_code = PositionCode.new()
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || PositionCode.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = PositionCode.find_by(id: id) 
      if datum.present?
        @position_code = datum
        @passed_object = @position_code
        @position_code.destroy
        message = "#{@current_user.capitalize_name} deleted Position Code \"#{@passed_object.position_description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@position_code = PositionCode.new) if @position_code.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      position_code: params[:position_code],
      position_description: params[:position_description], 
      position_abbreviation: params[:position_abbreviation], 
      educational_requirement: params[:educational_requirement],
      experience_requirement: params[:experience_requirement],
      eligibility_requirement: params[:eligibility_requirement],
      training_requirement: params[:training_requirement],
      position_classification: params[:position_classification]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid position code id"
    self
  end
  
  def create_response(error=nil)
    if !@position_code.blank? && @position_code.errors.blank?
      response[:success] = true
      response[:details] = @position_code
    else
      response[:success] = false
      response[:details] = error || @position_code.errors.full_messages.join(', ')
    end
  end

end
