ProjectCodeHandlerError = Class.new(StandardError)

class ProjectCodeHandler
  ADMIN_SLUG = "project_codes"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_project_code
    ActiveRecord::Base.transaction do
      @project_code  = ProjectCode.create(post_params)
      message = "#{@current_user.capitalize_name} created Project Code \"#{@project_code.project_description}\"."
      NotificationHandler.new(@project_code, message, "hr", "#{ADMIN_SLUG}/#{@project_code.id}").create if @project_code.errors.blank?
    end

    create_response
    self
  end

  def update_project_code
    @project_code = ProjectCode.find(params[:id])
    return invalid_id if @project_code.blank? || ProjectCode.count == 0
    message = "#{@current_user.capitalize_name} updated Project Code \"#{@project_code.project_description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:project_code) if @project_code.project_code == params[:project_code]
      update_params.delete(:project_order) if @project_code.project_order == params[:project_order]

      if @project_code.update_attributes(update_params)
        @project_code.reload
        NotificationHandler.new(@project_code, message, "hr", "#{ADMIN_SLUG}/#{@project_code.id}").create 
      else
        @errors = @project_code.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_project_code
    @project_code = ProjectCode.find_by(id: params[:id])
    return invalid_id if @project_code.blank? || ProjectCode.count == 0
  
    @passed_object = @project_code
    @project_code.destroy
    message = "#{@current_user.capitalize_name} deleted Project Code \"#{@passed_object.project_description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || ProjectCode.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = ProjectCode.find_by(id: id) 
      if datum.present?
        @project_code = datum
        @passed_object = @project_code
        @project_code.destroy
        message = "#{@current_user.capitalize_name} deleted Project Code \"#{@passed_object.project_description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@project_code = ProjectCode.new) if @project_code.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      project_code: params[:project_code],
      project_description: params[:project_description],
      project_order: params[:project_order]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid project_code id"
    self
  end
  
  def create_response(error=nil)
    if !@project_code.blank? && @project_code.errors.blank?
      response[:success] = true
      response[:details] = @project_code
    else
      response[:success] = false
      response[:details] = error || @project_code.errors.full_messages.join(', ')
    end
  end

end
