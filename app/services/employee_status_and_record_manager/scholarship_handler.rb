ScholarshipHandlerError = Class.new(StandardError)

class ScholarshipHandler
  ADMIN_SLUG = "scholarships"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_scholarship
    ActiveRecord::Base.transaction do
      @scholarship  = Scholarship.create(post_params)
      message = "#{@current_user.capitalize_name} created Scholarship \"#{@scholarship.description}\"."
      NotificationHandler.new(@scholarship, message, "hr", "#{ADMIN_SLUG}/#{@scholarship.id}").create if @scholarship.errors.blank?
    end

    create_response
    self
  end

  def update_scholarship
    @scholarship = Scholarship.find(params[:id])
    return invalid_id if @scholarship.blank? || Scholarship.count == 0
    message = "#{@current_user.capitalize_name} created Scholarship \"#{@scholarship.description}\"."
    ActiveRecord::Base.transaction do  
      if @scholarship.update_attributes(post_params)
        @scholarship.reload
        NotificationHandler.new(@scholarship, message, "hr", "#{ADMIN_SLUG}/#{@scholarship.id}").create
      else
        @errors = @scholarship.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_scholarship
    @scholarship = Scholarship.find_by(id: params[:id])
    return invalid_id if @scholarship.blank? || Scholarship.count == 0
  
    @passed_object = @scholarship
    @scholarship.destroy
    message = "#{@current_user.capitalize_name} deleted Scholarship \"#{@passed_object.description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || Scholarship.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Scholarship.find_by(id: id) 
      if datum.present?
        @scholarship = datum
        @passed_object = @scholarship
        @scholarship.destroy
        message = "#{@current_user.capitalize_name} deleted Scholarship \"#{@passed_object.description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@scholarship = Scholarship.new) if @scholarship.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      description: params[:description]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Scholarship id"
    self
  end
  
  def create_response(error=nil)
    if !@scholarship.blank? && @scholarship.errors.blank?
      response[:success] = true
      response[:details] = @scholarship
    else
      response[:success] = false
      response[:details] = error || @scholarship.errors.full_messages.join(', ')
    end
  end

end
