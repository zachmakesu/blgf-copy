ServiceCodeHandlerError = Class.new(StandardError)

class ServiceCodeHandler
  ADMIN_SLUG = "service_codes"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_service_code
    ActiveRecord::Base.transaction do
      @service_code  = ServiceCode.create(post_params)
      message = "#{@current_user.capitalize_name} created Service Code \"#{@service_code.service_description}\"."
      NotificationHandler.new(@service_code, message, "hr", "#{ADMIN_SLUG}/#{@service_code.id}").create if @service_code.errors.blank?
    end

    create_response
    self
  end

  def update_service_code
    @service_code = ServiceCode.find(params[:id])
    return invalid_id if @service_code.blank? || ServiceCode.count == 0
    message = "#{@current_user.capitalize_name} created Service Code \"#{@service_code.service_description}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:service_code) if @service_code.service_code == params[:service_code]
      if @service_code.update_attributes(update_params)
        @service_code.reload
        NotificationHandler.new(@service_code, message, "hr", "#{ADMIN_SLUG}/#{@service_code.id}").create
      else
        @errors = @service_code.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_service_code
    @service_code = ServiceCode.find_by(id: params[:id])
    return invalid_id if @service_code.blank? || ServiceCode.count == 0

    @passed_object = @service_code
    @service_code.destroy
    message = "#{@current_user.capitalize_name} deleted Service Code \"#{@passed_object.service_description}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || ServiceCode.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = ServiceCode.find_by(id: id) 
      if datum.present?
        @service_code = datum
        @passed_object = @service_code
        @service_code.destroy
        message = "#{@current_user.capitalize_name} deleted Service Code \"#{@passed_object.service_description}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@service_code = ServiceCode.new) if @service_code.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      service_code: params[:service_code],
      service_description: params[:service_description]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Service code id"
    self
  end
  
  def create_response(error=nil)
    if !@service_code.blank? && @service_code.errors.blank?
      response[:success] = true
      response[:details] = @service_code
    else
      response[:success] = false
      response[:details] = error || @service_code.errors.full_messages.join(', ')
    end
  end

end
