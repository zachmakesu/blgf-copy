ForgotPasswordHandlerError = Class.new(StandardError)

class ForgotPasswordHandler
  attr_accessor :user, :params, :response

  def initialize(params)
    @params   = params
    @user     = User.find_by(email: params[:email], employee_id: params[:employee_id])
    @response = Hash.new
  end

  def reset_password
    return default_error if @user.blank?
    password = Devise.friendly_token.first(8)
    @user.update(password: password)
    UserMailer.password_reset(@user, password).deliver_now
    create_response("Reset password has been sent to #{params[:email]}")
    self
  end

  private
  def create_response(responses)
    response[:success] = true
    response[:details] = responses 
  end

  def default_error
    response[:success] = false
    response[:details] = "Invalid email or Employee Id"
    self
  end
end
