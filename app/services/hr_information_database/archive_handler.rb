ArchiveHandlerError = Class.new(StandardError)

class ArchiveHandler
  
  attr_accessor :current_user

  def initialize(current_user)
    @current_user = current_user
  end

  def archive_pds
    date = DateTime.now.to_date.to_s
    User.all.each do |user|
      ArchivePdsWorker.perform_async(user.id, date)
    end
  end

  def archive_saln
    date = DateTime.now.to_date.to_s
    User.all.each do |user|
      ArchiveSalnWorker.perform_async(user.id, date)
    end
  end

end
