class HrUpdate::EducationHandler

  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      if @education = user.educations.create(education_params)
      else
        @errors = @education.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update
    ActiveRecord::Base.transaction do
      @education = user.educations.find(params[:id])
      if @education.update_attributes(education_params)
        @education.reload
      else
        @errors = @education.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @education = user.educations.find(params[:id])
    @education.destroy
    create_response
    self
  end

  private
  def education_params
    {
      education_level_id:               params[:education_level_id],
      school_name:                      params[:school_name],
      course_id:                        params[:course_id],
      highest_grade_level_units_earned: params[:highest_grade_level_units_earned],
      date_of_attendance_from:          params[:date_of_attendance_from],
      date_of_attendance_to:            params[:date_of_attendance_to],
      honors_received:                  params[:honors_received],
      year_graduated:                   params[:year_graduated],
    }
  end

  def create_response(error=nil)
    if !@education.nil? && @education.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @education.errors.full_messages.uniq.join(', ')
    end
  end
end
