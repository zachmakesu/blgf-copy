class HrUpdate::ExaminationHandler

  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      if @examination = user.examinations.create(examination_params)
      else
        @errors = @examination.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update
    ActiveRecord::Base.transaction do
      @examination = user.examinations.find(params[:id])
      if @examination.update_attributes(examination_params)
        @examination.reload
      else
        @errors = @examination.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @examination = user.examinations.find(params[:id])
    @examination.destroy
    create_response
    self
  end

  private
  def examination_params
    {
      exam_description:         params[:exam_description],
      place_of_exam:            params[:place_of_exam],
      rating:                   params[:rating],
      date_of_exam:             params[:date_of_exam],
      licence_number:           params[:licence_number],
      date_of_release:          params[:date_of_release]
    }
  end

  def create_response(error=nil)
    if !@examination.nil? && @examination.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @examination.errors.full_messages.uniq.join(', ')
    end
  end
end
