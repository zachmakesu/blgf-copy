class HrUpdate::FamilyInfoHandler

  attr_accessor :params, :employee, :response

  def initialize(params, roles, options={})
    @employee = User.by_roles(roles).find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def update
    return invalid_emp_id if employee.blank?
    @user = employee
    ActiveRecord::Base.transaction do
      @user.attributes = spouse_and_parent_options
      if @user.save
        @user.reload
      else
        @errors = @user.errors.full_messages.flatten.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def add_child
    return invalid_emp_id if employee.blank?
    ActiveRecord::Base.transaction do
      @user = employee.children.new(children_options)
      if @user.save
        @user.reload
      else
        @errors = @user.errors.full_messages.flatten.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_child
    return invalid_emp_id if employee.blank?
    @user = employee.children.find_by(id: params[:child_id])
    return invalid_child_id if @user.blank?
    ActiveRecord::Base.transaction do
      if @user.update_attributes(children_options)
        @user.reload
      else
        @errors = @user.errors.full_messages.flatten.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_child
    return invalid_emp_id if employee.blank?
    @user = employee.children.find_by(id: params[:child_id])
    return invalid_child_id if @user.blank?
    @user.destroy
    create_response
    self
  end

  private

  def invalid_emp_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end

  def invalid_child_id
    response[:success] = false
    response[:details] = "Invalid child id"
    self
  end

  def create_response(errors = nil)
    if errors.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = errors || @user.errors.full_messages.flatten.join(', ')
    end
  end

  def spouse_and_parent_options
    {
      spouse_name: params[:spouse_name],
      spouse_occupation: params[:spouse_occupation],
      spouse_employer_or_business_name: params[:spouse_employer_or_business_name],
      spouse_work_or_business_address: params[:spouse_work_or_business_address],
      spouse_contact_number: params[:spouse_contact_number],
      parent_fathers_name: params[:parent_fathers_name],
      parent_mothers_maiden_name: params[:parent_mothers_maiden_name],
      parent_address: params[:parent_address],

      spouse_first_name: params[:spouse_first_name],
      spouse_middle_name: params[:spouse_middle_name],
      spouse_last_name: params[:spouse_last_name],
      parant_fathers_first_name: params[:parant_fathers_first_name],
      parant_fathers_middle_name: params[:parant_fathers_middle_name],
      parant_fathers_last_name: params[:parant_fathers_last_name],
      parant_mothers_first_name: params[:parant_mothers_first_name],
      parant_mothers_middle_name: params[:parant_mothers_middle_name],
      parant_mothers_last_name: params[:parant_mothers_last_name]
    }
  end

  def children_options
    {
      first_name: params[:first_name],
      last_name: params[:last_name],
      middle_name: params[:middle_name],
      middle_initial: params[:middle_initial],
      relationship: params[:relationship],
      birthdate: params[:birthdate]
    }
  end

end
