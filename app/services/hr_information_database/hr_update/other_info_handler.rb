class HrUpdate::OtherInfoHandler

  attr_accessor :params, :employee, :response

  def initialize(params, roles, options={})
    @employee = User.by_roles(roles).find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def update
    @other_information = employee.other_information
    ActiveRecord::Base.transaction do
      @other_information.attributes = other_information_options
      @other_information.save
    end

    create_response
    self
  end

  def add_references
    ActiveRecord::Base.transaction do
      @reference = employee.references.new(references_params)
      if @reference.save
        @reference.reload
      else
        @errors = @reference.errors.full_messages.join(', ')
      end
    end

    create_reference_response(@errors)
    self
  end

  def update_references
    return invalid_reference_id if employee.references.count == 0
    ActiveRecord::Base.transaction do
      @reference = employee.references.find(params[:reference_id])
      if @reference.update(references_params)
        @reference.reload
      else
        @errors = @reference.errors.full_messages.join(', ')
      end
    end

    create_reference_response(@errors)
    self
  end

  def delete_references
    return invalid_reference_id if employee.references.count == 0
    ActiveRecord::Base.transaction do
      @reference = employee.references.find(params[:reference_id])
      @reference.destroy
    end
    
    create_reference_response
    self
  end

  private
  def build_error_msgs
    (@other_information.errors.full_messages).uniq
  end

  def invalid_reference_id
    response[:success] = false
    response[:details] = "Invalid reference id"
    self
  end

  def create_response
    if build_error_msgs.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = build_error_msgs.flatten.join(', ')
    end
  end

  def create_reference_response(errors = nil)
    if errors.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = errors || @reference.errors.full_messages.flatten.join(', ')
    end
  end

  def other_information_options
    {
      skills_and_hobbies: params[:skills_and_hobbies],
      non_academic_destinction: params[:non_academic_destinction],
      membership_in: params[:membership_in],
      third_degree_national: params[:third_degree_national],
      give_national: params[:give_national],
      fourth_degree_local: params[:fourth_degree_local],
      give_local: params[:give_local],
      formally_charge: params[:formally_charge],
      give_charge: params[:give_charge],
      administrative_offense: params[:administrative_offense],
      give_offense: params[:give_offense],
      any_violation: params[:any_violation],
      give_reasons: params[:give_reasons],
      canditate: params[:canditate],
      give_date_particulars: params[:give_date_particulars],
      indigenous_member: params[:indigenous_member],
      give_group: params[:give_group],
      differently_abled: params[:differently_abled],
      give_disability: params[:give_disability],
      solo_parent: params[:solo_parent],
      give_status: params[:give_status],
      is_separated: params[:is_separated],
      give_details: params[:give_details],
      signature: params[:signature],
      date_accomplished: params[:date_accomplished],
      ctc_number: params[:ctc_number],
      issued_at: params[:issued_at],
      issued_on: params[:issued_on]
    }
  end

  def references_params
    {
      name: params[:name],
      address: params[:address],
      contact_number: params[:contact_number]
    }
  end
end
