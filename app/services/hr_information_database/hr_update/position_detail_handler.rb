class HrUpdate::PositionDetailHandler
  TAX_STATUSES = ["Z", "S / ME", "ME1 / S1", "ME2 / S2", "ME3 / S3", "ME4 / S4"]
  attr_accessor :params, :employee, :response

  def initialize(params, roles, options={})
    @employee = User.by_roles(roles).find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def update
    return invalid_employee_message if @employee.blank?
    ActiveRecord::Base.transaction do
      @position = @employee.position_detail
      if @position.update_attributes(post_params)
        @position.reload
      else
        @errors = @position.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  private


  def post_params
    {
      service_code_id: params[:service_code_id],
      first_day_gov: params[:first_day_gov],
      first_day_agency: params[:first_day_agency],
      mode_of_separation_id: params[:mode_of_separation_id],
      separation_date: params[:separation_date],
      appointment_status_id: params[:appointment_status_id],
      job_category: params[:job_category],
      executive_office_id: params[:executive_office_id],
      service_id: params[:service_id],
      division_id: params[:division_id], 
      section_id: params[:section_id],
      place_of_assignment: params[:place_of_assignment],
      salary_effective_date: params[:salary_effective_date],
      employment_basis: params[:employment_basis],
      category_service: params[:category_service],
      tax_status: get_tax_status,
      dependents_number: params[:dependents_number],
      personnel_action: params[:personnel_action],
      payroll_group_id: params[:payroll_group_id],
      include_dtr: params[:include_dtr],
      attendance_scheme_id: params[:attendance_scheme_id],
      health_insurance_exception: params[:health_insurance_exception],
      include_in_payroll: params[:include_in_payroll],
      hazard_pay_factor: params[:hazard_pay_factor],
      plantilla_id: params[:plantilla_id],
      position_date: params[:position_date],
      date_increment: params[:date_increment],
      head_of_the_agency: params[:head_of_the_agency],
      step_number: params[:step_number]
    }
  end

  def get_tax_status
    TAX_STATUSES.include?(params[:tax_status]) ? params[:tax_status] : "Z"
  end

  def invalid_employee_message
    response[:success] = false
    response[:details] = "Invalid employee Id"
    self
  end

  def create_response(error=nil)
    if !@position.blank? && @position.errors.blank?
      response[:success] = true
      response[:details] = @position
    else
      response[:success] = false
      response[:details] = error || @position.errors.full_messages.join(', ')
    end
  end

end
