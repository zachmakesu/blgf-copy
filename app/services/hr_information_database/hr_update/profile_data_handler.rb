 class HrUpdate::ProfileDataHandler
  include ApiHelper
  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def update
    ActiveRecord::Base.transaction do
      if @user.update_attributes(update_params)
        if params[:image].present? && params[:image].is_a?(Hash)
          avatar = @user.photos.employee.find_by(position: 0)
          if avatar
            avatar.update(image: build_attachment(params[:image])) 
          else
            @user.photos.employee.create(image: build_attachment(params[:image]), position: 0) 
          end
        end

        if params[:signature_image].present? && params[:signature_image].is_a?(Hash)
          avatar = @user.photos.signature.find_by(position: 0)
          if avatar
            avatar.update(image: build_attachment(params[:signature_image])) 
          else
            @user.photos.signature.create(image: build_attachment(params[:signature_image]), position: 0)
          end
        end

        @user.reload
      else
        @errors = @user.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def add_address
    ActiveRecord::Base.transaction do
      if @address = user.addresses.create(address_params)
      else
        @errors = @address.errors.full_messages.join(', ')
      end
    end

    create_response_address(@errors)
    self
  end

  def update_address
    ActiveRecord::Base.transaction do
      @address = user.addresses.find(params[:id])
      if @address.update_attributes(address_params)
        @address.reload
      else
        @errors = @address.errors.full_messages.join(', ')
      end
    end
    create_response_address(@errors)
    self
  end

  def delete_address
    @address = user.addresses.find(params[:id])
    @address.destroy
    create_response
    self
  end

  def add_contact
    ActiveRecord::Base.transaction do
      if @contact = user.contacts.create(contact_params)
      else
        @errors = @contact.errors.full_messages.join(', ')
      end
    end

    create_response_contact(@errors)
    self
  end

  def update_contact
    ActiveRecord::Base.transaction do
      @contact = user.contacts.find(params[:id])

      if @contact.update_attributes(contact_params)
        @contact.reload
      else
        @errors = @contact.errors.full_messages.join(', ')
      end
    end
    create_response_contact(@errors)
    self
  end

  def delete_contact
    @contact = user.contacts.find(params[:id])
    @contact.destroy
    create_response
    self
  end

  private

  def address_params
    {
    location: params[:location],
    residency: params[:residency], 
    zipcode: params[:zipcode], 
    contact_number: params[:contact_number]
    }
  end

  def contact_params
    {
    device: params[:device],
    number: params[:number]
    }
  end

  def update_params
    {
      email:              params[:email],
      first_name:         params[:first_name],
      last_name:          params[:last_name],
      middle_name:        params[:middle_name],
      middle_initial:     params[:middle_initial],
      name_extension:     params[:name_extension],
      birthdate:          params[:birthdate],
      gender:             params[:gender],
      civil_status:       params[:civil_status],
      citizenship:        params[:citizenship],
      height:             params[:height],
      weight:             params[:weight],
      blood_type:         params[:blood_type],
      tin_number:         params[:tin_number],
      gsis_policy_number: params[:gsis_policy_number],
      pagibig_id_number:  params[:pagibig_id_number],
      philhealth_number:  params[:philhealth_number],
      sss_number:         params[:sss_number],
      remittance_id:      params[:remittance_id],
      biometric_id:       user.biometric_id
    }
  end

  def create_response(error=nil)
    if !@user.nil? && @user.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @user.errors.full_messages.uniq.join(', ')
    end
  end

  def create_response_address(error=nil)
    if !@address.nil? && @address.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @address.errors.full_messages.uniq.join(', ')
    end
  end
  
  def create_response_contact(error=nil)
    if !@contact.nil? && @contact.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @contact.errors.full_messages.uniq.join(', ')
    end
  end

end
