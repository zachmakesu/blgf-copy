class HrUpdate::TrainingHandler

  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      if @training = user.trainings_and_seminars.create(training_params)
      else
        @errors = @training.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update
    ActiveRecord::Base.transaction do
      @training = user.trainings_and_seminars.find(params[:id])
      if @training.update_attributes(training_params)
        @training.reload
      else
        @errors = @training.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @training = user.trainings_and_seminars.find(params[:id])
    @training.destroy
    create_response
    self
  end

  private
  def training_params
    {
      title:                 params[:title],
      inclusive_date_from:   params[:inclusive_date_from],
      inclusive_date_to:     params[:inclusive_date_to],
      number_of_hours:       params[:number_of_hours],
      conducted_by:          params[:conducted_by]
    }
  end

  def create_response(error=nil)
    if !@training.nil? && @training.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @training.errors.full_messages.uniq.join(', ')
    end
  end

end
