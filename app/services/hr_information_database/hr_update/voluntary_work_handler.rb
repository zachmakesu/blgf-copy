class HrUpdate::VoluntaryWorkHandler

  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      if @voluntary_work = user.voluntary_works.create(voluntary_work_params)
      else
        @errors = @voluntary_work.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update
    ActiveRecord::Base.transaction do
      @voluntary_work = user.voluntary_works.find(params[:id])
      if @voluntary_work.update_attributes(voluntary_work_params)
        @voluntary_work.reload
      else
        @errors = @voluntary_work.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @voluntary_work = user.voluntary_works.find(params[:id])
    @voluntary_work.destroy
    create_response
    self
  end
  
  private
  def voluntary_work_params
    {
      name_of_organization:      params[:name_of_organization],
      address_of_organization:   params[:address_of_organization],
      inclusive_date_from:       params[:inclusive_date_from],
      inclusive_date_to:         params[:inclusive_date_to],
      number_of_hours:           params[:number_of_hours],
      position_nature_of_work:   params[:position_nature_of_work]
    }
  end

  def create_response(error=nil)
    if !@voluntary_work.nil? && @voluntary_work.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @voluntary_work.errors.full_messages.uniq.join(', ')
    end
  end

end
