class HrUpdate::WorkExperienceHandler

  attr_accessor :params, :user, :response

  def initialize(params, user)
    @user = user
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      if @work_experience = user.work_experiences.create(work_experience_params)
      else
        @errors = @work_experience.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update
    ActiveRecord::Base.transaction do
      @work_experience = user.work_experiences.find(params[:id])
      if @work_experience.update_attributes(work_experience_params)
        @work_experience.reload
      else
        @errors = @work_experience.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @work_experience = user.work_experiences.find(params[:id])
    @work_experience.destroy
    create_response
    self
  end

  private
  def work_experience_params
    {
      position_title:             params[:position_title],
      inclusive_date_from:        params[:inclusive_date_from],
      inclusive_date_to:          params[:inclusive_date_to],
      department_agency_office:   params[:department_agency_office],
      monthly_salary:             params[:monthly_salary],
      salary_grade_and_step:      params[:salary_grade_and_step],
      appointment_status_id:      params[:appointment_status_id],
      government_service:         params[:government_service]
    }
  end

  def create_response(error=nil)
    if !@work_experience.nil? && @work_experience.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @work_experience.errors.full_messages.uniq.join(', ')
    end
  end

end
