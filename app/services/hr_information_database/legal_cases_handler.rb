LegalCasesHandlerError = Class.new(StandardError)

class LegalCasesHandler
  ADMIN_SLUG = "legal_cases"
  EMPLOYEE_SLUG = "my_legal_cases"
  attr_accessor :params, :response

  def initialize(params, user, searched_employee = nil)
    @params   = params
    @response = Hash.new
    @user = user
    @searched_employee = searched_employee
  end

  def create_legal_case
    ActiveRecord::Base.transaction do
      @mod_params = post_params
      @mod_params[:user_id] = @searched_employee.present? ? @searched_employee.id : @user.id
      @legal_case  = LegalCase.create(@mod_params)
      message = "#{@user.capitalize_name} created" + " " + "#{@user.role_message}" + " " + "legal case with case number: \"#{@legal_case.case_number}\"."
      if @user.hr?
        NotificationHandler.new(@legal_case, message, "hr", "#{ADMIN_SLUG}/#{@legal_case.id}", @user.id).create 
      else
        NotificationHandler.new(@legal_case, message, "all_user", "#{EMPLOYEE_SLUG}/#{@user.employee_id}/#{@legal_case.id}", @user.id).create 
      end
    end

    create_response
    self
  end

  def update_legal_case
    @legal_case = LegalCase.find_by(id: params[:legal_case_id])
    ActiveRecord::Base.transaction do
      message = "#{@user.capitalize_name} updated" + " " + "#{@user.role_message}" + " " + "legal case with case number: \"#{@legal_case.case_number}\"."
      @legal_case.update_attributes(post_params)
      if @user.hr?
        NotificationHandler.new(@legal_case, message, "hr", "#{ADMIN_SLUG}/#{@legal_case.id}", @user.id).create 
      else
        NotificationHandler.new(@legal_case, message, "all_user", "#{EMPLOYEE_SLUG}/#{@user.employee_id}/#{@legal_case.id}", @user.id).create 
      end
    end

    create_response
    self
  end

  def delete_legal_case
    @legal_case = LegalCase.find_by(id: params[:legal_case_id])
    @passed_object = @legal_case
    ActiveRecord::Base.transaction do
      message = "#{@user.capitalize_name} deleted" + " " + "#{@user.role_message}" + " " + "legal case with case number: \"#{@legal_case.case_number}\"."
      @legal_case.destroy
      if @user.hr?
        NotificationHandler.new(@legal_case, message, "hr", "#{ADMIN_SLUG}/#{@legal_case.id}", @user.id).create 
      else
        NotificationHandler.new(@legal_case, message, "all_user", "#{EMPLOYEE_SLUG}/#{@user.employee_id}/#{@legal_case.id}", @user.id).create 
      end
    end
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || LegalCase.count == 0

    ids = params[:ids].split('^').map(&:to_i).uniq
    ids.each do |id| 
      datum = LegalCase.find_by(id: id) 
      if datum.present?
        @legal_case = datum
        @passed_object = @legal_case
        @legal_case.destroy
        message = "#{@user.capitalize_name} deleted case \"#{@passed_object.case_number}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@legal_case = LegalCase.new) if @legal_case.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      case_number: params[:case_number],
      title: params[:title],
      charged_offenses: params[:charged_offenses],
      status: params[:status]
    }
  end

  def create_response(error=nil)
    if !@legal_case.blank? && @legal_case.errors.blank?
      response[:success] = true
      response[:details] = @legal_case
    else
      response[:success] = false
      response[:details] = error || @legal_case.errors.full_messages.join(', ')
    end
  end
end
