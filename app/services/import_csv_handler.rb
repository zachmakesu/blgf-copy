ImportCsvHandlerError = Class.new(StandardError)

class ImportCsvHandler
  include ApiHelper
  UPLOADTYPE = %w{ position_codes plantilles plantilla_groups users executive_offices services divisions sections position_details pending_cases leave_credits areas employee_deductions employee_benefits exam_types courses benefits deduction_types }
  attr_accessor :user, :csv_file, :upload_type, :response
  def initialize(csv_file, current_user, upload_type)
    @csv_file    = csv_file
    @upload_type = upload_type
    @user        = current_user
    @response    = Hash.new
  end

  def import_csv
    return error_message if invalid_csv?
    #save csv file first
    ActiveRecord::Base.transaction do
      @csv = user.csv_files.build()
      @csv.csv = build_attachment_file(@csv_file)
      if @csv.save
        @csv.reload
        total_rows = File.open(File.join(@csv.csv.path)).readlines.size
        @csv.update(total_rows: total_rows - 1)
        @csv.reload

        case upload_type
        when "position_codes"      then (PositionCodeImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "plantilla_groups"    then (PlantillaGroupsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "plantilles"          then (PlantillesImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "users"               then (UsersImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "executive_offices"   then (ExecutiveOfficesImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "services"            then (ServicesImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "divisions"           then (DivisionsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "sections"            then (SectionsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "position_details"    then (PositionDetailsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "pending_cases"       then (PendingCasesImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "leave_credits"       then (LeaveCreditsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "areas"               then (AreaImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "employee_deductions" then (EmployeeDeductionsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "employee_benefits"   then (EmployeeBenefitsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "exam_types"          then (ExamTypeImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "courses"             then (CourseImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "benefits"            then (BenefitsImportWorker.perform_async(@csv.id) if total_rows > 0)
        when "deduction_types"     then (DeductionTypesImportWorker.perform_async(@csv.id) if total_rows > 0)
        else
          #do nothing
        end

      else
        @errors = @csv.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  private
  def invalid_csv?
      @csv_file.blank? || !UPLOADTYPE.include?(@upload_type)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Csv"
    self
  end

  def create_response(errors = nil)
    if errors.blank?
      response[:success] = true
      response[:details] = {id: @csv.id, message:"Import processing Please wait to take effect" }
    else
      response[:success] = false
      response[:details] = errors || @csv.errors.full_messages.join(', ')
    end
  end

end
