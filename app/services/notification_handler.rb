NotificationHandlerError = Class.new(StandardError)

class NotificationHandler
  attr_accessor :object, :message, :role, :slug, :owner

  def initialize(object, message, role, slug, owner=nil)
    @owner = owner
    @object   = object
    @message  = message
    @role = role
    @slug = slug
  end

  def create
    notification = Notification.create(notificationable_type: @object.class ,message: @message, read_by: @role, api_slug: @slug, user_id: owner)
  end
  
end
