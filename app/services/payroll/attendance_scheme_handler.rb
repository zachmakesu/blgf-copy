AttendanceSchemeHandlerError = Class.new(StandardError)

class AttendanceSchemeHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @attendance_scheme  = AttendanceScheme.create(post_params)
    end

    create_response
    self
  end

  def update
    @attendance_scheme = AttendanceScheme.find_by(id: params[:id])
    return invalid_id if @attendance_scheme.blank? || AttendanceScheme.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:scheme_code) if @attendance_scheme.scheme_code == params[:scheme_code]

      if @attendance_scheme.update_attributes(update_params)
        @attendance_scheme.reload
      else
        @errors = @attendance_scheme.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @attendance_scheme = AttendanceScheme.find_by(id: params[:id])
    return invalid_id if @attendance_scheme.blank? || AttendanceScheme.count == 0
    @attendance_scheme.destroy
    create_response
    self
  end

  private
  def post_params
    { 
      scheme_code: params[:scheme_code],
      scheme_name: params[:scheme_name],
      am_time_in_from: params[:am_time_in_from],
      am_time_in_to: params[:am_time_in_to],
      pm_time_out_from: params[:pm_time_out_from],
      pm_time_out_to: params[:pm_time_out_to],
      nn_time_out_from: params[:nn_time_out_from],
      nn_time_out_to: params[:nn_time_out_to],
      nn_time_in_from: params[:nn_time_in_from],
      nn_time_in_to: params[:nn_time_in_to]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid attendance_scheme id"
    self
  end
  
  def create_response(error=nil)
    if !@attendance_scheme.blank? && @attendance_scheme.errors.blank?
      response[:success] = true
      response[:details] = @attendance_scheme
    else
      response[:success] = false
      response[:details] = error || @attendance_scheme.errors.full_messages.join(', ')
    end
  end

end
