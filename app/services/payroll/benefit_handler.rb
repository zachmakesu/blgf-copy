BenefitHandlerError = Class.new(StandardError)

class BenefitHandler
  ADMIN_SLUG = "benefits"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create
    ActiveRecord::Base.transaction do
      @benefit  = Benefit.create(post_params)
      message = "#{@current_user.capitalize_name} created Benefit \"#{@benefit.code}\"."
      NotificationHandler.new(@benefit, message, "hr", "#{ADMIN_SLUG}/#{@benefit.id}").create if @benefit.errors.blank?
    end

    create_response
    self
  end

  def update
    @benefit = Benefit.find_by(id: params[:id])
    return invalid_id if @benefit.blank? || Benefit.count == 0
    message = "#{@current_user.capitalize_name} updated Benefit \"#{@benefit.code}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      if @benefit.update_attributes(update_params)
        @benefit.reload
        NotificationHandler.new(@benefit, message, "hr", "#{ADMIN_SLUG}/#{@benefit.id}").create
      else
        @errors = @benefit.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @benefit = Benefit.find_by(id: params[:id])
    return invalid_id if @benefit.blank? || Benefit.count == 0
 
    @passed_object = @benefit
    @benefit.destroy
    message = "#{@current_user.capitalize_name} deleted Benefit \"#{@passed_object.code}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || Benefit.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Benefit.find_by(id: id) 
      if datum.present?
        @benefit = datum
        @passed_object = @benefit
        @benefit.destroy
        message = "#{@current_user.capitalize_name} deleted Benefit \"#{@passed_object.code}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end
    
    (@benefit = Benefit.new) if @benefit.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      code: params[:code],
      benefit_type: params[:benefit_type]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid benefit id"
    self
  end
  
  def create_response(error=nil)
    if !@benefit.blank? && @benefit.errors.blank?
      response[:success] = true
      response[:details] = @benefit
    else
      response[:success] = false
      response[:details] = error || @benefit.errors.full_messages.join(', ')
    end
  end

end
