CalculateLeaveCreditsHandlerError = Class.new(StandardError)

class CalculateLeaveCreditsHandler
  
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def calculate_leave_credits
    return error_message if missing_date?
    return month_error_message if invalid_date?

    @dtrs = EmployeeDtr.where('dtr_date iLIKE :search', search: "%#{params[:year]}-#{params[:month]}%")

    return not_existing_dtr_error_message if not_existing_dtr?
    return already_calculated_error_message if already_calculated?

    ActiveRecord::Base.transaction do
      calculation_date = "#{params[:year]}-#{params[:month]}"

      User.all.each do |user|
        user.calculate_leave_credits_this_month("#{calculation_date}-01")
      end

      LeaveCreditCalculationHistory.create(calculation_date: calculation_date, proccessed_by: current_user.id )
    end

    create_response
    self
  end

  private

  def missing_date?
    params[:year].blank? || params[:month].blank?
  end

  def invalid_date?
    (params[:year].to_i * 12 + params[:month].to_i) >= (Date.today.year * 12 + Date.today.month)
  end

  def not_existing_dtr?
    @dtrs.blank?
  end

  def already_calculated?
    LeaveCreditCalculationHistory.find_by(calculation_date: "#{params[:year]}-#{params[:month]}")
  end

  def error_message
    response[:success] = false
    response[:details] = "Missing date"
    self
  end

  def month_error_message
    response[:success] = false
    response[:details] = "Date should be lower than current month"
    self
  end

  def not_existing_dtr_error_message
    response[:success] = false
    response[:details] = "Please upload DTR of the selected month"
    self
  end
  
  def already_calculated_error_message
    response[:success] = false
    response[:details] = "Leave Credit calculation already done for #{params[:year]}-#{params[:month]}"
    self
  end

  def create_response
    response[:success] = true
    response[:details] = "Adjustments on Leave Credits for #{params[:year]}-#{params[:month]} applied"
  end

end
