CompensationHandlerError = Class.new(StandardError)

class CompensationHandler

  attr_accessor :response, :employee, :date

  def initialize(employee, date = nil)
    @response  = Hash.new
    @employee  = employee
    date.blank? ? date = "#{Time.now.to_s[0,7]}" : date = date

    y, m = date.split '-'
    d = 1
    if Date.valid_date? y.to_i, m.to_i, d.to_i
      Time.parse("#{date[0,7]}-01") >= Time.now ? @date = (Time.now - 1.months).to_s[0,7] : @date = date[0,7]
    else
      @date = "#{(Time.now - 1.months ).to_s[0,7]}"
    end
  end

  def compensation_summary
    #Authorized salary = Authorized Salary of employee base on its Salary Grade.
    #Days = Last month total Days
    #
    #Total deductible leave credits = Late/ Absent/ UT that is converted to deductible Leave Credits(float)
    #Check employee if its lc is enough or not. 
    #total deductible leave cretits

    #weekly_tdlc check if user has no LC and has absent UT and late

    weekly_authorized = authorized_salary / 4
    weekly_deductions = (deductions / 4) + weekly_tdlc
    net_pay = weekly_authorized - weekly_deductions

    weekly_compensation(weekly_authorized, weekly_deductions, net_pay)

  end

  private
  
  def weekly_tdlc
    y, m = @date.split '-'
    days = EmployeeDtr.days_of(m.to_i,y.to_i)
    tdlc = 1.626 #it should be employee.tdlc.present? tldc = employee.tdlc : tldc = 0
    ((authorized_salary / days) * tdlc) / 4
  end

  def authorized_salary
    employee.authorized_salary
  end

  def deductions
    list = employee.deduction_histories.date_applied(@date).first #Mandatory and Optional deductions
    list.present? ? list.deduction_history_children.sum(:amount) : 0
  end

  def weekly_compensation(weekly_authorized, weekly_deductions, net_pay)
    [
      {
      week: 1,
      period_pay: weekly_authorized,
      benefits: nil,
      deductions: weekly_deductions,
      net_pay: net_pay
      },
      {
        week: 2,
        period_pay: weekly_authorized,
        benefits: nil,
        deductions: weekly_deductions,
        net_pay: net_pay
      },
      {
        week: 3,
        period_pay: weekly_authorized,
        benefits: nil,
        deductions: weekly_deductions,
        net_pay: net_pay
      },
      {
        week: 4,
        period_pay: weekly_authorized,
        benefits: nil,
        deductions: weekly_deductions,
        net_pay: net_pay
      }
    ]
  end

end
