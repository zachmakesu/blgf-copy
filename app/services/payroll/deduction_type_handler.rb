DeductionTypeHandlerError = Class.new(StandardError)

class DeductionTypeHandler
  ADMIN_SLUG = "deduction_types"
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create
    ActiveRecord::Base.transaction do
      @deduction_type  = DeductionType.create(post_params)
      message = "#{@current_user.capitalize_name} created Deduction Type \"#{@deduction_type.deduction_code}\"."
      NotificationHandler.new(@deduction_type, message, "hr", "#{ADMIN_SLUG}/#{@deduction_type.id}").create if @deduction_type.errors.blank?
    end

    create_response
    self
  end

  def update
    @deduction_type = DeductionType.find_by(id: params[:id])
    return invalid_id if @deduction_type.blank? || DeductionType.count == 0
    message = "#{@current_user.capitalize_name} updated Deduction Type \"#{@deduction_type.deduction_code}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:deduction_code) if @deduction_type.deduction_code == params[:deduction_code]

      if @deduction_type.update_attributes(update_params)
        @deduction_type.reload
        NotificationHandler.new(@deduction_type, message, "hr", "#{ADMIN_SLUG}/#{@deduction_type.id}").create
      else
        @errors = @deduction_type.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @deduction_type = DeductionType.find_by(id: params[:id])
    return invalid_id if @deduction_type.blank? || DeductionType.count == 0

    @passed_object = @deduction_type
    @deduction_type.destroy
    message = "#{@current_user.capitalize_name} deleted Deduction Type \"#{@passed_object.deduction_code}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || DeductionType.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = DeductionType.find_by(id: id) 
      if datum.present?
        @deduction_type = datum
        @passed_object = @deduction_type
        @deduction_type.destroy
        message = "#{@current_user.capitalize_name} deleted Deduction Type \"#{@passed_object.deduction_code}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end

    (@deduction_type = DeductionType.new) if @deduction_type.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      deduction: params[:deduction],
      deduction_code: params[:deduction_code],
      mandatory: params[:mandatory],
      deduction_base_calculations: params[:deduction_base_calculations]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid deduction_type id"
    self
  end
  
  def create_response(error=nil)
    if !@deduction_type.blank? && @deduction_type.errors.blank?
      response[:success] = true
      response[:details] = @deduction_type
    else
      response[:success] = false
      response[:details] = error || @deduction_type.errors.full_messages.join(', ')
    end
  end

end
