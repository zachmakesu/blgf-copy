EmployeeBenefitsHandlerError = Class.new(StandardError)

class EmployeeBenefitsHandler

  attr_accessor :params, :employee, :response

  def initialize(params)
    @employee = User.find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def update_set_benefit
    return invalid_employee_id if employee.blank?
    fields = [ 
            :amount,
            :overwrite
            ]
    model = :set_benefit

    ActiveRecord::Base.transaction do
      @set_benefit = employee.set_benefits.find(params[:id])

      if @set_benefit.update(permited_params(model, fields))
        @set_benefit.reload
      else
        @errors = @set_benefit.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  private
  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def invalid_employee_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end
  
  def create_response(errors = nil)
    if @errors.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = @set_benefit.errors.full_messages.join(', ')
    end
  end

end
