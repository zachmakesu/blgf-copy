EmployeeDeductionsHandlerError = Class.new(StandardError)

class EmployeeDeductionsHandler

  attr_accessor :params, :employee, :response, :deduction_type

  def initialize(params)
    @employee = User.find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def create_history
    return invalid_employee_id if employee.blank?
    return invalid_date if params[:date].blank?

    @deduction_history = employee.deduction_histories.create(date_applied: params[:date])
    if @deduction_history.errors.blank?
      @deduction_history.deduction_history_children.create(deductives)
    else
      @errors = @deduction_history.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  # def update_deductives
  #   return invalid_employee_id if employee.blank?
  #   fields = [ 
  #           :deduction_type_id,
  #           :amount
  #           ]
  #   model = :deductive

  #   ActiveRecord::Base.transaction do
  #     @deductive = employee.deductives.find(params[:id])

  #     if @deductive.update(permited_params(model, fields))
  #       @deductive.reload
  #     else
  #       @errors = @deductive.errors.full_messages.join(', ')
  #     end
  #   end

  #   create_response(@errors)
  #   self
  # end

  def delete_history
    return invalid_employee_id if employee.blank?
    @deduction_history = employee.deduction_histories.find(params[:history_id])
    @deduction_history.destroy
    create_response
    self
  end

  private

  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def invalid_employee_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end

  def invalid_date
    response[:success] = false
    response[:details] = "Invalid date"
    self
  end
  
  def deductives
    employee.deductives.map do |deductive|
      {
        deduction_type_id: deductive.deduction_type_id,
        amount: deductive.amount
      }
    end
  end

  def create_response(errors = nil)
    if @errors.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = @deduction_history.errors.full_messages.join(', ')
    end
  end

end
