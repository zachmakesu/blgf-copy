EmployeeDeductivesHandlerError = Class.new(StandardError)

class EmployeeDeductivesHandler

  attr_accessor :params, :employee, :response

  def initialize(params)
    @employee = User.find_by(employee_id: params[:employee_id])
    @params   = params
    @response = Hash.new
  end

  def update_deductives
    return invalid_employee_id if employee.blank?
    fields = [ 
            :amount,
            :overwrite
            ]
    model = :deductive

    ActiveRecord::Base.transaction do
      @deductive = employee.deductives.find(params[:id])

      if @deductive.update(permited_params(model, fields))
        @deductive.reload
      else
        @errors = @deductive.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  private
  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def invalid_employee_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end
  
  def create_response(errors = nil)
    if @errors.blank?
      response[:success] = true
      response[:details] = employee
    else
      response[:success] = false
      response[:details] = @deductive.errors.full_messages.join(', ')
    end
  end

end
