EmployeeDtrCsvHandlerError = Class.new(StandardError)

class EmployeeDtrCsvHandler
  include ApiHelper
  attr_accessor :csv_file, :params, :response, :user

  def initialize(params, user)
    @csv_file = params[:employee_attendances]
    @params   = params
    @user     = user
    @response = Hash.new
  end

  def import_attendances
    return invalid_csv if csv_file.blank?
    #save csv file first
    ActiveRecord::Base.transaction do
      @csv = user.csv_files.build()
      @csv.csv = build_attachment_file(csv_file)
      if @csv.save  
        @csv.reload  
        total_rows = File.open(File.join(@csv.csv.path)).readlines.size
        @csv.update(total_rows: total_rows - 1)
        @csv.reload 
        EmployeeDtrImportWorker.perform_async(@csv.id) if total_rows > 0
      else
        @errors = @csv.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  private
  
  def invalid_csv
    response[:success] = false
    response[:details] = "Invalid file name"
    self
  end

  def create_response(errors = nil)
    if errors.blank?
      response[:success] = true
      response[:details] = {id: @csv.id, message:"Import processing Please wait to take effect" }
    else
      response[:success] = false
      response[:details] = errors || @csv.errors.full_messages.join(', ')
    end
  end

end
