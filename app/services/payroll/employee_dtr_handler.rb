EmployeeDtrHandlerError = Class.new(StandardError)

class EmployeeDtrHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  
  def get_dtr
    @employee = User.find_by(employee_id: params[:employee_id])
    return invalid_employee if @employee.blank?

    @dtr = EmployeeDtr.find_by(biometric_id: @employee.biometric_id, dtr_date: params[:dtr_date])
    return invalid_employee_dtr if @dtr.blank?

    create_response
    self
  end

  def update_dtr
    @employee = User.find_by(employee_id: params[:employee_id])
    return invalid_employee if @employee.blank?

    @dtr = EmployeeDtr.find_by(biometric_id: @employee.biometric_id, dtr_date: params[:dtr_date])
    return invalid_employee_dtr if @dtr.blank?

    if @dtr.update_attributes(post_params)
      @dtr.reload
    else
      @errors = @dtr.errors.full_messages.join(', ')
    end

    create_response(@errors)
    self
  end

  def add_dtr
    @employee = User.find_by(employee_id: params[:employee_id])
    return invalid_employee if @employee.blank?

    @dtr = EmployeeDtr.find_by(biometric_id: @employee.biometric_id, dtr_date: params[:dtr_date])
    return already_exist_employee_dtr if @dtr.present?

    @dtr = EmployeeDtr.create(create_params)
    create_response
    self
  end

  def delete_dtr
    @employee = User.find_by(employee_id: params[:employee_id])
    return invalid_employee if @employee.blank?

    @dtr = EmployeeDtr.find_by(biometric_id: @employee.biometric_id, dtr_date: params[:dtr_date])
    return invalid_employee_dtr if @dtr.blank?
    @dtr.destroy
    create_response
    self
  end

  private
  def post_params
    { 
      in_am: params[:in_am],
      out_am: params[:out_am],
      in_pm: params[:in_pm],
      out_pm: params[:out_pm],
      old_value: old_value,
      processed_by: params[:processed_by]
    }
  end

  def create_params
    { 
      biometric_id: @employee.biometric_id,
      dtr_date: params[:dtr_date],
      in_am: params[:in_am],
      out_am: params[:out_am],
      in_pm: params[:in_pm],
      out_pm: params[:out_pm],
      processed_by: params[:processed_by]
    }
  end

  def old_value
    dtr = EmployeeDtr.find_by(biometric_id: @employee.biometric_id, dtr_date: params[:dtr_date])

    {
    in_am: dtr.in_am,
    out_am: dtr.out_am,
    in_pm: dtr.in_pm,
    out_pm: dtr.out_pm,
    in_ot: dtr.in_ot,
    out_ot: dtr.out_ot,
    ot: dtr.ot,
    late: dtr.late,
    ut: dtr.ut
    }
  end

  def invalid_employee
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end

  def invalid_employee_dtr
    response[:success] = false
    response[:details] = "Invalid employee's dtr date"
    self
  end

  def already_exist_employee_dtr
    response[:success] = false
    response[:details] = "Employee's dtr date already exist"
    self
  end
  
  def create_response(error=nil)
    if !@dtr.blank? && @dtr.errors.blank?
      response[:success] = true
      response[:details] = @dtr
    else
      response[:success] = false
      response[:details] = error || @dtr.errors.full_messages.join(', ')
    end
  end

end
