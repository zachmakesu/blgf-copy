GeneratePayslipHandlerError = Class.new(StandardError)

class GeneratePayslipHandler
  
  attr_accessor :params, :response, :current_user

  def initialize(params, current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def generate
    return error_message if missing_date?
    return month_error_message if invalid_date?
    
    ActiveRecord::Base.transaction do
      date = "#{params[:year]}-#{params[:month]}"
      User.all.each { |user| GeneratePayslipWorker.perform_async(user.id, date) }
    end

    create_response
    self
  end

  private

  def missing_date?
    params[:year].blank? || params[:month].blank?
  end

  def invalid_date?
    (params[:year].to_i * 12 + params[:month].to_i) >= (Date.today.year * 12 + Date.today.month)
  end

  def error_message
    response[:success] = false
    response[:details] = "Missing date"
    self
  end

  def month_error_message
    response[:success] = false
    response[:details] = "Date should be lower than current month"
    self
  end
  
  def create_response
    response[:success] = true
    response[:details] = "Generating payslip for #{params[:year]}-#{params[:month]} "
  end

end
