GlobalDeductionHandlerError = Class.new(StandardError)

class GlobalDeductionHandler
  attr_accessor :deduction_type, :params, :response

  def initialize(params)
    @params = params
    @deduction_type = DeductionType.find(params[:deduction_type_id])
    @response = Hash.new
  end

  
  def set_global_deduction
    User.all.each do |user|
      ActiveRecord::Base.transaction do
        deductive = user.deductives.find_by(deduction_type_id: params[:deduction_type_id])
        deductive.update_attributes(amount: params[:amount].to_f, overwrite: true) if deductive
      end
    end
    create_response
    self
  end

  private
  def create_response
    response[:success] = true
    response[:details] = "Global Deduction Set!"
  end
end
