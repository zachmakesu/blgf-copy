LeaveTypeHandlerError = Class.new(StandardError)

class LeaveTypeHandler

  attr_accessor :parent, :params, :response

  def initialize(params, parent)
    @params   = params
    @parent   = parent
    @response = Hash.new
  end

  def create_leave_type
    ActiveRecord::Base.transaction do
      @leave_type  = LeaveType.create(post_params)
    end

    create_response
    self
  end

  def update_leave_type
    @leave_type = LeaveType.no_specific_leaves.find_by(id: params[:id])
    return invalid_leave_type_id if @leave_type.blank? || LeaveType.no_specific_leaves.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:leave_code) if @leave_type.leave_code == params[:leave_code]

      if @leave_type.update_attributes(update_params)
        @leave_type.reload
      else
        @errors = @leave_type.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_leave_type
    @leave_type = LeaveType.no_specific_leaves.find_by(id: params[:id])
    return invalid_leave_type_id if @leave_type.blank? || LeaveType.no_specific_leaves.count == 0
    @leave_type.destroy
    create_response
    self
  end


  def create_specific_leave
    leave_type = LeaveType.no_specific_leaves.find_by(id: params[:leave_type_id])
    return invalid_leave_type_id if leave_type.blank? || LeaveType.no_specific_leaves.count == 0
    ActiveRecord::Base.transaction do
      @leave_type  = LeaveType.create(post_params)
    end

    create_response
    self
  end

  def update_specific_leave
    @specific_leave = LeaveType.specific_leaves.find_by(id: params[:id])
    leave_type = LeaveType.no_specific_leaves.find_by(id: params[:leave_type_id])

    return invalid_leave_type_id if leave_type.blank?
    return invalid_specific_leave_id if @specific_leave.blank? || LeaveType.specific_leaves.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      @leave_type = @specific_leave
      if @leave_type.update_attributes(update_params)
        @leave_type.reload
      else
        @errors = @leave_type.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_specific_leave
    @leave_type = LeaveType.specific_leaves.find_by(id: params[:id])
    return invalid_specific_leave_id if @leave_type.blank? || LeaveType.specific_leaves.count == 0
    @leave_type.destroy
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || LeaveType.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = LeaveType.find_by(id: id) 
      if datum.present?
        @leave_type = datum
        @leave_type.destroy
      end
    end

    (@leave_type = LeaveType.new) if @leave_type.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    if parent
      { 
        leave_code: params[:leave_code],
        leave_type: params[:leave_type],
        no_of_days: params[:no_of_days],
        parent: parent
      }
    else
      { 
        leave_type_id: params[:leave_type_id],
        specific_leave: params[:specific_leave],
        parent: parent
      }
    end
  end

  def invalid_leave_type_id
    response[:success] = false
    response[:details] = "Invalid or missing leave type id"
    self
  end

  def invalid_specific_leave_id
    response[:success] = false
    response[:details] = "Invalid specific leave id"
    self
  end
  
  def create_response(error=nil)
    if !@leave_type.blank? && @leave_type.errors.blank?
      response[:success] = true
      response[:details] = @leave_type
    else
      response[:success] = false
      response[:details] = error || @leave_type.errors.full_messages.join(', ')
    end
  end

end
