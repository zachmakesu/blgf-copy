PayrollGroupHandlerError = Class.new(StandardError)

class PayrollGroupHandler
  ADMIN_SLUG = "payroll_groups"
  attr_accessor :params, :response, :current_user

  def initialize(params,current_user)
    @params   = params
    @response = Hash.new
    @current_user = current_user
  end

  def create_payroll_group
    ActiveRecord::Base.transaction do
      @payroll_group  = PayrollGroup.create(post_params)
      message = "#{@current_user.capitalize_name} created Payroll Group \"#{@payroll_group.payroll_group_name}\"."
      NotificationHandler.new(@payroll_group, message, "hr", "#{ADMIN_SLUG}/#{@payroll_group.id}").create if @payroll_group.errors.blank?
    end

    create_response
    self
  end

  def update_payroll_group
    @payroll_group = PayrollGroup.find_by(id: params[:id])
    return invalid_id if @payroll_group.blank? || PayrollGroup.count == 0
    message = "#{@current_user.capitalize_name} created Payroll Group \"#{@payroll_group.payroll_group_name}\"."
    ActiveRecord::Base.transaction do
      update_params = post_params
      update_params.delete(:payroll_group_code) if @payroll_group.payroll_group_code == params[:payroll_group_code]
      update_params.delete(:payroll_group_order) if @payroll_group.payroll_group_order == params[:payroll_group_order]

      if @payroll_group.update_attributes(update_params)
        @payroll_group.reload
        NotificationHandler.new(@payroll_group, message, "hr", "#{ADMIN_SLUG}/#{@payroll_group.id}").create 
      else
        @errors = @payroll_group.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_payroll_group
    @payroll_group = PayrollGroup.find_by(id: params[:id])
    return invalid_id if @payroll_group.blank? || PayrollGroup.count == 0
    @passed_object = @payroll_group
    @payroll_group.destroy
    message = "#{@current_user.capitalize_name} deleted Payroll Group \"#{@passed_object.payroll_group_name}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || PayrollGroup.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = PayrollGroup.find_by(id: id) 
      if datum.present?
        @payroll_group = datum
        @passed_object = @payroll_group
        @payroll_group.destroy
        message = "#{@current_user.capitalize_name} deleted Payroll Group \"#{@passed_object.payroll_group_name}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}/#{@passed_object.id}").create
      end
    end
    
    (@payroll_group = PayrollGroup.new) if @payroll_group.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    project_code = ProjectCode.find(params[:project_code_id])
    { 
      project_code_id: project_code.id,
      payroll_group_code: params[:payroll_group_code],
      payroll_group_name: params[:payroll_group_name],
      payroll_group_order: params[:payroll_group_order]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid payroll_group id"
    self
  end
  
  def create_response(error=nil)
    if !@payroll_group.blank? && @payroll_group.errors.blank?
      response[:success] = true
      response[:details] = @payroll_group
    else
      response[:success] = false
      response[:details] = error || @payroll_group.errors.full_messages.join(', ')
    end
  end

end
