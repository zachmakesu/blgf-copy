PhilHealthTableHandlerError = Class.new(StandardError)

class PhilHealthTableHandler
  attr_accessor :csv_file, :params, :response

  def initialize(params)
    @csv_file = params[:phil_health_table]
    @response = Hash.new
  end

  
  def import_csv_file
    return invalid_csv if csv_file.blank?
    require 'csv' 
    
    begin
      ActiveRecord::Base.transaction do
        PhilHealthTable.delete_all
        CSV.foreach(csv_file[:tempfile], headers: true) do |row|
          PhilHealthTable.create!(row.to_hash)
        end
      end
    rescue => @exception
      # something went wrong, transaction rolled back
    end

    create_response
    self
  end

  private

  def invalid_csv
    response[:success] = false
    response[:details] = "invalid_csv"
    self
  end

  def create_response
    if @exception.blank?
      response[:success] = true
      response[:details] = "Upload success!"
    else
      response[:success] = false
      response[:details] = @exception.message
    end
  end

end
