SalaryScheduleHandlerError = Class.new(StandardError)

class SalaryScheduleHandler
  attr_accessor :csv_file, :params, :response

  def initialize(params)
    @csv_file = params[:salary_schedule]
    @response = Hash.new
  end

  
  def import_csv_file
    return invalid_csv if csv_file.blank?
    require 'csv' 
    
    begin
      ActiveRecord::Base.transaction do
        SalarySchedule.delete_all
        file = File.read(csv_file[:tempfile], encoding: 'ISO-8859-1:UTF-8')
        file = file.tr("\n", '')
        CSV.parse(file, headers: true) do |row|
          SalarySchedule.create!(row.to_hash) if row.to_hash.present?
        end
      end
    rescue => @exception
      # something went wrong, transaction rolled back
    end

    create_response
    self
  end

  private

  def invalid_csv
    response[:success] = false
    response[:details] = "invalid_csv"
    self
  end

  def create_response
    if @exception.blank?
      response[:success] = true
      response[:details] = "Upload success!"
    else
      response[:success] = false
      response[:details] = @exception.message
    end
  end

end
