TaxGeneratorHandlerError = Class.new(StandardError)

class TaxGeneratorHandler

  attr_accessor :taxable_income, :tax_status, :schedule

  def initialize(taxable_income, tax_status, schedule)
    @taxable_income = taxable_income
    @tax_status     = tax_status
    @schedule       = schedule
  end

  def generate_tax
    if taxable_income <= 0
      0
    else
      tax_factor = bir_monthly_chart[get_tax_status][:tax_factors].select{ |tax_factor| tax_factor[:range] <= taxable_income }.last
      
      @range  = tax_factor[:range]
      @factor = tax_factor[:factor]
      @tax    = tax_factor[:tax]

      ((taxable_income - @range) * @factor) + @tax
    end
  end

  
  private

  def get_tax_status
    bir_monthly_chart.keys.include?(tax_status) ? tax_status : "Z"
  end

  def bir_monthly_chart
    {
      "Z" => {
                dependents: 0,
                ooop: 0.0,
                tax_factors: [
                                {
                                  range: 1.0,
                                  factor: 0.0,
                                  tax: 0.0
                                },
                                {
                                  range: 0.0,
                                  factor: 0.05,
                                  tax: 0.0
                                },
                                {
                                  range: 833.0,
                                  factor: 0.1,
                                  tax: 41.67
                                },
                                {
                                  range: 2500.0,
                                  factor: 0.15,
                                  tax: 208.33
                                },
                                {
                                  range: 5833.0,
                                  factor: 0.2,
                                  tax: 708.33
                                },
                                {
                                  range: 11667.0,
                                  factor: 0.25,
                                  tax: 1875.0
                                },
                                {
                                  range: 20833.0,
                                  factor: 0.3,
                                  tax: 4166.67
                                },
                                {
                                  range: 41667.0,
                                  factor: 0.32,
                                  tax: 10416.67
                                }
                              ]
              },
      "S / ME" => {
                    dependents: 0,
                    ooop: 50.0,
                    tax_factors: [
                                    {
                                      range: 1.0,
                                      factor: 0.0,
                                      tax: 0.0
                                    },
                                    {
                                      range: 4167.0,
                                      factor: 0.05,
                                      tax: 0.0
                                    },
                                    {
                                      range: 5000.0,
                                      factor: 0.1,
                                      tax: 41.67
                                    },
                                    {
                                      range: 6667.0,
                                      factor: 0.15,
                                      tax: 208.33
                                    },
                                    {
                                      range: 10000.0,
                                      factor: 0.2,
                                      tax: 708.33
                                    },
                                    {
                                      range: 15833.0,
                                      factor: 0.25,
                                      tax: 1875.0
                                    },
                                    {
                                      range: 25000.0,
                                      factor: 0.3,
                                      tax: 4166.67
                                    },
                                    {
                                      range: 45833.0,
                                      factor: 0.32,
                                      tax: 10416.67
                                    }
                                  ]
                  },
      "ME1 / S1" => {
                      dependents: 1,
                      ooop: 75.0,
                      tax_factors: [
                                      {
                                        range: 1.0,
                                        factor: 0.0,
                                        tax: 0.0
                                      },
                                      {
                                        range: 6250.0,
                                        factor: 0.05,
                                        tax: 0.0
                                      },
                                      {
                                        range: 7083.0,
                                        factor: 0.1,
                                        tax: 41.67
                                      },
                                      {
                                        range: 8750.0,
                                        factor: 0.15,
                                        tax: 208.33
                                      },
                                      {
                                        range: 12083.0,
                                        factor: 0.2,
                                        tax: 708.33
                                      },
                                      {
                                        range: 17917.0,
                                        factor: 0.25,
                                        tax: 1875.0
                                      },
                                      {
                                        range: 27083.0,
                                        factor: 0.3,
                                        tax: 4166.67
                                      },
                                      {
                                        range: 47917.0,
                                        factor: 0.32,
                                        tax: 10416.67
                                      }
                                    ]
                    },
      "ME2 / S2" => {
                      dependents: 2,
                      ooop: 100.0,
                      tax_factors: [
                                      {
                                        range: 1.0,
                                        factor: 0.0,
                                        tax: 0.0
                                      },
                                      {
                                        range: 8333.0,
                                        factor: 0.05,
                                        tax: 0.0
                                      },
                                      {
                                        range: 9167.0,
                                        factor: 0.1,
                                        tax: 41.67
                                      },
                                      {
                                        range: 10833.0,
                                        factor: 0.15,
                                        tax: 208.33
                                      },
                                      {
                                        range: 14167.0,
                                        factor: 0.2,
                                        tax: 708.33
                                      },
                                      {
                                        range: 20000.0,
                                        factor: 0.25,
                                        tax: 1875.0
                                      },
                                      {
                                        range: 29167.0,
                                        factor: 0.3,
                                        tax: 4166.67
                                      },
                                      {
                                        range: 50000.0,
                                        factor: 0.32,
                                        tax: 10416.67
                                      }
                                    ]
                    },
      "ME3 / S3" => {
                      dependents: 3,
                      ooop: 125.0,
                      tax_factors: [
                                      {
                                        range: 1.0,
                                        factor: 0.0,
                                        tax: 0.0
                                      },
                                      {
                                        range: 10417.0,
                                        factor: 0.05,
                                        tax: 0.0
                                      },
                                      {
                                        range: 11250.0,
                                        factor: 0.1,
                                        tax: 41.67
                                      },
                                      {
                                        range: 12917.0,
                                        factor: 0.15,
                                        tax: 208.33
                                      },
                                      {
                                        range: 16250.0,
                                        factor: 0.2,
                                        tax: 708.33
                                      },
                                      {
                                        range: 22083.0,
                                        factor: 0.25,
                                        tax: 1875.0
                                      },
                                      {
                                        range: 31250.0,
                                        factor: 0.3,
                                        tax: 4166.67
                                      },
                                      {
                                        range: 52083.0,
                                        factor: 0.32,
                                        tax: 10416.67
                                      }
                                    ]
                    },
      "ME4 / S4" => {
                      dependents: 4,
                      ooop: 150.0,
                      tax_factors: [
                                      {
                                        range: 1.0,
                                        factor: 0.0,
                                        tax: 0.0
                                      },
                                      {
                                        range: 12500.0,
                                        factor: 0.05,
                                        tax: 0.0
                                      },
                                      {
                                        range: 13333.0,
                                        factor: 0.1,
                                        tax: 41.67
                                      },
                                      {
                                        range: 15000.0,
                                        factor: 0.15,
                                        tax: 208.33
                                      },
                                      {
                                        range: 18333.0,
                                        factor: 0.2,
                                        tax: 708.33
                                      },
                                      {
                                        range: 24167.0,
                                        factor: 0.25,
                                        tax: 1875.0
                                      },
                                      {
                                        range: 33333.0,
                                        factor: 0.3,
                                        tax: 4166.67
                                      },
                                      {
                                        range: 54167.0,
                                        factor: 0.32,
                                        tax: 10416.67
                                      }
                                    ]
                    }
    }
  end

end
