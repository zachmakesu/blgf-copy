TaxRangeHandlerError = Class.new(StandardError)

class TaxRangeHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @tax_range  = TaxRange.create(post_params)
    end

    create_response
    self
  end

  def update
    @tax_range = TaxRange.find_by(id: params[:id])
    return invalid_id if @tax_range.blank? || TaxRange.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      if @tax_range.update_attributes(update_params)
        @tax_range.reload
      else
        @errors = @tax_range.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @tax_range = TaxRange.find_by(id: params[:id])
    return invalid_id if @tax_range.blank? || TaxRange.count == 0
    @tax_range.destroy
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || TaxRange.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = TaxRange.find_by(id: id) 
      if datum.present?
        @tax_range = datum
        @tax_range.destroy
      end
    end
    
    (@tax_range = TaxRange.new) if @tax_range.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def post_params
    { 
      tax_base: params[:tax_base],
      tax_deduction: params[:tax_deduction],
      factor: params[:factor],
      taxable_from: params[:taxable_from],
      taxable_to: params[:taxable_to]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid tax_range id"
    self
  end
  
  def create_response(error=nil)
    if !@tax_range.blank? && @tax_range.errors.blank?
      response[:success] = true
      response[:details] = @tax_range
    else
      response[:success] = false
      response[:details] = error || @tax_range.errors.full_messages.join(', ')
    end
  end

end
