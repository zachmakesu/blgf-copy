TaxStatusHandlerError = Class.new(StandardError)

class TaxStatusHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @tax_status  = TaxStatus.create(post_params)
    end

    create_response
    self
  end

  def update
    @tax_status = TaxStatus.find_by(id: params[:id])
    return invalid_id if @tax_status.blank? || TaxStatus.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      if @tax_status.update_attributes(update_params)
        @tax_status.reload
      else
        @errors = @tax_status.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @tax_status = TaxStatus.find_by(id: params[:id])
    return invalid_id if @tax_status.blank? || TaxStatus.count == 0
    @tax_status.destroy
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || TaxStatus.count == 0
    
    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = TaxStatus.find_by(id: id) 
      if datum.present?
        @tax_status = datum
        @tax_status.destroy
      end
    end
    
    (@tax_status = TaxStatus.new) if @tax_status.blank?
    create_response
    self
  end

  private
  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      tax_status: params[:tax_status],
      exemption_amount: params[:exemption_amount]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid tax_status id"
    self
  end
  
  def create_response(error=nil)
    if !@tax_status.blank? && @tax_status.errors.blank?
      response[:success] = true
      response[:details] = @tax_status
    else
      response[:success] = false
      response[:details] = error || @tax_status.errors.full_messages.join(', ')
    end
  end

end
