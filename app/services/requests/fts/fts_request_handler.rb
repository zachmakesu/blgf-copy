FtsRequestHandlerError = Class.new(StandardError)

class Fts::FtsRequestHandler

  attr_accessor :params, :owner, :response

  def initialize(params)
    @owner      = User.find(params[:owner_id])
    @params     = params
    @response   = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @fts_request = FtsRequest.create(post_params)
    end

    create_response
    self
  end

  def update
    @fts_request = owner.fts_requests.find_by(id: params[:id], status: 0)
    return invalid_id if @fts_request.blank? || owner.fts_requests.pending.count == 0  

    ActiveRecord::Base.transaction do
      update_params = post_params
      if @fts_request.update_attributes(update_params)
        @fts_request.reload
      else
        @errors = @fts_request.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @fts_request = owner.fts_requests.find_by(id: params[:id], status: 0)
    return invalid_id if @fts_request.blank? || owner.fts_requests.pending.count == 0 
    
    @fts_request.destroy

    create_response
    self
  end

  private
  def post_params
    dtr_date = Date.parse("#{params[:dtr_date]}") rescue nil 
    in_am = Time.parse("#{params[:in_am]}") rescue nil
    out_am = Time.parse("#{params[:out_am]}") rescue nil 
    in_pm = Time.parse("#{params[:in_pm]}") rescue nil
    out_pm = Time.parse("#{params[:out_pm]}") rescue nil 

    in_am = params[:in_am] if in_am
    out_am = params[:out_am] if out_am
    in_pm = params[:in_pm] if in_pm
    out_pm = params[:out_pm] if out_pm
    { 
      owner_id:  owner.id,
      biometric_id: owner.biometric_id,
      dtr_date: dtr_date,
      in_am: in_am,
      out_am: out_am,
      in_pm: in_pm,
      out_pm: out_pm
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Request id"
    self
  end
  
  def create_response(error=nil)
    if !@fts_request.blank? && @fts_request.errors.blank?
      response[:success] = true
      response[:details] = @fts_request
    else
      response[:success] = false
      response[:details] = error || @fts_request.errors.full_messages.join(', ')
    end
  end

end
