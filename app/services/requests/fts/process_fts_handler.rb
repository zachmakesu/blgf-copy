ProcessFtsHandlerError = Class.new(StandardError)

class Fts::ProcessFtsHandler

  attr_accessor :params, :response

  def initialize(params)
    @params         = params
    @response       = Hash.new
  end

  def process_update
    @fts_request    = FtsRequest.pending.find(params[:id])
    @dtr = EmployeeDtr.find_by(biometric_id: @fts_request.biometric_id, dtr_date: @fts_request.dtr_date)

    return invalid_id if @dtr.blank? 
    return missing_status if params[:status].blank? 
    
    if params[:status] == "approved"
      ActiveRecord::Base.transaction do
        @dtr.old_value = {
          in_am: @dtr.in_am,
          out_am: @dtr.out_am,
          in_pm: @dtr.in_pm,
          out_pm: @dtr.out_pm,
          ut: @dtr.ut,
          ot: @dtr.ot,
          late: @dtr.late,
          remarks: @dtr.remarks
        }
        
        @dtr.in_am  = @fts_request.in_am
        @dtr.out_am = @fts_request.out_am
        @dtr.in_pm  = @fts_request.in_am
        @dtr.out_pm = @fts_request.out_pm

        if @dtr.save
          if @fts_request.update_attributes(post_params)
            @fts_request.reload
          else
            @errors = @fts_request.errors.full_messages.join(', ')
          end
        else
          @errors = @dtr.errors.full_messages.join(', ')
        end
      end

    else
      if @fts_request.update_attributes(post_params)
        @fts_request.reload
      else
        @errors = @fts_request.errors.full_messages.join(', ')
      end
    end
  
    create_response(@errors)
    self
  end

  private

  def post_params
    {
      processor_id: params[:processor_employee_id], 
      status: params[:status], 
      remarks:  params[:remarks]
    }
  end

  def missing_status
    response[:success] = false
    response[:details] = "Missing status."
    self
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Dtr Date"
    self
  end
  
  def create_response(error=nil)
    if !@fts_request.blank? && @fts_request.errors.blank?
      response[:success] = true
      response[:details] = @fts_request
    else
      response[:success] = false
      response[:details] = error || @fts_request.errors.full_messages.join(', ')
    end
  end

end
