LeaveCreditsHandlerError = Class.new(StandardError)

class Leave::LeaveCreditsHandler

  attr_accessor :params, :response

  def initialize(params)
    @params         = params
    @response       = Hash.new
    @employee       = User.find_by(employee_id: params[:employee_id])
  end


  def add_or_remove_leave_credits
    @leave_credit_history = @employee.leave_credit_histories.create(create_params)

    #update position detail include in DTR if
    @employee.position_detail.update(include_in_payroll: (@employee.total_leave_credits > 10))

    create_response
    self
  end

  private
  def create_params
    {
      credit_type: params[:credit_type],
      credits: params[:credits],
      created_by: params[:created_by],
      deductible: params[:deductible]
    }
  end

  def create_response(error=nil)
    if !@leave_credit_history.blank? && @leave_credit_history.errors.blank?
      response[:success] = true
      response[:details] = @employee
    else
      response[:success] = false
      response[:details] = error || @leave_credit_history.errors.full_messages.join(', ')
    end
  end

end
