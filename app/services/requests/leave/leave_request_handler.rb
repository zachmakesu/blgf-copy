LeaveRequestHandlerError = Class.new(StandardError)

class Leave::LeaveRequestHandler
  ADMIN_SLUG = "leave_management_manager"
  ALL_SLUG = "leave_requests"
  attr_accessor :params, :owner, :slc, :vlc, :response

  def initialize(params)
    @owner      = User.find(params[:owner_id])
    @slc        = @owner.sick_leave_credits_left_count
    @vlc        = @owner.vacation_leave_credits_left_count
    @tlc        = @owner.total_leave_credits
    @params     = params
    @response   = Hash.new
  end

  def notif_message
    "#{@owner.capitalize_name} requested for a leave."
  end

  def create_sick_leave_whole_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.sick_leave.whole_day.build(whole_day_params)
    return no_lc_error_message if no_leave_credits_left?
    ActiveRecord::Base.transaction do
      NotificationHandler.new(@leave_request, notif_message, "hr", "#{ADMIN_SLUG}/#{@leave_request.id}").create if @leave_request.save
    end

    create_response
    self
  end

  def update_sick_leave_whole_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.sick_leave.whole_day.build(whole_day_params)
    return no_lc_error_message if no_leave_credits_left?
    
    @leave_request = @owner.leave_requests.sick_leave.whole_day.pending.find(params[:id])

    ActiveRecord::Base.transaction do
      if @leave_request.update_attributes(whole_day_params)
        @leave_request.reload
      else
        @errors = @leave_request.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def create_sick_leave_half_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.sick_leave.half_day.build(half_day_params)

    return no_lc_error_message if no_leave_credits_left?
    ActiveRecord::Base.transaction do
      NotificationHandler.new(@leave_request, notif_message, "hr", "#{ADMIN_SLUG}/#{@leave_request.id}").create if @leave_request.save
    end

    create_response
    self
  end

  def update_sick_leave_half_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.sick_leave.half_day.build(half_day_params)
    return no_lc_error_message if no_leave_credits_left?
    
    @leave_request = @owner.leave_requests.sick_leave.half_day.pending.find(params[:id])

    ActiveRecord::Base.transaction do
      if @leave_request.update_attributes(half_day_params)
        @leave_request.reload
      else
        @errors = @leave_request.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def create_vacation_leave_whole_day
    #if no vacation leave credits they can not use sick leave credits
    @leave_request  = @owner.leave_requests.vacation_leave.whole_day.build(whole_day_params)

    return no_lc_error_message if no_leave_credits_left?
    ActiveRecord::Base.transaction do
      NotificationHandler.new(@leave_request, notif_message, "hr", "#{ADMIN_SLUG}/#{@leave_request.id}").create if @leave_request.save
    end

    create_response
    self
  end

  def update_vacation_leave_whole_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.vacation_leave.whole_day.build(whole_day_params)
    return no_lc_error_message if no_leave_credits_left?
    
    @leave_request = @owner.leave_requests.vacation_leave.whole_day.pending.find(params[:id])

    ActiveRecord::Base.transaction do
      if @leave_request.update_attributes(whole_day_params)
        @leave_request.reload
      else
        @errors = @leave_request.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def create_vacation_leave_half_day
    #if no vacation leave credits they can not use sick leave credits
    @leave_request  = @owner.leave_requests.vacation_leave.half_day.build(half_day_params)

    return no_lc_error_message if no_leave_credits_left?
    ActiveRecord::Base.transaction do
      NotificationHandler.new(@leave_request, notif_message, "hr", "#{ADMIN_SLUG}/#{@leave_request.id}").create if @leave_request.save
    end

    create_response
    self
  end

  def update_vacation_leave_half_day
    #if no sick leave credits they can use vacation leave credits
    @leave_request  = @owner.leave_requests.vacation_leave.half_day.build(half_day_params)
    return no_lc_error_message if no_leave_credits_left?
    
    @leave_request = @owner.leave_requests.vacation_leave.half_day.pending.find(params[:id])

    ActiveRecord::Base.transaction do
      if @leave_request.update_attributes(half_day_params)
        @leave_request.reload
      else
        @errors = @leave_request.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end
  
  def delete
    @leave_request = @owner.leave_requests.pending.find(params[:id])
    
    @leave_request.destroy

    create_response
    self
  end

  private

  def whole_day_params
    date_from = Date.parse("#{params[:date_from]}") rescue nil
    date_to = Date.parse("#{params[:date_to]}") rescue nil 

    date_from = params[:date_from] if date_from.present?
    date_to = params[:date_to] if date_to.present?
    { 
      date_from: date_from,
      date_to: date_to,
      reason: params[:reason] 
    }
  end

  def half_day_params
    half_date = Date.parse("#{params[:half_date]}") rescue nil
    half_date = params[:half_date] if half_date.present?
    { 
      half_date: half_date,
      reason: params[:reason]
    }
  end

  def no_leave_credits_left?
    dc = @leave_request.deductible_credits
    if @leave_request.leave_type == "sick_leave"
      @tlc < dc
    elsif @leave_request.leave_type == "vacation_leave"
      @vlc < dc
    else
      #do nothing
    end
  end

  def no_lc_error_message
    response[:success] = false
    response[:details] = "Not enough Leave Credits"
    self
  end
  
  def invalid_id
    response[:success] = false
    response[:details] = "Invalid Request id"
    self
  end
  
  def create_response(error=nil)
    if !@leave_request.blank? && @leave_request.errors.blank?
      response[:success] = true
      response[:details] = @leave_request
    else
      response[:success] = false
      response[:details] = error || @leave_request.errors.full_messages.join(', ')
    end
  end

end
