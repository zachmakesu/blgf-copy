ProcessLeaveHandlerError = Class.new(StandardError)

class Leave::ProcessLeaveHandler
  ALL_SLUG = "leave_requests"
  attr_accessor :params, :leave_request, :owner, :response

  def initialize(params)
    @params         = params
    @response       = Hash.new
    @leave_request  = LeaveRequest.pending.find(params[:id])
    @owner          = @leave_request.owner
    @slc            = @owner.sick_leave_credits_left_count
    @vlc            = @owner.vacation_leave_credits_left_count
    @tlc            = @owner.total_leave_credits
    @dc             = @leave_request.deductible_credits
  end

  def notif_message_all
    "#{@leave_request.processor.capitalize_name} #{@leave_request.status} your leave request."
  end

  def process_update
    return missing_status if params[:status].blank? 
    if params[:status] == "approved" 
      ActiveRecord::Base.transaction do
        if @leave_request.update_attributes(update_params)
          NotificationHandler.new(@leave_request, notif_message_all, "all_user", "#{ALL_SLUG}/#{@leave_request.id}", @owner.id).create
          @leave_request.reload

          if @leave_request.leave_type == "sick_leave"
            if @slc < @dc
              vlc_dedictible = @dc - @slc
              @owner.leave_credit_histories.create(leave_request_id: @leave_request.id, credit_type: 0, credits: @slc, deductible: true)
              @owner.leave_credit_histories.create(leave_request_id: @leave_request.id, credit_type: 1, credits: vlc_dedictible, deductible: true)
            else
              @owner.leave_credit_histories.create(leave_request_id: @leave_request.id, credit_type: 0, credits: @dc, deductible: true)
            end
          elsif @leave_request.leave_type == "vacation_leave"
            @owner.leave_credit_histories.create(leave_request_id: @leave_request.id, credit_type: 1, credits: @dc, deductible: true)
          else
            # do nothing
          end
        else
          @errors = @leave_request.errors.full_messages.join(', ')
        end
      end
    else
      return missing_remarks if params[:remarks].blank? 
      @leave_request.update_attributes(update_params)
      NotificationHandler.new(@leave_request, notif_message_all, "all_user", "#{ALL_SLUG}/#{@leave_request.id}", @owner.id).create
    end
    
  
    create_response(@errors)
    self
  end

  private

  def update_params
    {
      processor_id: params[:processor_employee_id], 
      status: params[:status], 
      remarks:  params[:remarks]
    }
  end

  def missing_remarks
    response[:success] = false
    response[:details] = "Missing remarks."
    self
  end

  def missing_status
    response[:success] = false
    response[:details] = "Missing status."
    self
  end
  
  def create_response(error=nil)
    if !@leave_request.blank? && @leave_request.errors.blank?
      response[:success] = true
      response[:details] = @leave_request
    else
      response[:success] = false
      response[:details] = error || @leave_request.errors.full_messages.join(', ')
    end
  end

end
