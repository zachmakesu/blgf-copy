DraftHandlerError = Class.new(StandardError)

class Requests::PDS::DraftHandler
  ADMIN_SLUG = "employee_pds_requests"
  ALL_SLUG = "pds_request"
  attr_accessor :params, :options, :owner, :response

  def initialize(params, options={})
    @params     = params
    @owner      = User.find_by(employee_id: params[:owner_employee_id])
    @response   = Hash.new
  end

  def notif_message_admin
    "#{@owner.capitalize_name} requested for #{@draft.notification_classification} in PDS"
  end

  def notif_message_all
    "#{@draft.processor.first_name} #{@draft.processor.last_name} #{@draft.status} your #{@draft.notification_classification} request in PDS"
  end

  def create
    @draft = owner.drafts.create(drafts_options)
    NotificationHandler.new(@draft, notif_message_admin, "hr", "#{ADMIN_SLUG}/#{@draft.id}").create if @draft.errors.blank?
    create_response
    self
  end

  def update
    @draft =  owner.drafts.pending.find_by(id: params[:draft_id])
    return default_error_response if @draft.blank? 
    @draft.update_attributes(drafts_options)
    create_response
    self
  end

  def bulk_approve
    return error_message if params[:draft_ids].blank?

    processor = User.find(params[:processor_employee_id])
    params[:processor_id] = processor.id
    params[:status] = "approved"

    ids = params[:draft_ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      @draft =  Draft.per_roles(User::Role::ALL.values).pending.find_by(id: id)

      if @draft.present?
        params[:draft_id] = @draft.id

        @object_draft = case @draft.classification
          when "update_profile_data"            then Requests::PDS::Drafts::ProfileDataHandler.new(params).update
          when "update_profile_image"           then Requests::PDS::Drafts::ProfileDataHandler.new(params).update_profile_image
          when "update_signature_image"         then Requests::PDS::Drafts::ProfileDataHandler.new(params).update_signature_image

          when "add_contact"                    then Requests::PDS::Drafts::ContactHandler.new(params).add_contact
          when "update_contact"                 then Requests::PDS::Drafts::ContactHandler.new(params).update_contact
          when "delete_contact"                 then Requests::PDS::Drafts::ContactHandler.new(params).delete_contact

          when "add_address"                    then Requests::PDS::Drafts::AddressHandler.new(params).add_address
          when "update_address"                 then Requests::PDS::Drafts::AddressHandler.new(params).update_address
          when "delete_address"                 then Requests::PDS::Drafts::AddressHandler.new(params).delete_address

          when "add_education"                  then Requests::PDS::Drafts::EducationHandler.new(params).add_education
          when "update_education"               then Requests::PDS::Drafts::EducationHandler.new(params).update_education
          when "delete_education"               then Requests::PDS::Drafts::EducationHandler.new(params).delete_education

          when "add_examination"                then Requests::PDS::Drafts::ExaminationHandler.new(params).add_examination
          when "update_examination"             then Requests::PDS::Drafts::ExaminationHandler.new(params).update_examination
          when "delete_examination"             then Requests::PDS::Drafts::ExaminationHandler.new(params).delete_examination

          when "add_work_experience"            then Requests::PDS::Drafts::WorkExperienceHandler.new(params).add_work_experience
          when "update_work_experience"         then Requests::PDS::Drafts::WorkExperienceHandler.new(params).update_work_experience
          when "delete_work_experience"         then Requests::PDS::Drafts::WorkExperienceHandler.new(params).delete_work_experience


          when "add_voluntary_work"             then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).add_voluntary_work
          when "update_voluntary_work"          then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).update_voluntary_work
          when "delete_voluntary_work"          then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).delete_voluntary_work

          when "update_family_information"      then Requests::PDS::Drafts::FamilyInfoHandler.new(params).update

          when "add_child"                      then Requests::PDS::Drafts::ChildHandler.new(params).add_child
          when "update_child"                   then Requests::PDS::Drafts::ChildHandler.new(params).update_child
          when "delete_child"                   then Requests::PDS::Drafts::ChildHandler.new(params).delete_child

          when "add_training_and_seminar"       then Requests::PDS::Drafts::TrainingHandler.new(params).add_training_and_seminar
          when "update_training_and_seminar"    then Requests::PDS::Drafts::TrainingHandler.new(params).update_training_and_seminar
          when "delete_training_and_seminar"    then Requests::PDS::Drafts::TrainingHandler.new(params).delete_training_and_seminar

          when "update_other_information"       then Requests::PDS::Drafts::OtherInfoHandler.new(params).update

          when "add_reference"                  then Requests::PDS::Drafts::ReferenceHandler.new(params).add_reference
          when "update_reference"               then Requests::PDS::Drafts::ReferenceHandler.new(params).update_reference
          when "delete_reference"               then Requests::PDS::Drafts::ReferenceHandler.new(params).delete_reference
          end
        
        @draft = @object_draft.draft
        
        @draft.update_attributes(processor_id: processor.id, status: params[:status], processed_at: DateTime.now) if @object_draft.present? && @object_draft.response[:success]
        NotificationHandler.new(@draft, notif_message_all, "all_user", "#{ALL_SLUG}/#{@draft.id}", @draft.owner_id).create if @object_draft.present? && @object_draft.response[:success]
      end
    end
  end

  def process_update
    @draft =  Draft.per_roles(User::Role::ALL.values).pending.find_by(id: params[:draft_id])
    return default_error_response if @draft.blank? 
    return missing_status if params[:status].blank? 
    
    processor = User.find(params[:processor_employee_id])
  
    if params[:status] == "approved"
      params[:processor_id] = processor.id
      @object_draft = case @draft.classification
              when "update_profile_data"            then Requests::PDS::Drafts::ProfileDataHandler.new(params).update
              when "update_profile_image"           then Requests::PDS::Drafts::ProfileDataHandler.new(params).update_profile_image
              when "update_signature_image"         then Requests::PDS::Drafts::ProfileDataHandler.new(params).update_signature_image

              when "add_contact"                    then Requests::PDS::Drafts::ContactHandler.new(params).add_contact
              when "update_contact"                 then Requests::PDS::Drafts::ContactHandler.new(params).update_contact
              when "delete_contact"                 then Requests::PDS::Drafts::ContactHandler.new(params).delete_contact

              when "add_address"                    then Requests::PDS::Drafts::AddressHandler.new(params).add_address
              when "update_address"                 then Requests::PDS::Drafts::AddressHandler.new(params).update_address
              when "delete_address"                 then Requests::PDS::Drafts::AddressHandler.new(params).delete_address

              when "add_education"                  then Requests::PDS::Drafts::EducationHandler.new(params).add_education
              when "update_education"               then Requests::PDS::Drafts::EducationHandler.new(params).update_education
              when "delete_education"               then Requests::PDS::Drafts::EducationHandler.new(params).delete_education

              when "add_examination"                then Requests::PDS::Drafts::ExaminationHandler.new(params).add_examination
              when "update_examination"             then Requests::PDS::Drafts::ExaminationHandler.new(params).update_examination
              when "delete_examination"             then Requests::PDS::Drafts::ExaminationHandler.new(params).delete_examination

              when "add_work_experience"            then Requests::PDS::Drafts::WorkExperienceHandler.new(params).add_work_experience
              when "update_work_experience"         then Requests::PDS::Drafts::WorkExperienceHandler.new(params).update_work_experience
              when "delete_work_experience"         then Requests::PDS::Drafts::WorkExperienceHandler.new(params).delete_work_experience


              when "add_voluntary_work"             then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).add_voluntary_work
              when "update_voluntary_work"          then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).update_voluntary_work
              when "delete_voluntary_work"          then Requests::PDS::Drafts::VoluntaryWorkHandler.new(params).delete_voluntary_work

              when "update_family_information"      then Requests::PDS::Drafts::FamilyInfoHandler.new(params).update

              when "add_child"                      then Requests::PDS::Drafts::ChildHandler.new(params).add_child
              when "update_child"                   then Requests::PDS::Drafts::ChildHandler.new(params).update_child
              when "delete_child"                   then Requests::PDS::Drafts::ChildHandler.new(params).delete_child

              when "add_training_and_seminar"       then Requests::PDS::Drafts::TrainingHandler.new(params).add_training_and_seminar
              when "update_training_and_seminar"    then Requests::PDS::Drafts::TrainingHandler.new(params).update_training_and_seminar
              when "delete_training_and_seminar"    then Requests::PDS::Drafts::TrainingHandler.new(params).delete_training_and_seminar

              when "update_other_information"       then Requests::PDS::Drafts::OtherInfoHandler.new(params).update

              when "add_reference"                  then Requests::PDS::Drafts::ReferenceHandler.new(params).add_reference
              when "update_reference"               then Requests::PDS::Drafts::ReferenceHandler.new(params).update_reference
              when "delete_reference"               then Requests::PDS::Drafts::ReferenceHandler.new(params).delete_reference
              end
              
      @draft = @object_draft.draft
      NotificationHandler.new(@draft, notif_message_all, "all_user", "#{ALL_SLUG}/#{@draft.id}", @draft.owner_id).create if @object_draft.present? && @object_draft.response[:success]
    else
      return missing_remarks if params[:remarks].blank? 
      @draft.update_attributes(processor_id: processor.id, status: params[:status], remarks:  params[:remarks], processed_at: DateTime.now)
      NotificationHandler.new(@draft, notif_message_all, "all_user", "#{ALL_SLUG}/#{@draft.id}", @draft.owner_id).create if @draft.errors.blank?
    end

    create_response(@object_draft)
    self
  end

  private
  def default_error_response
    response[:success] = false
    response[:details] = "Pending draft not found."
    self
  end

  def missing_status
    response[:success] = false
    response[:details] = "Missing status."
    self
  end

  def missing_remarks
    response[:success] = false
    response[:details] = "Missing remarks."
    self
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def create_response(draft=nil)
    if draft
      response[:success] = draft.response[:success]
      response[:details] = draft.response[:details]
    else
      if @draft.errors.blank?
        response[:success] = true
        response[:details] = @draft
      else
        response[:success] = false
        response[:details] = @draft.errors.full_messages.join(', ')
      end

    end
  end

  def drafts_options
    {
      classification: params[:classification],
      object:         params[:draft]
    }
  end

end
