AddressHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::AddressHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_address
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @address = @owner.addresses.create(address_options)
      @draft.update_attributes(process_options) if @address.errors.blank? 
    end

    create_response
    self
  end

  def update_address
    @owner  = draft.owner
    @address = @owner.addresses.find_by(id: object.address_id)
    return error_message if @address.blank?

    ActiveRecord::Base.transaction do
      @address.update_attributes(address_options)
      @draft.update_attributes(process_options) if @address.errors.blank? 
    end

    create_response
    self
  end

  def delete_address
    @owner  = draft.owner
    @address = @owner.addresses.find_by(id: object.address_id)
    return error_message if @address.blank?

    ActiveRecord::Base.transaction do
      @address.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Address already deleted. Update unavailable."
    self
  end

  def create_response
    if @address.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @address.errors.full_messages.join(', ')
    end
  end

  def address_options
    {
      residency:         object[:residency],
      location:         object[:location],
      zipcode:         object[:zipcode],
      contact_number:         object[:contact_number]
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
