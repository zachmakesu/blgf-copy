ChildHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::ChildHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_child
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @child = @owner.children.create(child_options)
      @draft.update_attributes(process_options) if @child.errors.blank? 
    end

    create_response
    self
  end

  def update_child
    @owner  = draft.owner
    @child = @owner.children.find_by(id: object.child_id)
    return error_message if @child.blank?

    ActiveRecord::Base.transaction do
      @child.update_attributes(child_options)
      @draft.update_attributes(process_options) if @child.errors.blank? 
    end

    create_response
    self
  end

  def delete_child
    @owner  = draft.owner
    @child = @owner.children.find_by(id: object.child_id)
    return error_message if @child.blank?

    ActiveRecord::Base.transaction do
      @child.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Child already deleted. Update unavailable."
    self
  end

  def create_response
    if @child.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @child.errors.full_messages.join(', ')
    end
  end

  def child_options
    {
      first_name:        object[:first_name],
      last_name:         object[:last_name],
      middle_initial:    object[:middle_initial],
      middle_name:       object[:middle_name],
      relationship:      object[:relationship],
      birthdate:         object[:birthdate]
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
