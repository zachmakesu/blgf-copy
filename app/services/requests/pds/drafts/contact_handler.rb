ContactHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::ContactHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_contact
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @contact = @owner.contacts.create(contact_options)
      @draft.update_attributes(process_options) if @contact.errors.blank? 
    end

    create_response
    self
  end

  def update_contact
    @owner  = draft.owner
    @contact = @owner.contacts.find_by(id: object.contact_id)
    return error_message if @contact.blank?

    ActiveRecord::Base.transaction do
      @contact.update_attributes(contact_options)
      @draft.update_attributes(process_options) if @contact.errors.blank? 
    end

    create_response
    self
  end

  def delete_contact
    @owner  = draft.owner
    @contact = @owner.contacts.find_by(id: object.contact_id)
    return error_message if @contact.blank?

    ActiveRecord::Base.transaction do
      @contact.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Contact already deleted. Update unavailable."
    self
  end

  def create_response
    if @contact.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @contact.errors.full_messages.join(', ')
    end
  end

  def contact_options
    {
      device:         object[:device],
      number:         object[:number]
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
