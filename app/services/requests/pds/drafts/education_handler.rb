EducationHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::EducationHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_education
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @education = @owner.educations.create(education_options)
      @draft.update_attributes(process_options) if @education.errors.blank? 
    end

    create_response
    self
  end

  def update_education
    @owner  = draft.owner
    @education = @owner.educations.find_by(id: object.education_id)
    return error_message if @education.blank?

    ActiveRecord::Base.transaction do
      @education.update_attributes(education_options)
      @draft.update_attributes(process_options) if @education.errors.blank? 
    end

    create_response
    self
  end

  def delete_education
    @owner  = draft.owner
    @education = @owner.educations.find_by(id: object.education_id)
    return error_message if @education.blank?

    ActiveRecord::Base.transaction do
      @education.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Education already deleted. Update unavailable."
    self
  end

  def create_response
    if @education.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @education.errors.full_messages.join(', ')
    end
  end

  def education_options
    {
      education_level_id:               object[:education_level_id],
      school_name:                      object[:school_name],
      course_id:                        object[:course_id],
      highest_grade_level_units_earned: object[:highest_grade_level_units_earned],
      date_of_attendance_from:          object[:date_of_attendance_from],
      date_of_attendance_to:            object[:date_of_attendance_to],
      honors_received:                  object[:honors_received],
      year_graduated:                   object[:year_graduated]
    }     
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
