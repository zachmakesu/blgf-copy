ExaminationHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::ExaminationHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_examination
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @examination = @owner.examinations.create(examination_options)
      @draft.update_attributes(process_options) if @examination.errors.blank? 
    end

    create_response
    self
  end

  def update_examination
    @owner  = draft.owner
    @examination = @owner.examinations.find_by(id: object.examination_id)
    return error_message if @examination.blank?

    ActiveRecord::Base.transaction do
      @examination.update_attributes(examination_options)
      @draft.update_attributes(process_options) if @examination.errors.blank? 
    end

    create_response
    self
  end

  def delete_examination
    @owner  = draft.owner
    @examination = @owner.examinations.find_by(id: object.examination_id)
    return error_message if @examination.blank?

    ActiveRecord::Base.transaction do
      @examination.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Examination already deleted. Update unavailable."
    self
  end

  def create_response
    if @examination.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @examination.errors.full_messages.join(', ')
    end
  end

  def examination_options
    {
      exam_description:     object[:exam_description],
      place_of_exam:        object[:place_of_exam],
      rating:               object[:rating],
      date_of_exam:         object[:date_of_exam],
      licence_number:       object[:licence_number],
      date_of_release:      object[:date_of_release]
    }     
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
