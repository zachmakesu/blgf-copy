FamilyInfoHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::FamilyInfoHandler

  attr_accessor :status, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def update
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      if @owner.update_attributes(user_options)
        @owner.reload
        @draft.update_attributes(process_options)
      else
        @errors = @owner.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  private
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = error
    end
  end

  def user_options
    {
      spouse_name:                      object[:spouse_name],
      spouse_occupation:                object[:spouse_occupation],
      spouse_employer_or_business_name: object[:spouse_employer_or_business_name],
      spouse_work_or_business_address:  object[:spouse_work_or_business_address],
      spouse_contact_number:            object[:spouse_contact_number],
      parent_fathers_name:              object[:parent_fathers_name],
      parent_mothers_maiden_name:       object[:parent_mothers_maiden_name],
      parent_address:                   object[:parent_address],

      spouse_first_name:     object[:spouse_first_name],
      spouse_middle_name:     object[:spouse_middle_name],
      spouse_last_name:     object[:spouse_last_name],
      parant_fathers_first_name:     object[:parant_fathers_first_name],
      parant_fathers_middle_name:     object[:parant_fathers_middle_name],
      parant_fathers_last_name:     object[:parant_fathers_last_name],
      parant_mothers_first_name:     object[:parant_mothers_first_name],
      parant_mothers_middle_name:     object[:parant_mothers_middle_name],
      parant_mothers_last_name:     object[:parant_mothers_last_name],
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
