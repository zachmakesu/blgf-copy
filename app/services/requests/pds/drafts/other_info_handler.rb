OtherInfoHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::OtherInfoHandler

  attr_accessor :status, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def update
    @other_information = draft.owner.other_information
    ActiveRecord::Base.transaction do
      if @other_information.update_attributes(other_information_options)
        @other_information.reload
        @draft.update_attributes(process_options)
      else
        @errors = @other_information.errors.full_messages.join(', ')
      end
    end
    
    create_response(@errors)
    self
  end

  private
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = error
    end
  end

  def other_information_options
    {
      skills_and_hobbies:       object[:skills_and_hobbies],
      non_academic_destinction: object[:non_academic_destinction],
      membership_in:            object[:membership_in],
      third_degree_national:    object[:third_degree_national],
      give_national:            object[:give_national],
      fourth_degree_local:      object[:fourth_degree_local],
      give_local:               object[:give_local],
      formally_charge:          object[:formally_charge],
      give_charge:              object[:give_charge],
      administrative_offense:   object[:administrative_offense],
      give_offense:             object[:give_offense],
      any_violation:            object[:any_violation],
      give_reasons:             object[:give_reasons],
      canditate:                object[:canditate],
      give_date_particulars:    object[:give_date_particulars],
      indigenous_member:        object[:indigenous_member],
      give_group:               object[:give_group],
      differently_abled:        object[:differently_abled],
      give_disability:          object[:give_disability],
      solo_parent:              object[:solo_parent],
      give_status:              object[:give_status],
      is_separated:             object[:is_separated],
      give_details:             object[:give_details],
      signature:                object[:signature],
      date_accomplished:        object[:date_accomplished],
      ctc_number:               object[:ctc_number],
      issued_at:                object[:issued_at],
      issued_on:                object[:issued_on]
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
