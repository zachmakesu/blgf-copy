ProfileDataHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::ProfileDataHandler

  attr_accessor :status, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def update
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      if @owner.update_attributes(user_options)
        @owner.reload
        @draft.update_attributes(process_options)
      else
        @errors = @owner.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_profile_image
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @owner.photos.employee.each{|photo| photo.update(position: 1)}
      @photo = @owner.photos.employee.find(object[:image_id])
      @photo.update(position: 0)
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  def update_signature_image
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @owner.photos.signature.each{|photo| photo.update(position: 1)}
      @photo = @owner.photos.signature.find(object[:image_id])
      @photo.update(position: 0)
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = error
    end
  end

  def user_options
    {
      email:              object[:email],
      first_name:         object[:first_name],
      last_name:          object[:last_name],
      middle_name:        object[:middle_name],
      middle_initial:     object[:middle_initial],
      name_extension:     object[:name_extension],
      birthdate:          object[:birthdate],
      gender:             object[:gender],
      civil_status:       object[:civil_status],
      citizenship:        object[:citizenship],
      height:             object[:height],
      weight:             object[:weight],
      blood_type:         object[:blood_type],
      tin_number:         object[:tin_number],
      gsis_policy_number: object[:gsis_policy_number],
      pagibig_id_number:  object[:pagibig_id_number],
      philhealth_number:  object[:philhealth_number],
      sss_number:         object[:sss_number],
      remittance_id:      object[:remittance_id],
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
