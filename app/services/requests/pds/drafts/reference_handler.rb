ReferenceHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::ReferenceHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_reference
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @reference = @owner.references.create(reference_options)
      @draft.update_attributes(process_options) if @reference.errors.blank? 
    end

    create_response
    self
  end

  def update_reference
    @owner  = draft.owner
    @reference = @owner.references.find_by(id: object.reference_id)
    return error_message if @reference.blank?

    ActiveRecord::Base.transaction do
      @reference.update_attributes(reference_options)
      @draft.update_attributes(process_options) if @reference.errors.blank? 
    end

    create_response
    self
  end

  def delete_reference
    @owner  = draft.owner
    @reference = @owner.references.find_by(id: object.reference_id)
    return error_message if @reference.blank?

    ActiveRecord::Base.transaction do
      @reference.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Reference already deleted. Update unavailable."
    self
  end

  def create_response
    if @reference.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @reference.errors.full_messages.join(', ')
    end
  end

  def reference_options
    {
      name:           object[:name],
      address:        object[:address],
      contact_number: object[:contact_number]
    }
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
