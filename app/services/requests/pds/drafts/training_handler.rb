TrainingHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::TrainingHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_training_and_seminar
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @training_and_seminar = @owner.trainings_and_seminars.create(training_and_seminar_options)
      @draft.update_attributes(process_options) if @training_and_seminar.errors.blank? 
    end

    create_response
    self
  end

  def update_training_and_seminar
    @owner  = draft.owner
    @training_and_seminar = @owner.trainings_and_seminars.find_by(id: object.training_and_seminar_id)
    return error_message if @training_and_seminar.blank?

    ActiveRecord::Base.transaction do
      @training_and_seminar.update_attributes(training_and_seminar_options)
      @draft.update_attributes(process_options) if @training_and_seminar.errors.blank? 
    end

    create_response
    self
  end

  def delete_training_and_seminar
    @owner  = draft.owner
    @training_and_seminar = @owner.trainings_and_seminars.find_by(id: object.training_and_seminar_id)
    return error_message if @training_and_seminar.blank?

    ActiveRecord::Base.transaction do
      @training_and_seminar.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Training or Seminar already deleted. Update unavailable."
    self
  end

  def create_response
    if @training_and_seminar.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @training_and_seminar.errors.full_messages.join(', ')
    end
  end

  def training_and_seminar_options
    {
      title:                object[:title],
      inclusive_date_from:  object[:inclusive_date_from],
      inclusive_date_to:    object[:inclusive_date_to],
      number_of_hours:      object[:number_of_hours],
      conducted_by:         object[:conducted_by]
    }     
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
