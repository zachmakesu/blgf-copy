VoluntaryWorkHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::VoluntaryWorkHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_voluntary_work
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @voluntary_work = @owner.voluntary_works.create(voluntary_work_options)
      @draft.update_attributes(process_options) if @voluntary_work.errors.blank? 
    end

    create_response
    self
  end

  def update_voluntary_work
    @owner  = draft.owner
    @voluntary_work = @owner.voluntary_works.find_by(id: object.voluntary_work_id)
    return error_message if @voluntary_work.blank?

    ActiveRecord::Base.transaction do
      @voluntary_work.update_attributes(voluntary_work_options)
      @draft.update_attributes(process_options) if @voluntary_work.errors.blank? 
    end

    create_response
    self
  end

  def delete_voluntary_work
    @owner  = draft.owner
    @voluntary_work = @owner.voluntary_works.find_by(id: object.voluntary_work_id)
    return error_message if @voluntary_work.blank?

    ActiveRecord::Base.transaction do
      @voluntary_work.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "VoluntaryWork already deleted. Update unavailable."
    self
  end

  def create_response
    if @voluntary_work.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @voluntary_work.errors.full_messages.join(', ')
    end
  end

  def voluntary_work_options
    {
      name_of_organization:      object[:name_of_organization],
      address_of_organization:   object[:address_of_organization],
      inclusive_date_from:       object[:inclusive_date_from],
      inclusive_date_to:         object[:inclusive_date_to],
      number_of_hours:           object[:number_of_hours],
      position_nature_of_work:   object[:position_nature_of_work]
    }     
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
