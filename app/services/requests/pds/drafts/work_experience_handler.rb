WorkExperienceHandlerError = Class.new(StandardError)

class Requests::PDS::Drafts::WorkExperienceHandler

  attr_accessor :status, :processor_id, :draft, :object, :response

  def initialize(params, options={})
    @draft    = Draft.find(params[:draft_id])
    @status   = params[:status]
    @processor_id = params[:processor_id]
    @object   = draft.object
    @response = Hash.new
  end

  def add_work_experience
    @owner  = draft.owner
    ActiveRecord::Base.transaction do
      @work_experience = @owner.work_experiences.create(work_experience_options)
      @draft.update_attributes(process_options) if @work_experience.errors.blank? 
    end

    create_response
    self
  end

  def update_work_experience
    @owner  = draft.owner
    @work_experience = @owner.work_experiences.find_by(id: object.work_experience_id)
    return error_message if @work_experience.blank?

    ActiveRecord::Base.transaction do
      @work_experience.update_attributes(work_experience_options)
      @draft.update_attributes(process_options) if @work_experience.errors.blank? 
    end

    create_response
    self
  end

  def delete_work_experience
    @owner  = draft.owner
    @work_experience = @owner.work_experiences.find_by(id: object.work_experience_id)
    return error_message if @work_experience.blank?

    ActiveRecord::Base.transaction do
      @work_experience.destroy
      @draft.update_attributes(process_options)
    end

    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "WorkExperience already deleted. Update unavailable."
    self
  end

  def create_response
    if @work_experience.errors.blank?
      response[:success] = true
      response[:details] = @draft
    else
      response[:success] = false
      response[:details] = @work_experience.errors.full_messages.join(', ')
    end
  end

  def work_experience_options
    {
      position_title:               object[:position_title],
      inclusive_date_to:            object[:inclusive_date_to],
      inclusive_date_from:          object[:inclusive_date_from],
      department_agency_office:     object[:department_agency_office],
      monthly_salary:               object[:monthly_salary],
      salary_grade_and_step:        object[:salary_grade_and_step],
      appointment_status_id:        object[:appointment_status_id],
      government_service:           object[:government_service],
    }     
  end

  def process_options
    {
      processor_id: @processor_id,
      status:       status,
      processed_at: DateTime.now
    }
  end
end
