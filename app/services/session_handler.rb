SessionHandlerError = Class.new(StandardError)

class SessionHandler
  attr_accessor :user, :params, :response

  def initialize(params)
    @params   = params
    @user     = User.not_deleted.find_by(employee_id: params[:employee_id])
    @response = Hash.new
  end

  def login
    return default_error unless valid_user
    if user.locked_at.nil?
      user.delete_previous_api_keys
      key = user.api_keys.new
      access_token = key.access_token = SecureRandom.hex(16)
      key.save

      create_response({access_token: access_token, user: user })
    elsif (user.locked_at <= DateTime.now - 1.hour)
      user.unlock_access!
      user.reload

      user.delete_previous_api_keys
      key = user.api_keys.new
      access_token = key.access_token = SecureRandom.hex(16)
      key.save

      create_response({access_token: access_token, user: user })
    else
      return default_error
    end 
    
    self
  end

  def logout
    if user
      user.api_keys.update_all(active: false) #make all api_keys invalid
    end
    create_response( {messages: "Successfully logout."} )
    self
  end

  private
  def create_response(responses)
    response[:success] = true
    response[:details] = responses 
  end

  def default_error
    if user.present?
      user.update(failed_attempts: user.failed_attempts + 1)
      user.reload
      if user.failed_attempts >= 3
        user.lock_access! if user.failed_attempts == 3
        response[:success] = false
        response[:details] = "Account lock. Please wait 1 hour or check your email to unlock your account."
      else
        response[:success] = false
        response[:details] = "#{3 - user.failed_attempts} login attemp/s remaining"
      end
        
    else
      response[:success] = false
      response[:details] = "Invalid employee id or password."
    end
    self
  end

  def valid_user
    !params[:employee_id].blank? && !params[:password].blank? && !user.blank? && user.valid_password?(params[:password])
  end

end
