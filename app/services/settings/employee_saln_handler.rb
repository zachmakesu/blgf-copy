EmployeeSalnHandlerError = Class.new(StandardError)

class EmployeeSalnHandler
  ADMIN_SLUG = "employee_saln_manager"
  EMPLOYEE_SLUG = "employee_saln"
  attr_accessor :params, :response, :employee, :admin

  def initialize(employee, params ,admin = nil)
    @employee = employee
    @admin    = admin
    @params   = params
    @response = Hash.new
    @gender = @employee.male? ? "his" : "her"
  end

  def update_basic
    fields = [ 
            :statement_filing_status, 
            :last_name, 
            :first_name, 
            :middle_initial, 
            :address,
            :position,
            :office,
            :office_address,
            :spouse_first_name,
            :spouse_last_name,
            :spouse_middle_initial,
            :spouse_position,
            :spouse_office,
            :spouse_office_address
            ]
    model = :saln
    
    ActiveRecord::Base.transaction do 
      @saln = employee.saln
      if @saln.update(permited_params(model, fields))
        @saln.reload
        process_notification("updated basic info")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def add_child
    fields = [ 
            :name, 
            :birthdate, 
            :age
            ]
    model = :saln_child
    ActiveRecord::Base.transaction do 
      if @saln = employee.saln.saln_children.create(permited_params(model, fields))
        @saln = employee.saln
        process_notification("added a child")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_child
    @saln = employee.saln.saln_children.find(params[:id])

    fields = [ 
            :name, 
            :birthdate, 
            :age
            ]
    model = :saln_child
    return error_id if @saln.blank?

    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated a child")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_child
    @saln = employee.saln.saln_children.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_children.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted a child")
    create_response
    self
  end

  def add_real_asset
    fields = [ 
            :asset_type,
            :description, 
            :kind, 
            :exact_location,
            :assesed_value,
            :current_market_value,
            :acquisition_year,
            :acquisition_mode,
            :acquisition_cost
            ]
    model = :saln_asset
    
    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_assets.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added real asset")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_real_asset
    @saln = employee.saln.saln_assets.real.find(params[:id])

    fields = [ 
            :asset_type,
            :description, 
            :kind, 
            :exact_location,
            :assesed_value,
            :current_market_value,
            :acquisition_year,
            :acquisition_mode,
            :acquisition_cost
            ]
    model = :saln_asset

    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated real asset")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_real_asset
    @saln = employee.saln.saln_assets.real.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_assets.real.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted real asset")
    create_response
    self
  end

  def add_personal_asset
    fields = [ 
            :asset_type,
            :description, 
            :acquisition_year,
            :acquisition_cost
            ]
    model = :saln_asset
    
    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_assets.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added personal asset")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_personal_asset
    @saln = employee.saln.saln_assets.personal.find(params[:id])

    fields = [ 
            :asset_type,
            :description, 
            :acquisition_year,
            :acquisition_cost
            ]
    model = :saln_asset

    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated personal asset")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_personal_asset
    @saln = employee.saln.saln_assets.personal.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_assets.personal.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted personal asset")
    create_response
    self
  end

  def add_liability
    fields = [ 
            :nature,
            :creditors_name, 
            :outstanding_balance
            ]
    model = :saln_liability
    
    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_liabilities.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added liability")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_liability
    @saln = employee.saln.saln_liabilities.find(params[:id])
    fields = [ 
            :nature,
            :creditors_name, 
            :outstanding_balance
            ]
    model = :saln_liability

    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated liability")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_liability
    @saln = employee.saln.saln_liabilities.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_liabilities.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted liability")
    create_response
    self
  end

  def add_bi_and_fc
    fields = [ 
            :acquisition_date,
            :business_address, 
            :business_enterprise,
            :business_financial_nature
            ]
    model = :saln_bi_and_fc

    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_bi_and_fcs.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added business interest and financial connection")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_bi_and_fc
    @saln = employee.saln.saln_bi_and_fcs.find(params[:id])
    fields = [ 
            :acquisition_date,
            :business_address, 
            :business_enterprise,
            :business_financial_nature
            ]
    model = :saln_bi_and_fc
    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated business interest and financial connection")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_bi_and_fc
    @saln = employee.saln.saln_bi_and_fcs.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_bi_and_fcs.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted business interest and financial connection")
    create_response
    self
  end

  def add_government_relative
    fields = [ 
            :name,
            :office_address, 
            :position,
            :relationship
            ]
    model = :saln_government_relative
    
    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_government_relatives.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added relative in the government service")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_government_relative
    @saln = employee.saln.saln_government_relatives.find(params[:id])
    fields = [ 
            :name,
            :office_address, 
            :position,
            :relationship
            ]
    model = :saln_government_relative

    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated relative in the government service")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_government_relative
    @saln = employee.saln.saln_government_relatives.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_government_relatives.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted relative in the government service")
    create_response
    self
  end

  def add_issued_id
    fields = [ 
            :date_issued,
            :id_type,
            :id_no
            ]
    model = :saln_issued_id

    ActiveRecord::Base.transaction do 
    @saln = employee.saln.saln_issued_ids.create(permited_params(model, fields))
      if @saln.errors.blank?
        @saln = employee.saln
        process_notification("added signature and government issued id")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def update_issued_id
    @saln = employee.saln.saln_issued_ids.find(params[:id])
    fields = [ 
            :date_issued,
            :id_type,
            :id_no
            ]
    model = :saln_issued_id
    return error_id if @saln.blank?
    ActiveRecord::Base.transaction do 
      
      if @saln.update(permited_params(model, fields))
        @saln = employee.saln
        process_notification("updated signature and government issued id")
      else
        @errors = @saln.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_issued_id
    @saln = employee.saln.saln_issued_ids.find(params[:id])
    return error_id if @saln.blank? || employee.saln.saln_issued_ids.count == 0
    @saln.destroy
    @saln = employee.saln
    process_notification("deleted signature and government issued id")
    create_response
    self
  end

  private
  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_id
    response[:success] = false
    response[:details] = "Invalid id"
    self
  end

  def create_response(error=nil)
    if !@saln.blank? && @saln.errors.blank?
      response[:success] = true
      response[:details] = @saln
    else
      response[:success] = false
      response[:details] = error || @saln.errors.full_messages.join(', ')
    end
  end

  def process_notification(action_saln_field)
    object = @saln
    
    if admin.blank?
      message = "#{@employee.capitalize_name} #{action_saln_field} in #{@gender} saln"
      role = "hr"
      slug = "#{ADMIN_SLUG}/#{employee.employee_id}"
      owner = nil
    else
      message = "#{@admin.capitalize_name} #{action_saln_field} in your saln"
      role = "all_user"
      slug = "#{EMPLOYEE_SLUG}"
      owner = @employee.id
    end
    NotificationHandler.new(object, message, role, slug, owner).create 
  end

end
