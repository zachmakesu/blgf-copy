LguNameHandlerError = Class.new(StandardError)

class LguNameHandler
  include ApiHelper
  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [
            :name,
            :lgu_type_id,
            :region_id,
            :province_id,
            :lgu_code,
            :latitude,
            :longitude
            ]
    model = :lgu_name

    ActiveRecord::Base.transaction do
      @lgu_name = LguName.create(permited_params(model, fields))

      if @lgu_name.errors.blank?
        if params[:image].present? && params[:image].is_a?(Hash)
          photo = @lgu_name.photos.seal.build()
          photo.image = build_attachment(params[:image])
          photo.position = 0
          photo.default_type = 2
          photo.save
        end

        if params[:background_image].present? && params[:background_image].is_a?(Hash)
          photo = @lgu_name.photos.background.build()
          photo.image = build_attachment(params[:background_image])
          photo.position = 0
          photo.default_type = 2
          photo.save
        end
      end

      @errors = @lgu_name.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    fields = [
            :name,
            :lgu_type_id,
            :region_id,
            :province_id,
            :lgu_code,
            :latitude,
            :longitude
            ]
    model = :lgu_name

    @lgu_name = LguName.find_by(id: params[:id])
    return error_message if @lgu_name.blank? || LguName.count == 0
    ActiveRecord::Base.transaction do
      if @lgu_name.update(permited_params(model, fields))
        @lgu_name.reload
      else
        @errors = @lgu_name.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def update_images
    @lgu_name = LguName.find_by(id: params[:id])
    return error_message if @lgu_name.blank? || LguName.count == 0
    ActiveRecord::Base.transaction do

      if params[:image].present? && params[:image].is_a?(Hash)
        photo = @lgu_name.photos.seal.build()
        photo.image = build_attachment(params[:image])
        photo.position = 0
        photo.default_type = 2
        photo.save
      end

      if params[:background_image].present? && params[:background_image].is_a?(Hash)
        photo = @lgu_name.photos.background.build()
        photo.image = build_attachment(params[:background_image])
        photo.position = 0
        photo.default_type = 2
        photo.save
      end

    end
    create_response(@errors)
    self
  end

  def delete
    @lgu_name = LguName.find_by(id: params[:id])
    return error_message if @lgu_name.blank? || LguName.count == 0
    @lgu_name.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || LguName.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id|
      datum = LguName.find_by(id: id)
      if datum.present?
        @lgu_name = datum
        @lgu_name.destroy
      end
    end

    (@lgu_name = LguName.new) if @lgu_name.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end

  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @lgu_name
    else
      response[:success] = false
      response[:details] = error || @lgu_name.errors.full_messages.join(', ')
    end
  end

end
