LguTypeHandlerError = Class.new(StandardError)

class LguTypeHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [
            :name
            ]
    model = :lgu_type

    ActiveRecord::Base.transaction do
      @lgu_type = LguType.create(permited_params(model, fields))
      @errors = @lgu_type.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    fields = [
            :name
            ]
    model = :lgu_type

    @lgu_type = LguType.find_by(id: params[:id])
    return error_message if @lgu_type.blank? || LguType.count == 0
    ActiveRecord::Base.transaction do
      if @lgu_type.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @lgu_type.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @lgu_type = LguType.find_by(id: params[:id])
    return error_message if @lgu_type.blank? || LguType.count == 0
    @lgu_type.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || LguType.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id|
      datum = LguType.find_by(id: id)
      if datum.present?
        @lgu_type = datum
        @lgu_type.destroy
      end
    end

    (@lgu_type = LguType.new) if @lgu_type.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end

  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @lgu_type
    else
      response[:success] = false
      response[:details] = error || @lgu_type.errors.full_messages.join(', ')
    end
  end

end
