OrganizationChildHandlerError = Class.new(StandardError)

class OrganizationChildHandler
  attr_accessor :organization, :level_1, :level_2, :level_3, :employee, :params, :response
  def initialize(params)
    @params         = params
    @organization   = Organization.find(params[:organization_id])
    @level_1        = OrganizationChild.find_by(id: params[:level_1_id], organization_id: 1)
    @level_2        = OrganizationChild.find_by(id: params[:level_2_id], organization_id: 2)
    @level_3        = OrganizationChild.find_by(id: params[:level_3_id], organization_id: 3)

    @employee       = User.not_deleted.find_by(employee_id: params[:employee_id])
    @response       = Hash.new
  end

  def create_child
    return invalid_employee_id if employee.blank?
    ActiveRecord::Base.transaction do
      @child = organization.organization_children.create(post_params)
    end

    create_response
    self
  end

  def update_child
    @child = organization.organization_children.find(params[:id])
    return invalid_employee_id if employee.blank?
    return invalid_child_id if @child.blank? || OrganizationChild.count == 0
    ActiveRecord::Base.transaction do
      if @child.update_attributes(post_params)
        @child.reload
      else
        @errors = child.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_child
    @child = organization.organization_children.find(params[:id])
    @child.destroy
    create_response
    self
  end

  def add_custodian
    @child = organization.organization_children.find(params[:id])
    return invalid_employee_id if employee.blank?
    return invalid_child_id if @child.blank? || OrganizationChild.count == 0
    ActiveRecord::Base.transaction do
      if @child.custodians.create(user: employee)
        @child.reload
      else
        @errors = child.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def remove_custodian
    @child = organization.organization_children.find(params[:id])
    return invalid_employee_id if employee.blank?
    return invalid_child_id if @child.blank? || OrganizationChild.count == 0
    @child.custodians.find_by!(user_id: employee.id).delete

    create_response
    self
  end

  private
  def post_params
    { 
      organization: organization, 
      executive_office: level_1, 
      service: level_2, 
      division: level_3, 
      head_title:params[:head_title], 
      code: params[:code], 
      name: params[:name], 
      user: employee 
    }
  end

  def invalid_child_id
    response[:success] = false
    response[:details] = "Invalid child id"
    self
  end

  def invalid_employee_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end

  def create_response(error=nil)
    if !@child.blank? && @child.errors.blank?
      response[:success] = true
      response[:details] = @child
    else
      response[:success] = false
      response[:details] = error || @child.errors.full_messages.join(', ')
    end
  end

end
