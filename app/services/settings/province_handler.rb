ProvinceHandlerError = Class.new(StandardError)

class ProvinceHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [ 
            :name,
            :geocode
            ]
    model = :province

    ActiveRecord::Base.transaction do 
      @province = Province.create(permited_params(model, fields))
      @errors = @province.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    fields = [ 
            :name,
            :geocode
            ]
    model = :province

    @province = Province.find_by(id: params[:id])
    return error_message if @province.blank? || Province.count == 0
    ActiveRecord::Base.transaction do
      if @province.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @province.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @province = Province.find_by(id: params[:id])
    return error_message if @province.blank? || Province.count == 0
    @province.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || Province.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Province.find_by(id: id) 
      if datum.present?
        @province = datum
        @province.destroy
      end
    end

    (@province = Province.new) if @province.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @province
    else
      response[:success] = false
      response[:details] = error || @province.errors.full_messages.join(', ')
    end
  end

end
