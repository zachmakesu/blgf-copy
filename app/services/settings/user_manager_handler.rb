UserManagerHandlerError = Class.new(StandardError)

class UserManagerHandler
  ADMIN_SLUG = "user_manager"
  include ApiHelper
  attr_accessor :user, :params, :response, :current_user

  def initialize(params, current_user)
    @user       = User.find_by(employee_id: params[:employee_id])
    @current_user = current_user
    @params     = params
    @response   = Hash.new
  end

  def create_user
    return error_message if missing_params?
    ActiveRecord::Base.transaction do
      @user = User.create(post_params)
      @user.photos.employee.create(image: build_attachment(params[:image]), position: 0) if @user.errors.blank? && params[:image].present? && params[:image].is_a?(Hash)
      @user.photos.signature.create(image: build_attachment(params[:signature_image]), position: 0) if @user.errors.blank? && params[:signature_image].present? && params[:signature_image].is_a?(Hash)

      if @user.errors.blank? && @user.treasurer? && params[:treasurer_subrole].present?
        @user.reload
        @user.treasurer_assignment.update_attributes(treasurer_subrole_params)
      end
    end

    create_response
    self
  end

  def update_user
    return error_message if missing_params? || user.blank?

    ActiveRecord::Base.transaction do
      update_params = post_params
      if @user.update_attributes(update_params)

        if params[:image].present? && params[:image].is_a?(Hash)
          avatar = @user.photos.employee.find_by(position: 0)
          if avatar
            avatar.update(image: build_attachment(params[:image]))
          else
            @user.photos.employee.create(image: build_attachment(params[:image]), position: 0)
          end
        end

        if params[:signature_image].present? && params[:signature_image].is_a?(Hash)
          avatar = @user.photos.signature.find_by(position: 0)
          if avatar
            avatar.update(image: build_attachment(params[:signature_image]))
          else
            @user.photos.signature.create(image: build_attachment(params[:signature_image]), position: 0)
          end
        end

        if @user.errors.blank? && @user.treasurer? && params[:treasurer_subrole].present?
          @user.reload
          @user.treasurer_assignment.update_attributes(treasurer_subrole_params)
        end

        @user.reload
      else
        @errors = @user.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete_user
    return error_message_ids if @user.blank? || @user.employee_id == @current_user.employee_id
    @passed_object = @user
    @user.destroy
    message = "#{@current_user.capitalize_name} deleted User \"#{@passed_object.employee_id}\"."
    NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}").create

    create_response
    self
  end

  def bulk_delete_users
    return error_message_ids if params[:employee_ids].blank?  || User.count == 0

    ids = params[:employee_ids].split('^')
    ids = ids.uniq
    ids.each do |employee_id| 
      datum = User.find_by(employee_id: employee_id)
      if datum.present? && datum.employee_id != @current_user.employee_id
        @user = datum
        @passed_object = @user
        @user.destroy
        message = "#{@current_user.capitalize_name} deleted User \"#{@passed_object.employee_id}\"."
        NotificationHandler.new(@passed_object, message, "hr", "#{ADMIN_SLUG}").create
      end
    end

    (@user = User.new) if @user.blank?
    create_response
    self
  end

  private

  def post_params
    employee_id = "#{office_designation}#{'%04d' % (User.count+1)}"
    input_params = {
      employee_id:        employee_id,
      password:           "password123",
      email:              params[:email],
      first_name:         params[:first_name],
      middle_name:        params[:middle_name],
      last_name:          params[:last_name],
      gender:             params[:gender],
      office_designation: params[:office_designation],
      role:               params[:role],
      biometric_id:       params[:biometric_id].present? ? params[:biometric_id] : nil
    }

    if user.present?
      new_params = input_params.except(:employee_id, :password)
      new_params[:password] = params[:password] if params[:password].present?

      new_params
    else
      input_params
    end
  end

  def office_designation
    params[:office_designation].upcase == "CENTRAL" ? "C" : "R"
  end

  def error_message
    response[:success] = false
    response[:details] = "Missing parameters"
    self
  end

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def treasurer_subrole_params
    {
      treasurer_subrole: params[:treasurer_subrole],
      lgu_name: get_lgu_name
    }
  end

  def get_lgu_name
    LguName.find_by(lgu_code: params[:lgu_code])
  end

  def missing_params?
    params[:office_designation].blank? || params[:email].blank? || params[:first_name].blank? || params[:middle_name].blank? || params[:last_name].blank? || params[:gender].blank? || params[:role].blank?
  end

  def create_response(error=nil)
    if !@user.nil? && @user.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @user.errors.full_messages.join(', ')
    end
  end
end
