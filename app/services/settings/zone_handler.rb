ZoneHandlerError = Class.new(StandardError)

class ZoneHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    ActiveRecord::Base.transaction do
      @zone  = Zone.create(post_params)
    end

    create_response
    self
  end

  def update
    @zone = Zone.find_by(id: params[:id])
    return invalid_id if @zone.blank? || Zone.count == 0

    ActiveRecord::Base.transaction do
      update_params = post_params
      if @zone.update_attributes(update_params)
        @zone.reload
      else
        @errors = @zone.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def delete
    @zone = Zone.find_by(id: params[:id])
    return invalid_id if @zone.blank? || Zone.count == 0
    @zone.destroy
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || Zone.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Zone.find_by(id: id) 
      if datum.present?
        @zone = datum
        @zone.destroy
      end
    end

    (@zone = Zone.new) if @zone.blank?
    create_response
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def post_params
    { 
      zone_code: params[:zone_code], 
      description: params[:description],
      server_name: params[:server_name],
      username: params[:username],
      password: params[:password],
      database: params[:database]
    }
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid zone id"
    self
  end
  
  def create_response(error=nil)
    if !@zone.blank? && @zone.errors.blank?
      response[:success] = true
      response[:details] = @zone
    else
      response[:success] = false
      response[:details] = error || @zone.errors.full_messages.join(', ')
    end
  end

end
