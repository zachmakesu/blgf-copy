PdfFileHandlerError = Class.new(StandardError)

class PdfFileHandler
  ADMIN_SLUG = "treasurer_dms_manager"
  TREASURER_SLUG = "treasurer_pdf_files"
  include ApiHelper
  attr_accessor :user, :params, :response, :admin

  def initialize(user,params,admin = nil)
    @user            = user
    @admin           = admin
    @params          = params
    @response        = Hash.new
    @gender = @user.male? ? "his" : "her"
  end

  def create_file
    DocketManagementFile.find(params[:docket_management_file_id])
    return missing_file if params[:pdf].blank?
    ActiveRecord::Base.transaction do
      @pdf  = user.pdf_files.create(post_params)
      process_notification("uploaded")
    end

    create_response
    self
  end

  def update_file
    @pdf = user.pdf_files.find(params[:id])
    DocketManagementFile.find(params[:docket_management_file_id])
    ActiveRecord::Base.transaction do 
      if @pdf.update_attributes(post_params)
        @pdf.reload
        process_notification("updated")
      else
        @errors = @pdf.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete_file
    @pdf = user.pdf_files.find(params[:id])
    @passed_object = @pdf
    @pdf.destroy

    process_notification("deleted")
    create_response
    self
  end

  def bulk_delete
    return error_message if params[:ids].blank?  || @user.pdf_files.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = @user.pdf_files.find_by(id: id) 
      if datum.present?
        @pdf = datum
        @passed_object = @pdf
        @pdf.destroy

        process_notification("deleted")
      end
    end

    (@pdf = @user.pdf_files.new) if @pdf.blank?
    create_response
    self
  end


  private
  def post_params
    if params[:pdf].blank? 
      {
        docket_management_file_id: params[:docket_management_file_id]
      }
    else          
      {
        pdf: build_attachment_file(params[:pdf]),
        docket_management_file_id: params[:docket_management_file_id]
      }
    end
  end

  def missing_file
    response[:success] = false
    response[:details] = "Missing file"
    self
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def create_response(error=nil)
    if !@pdf.blank? && @pdf.errors.blank?
      response[:success] = true
      response[:details] = @pdf
    else
      response[:success] = false
      response[:details] = error || @pdf.errors.full_messages.join(', ')
    end
  end

  def process_notification(action_message)
    object = @pdf
    if admin.blank?
      message = "#{@user.capitalize_name} #{action_message} #{@gender} pdf"
      role = "hr"
      slug = "#{ADMIN_SLUG}/#{@user.employee_id}"
      owner = nil
    else
      message = "#{@admin.capitalize_name} #{action_message} pdf in your DMS"
      role = "treasurer"
      slug = "#{TREASURER_SLUG}"
      owner = @user.id
    end
    NotificationHandler.new(object, message, role, slug, owner).create if @pdf.errors.blank?
  end
end
