class AnnouncementWorker
  include Sidekiq::Worker
  
  def perform(announcement_id)
    @announcement = Announcement.find_by(id: announcement_id)
    if @announcement
      roles = @announcement.send_to.split("^")
      roles.each do |role|
        User.send(role).each {|user| UserMailer.announcement_email(user, @announcement).deliver_now }
      end
    end
  end
end 