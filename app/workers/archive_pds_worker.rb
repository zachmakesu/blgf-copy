class ArchivePdsWorker
  include Sidekiq::Worker
  
  def perform(employee_id, date)
    user = User.find(employee_id)

    personal_info            = Entities::V1::MyPersonalDataSheet::PersonalInfo::PersonalInformation.represent(user).as_json
    family                   = Entities::V1::MyPersonalDataSheet::Family::FamilyInformation.represent(user).as_json
    educations               = Entities::V1::MyPersonalDataSheet::Education::EducationInformation.represent(user).as_json
    examinations             = Entities::V1::MyPersonalDataSheet::Examination::ExaminationInfomation.represent(user).as_json
    work_experiences         = Entities::V1::MyPersonalDataSheet::WorkExperience::WorkExperienceInformation.represent(user).as_json
    voluntary_works          = Entities::V1::MyPersonalDataSheet::VoluntaryWork::VoluntaryWorkInformation.represent(user).as_json
    trainings_and_seminars   = Entities::V1::MyPersonalDataSheet::TrainingsAndSeminars::TrainingsAndSeminarsInformation.represent(user).as_json
    other_informations       = Entities::V1::MyPersonalDataSheet::OtherInformations::OtherInformation.represent(user).as_json

    json_data = {
      personal_info: personal_info,
      family: family,
      educations: educations,
      examinations: examinations,
      work_experiences: work_experiences,
      voluntary_works: voluntary_works,
      trainings_and_seminars: trainings_and_seminars,
      other_informations: other_informations
    }
    
    @archive = Archive.pds.find_or_create_by(user: user, archive_date: date) do |user|
      user.info = json_data
    end

    @archive.update_attributes(info: json_data) if @lgu_name
  end

end 