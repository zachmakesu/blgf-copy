class ArchiveSalnWorker
  include Sidekiq::Worker
  
  def perform(employee_id, date)
    user = User.find(employee_id)
    saln = Entities::V1::Treasurer::SalnRecord::Index.represent(user.saln).as_json
    
    json_data = {
      saln: saln
    }
    
    @archive = Archive.saln.find_or_create_by(user: user, archive_date: date) do |user|
      user.info = json_data
    end

    @archive.update_attributes(info: json_data) if @lgu_name
  end
end 