class AreaImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = Hash.new
    @csv_file[:tempfile] = File.open(File.join(@file.csv.path))

    require 'csv'
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file[:tempfile], headers: true) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      name = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      lgu_type  = LguType.find_or_create_by(name: name) { |data| data.name = name }

      name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      region    = Region.find_or_create_by(name: name) { |data| data.name = name }

      name = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      geocode = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      province  = Province.find_or_create_by(name: name, geocode: geocode) do |data|
        data.name = name
        data.geocode = geocode
      end

      name = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @lgu_type_id   = LguType.find_by(name: name).try(:id)

      name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @region_id     = Region.find_by(name: name).try(:id)

      geocode = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      @province_id   = Province.find_by(geocode: geocode).try(:id)

      name = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : nil
      lgu_code = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil

      @lgu_name = LguName.find_or_create_by(lgu_code: lgu_code) do |data|
        data.name = name
        data.lgu_type_id = @lgu_type_id
        data.region_id = @region_id
        data.province_id = @province_id
        data.lgu_code = lgu_code
      end

      @lgu_name.update_attributes(name: name, lgu_type_id: @lgu_type_id, region_id: @region_id, province_id: @province_id ) if @lgu_name.present?

      @file.update(total_processed_rows: $.)
      @file.update(finished: true) if @file.total_processed_rows == @file.total_rows
    end

  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end
