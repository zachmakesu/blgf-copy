class BenefitsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @benefit_type = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip.downcase : nil

      process_row if @code

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

    
  end

  def update_params
    { 
      code: @code,
      benefit_type: "bonus"
    }
  end

  def get_benefit_type
    Benefit.benefit_types.include?(@benefit_type) ? @benefit_type : "bonus"
  end

  def process_row
    @benefit_object  = Benefit.find_or_create_by(code: @code) do|data| 
      data.code = @code
      data.benefit_type = "bonus"
    end

    @benefit_object.update_attributes(update_params) if @benefit_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end