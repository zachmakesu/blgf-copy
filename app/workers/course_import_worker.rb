class CourseImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @course_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @course_description = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil

      process_row if @course_code && @course_description

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

    
  end

  def update_params
    { 
      course_code: @course_code,
      course_description: @course_description
    }
  end

  def process_row
    @course_object  = Course.find_or_create_by(course_code: @course_code) do|data| 
      data.course_code = @course_code
      data.course_description = @course_description
    end

    @course_object.update_attributes(update_params) if @course_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end