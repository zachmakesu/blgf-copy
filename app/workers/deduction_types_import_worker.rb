class DeductionTypesImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @deduction_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @deduction = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @mandatory = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.downcase : false

      process_row if @deduction_code && @deduction

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def update_params
    { 
      deduction_code: @deduction_code,
      deduction: @deduction,
      mandatory: get_mandatory,
      deduction_base_calculations: "None"
    }
  end

  def get_mandatory
    @mandatory == "true"
  end

  def process_row
    @deduction_type_object  = DeductionType.find_or_create_by(deduction_code: @deduction_code) do|data| 
      data.deduction_code = @deduction_code
      data.deduction = @deduction
      data.mandatory = get_mandatory
      data.deduction_base_calculations = "None"
    end

    @deduction_type_object.update_attributes(update_params) if @deduction_type_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end