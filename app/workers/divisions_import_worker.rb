class DivisionsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @level_1 = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @level_2 = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @code = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      @name = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      @head = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip.to_i : nil
      @head_title = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil
      @custodian = arr_data[6][1].present? ? arr_data[6][1].rstrip.lstrip : nil

      process_row if @code && @name && get_level_1 && get_level_2

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @head)
  end

  def get_custodian
    User.find_by(biometric_id: @custodian)
  end

  def get_level_1
    Organization.first.organization_children.find_by(code: @level_1)
  end

  def get_level_2
    Organization.second.organization_children.find_by(code: @level_2)
  end

  def update_params
    { 
      executive_office: get_level_1,
      service: get_level_2,
      code: @code,
      name: @name, 
      user: get_user, 
      head_title: @head_title,
      custodian: get_custodian
    }
  end

  def process_row
    @division_object  = OrganizationChild.find_or_create_by(code: @code) do|data| 
      data.organization_id = 3
      data.executive_office = get_level_1
      data.service = get_level_2
      data.code = @code
      data.name = @name 
      data.user = get_user
      data.head_title = @head_title
      data.custodian = get_custodian
    end

    @division_object.update_attributes(update_params) if @division_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end