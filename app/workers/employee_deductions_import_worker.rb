class EmployeeDeductionsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @biometric_id = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @deduction_type = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @amount = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.to_f : nil

      process_row if get_user && get_deduction_type && @amount

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @biometric_id)
  end

  def get_deduction_type
    DeductionType.find_by(deduction: @deduction_type)
  end


  def update_params
    { 
      amount: @amount,
      overwrite: true
    }
  end

  def process_row
    deductive = get_user.deductives.find_by(deduction_type_id: get_deduction_type.id)
    deductive.update(update_params) if deductive
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end