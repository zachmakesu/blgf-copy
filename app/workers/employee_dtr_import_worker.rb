class EmployeeDtrImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = Hash.new
    @csv_file[:tempfile] = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'


    CSV.foreach(@csv_file[:tempfile], headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @biometric_id = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @dtr_date = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip.to_date.strftime : nil

      @in_am = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      @out_am = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      @in_pm = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : nil
      @out_pm = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil

      process_row if @biometric_id && @dtr_date && valid_time(@in_am) && valid_time(@out_am) && valid_time(@in_pm) && valid_time(@out_pm)

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def old_value
    {
    in_am: @dtr.in_am,
    out_am: @dtr.out_am,
    in_pm: @dtr.in_pm,
    out_pm: @dtr.out_pm,
    in_ot: @dtr.in_ot,
    out_ot: @dtr.out_ot,
    ot: @dtr.ot,
    late: @dtr.late,
    ut: @dtr.ut
    }
  end

  def process_row
    @dtr  = EmployeeDtr.find_or_create_by(biometric_id: @biometric_id, dtr_date: @dtr_date) do|data| 
      data.biometric_id = @biometric_id 
      data.dtr_date = @dtr_date 
      data.in_am  = @in_am
      data.out_am = @out_am
      data.in_pm  = @in_pm
      data.out_pm = @out_pm
    end

    @dtr.update_attributes(in_am: @in_am, out_am: @out_am, in_pm: @in_pm, out_pm: @out_pm, old_value: old_value ) if @dtr.present?
  end

  def valid_time(time) #hh:mm:ss
    time.present? && (time =~ /^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/ ).present?
  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end