class ExamTypeImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @exam_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @exam_description = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @csc_eligible = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.downcase : false


      process_row if @exam_code && @exam_description

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

    
  end

  def update_params
    { 
      exam_code: @exam_code,
      exam_description: @exam_description, 
      csc_eligible: get_csc_eligible
    }
  end

  def get_csc_eligible
    @csc_eligible == "true"
  end


  def process_row
    @exam_type_object  = ExamType.find_or_create_by(exam_code: @exam_code) do|data| 
      data.exam_code = @exam_code 
      data.exam_description = @exam_description
      data.csc_eligible  = get_csc_eligible
    end

    @exam_type_object.update_attributes(update_params) if @exam_type_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end