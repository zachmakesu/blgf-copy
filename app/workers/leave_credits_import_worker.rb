class LeaveCreditsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'


    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @biometric_id = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @credit_type = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip.downcase : nil
      @deductible = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.downcase : nil
      @credits = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip.to_f : 0.0

      process_row if get_user && valid_credit_type && valid_deductible

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @biometric_id)
  end

  def valid_credit_type
    LeaveCreditHistory.credit_types.keys.include?(@credit_type)
  end

  def valid_deductible
    ["true", "false"].include?(@deductible)
  end

  def get_deductible
    @deductible == "true"
  end

  def process_row
    get_user.leave_credit_histories.create(credit_type: @credit_type, credits: @credits, deductible: get_deductible)
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end