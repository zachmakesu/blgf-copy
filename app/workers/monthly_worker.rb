class MonthlyWorker
  include Sidekiq::Worker
  
  def perform
    if Date.today == Date.today.end_of_month
      User.all.each do |user|
        user.add_credits_at_month_end
      end
      MonthlyWorker.perform_at(Date.today.next_month.end_of_month.to_datetime)
    else
      MonthlyWorker.perform_at(Date.today.end_of_month.to_datetime)
    end
  end
end 