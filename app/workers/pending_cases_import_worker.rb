class PendingCasesImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a
      
      @biometric_id = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip.to_i : nil
      @case_number = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @charged_offenses = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      @title = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      @status = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : nil

      process_row if get_user && @case_number

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @biometric_id)
  end

  def update_params
    { 
      charged_offenses: @charged_offenses, 
      title: @title, 
      status: @status
    }
  end

  def process_row
    @legal_case_object  = get_user.legal_cases.find_or_create_by(case_number: @case_number) do|data| 
      data.case_number = @case_number
      data.charged_offenses = @charged_offenses
      data.title = @title 
      data.status = @status
    end

    @legal_case_object.update_attributes(update_params) if @legal_case_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end