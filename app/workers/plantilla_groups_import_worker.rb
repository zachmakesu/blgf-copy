class PlantillaGroupsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv'
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @group_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @group_name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @group_order = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip.to_f : nil

      process_row if @group_code && @group_name && @group_order

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

  end

  def update_params
    {
      group_code: @group_code,
      group_name: @group_name,
      group_order: @group_order
    }
  end

  def process_row
    @plantilla_group  = PlantillaGroup.find_or_create_by(group_code: @group_code) do|data|
      data.group_code = @group_code
      data.group_name = @group_name
      data.group_order  = @group_order
    end

    @plantilla_group.update_attributes(update_params) if @plantilla_group.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end
