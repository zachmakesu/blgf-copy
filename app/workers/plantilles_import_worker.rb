class PlantillesImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv'
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @item_number = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @salary_grade_number = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @position_code = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      @plantilla_group_code = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil

      @area_code = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : "000"
      @area_type = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip.upcase : "R"
      @csc_eligibility = arr_data[6][1].present? ? arr_data[6][1].rstrip.lstrip.downcase : "false"
      @rationalized = arr_data[7][1].present? ? arr_data[7][1].rstrip.lstrip.downcase : "false"

      process_row if @item_number && get_salary_grade && get_position_code  && get_plantilla_group

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

  end

  def update_params
    {
      item_number: @item_number,
      position_code: get_position_code,
      plantilla_group: get_plantilla_group,
      salary_schedule: get_salary_grade,
      area_code: @area_code,
      area_type: @area_type,
      csc_eligibility: get_csc_eligibility,
      rationalized: get_rationalized
    }
  end

  def process_row
    @plantilla  = Plantilla.find_or_create_by(item_number: @item_number) do|data|
      data.item_number = @item_number
      data.salary_schedule = get_salary_grade
      data.position_code  = get_position_code
      data.plantilla_group = get_plantilla_group
      data.area_code  = @area_code
      data.area_type = @area_type
      data.csc_eligibility = get_csc_eligibility
      data.rationalized = get_rationalized
    end

    @plantilla.update_attributes(update_params) if @position_code.present?
  end

  def get_salary_grade
    SalarySchedule.find_by(salary_grade: @salary_grade_number)
  end

  def get_position_code
    PositionCode.find_by(position_code: @position_code)
  end

  def get_plantilla_group
    PlantillaGroup.find_by(group_code: @plantilla_group_code)
  end

  def get_csc_eligibility
    @csc_eligibility == "true"
  end

  def get_rationalized
    @rationalized == "true"
  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end
