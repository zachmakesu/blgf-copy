class PositionCodeImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash
      arr_data = hash_data.to_a

      @position_code = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @position_description = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil

      @position_abbreviation = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
      @educational_requirement = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
      @experience_requirement = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : nil
      @eligibility_requirement = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil
      @training_requirement = arr_data[6][1].present? ? arr_data[6][1].rstrip.lstrip : nil
      @position_classification = arr_data[7][1].present? ? arr_data[7][1].rstrip.lstrip : nil

      process_row if @position_code && @position_description

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

    
  end

  def update_params
    { 
      position_code: @position_code,
      position_description: @position_description, 
      position_abbreviation: @position_abbreviation, 
      educational_requirement: @educational_requirement,
      experience_requirement: @experience_requirement,
      eligibility_requirement: @eligibility_requirement,
      training_requirement: @training_requirement,
      position_classification: @position_classification 
    }
  end

  def process_row
    @position_code_object  = PositionCode.find_or_create_by(position_code: @position_code) do|data| 
      data.position_code = @position_code 
      data.position_description = @position_description
      data.position_abbreviation  = @position_abbreviation
      data.educational_requirement = @educational_requirement
      data.experience_requirement  = @experience_requirement
      data.eligibility_requirement = @eligibility_requirement
      data.training_requirement = @training_requirement
      data.position_classification = @position_classification
    end

    @position_code_object.update_attributes(update_params) if @position_code_object.present?
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end