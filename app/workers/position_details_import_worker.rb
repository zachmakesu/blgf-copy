class PositionDetailsImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'


    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @biometric_id = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
      @service_code = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
      @first_day_gov = arr_data[2][1].to_date.strftime rescue nil
      @first_day_agency = arr_data[3][1].to_date.strftime rescue nil
      @mode_of_separation = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : "N/A"
      @executive_offices_code= arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil
      @services_code = arr_data[6][1].present? ? arr_data[6][1].rstrip.lstrip : nil
      @divisions_code = arr_data[7][1].present? ? arr_data[7][1].rstrip.lstrip : nil
      @sections_code = arr_data[8][1].present? ? arr_data[8][1].rstrip.lstrip : nil
      @tax_status = arr_data[9][1].present? ? arr_data[9][1].rstrip.lstrip : "Single"
      @dependents_number = arr_data[10][1].present? ? arr_data[10][1].rstrip.lstrip : nil
      @payroll_group_code = arr_data[11][1].present? ? arr_data[11][1].rstrip.lstrip : nil
      @include_in_dtr = arr_data[12][1].present? ? arr_data[12][1].rstrip.lstrip.downcase : "false"
      @include_in_payroll = arr_data[13][1].present? ? arr_data[13][1].rstrip.lstrip.downcase : "false"
      @item_number = arr_data[14][1].present? ? arr_data[14][1].rstrip.lstrip : nil
      @step_number = arr_data[15][1].present? ? arr_data[15][1].rstrip.lstrip.to_i : 0
      @position_date = arr_data[16][1].to_date.strftime rescue nil
      
      process_row if get_user

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @biometric_id)
  end

  def get_service_code
    ServiceCode.find_by(service_code: @service_code)
  end

  def get_mode_of_separation
    ModeOfSeparation.find_by(separation_mode: @mode_of_separation) if ModeOfSeparation.all.map(&:separation_mode).include?(@mode_of_separation)
  end

  def get_executive_office
    OrganizationChild.find_by(code: @executive_offices_code)
  end
  
  def get_service
    OrganizationChild.find_by(code: @services_code)
  end

  def get_division
    OrganizationChild.find_by(code: @divisions_code)
  end

  def get_section
    OrganizationChild.find_by(code: @sections_code)
  end

  def get_tax_status
    TaxStatus.find_by(tax_status: @tax_status) if TaxStatus.all.map(&:tax_status).include?(@tax_status)
  end

  def get_payroll_group
    PayrollGroup.find_by(payroll_group_code: @payroll_group_code)
  end

  def get_include_in_dtr
    @include_in_dtr == "true"
  end

  def get_include_in_payroll
    @include_in_payroll == "true"
  end

  def get_plantilla
    Plantilla.find_by(item_number: @item_number)
  end

  def update_params
    { 
      service_code: get_service_code,
      first_day_gov: @first_day_gov,
      first_day_agency: @first_day_agency,
      mode_of_separation: get_mode_of_separation, 
      executive_office: get_executive_office, 
      service: get_service,
      division: get_division,
      section: get_section,
      tax_status: get_tax_status,
      dependents_number: @dependents_number,
      payroll_group: get_payroll_group,
      include_dtr: @include_in_dtr,
      include_in_payroll: @include_in_payroll,
      plantilla: get_plantilla,
      step_number: @step_number,
      position_date: @position_date
    }
  end

  def process_row
    get_user.position_detail.update_attributes(update_params)
  end


  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end