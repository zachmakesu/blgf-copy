class ServicesImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv' 
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @level_1 = arr_data[0][1].present? ? to_string(arr_data[0][1]) : nil
      @code = arr_data[1][1].present? ? to_string(arr_data[1][1]) : nil
      @name = arr_data[2][1].present? ? to_string(arr_data[2][1]) : nil
      @head = arr_data[3][1].present? ? to_string(arr_data[3][1]) : nil
      @head_title = arr_data[4][1].present? ? to_string(arr_data[4][1]) : nil
      @custodian = arr_data[5][1].present? ? to_string(arr_data[5][1]) : nil

      process_row if @code && @name && get_level_1

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end
    
  end

  def get_user
    User.find_by(biometric_id: @head)
  end

  def get_custodian
    User.find_by(biometric_id: @custodian)
  end

  def get_level_1
    Organization.first.organization_children.find_by(code: @level_1)
  end

  def update_params
    { 
      executive_office: get_level_1,
      code: @code,
      name: @name, 
      user: get_user, 
      head_title: @head_title,
      custodian: get_custodian
    }
  end

  def process_row
    @service_object  = OrganizationChild.find_or_create_by(code: @code) do|data| 
      data.organization_id = 2
      data.executive_office = get_level_1
      data.code = @code
      data.name = @name 
      data.user = get_user
      data.head_title = @head_title
      data.custodian = get_custodian
    end

    @service_object.update_attributes(update_params) if @service_object.present?
  end

  def to_string(field)
    field.to_s.rstrip.lstrip
  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end