class UserDeductivesWorker
  include Sidekiq::Worker
  
  def perform(user_id, object_id)
    @user = User.find(user_id)
    @object = DeductionType.find(object_id)
    @user.deductives.create(deduction_type_id: @object.id, amount: 0)
  end
end 