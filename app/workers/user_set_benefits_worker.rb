class UserSetBenefitsWorker
  include Sidekiq::Worker
  
  def perform(user_id, object_id)
    @user = User.find(user_id)
    @object = Benefit.find(object_id)
    @user.set_benefits.create(benefit_id: @object.id, amount: 0)
  end
end 