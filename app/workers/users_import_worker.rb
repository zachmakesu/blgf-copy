class UsersImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end
    Encoding.default_external = "iso-8859-1"

    @file = CsvFile.find_by(id: file_id)
    @csv_file = File.open(File.join(@file.csv.path))

    require 'csv'
    require 'json'
    require 'fileutils'

    CSV.foreach(@csv_file, headers: true, skip_blanks: true ) do |row|
      hash_data = row.to_hash

      arr_data = hash_data.to_a

      @biometric_id = arr_data[0][1].present? ? to_string(arr_data[0][1]) : nil
      @office_designation = arr_data[1][1].present? ? to_string(arr_data[1][1]).upcase : nil
      @last_name = arr_data[2][1].present? ? to_string(arr_data[2][1]) : nil
      @first_name = arr_data[3][1].present? ? to_string(arr_data[3][1]) : nil
      @middle_name = arr_data[4][1].present? ? to_string(arr_data[4][1]) : nil
      @middle_initial = arr_data[5][1].present? ? to_string(arr_data[5][1]) : nil
      @name_extension = arr_data[6][1].present? ? to_string(arr_data[6][1]) : nil
      @email = arr_data[7][1].present? ? to_string(arr_data[7][1]) : nil
      @birthdate = arr_data[8][1].to_date.strftime rescue nil
      @birthplace = arr_data[9][1].present? ? to_string(arr_data[9][1]) : nil
      @gender = arr_data[10][1].present? ? to_string(arr_data[10][1]).downcase : nil
      @role = arr_data[11][1].present? ? to_string(arr_data[11][1]).downcase : nil
      @civil_status = arr_data[12][1].present? ? to_string(arr_data[12][1]).downcase : nil
      @citizenship = arr_data[13][1].present? ? to_string(arr_data[13][1]) : nil
      @blood_type = arr_data[14][1].present? ? to_string(arr_data[14][1]) : nil
      @height = arr_data[15][1].present? ? arr_data[15][1].rstrip.lstrip.to_f : 0.0
      @weight = arr_data[16][1].present? ? arr_data[16][1].rstrip.lstrip.to_f : 0.0
      @gsis_policy_number = arr_data[17][1].present? ? to_string(arr_data[17][1]) : nil
      @pagibig_id_number = arr_data[18][1].present? ? to_string(arr_data[18][1]) : nil
      @philhealth_number = arr_data[19][1].present? ? to_string(arr_data[19][1]) : nil
      @sss_number = arr_data[20][1].present? ? to_string(arr_data[20][1]) : nil
      @tin_number = arr_data[21][1].present? ? to_string(arr_data[21][1]) : nil
      @remittance_id = arr_data[22][1].present? ? to_string(arr_data[22][1]) : nil

      process_row if @biometric_id && @email && @last_name && @first_name && get_employee_id

      @file.update(total_processed_rows: $.)
      @file.reload
      @file.update(finished: true) if @file.total_processed_rows >= @file.total_rows
    end

  end

  def get_office_designation
    @office_designation == "R" ? ["R","regional"] : ["C","central"]
  end

  def get_gender
    @gender == "male" ? "male" : "female"
  end

  def get_role
    User.roles.keys.include?(@role) ? @role : "employee"
  end

  def get_civil_status
    User.civil_statuses.keys.include?(@civil_status) ? @civil_status : "single"
  end

  def get_employee_id
    "#{get_office_designation[0]}#{'%04d' % (User.count+1)}"
  end

  def update_params
    {
      last_name: @last_name,
      first_name: @first_name,
      middle_name: @middle_name,
      middle_initial: @middle_initial,
      name_extension: @name_extension,
      email: @email,
      birthdate: @birthdate,
      birthplace: @birthplace,
      gender: get_gender,
      role: get_role,
      civil_status: get_civil_status,
      citizenship: @citizenship,
      blood_type: @blood_type,
      height: @height,
      weight: @weight,
      gsis_policy_number: @gsis_policy_number,
      pagibig_id_number: @pagibig_id_number,
      philhealth_number: @philhealth_number,
      sss_number: @sss_number,
      tin_number: @tin_number,
      remittance_id: @remittance_id
    }
  end

  def process_row
    @user_object  = User.find_or_create_by(biometric_id: @biometric_id) do|data|
      data.biometric_id = @biometric_id
      data.password = "password123"
      data.office_designation = get_office_designation[1]
      data.employee_id = get_employee_id
      data.last_name = @last_name
      data.first_name = @first_name
      data.middle_name = @middle_name
      data.middle_initial = @middle_initial
      data.name_extension = @name_extension
      data.email = @email
      data.birthdate = @birthdate
      data.birthplace = @birthplace
      data.gender = get_gender
      data.role = get_role
      data.civil_status = get_civil_status
      data.citizenship = @citizenship
      data.blood_type = @blood_type
      data.height = @height
      data.weight = @weight
      data.gsis_policy_number = @gsis_policy_number
      data.pagibig_id_number = @pagibig_id_number
      data.philhealth_number = @philhealth_number
      data.sss_number = @sss_number
      data.tin_number = @tin_number
      data.remittance_id = @remittance_id
    end
    @user_object.update_attributes(update_params) if @user_object.present?
  end

  def to_string(field)
    field.to_s.rstrip.lstrip
  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end
