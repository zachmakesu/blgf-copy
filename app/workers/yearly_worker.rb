class YearlyWorker
  include Sidekiq::Worker
  
  def perform
    if Date.today == Date.today.end_of_year
      User.all.each do |user|
        user.force_leave_at_year_end
      end
      YearlyWorker.perform_at(Date.today.next_year.end_of_year.to_datetime)
    else
      YearlyWorker.perform_at(Date.today.end_of_year.to_datetime)
    end
  end
end 