# config valid only for current version of Capistrano
lock '3.5.0'
require 'capistrano-db-tasks'

set :application, 'blgf'
set :repo_url, 'git@bitbucket.org:gorated/blgf.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
fetch(:mb_recipes) << "sidekiq"
fetch(:mb_aptitude_packages).merge!(
  "redis-server@ppa:rwky/redis" => :redis
)

set :mb_recipes, %w(
  user
  aptitude
  logrotate
  migrate
  seed
  nginx
  postgresql
  rake
  rbenv
  ufw
  unicorn
  version
)

set :linked_files, -> {
  [
    fetch(:mb_dotenv_filename)] +
  %w(
    config/database.yml
    config/unicorn.rb
  )
}


set :mb_dotenv_keys, %w(
  rails_secret_key_base
  sidekiq_web_username
  sidekiq_web_password
  hmac_secret1
  hmac_secret2
  gmail_username
  gmail_password
)
set :rbenv_ruby, '2.3.1'
set :db_local_clean, true
set :db_remote_clean, true
set :locals_rails_env, "production"
set :assets_dir, "public/assets"