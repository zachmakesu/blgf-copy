Sidekiq.configure_server do |config|
  config.redis = { :namespace => "blgf", :url => 'redis://127.0.0.1:6379/1'}
end

Sidekiq.configure_client do |config|
  config.redis = { :namespace => "blgf", :url => 'redis://127.0.0.1:6379/1'}
end

require "sidekiq/web"
Sidekiq::Web.app_url = "/"
Sidekiq::Web.use(Rack::Auth::Basic, "Application") do |username, password|
  username == ENV.fetch("SIDEKIQ_WEB_USERNAME") &&
  password == ENV.fetch("SIDEKIQ_WEB_PASSWORD")
end

Sidekiq.configure_client do |config|
    Rails.application.config.after_initialize do
      Sidekiq.redis { |conn| conn.flushdb }
      MonthlyWorker.perform_async
      YearlyWorker.perform_async
    end
end