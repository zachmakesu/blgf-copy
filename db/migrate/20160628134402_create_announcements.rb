class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.integer :announcement_type
      t.string :posted_at
      t.text :message
      t.text :description
      t.integer :send_to

      t.timestamps null: false
    end
  end
end
