class AddAttrToUser < ActiveRecord::Migration
  def change
	add_column :users, :first_name,                          :string
	add_column :users, :last_name,                           :string
	add_column :users, :middle_name,                         :string
	add_column :users, :middle_initial,                      :string
	add_column :users, :name_extension,                      :string
	add_column :users, :birthdate,                           :string
	add_column :users, :gender,                              :integer, default: 0, null: false
	add_column :users, :civil_status,                        :integer, default: 0, null: false
	add_column :users, :citizenship,                         :string
	add_column :users, :height,                              :float
	add_column :users, :weight,                              :float
	add_column :users, :blood_type,                          :string
	add_column :users, :cell_phone_number,                   :string
	add_column :users, :agency_employee_number,              :string
	add_column :users, :tin_number,                          :string
	add_column :users, :gsis_policy_number,                  :string
	add_column :users, :pagibig_id_number,                   :string
	add_column :users, :philhealth_number,                   :string
	add_column :users, :sss_number,                          :string
	add_column :users, :residential_address,                 :text
	add_column :users, :residential_zip_code,                :string
	add_column :users, :residential_landline_number,         :string
	add_column :users, :permanent_address,                   :text
	add_column :users, :permanent_zip_code,                  :string
	add_column :users, :permanent_landline_number,           :string
	add_column :users, :spouse_name,                         :string
	add_column :users, :spouse_occupation,                   :string
	add_column :users, :spouse_employer_or_business_name,    :string
	add_column :users, :spouse_work_or_business_address,     :string
	add_column :users, :spouse_mobile_or_telephone_number,   :string
	add_column :users, :parent_fathers_name,                 :string
	add_column :users, :parent_mothers_maiden_name,          :string
	add_column :users, :parent_address,                      :text

	add_column :users, :employee_id,                         :string, null: false
	add_column :users, :department_id,                       :integer
	add_column :users, :division_id,                         :integer
	add_column :users, :position_id,                         :integer
	add_column :users, :employed_date_at,                    :integer
	add_column :users, :employed_status,                     :boolean

	add_column :users, :role, 															 :integer, default: 0, null: false
	add_column :users, :deleted_at, 												 :string
  end
end