class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.references  :user, index: true, foreign_key: true
      t.string      :first_name
      t.string      :last_name
      t.string      :middle_name
      t.string      :middle_initial
      t.string      :relationship
      t.string      :birthdate

      t.timestamps null: false
    end
  end
end
