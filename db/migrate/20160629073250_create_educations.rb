class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.references   :user, index: true, foreign_key: true
      t.integer      :education_type
      t.string       :school_name
      t.string       :degree_course
      t.string       :highest_grade_level_units_earned
      t.string       :date_of_attendance_from
      t.string       :date_of_attendance_to
      t.text         :honors_received
      t.timestamps null: false
    end
  end
end
