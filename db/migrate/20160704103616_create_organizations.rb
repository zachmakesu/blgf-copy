class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string    :name
      t.integer   :level
      t.string    :deleted_at, default: nil
      t.timestamps null: false
    end
  end
end
