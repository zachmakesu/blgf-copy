class CreateOrganizationChildren < ActiveRecord::Migration
  def change
    create_table :organization_children do |t|
      t.references  :organization,  index: true, foreign_key: true
      t.integer     :executive_office_id
      t.integer     :service_id
      t.integer     :division_id
      t.string      :code
      t.string      :name
      t.string      :head_title
      t.integer     :user_id
      t.text        :custodian
      t.timestamps null: false
    end
  end
end
