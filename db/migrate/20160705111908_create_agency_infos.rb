class CreateAgencyInfos < ActiveRecord::Migration
  def change
    create_table :agency_infos do |t|
      t.string      :agency_name
      t.string      :agency_code
      t.string      :region
      t.string      :tin_number
      t.text        :address
      t.string      :zip_code
      t.string      :telephone_number
      t.string      :facsimile
      t.string      :email
      t.string      :website
      t.string      :salary_schedule
      t.string      :gsis_number
      t.float       :gsis_employee_percent_share
      t.float       :gsis_employer_percent_share      
      t.string      :pagibig_number
      t.float       :pagibig_employee_percent_share
      t.float       :pagibig_employer_percent_share 
      t.float       :privident_employee_percent_share
      t.float       :privident_employer_percent_share 
      t.float       :philhealth_employee_percent_share
      t.float       :philhealth_employer_percent_share 
      t.float       :philhealth_percentage
      t.string      :philhealth_number
      t.text        :mission
      t.text        :vision
      t.text        :mandate
      t.string      :bank_account_number
      t.timestamps null: false
    end
  end
end
