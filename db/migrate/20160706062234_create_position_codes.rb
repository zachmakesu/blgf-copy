class CreatePositionCodes < ActiveRecord::Migration
  def change
    create_table :position_codes do |t|
      t.string      :position_code
      t.text        :position_description
      t.string      :position_abbreviation
      t.text        :educational_requirement
      t.text        :experience_requirement
      t.text        :eligibility_requirement
      t.text        :training_requirement
      t.string      :position_classification
      t.timestamps null: false
    end
  end
end
