class CreateAppointmentStatuses < ActiveRecord::Migration
  def change
    create_table :appointment_statuses do |t|
      t.string      :appointment_code
      t.string      :appointment_status
      t.boolean     :leave_entitled, default: false
      t.timestamps null: false
    end
  end
end
