class CreateDutyAndResponsibilities < ActiveRecord::Migration
  def change
    create_table :duty_and_responsibilities do |t|
      t.references  :position_code, index: true, foreign_key: true
      t.float       :percent_work
      t.string      :duties
      t.timestamps null: false
    end
  end
end
