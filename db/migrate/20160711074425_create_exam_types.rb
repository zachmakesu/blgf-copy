class CreateExamTypes < ActiveRecord::Migration
  def change
    create_table :exam_types do |t|
      t.string      :exam_code
      t.string      :exam_description
      t.boolean     :csc_eligible, default: false
      t.timestamps null: false
    end
  end
end
