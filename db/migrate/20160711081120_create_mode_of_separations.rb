class CreateModeOfSeparations < ActiveRecord::Migration
  def change
    create_table :mode_of_separations do |t|
      t.string    :separation_mode
      t.timestamps null: false
    end
  end
end
