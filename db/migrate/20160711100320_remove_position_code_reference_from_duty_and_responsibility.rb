class RemovePositionCodeReferenceFromDutyAndResponsibility < ActiveRecord::Migration
  def change
    remove_reference :duty_and_responsibilities, :position_code, index: true, foreign_key: true
    add_column       :duty_and_responsibilities, :position_code_id, :integer, null: false
  end
end
