class CreatePlantillaGroups < ActiveRecord::Migration
  def change
    create_table :plantilla_groups do |t|
      t.string     :group_code
      t.integer    :group_order
      t.string     :group_name
      t.timestamps null: false
    end
  end
end
