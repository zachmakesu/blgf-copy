class CreateSalaryGrades < ActiveRecord::Migration
  def change
    create_table :salary_grades do |t|
      t.integer   :salary_grade, null: false
      t.float     :salary
      t.float     :step_1
      t.float     :step_2
      t.float     :step_3
      t.float     :step_4
      t.float     :step_5
      t.float     :step_6
      t.float     :step_7
      t.float     :step_8
      t.float     :step_9
      t.float     :step_10
      t.timestamps null: false
    end
  end
end
