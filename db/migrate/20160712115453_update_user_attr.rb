class UpdateUserAttr < ActiveRecord::Migration
  def change
    rename_column :users, :cell_phone_number, :mobile_number
    rename_column :users, :spouse_mobile_or_telephone_number, :spouse_contact_number
    add_column    :users, :telephone_number, :string
    add_column    :users, :address_1, :text
    add_column    :users, :address_2, :text
    add_column    :users, :address_3, :text
    add_column    :users, :zip_code, :string
    add_column    :users, :city, :string
    add_column    :users, :country, :string
  end
end
