class CreateExaminations < ActiveRecord::Migration
  def change
    create_table :examinations do |t|
      t.integer     :user_id
      t.integer     :exam_type_id
      t.float       :rating
      t.string      :date_of_exam
      t.string      :place_of_exam
      t.string      :licence_number
      t.string      :date_of_release
      t.timestamps null: false
    end
  end
end
