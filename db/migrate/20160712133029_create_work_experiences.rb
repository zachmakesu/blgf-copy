class CreateWorkExperiences < ActiveRecord::Migration
  def change
    create_table :work_experiences do |t|
      t.integer   :user_id
      t.integer   :appointment_status_id
      t.integer   :mode_of_separation_id

      t.string    :inclusive_date_from
      t.string    :inclusive_date_to
      t.string    :position_title
      t.string    :department_agency_office
      t.string    :monthly_salary
      t.boolean   :government_service, default: false
      t.string    :salay_grade_and_step
      t.integer   :branch
      t.string    :separation_date
      t.timestamps null: false
    end
  end
end
