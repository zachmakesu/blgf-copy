class CreateVoluntaryWorks < ActiveRecord::Migration
  def change
    create_table :voluntary_works do |t|
      t.integer   :user_id
      t.string    :name_of_organization
      t.text      :address_of_organization
      t.string    :inclusive_date_from
      t.string    :inclusive_date_to
      t.integer   :number_of_hours, null: false
      t.string    :position_nature_of_work
      t.timestamps null: false
    end
  end
end
