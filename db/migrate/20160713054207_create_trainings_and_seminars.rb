class CreateTrainingsAndSeminars < ActiveRecord::Migration
  def change
    create_table :trainings_and_seminars do |t|
      t.integer     :user_id
      t.string      :title
      t.string      :inclusive_date_from
      t.string      :inclusive_date_to
      t.integer     :number_of_hours, null: false  
      t.string      :conducted_by
      t.timestamps null: false
    end
  end
end
