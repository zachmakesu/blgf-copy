class CreateReferences < ActiveRecord::Migration
  def change
    create_table :references do |t|
      t.integer   :user_id
      t.string    :name, null: false
      t.text      :address
      t.string    :contact_number
      t.timestamps null: false
    end
  end
end
