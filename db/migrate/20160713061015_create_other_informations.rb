class CreateOtherInformations < ActiveRecord::Migration
  def change
    create_table :other_informations do |t|
      t.integer     :user_id
      t.text        :skills_and_hobbies
      t.text        :non_academic_destinction
      t.string      :membership_in
      t.boolean     :third_degree_national, default: false
      t.text        :give_national
      t.boolean     :fourth_degree_local, default: false
      t.text        :give_local
      t.boolean     :formally_charge, default: false
      t.text        :give_charge
      t.boolean     :administrative_offense, default: false
      t.text        :give_offense
      t.boolean     :any_violation, default: false
      t.text        :give_reasons
      t.boolean     :canditate, default: false
      t.text        :give_date_particulars
      t.boolean     :indigenous_member, default: false
      t.text        :give_group
      t.boolean     :differently_abled, default: false
      t.text        :give_disability
      t.boolean     :solo_parent, default: false
      t.text        :give_status

      t.string      :signature
      t.string      :date_accomplished
      t.string      :ctc_number
      t.string      :issued_at
      t.string      :issued_on

      t.timestamps null: false
    end
  end
end
