class CreateProjectCodes < ActiveRecord::Migration
  def change
    create_table :project_codes do |t|
      t.string     :project_code
      t.integer    :project_order
      t.string     :project_description
      t.timestamps null: false
    end
  end
end
