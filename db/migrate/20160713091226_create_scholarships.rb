class CreateScholarships < ActiveRecord::Migration
  def change
    create_table :scholarships do |t|
      t.string    :description
      t.timestamps null: false
    end
  end
end
