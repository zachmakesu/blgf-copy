class CreateServiceCodes < ActiveRecord::Migration
  def change
    create_table :service_codes do |t|
      t.string      :service_code
      t.string      :service_description
      t.timestamps null: false
    end
  end
end
