class ChangeSalaryGradeName < ActiveRecord::Migration
  def change
    rename_table :salary_grades, :salary_schedules
  end
end
