class CreatePlantillas < ActiveRecord::Migration
  def change
    create_table :plantillas do |t|
      t.string      :item_number
      t.integer     :position_code_id
      t.integer     :salary_schedule_grade
      t.integer     :plantilla_group_id
      t.string      :area_code
      t.string      :area_type
      t.boolean     :csc_eligibility, default: false
      t.boolean     :rationalized, default: false

      t.timestamps null: false
    end
  end
end
