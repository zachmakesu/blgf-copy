class CreateDrafts < ActiveRecord::Migration
  def change
    create_table :drafts do |t|
      t.string    :classification
      t.integer   :owner_id
      t.text      :object
      t.integer   :status, default: 0
      t.integer   :processor_id
      t.datetime  :processed_at

      t.timestamps null: false
    end
  end
end
