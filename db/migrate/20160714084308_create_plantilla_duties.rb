class CreatePlantillaDuties < ActiveRecord::Migration
  def change
    create_table :plantilla_duties do |t|
      t.integer     :plantilla_id
      t.float       :percent_work
      t.string      :duties
      t.timestamps null: false
    end
  end
end
