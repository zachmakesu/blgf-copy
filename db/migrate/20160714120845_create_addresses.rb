class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user, index: true, foreign_key: true
      t.integer    :residency
      t.text       :location
      t.string     :zipcode
      t.string     :contact_number
      t.timestamps null: false
    end
  end
end
