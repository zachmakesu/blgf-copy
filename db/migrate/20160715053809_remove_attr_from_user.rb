class RemoveAttrFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :residential_address
    remove_column :users, :residential_zip_code
    remove_column :users, :residential_landline_number
    remove_column :users, :permanent_address
    remove_column :users, :permanent_zip_code
    remove_column :users, :permanent_landline_number
    remove_column :users, :mobile_number
    remove_column :users, :telephone_number
    remove_column :users, :contact_number
    remove_column :users, :address_1
    remove_column :users, :address_2
    remove_column :users, :address_3
    remove_column :users, :city
    remove_column :users, :country
    remove_column :users, :zip_code
    remove_column :users, :agency_employee_number

  end
end
