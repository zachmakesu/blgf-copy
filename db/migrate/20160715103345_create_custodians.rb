class CreateCustodians < ActiveRecord::Migration
  def change
    create_table :custodians do |t|
      t.integer   :organization_child_id
      t.integer   :user_id
      t.timestamps null: false
    end
  end
end
