class CreatePayrollGroups < ActiveRecord::Migration
  def change
    create_table :payroll_groups do |t|
      t.integer     :project_code_id
      t.integer     :payroll_group_order
      t.string      :payroll_group_code
      t.string      :payroll_group_name
      t.timestamps null: false
    end
  end
end
