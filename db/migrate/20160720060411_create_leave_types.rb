class CreateLeaveTypes < ActiveRecord::Migration
  def change
    create_table :leave_types do |t|
      t.string      :leave_code
      t.string      :leave_type
      t.string      :specific_leave
      t.integer     :no_of_days, default: 0
      t.boolean     :parent, null: false
      t.integer     :leave_type_id
      t.timestamps null: false
    end
  end
end
