class CreateLeaveCreditHistories < ActiveRecord::Migration
  def change
    create_table :leave_credit_histories do |t|
      t.integer       :user_id, null: false
      t.integer       :credit_type, null: false

      t.integer       :created_by
      t.float         :credits, default: 0
      t.boolean       :deductible, null: false

      t.integer       :leave_request_id
      t.timestamps null: false
    end
  end
end
