class CreateRequestTypes < ActiveRecord::Migration
  def change
    create_table :request_types do |t|
      t.string     :type_of_request, null: false
      t.integer    :applicants
      t.integer    :first_signatory_id
      t.integer    :second_signatory_id
      t.integer    :third_signatory_id
      t.integer    :fourth_signatory_id
      t.timestamps null: false
    end
  end
end
