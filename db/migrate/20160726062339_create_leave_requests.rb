class CreateLeaveRequests < ActiveRecord::Migration
  def change
    create_table :leave_requests do |t|
      t.integer   :owner_id
      t.integer   :leave_type_id
      t.float     :applied_days
      t.integer   :status
      t.string    :date_from
      t.string    :date_to
      t.integer   :processor_id
      t.timestamps null: false
    end
  end
end
