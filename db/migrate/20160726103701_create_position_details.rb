class CreatePositionDetails < ActiveRecord::Migration
  def change
    create_table :position_details do |t|
      t.references :user
      #position details
      t.integer   :service_code_id
      t.string    :first_day_gov
      t.string    :first_day_agency
      t.integer   :mode_of_separation_id
      t.string    :separation_date
      t.integer   :appointment_status_id
      t.integer   :executive_office_id
      t.integer   :service_id
      t.integer   :division_id
      t.integer   :section_id
      t.string    :place_of_assignment
      t.string    :personnel_action
      t.integer   :dependents_number, default: 0
      t.string    :salary_effective_date
      t.integer   :category_basis #enum not_applicable, career, non_career
      t.integer   :employement_basis #enum full_time, #part_time
      t.integer   :tax_status, default: 0 #enum head, married, single

      #payroll

      t.integer   :payroll_group_id
      t.boolean   :include_dtr, default: false
      t.boolean   :include_in_payroll, default: false
      t.integer   :attendance_scheme_id, default: 1
      t.float     :hazard_pay_factor, default: 0
      t.boolean   :healt_insurance_exception, default: false

      #planilla position
      t.integer   :plantilla_id
      t.float     :actual_salary, default: 0
      t.integer   :step_number, default: 0
      t.string    :position_date
      

      t.timestamps null: false
    end
  end
end
