class AddRemarksOnDraft < ActiveRecord::Migration
  def change
    add_column :drafts, :remarks, :text
  end
end
