class AddRemarksOnLeaveRequests < ActiveRecord::Migration
  def change
    add_column :leave_requests, :remarks, :string
    remove_column :leave_requests, :status
    add_column :leave_requests, :status, :integer, default: 0
  end
end
