class CreatePhilHealthTables < ActiveRecord::Migration
  def change
    create_table :phil_health_tables do |t|

      t.integer    :salary_bracket, null: false
      t.string     :salary_range
      t.float      :salary_base
      t.float      :total_monthly_premium, null: false
      t.float      :employee_share, null: false
      t.float      :employer_share, null: false
      t.timestamps null: false
    end
  end
end
