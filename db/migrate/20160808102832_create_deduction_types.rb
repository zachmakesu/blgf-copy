class CreateDeductionTypes < ActiveRecord::Migration
  def change
    create_table :deduction_types do |t|
      t.string     :deduction_code, null: false
      t.boolean    :mandatory, default: false
      t.string     :deduction, null: false
      t.timestamps null: false
    end
  end
end
