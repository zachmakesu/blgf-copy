class CreateDeductionHistories < ActiveRecord::Migration
  def change
    create_table :deduction_histories do |t|
      t.integer    :user_id, null: false
      t.timestamps null: false
    end
  end
end
