class CreateDeductives < ActiveRecord::Migration
  def change
    create_table :deductives do |t|
      t.integer    :deduction_history_id, null: false
      t.integer    :deduction_type_id, null: false
      t.float      :amount, default: 0
      t.timestamps null: false
    end
  end
end
