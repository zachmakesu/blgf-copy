class CreateEmployeeDtrs < ActiveRecord::Migration
  def change
    create_table :employee_dtrs do |t|
      t.string     :biometric_id, null: false
      t.string     :dtr_date, null: false, default: "0000-00-00"
      t.string     :in_am, null: false, default: "00:00:00"
      t.string     :out_am, null: false, default: "00:00:00"
      t.string     :in_pm, null: false, default: "00:00:00"
      t.string     :out_pm, null: false, default: "00:00:00"
      t.string     :in_ot, null: false, default: "00:00:00"
      t.string     :out_ot, null: false, default: "00:00:00"
      t.string     :remarks
      t.string     :other_info
      t.string     :name
      t.string     :ip
      t.string     :editdate
      t.string     :perdiem
      t.string     :old_value

      t.string     :ot, null: false, default: "00:00:00"
      t.string     :late, null: false, default: "00:00:00"
      t.string     :ut, null: false, default: "00:00:00"
      t.integer    :attendance_scheme_id_used, null: false, default: 0

      t.timestamps null: false
    end
  end
end
