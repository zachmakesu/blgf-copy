class CreateAttendanceSchemes < ActiveRecord::Migration
  def change
    create_table :attendance_schemes do |t|
      t.string     :scheme_code, null: false
      t.integer    :scheme_type, null: false, default: 0
      t.string     :scheme_name, null: false
      t.string     :am_time_in_from, default: "00:00:00"
      t.string     :am_time_in_to, default: "00:00:00"

      t.string     :pm_time_out_from, default: "00:00:00"
      t.string     :pm_time_out_to, default: "00:00:00"

      t.string     :nn_time_out_from, default: "00:00:00"
      t.string     :nn_time_out_to, default: "00:00:00"

      t.string     :nn_time_in_from, default: "00:00:00"
      t.string     :nn_time_in_to, default: "00:00:00"

      t.string     :over_time_starts, default: "00:00:00"
      t.string     :over_time_ends, default: "00:00:00"

      t.string     :grace_period, default: "00:00:00"
      t.boolean    :gp_leave_creadits, default: false
      t.boolean    :gp_late, default: false
      t.string     :wrkhr_leave, default: "00:00:00"
      t.boolean    :hlf_late_und, default: false

      t.timestamps null: false
    end
  end
end
