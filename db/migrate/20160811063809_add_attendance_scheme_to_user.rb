class AddAttendanceSchemeToUser < ActiveRecord::Migration
  def change
    add_column :users, :office_designation, :integer, null: false, default: 0
  end
end
