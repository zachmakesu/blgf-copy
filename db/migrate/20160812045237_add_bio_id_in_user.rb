class AddBioIdInUser < ActiveRecord::Migration
  def change
    add_column :users, :biometric_id, :string
  end
end
