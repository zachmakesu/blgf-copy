class CreateFtsRequests < ActiveRecord::Migration
  def change
    create_table :fts_requests do |t|
      t.integer  :owner_id, null: false
      t.string   :biometric_id, null: false
      t.integer  :processor_id
      t.string   :dtr_date, null: false, default: "0000-00-00"
      t.string   :in_am, null: false, default: "00:00:00"
      t.string   :out_am, null: false, default: "00:00:00"
      t.string   :in_pm, null: false, default: "00:00:00"
      t.string   :out_pm, null: false, default: "00:00:00"
      t.string   :in_ot, null: false, default: "00:00:00"
      t.string   :out_ot, null: false, default: "00:00:00"

      t.string   :remarks
      t.integer  :status, null: false, default: 0
      t.timestamps null: false
    end
  end
end
