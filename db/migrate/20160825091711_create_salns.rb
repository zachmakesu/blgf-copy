class CreateSalns < ActiveRecord::Migration
  def change
    create_table :salns do |t|
      t.references :user, index: true
      t.integer :statement_filing_status, null: false, default: 0

      t.string :first_name
      t.string :middle_initial
      t.string :last_name
      t.text   :address
      t.string :position
      t.string :office
      t.text   :office_address

      t.string :spouse_first_name
      t.string :spouse_middle_initial
      t.string :spouse_last_name
      t.string :spouse_position
      t.string :spouse_office
      t.text   :spouse_office_address

      t.timestamps null: false
    end
  end
end
