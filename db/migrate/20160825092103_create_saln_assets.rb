class CreateSalnAssets < ActiveRecord::Migration
  def change
    create_table :saln_assets do |t|
    	t.references :saln, index: true
    	t.integer    :asset_type, null: false
    	t.text       :description
    	t.string     :kind
    	t.text       :exact_location
    	t.float      :assesed_value
    	t.float      :current_market_value
    	t.string     :acquisition_year
    	t.string     :acquisition_mode
    	t.float      :acquisition_cost
      t.timestamps null: false
    end
  end
end
