class CreateSalnLiabilities < ActiveRecord::Migration
  def change
    create_table :saln_liabilities do |t|
      t.references :saln, index: true
      t.string     :nature, null: false
      t.text       :creditors_name
      t.float      :outstanding_balance, null: false

      t.timestamps null: false
    end
  end
end
