class CreateSalnBiAndFcs < ActiveRecord::Migration
  def change
    create_table :saln_bi_and_fcs do |t|
      t.references :saln, index: true
      t.string     :business_enterprise, null: false
      t.text       :business_address
      t.text       :business_financial_nature
      t.string     :acquisition_date
      t.timestamps null: false
    end
  end
end