class CreateSalnGovernmentRelatives < ActiveRecord::Migration
  def change
    create_table :saln_government_relatives do |t|
      t.references :saln, index: true
      t.string     :name, null: false
      t.string     :relationship
      t.string     :position
      t.text       :office_address

      t.timestamps null: false
    end
  end
end