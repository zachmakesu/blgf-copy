class CreateSalnIssuedIds < ActiveRecord::Migration
  def change
    create_table :saln_issued_ids do |t|
      t.references :saln, index: true
      t.string     :id_no, null: false
      t.string     :date_issued, null: false
      t.timestamps null: false
    end
  end
end
