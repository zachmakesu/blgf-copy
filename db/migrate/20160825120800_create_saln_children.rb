class CreateSalnChildren < ActiveRecord::Migration
  def change
    create_table :saln_children do |t|
      t.references :saln, index: true
      t.string     :name, null: false
      t.string     :birthdate
      t.integer    :age
      t.timestamps null: false
    end
  end
end
