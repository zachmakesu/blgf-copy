class AddAttrsInPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :default, :boolean, null: false, default: false
    add_column :photos, :position, :integer, null: false
    add_column :photos, :default_type, :integer, null: false
    add_column :photos, :imageable_id,   :integer
    add_column :photos, :imageable_type, :string
    remove_column :photos, :user_id
  end
end
