class ChangeStepNumberDefaultInPositionDetails < ActiveRecord::Migration
  def change
  	remove_column :position_details, :step_number
  	add_column :position_details, :step_number, :integer, default: 1
  end
end
