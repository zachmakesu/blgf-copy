class CreateTaxRanges < ActiveRecord::Migration
  def change
    create_table :tax_ranges do |t|

      t.float   :tax_base, null: false, default: 0
      t.float   :tax_deduction, null: false, default: 0
      t.float   :factor, default: 0
      t.float   :taxable_from, default: 0
      t.float   :taxable_to, default: 0
      t.timestamps null: false
    end
  end
end
