class CreateTaxStatuses < ActiveRecord::Migration
  def change
    create_table :tax_statuses do |t|
      t.string  :tax_status, null: false
      t.float   :exemption_amount, null: false, default: 0
      t.timestamps null: false
    end
  end
end
