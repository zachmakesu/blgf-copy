class ChangeTaxtStatusInPositionDetail < ActiveRecord::Migration
  def change
  	remove_column :position_details, :tax_status
  	add_column    :position_details, :tax_status_id, :integer, null: false, default: 1
  end
end
