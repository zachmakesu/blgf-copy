class AddDateAppliedInDeductionHistory < ActiveRecord::Migration
  def change
  	add_column :deduction_histories, :date_applied, :string, null: false
  end
end
