class ChangeAttrInDeductives < ActiveRecord::Migration
  def change
  	remove_column :deductives, :deduction_history_id
  	add_column :deductives, :user_id, :integer, null: false
  end
end
