class CreateDeductionHistoryChildren < ActiveRecord::Migration
  def change
    create_table :deduction_history_children do |t|
    	t.references :deduction_history
    	t.integer    :deduction_type_id, null: false
    	t.integer    :amount, null: false, default: 0
      t.timestamps null: false
    end
  end
end
