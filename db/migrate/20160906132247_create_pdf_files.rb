class CreatePdfFiles < ActiveRecord::Migration
  def change
    create_table :pdf_files do |t|
      t.attachment :pdf
      t.integer    :pdfable_id 
      t.string     :pdfable_type
      t.string     :title

      t.timestamps null: false
    end
  end
end
