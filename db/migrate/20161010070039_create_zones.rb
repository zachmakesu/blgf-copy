class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
      t.string  :zone_code, null: false
      t.text  :description
      t.string :server_name
      t.string :username
      t.string :password
      t.string :database
      
      t.timestamps null: false
    end
  end
end
