class CreateKnowledgeBases < ActiveRecord::Migration
  def change
    create_table :knowledge_bases do |t|
      t.string  :code, null: false
      t.string  :description
    
      t.timestamps null: false
    end
  end
end
