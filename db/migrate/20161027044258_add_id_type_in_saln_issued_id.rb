class AddIdTypeInSalnIssuedId < ActiveRecord::Migration
  def change
    add_column :saln_issued_ids, :id_type, :integer, null: false
  end
end
