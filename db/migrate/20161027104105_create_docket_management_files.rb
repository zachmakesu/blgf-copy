class CreateDocketManagementFiles < ActiveRecord::Migration
  def change
    create_table :docket_management_files do |t|
      t.string  :title, null: false
      t.timestamps null: false
    end
  end
end
