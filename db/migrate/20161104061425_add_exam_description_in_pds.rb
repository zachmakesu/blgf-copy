class AddExamDescriptionInPds < ActiveRecord::Migration
  def change
    add_column :examinations, :exam_description, :string
  end
end
