class UpdateEducationAttributes < ActiveRecord::Migration
  def change
    remove_column :educations, :education_type
    add_column :educations, :education_level_id, :integer
  end
end
