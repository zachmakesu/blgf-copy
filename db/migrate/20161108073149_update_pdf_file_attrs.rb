class UpdatePdfFileAttrs < ActiveRecord::Migration
  def change
    remove_column :pdf_files, :title
    add_column :pdf_files, :docket_management_file_id, :integer
  end
end
