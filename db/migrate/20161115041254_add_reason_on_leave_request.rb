class AddReasonOnLeaveRequest < ActiveRecord::Migration
  def change
    add_column :leave_requests, :reason, :text
  end
end
