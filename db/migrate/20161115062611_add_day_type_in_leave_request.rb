class AddDayTypeInLeaveRequest < ActiveRecord::Migration
  def change
    add_column :leave_requests, :day_type, :integer
    add_column :leave_requests, :half_date, :string
    remove_column :leave_requests, :leave_type_id
    add_column :leave_requests, :leave_type, :integer
  end
end
