class AddDeductibeCreditsInLeaveRequest < ActiveRecord::Migration
  def change
    add_column :leave_requests, :deductible_credits, :float, default: 0
  end
end
