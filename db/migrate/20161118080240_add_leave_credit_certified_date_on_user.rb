class AddLeaveCreditCertifiedDateOnUser < ActiveRecord::Migration
  def change
    add_column :users, :leave_credit_certified_date_month, :date
    add_column :users, :leave_credit_certified_date_year, :date
  end
end
