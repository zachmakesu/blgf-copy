class AddAttrsInEmployeeDtr < ActiveRecord::Migration
  def change
    add_column :employee_dtrs, :is_edited, :boolean, default: false
    add_column :employee_dtrs, :processed_by, :integer
  end
end
