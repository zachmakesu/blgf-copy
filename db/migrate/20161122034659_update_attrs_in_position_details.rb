class UpdateAttrsInPositionDetails < ActiveRecord::Migration
  def change
    remove_column :position_details, :category_basis
    add_column :position_details, :category_service, :integer, default: 0

    remove_column :position_details, :employement_basis
    add_column :position_details, :employment_basis, :integer, default: 0

    remove_column :position_details, :healt_insurance_exception
    add_column :position_details, :health_insurance_exception, :boolean, default: false

    add_column :position_details, :head_of_the_agency, :boolean, default: false
    add_column :position_details, :date_increment, :string
  end
end
