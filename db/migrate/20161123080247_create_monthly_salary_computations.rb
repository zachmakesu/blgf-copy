class CreateMonthlySalaryComputations < ActiveRecord::Migration
  def change
    create_table :monthly_salary_computations do |t|
      t.references :user
      t.string     :this_month, null: false
      t.text       :deductions_this_month, null: false
      t.boolean    :include_in_dtr_this_month, null: false, default: false
      t.boolean    :include_in_payroll_this_month, null: false, default: false
      t.float      :authorized_salary_this_month, null: false
      t.text       :leave_credits_this_month, null: false
      t.timestamps null: false
    end
  end
end
