class CreateBenefits < ActiveRecord::Migration
  def change
    create_table :benefits do |t|
      t.string :code, null: false
      t.integer :benefit_type, null: false, default: 0
      t.timestamps null: false
    end
  end
end
