class CreateSetBenefits < ActiveRecord::Migration
  def change
    create_table :set_benefits do |t|
      t.references :user
      t.float      :amount, null: false, default: 0
      t.integer    :benefit_id, null: false
      t.timestamps null: false
    end
  end
end
