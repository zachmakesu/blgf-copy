class AddBenefitsThisMonthInMonthlySalaryComputation < ActiveRecord::Migration
  def change
    add_column :monthly_salary_computations, :benefits_this_month, :text, null: false
  end
end
