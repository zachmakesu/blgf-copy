class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user, index: true
      t.references :notified_by, index: true
      t.integer    :notificationable_id 
      t.string  :notificationable_type
      t.text   :message
      t.string :api_slug
      t.boolean :read, default: false
      t.integer :read_by, null: false
      t.timestamps null: false
    end
  end
end
