class CreateUserSeenNotifications < ActiveRecord::Migration
  def change
    create_table :user_seen_notifications do |t|
      t.references :user, index: true
      t.integer :notification_id
      t.timestamps null: false
    end
  end
end
