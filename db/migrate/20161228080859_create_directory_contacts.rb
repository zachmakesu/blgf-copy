class CreateDirectoryContacts < ActiveRecord::Migration
  def change
    create_table :directory_contacts do |t|
      t.references   :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
