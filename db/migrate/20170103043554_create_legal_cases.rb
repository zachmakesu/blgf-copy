class CreateLegalCases < ActiveRecord::Migration
  def change
    create_table :legal_cases do |t|
      t.integer :case_number, null: false
      t.text  :title
      t.string  :charged_offenses
      t.timestamps :application_date
      t.text  :status
      t.timestamps null: false
    end
      add_index :legal_cases, :case_number
  end
end
