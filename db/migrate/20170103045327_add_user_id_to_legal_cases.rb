class AddUserIdToLegalCases < ActiveRecord::Migration
  def change
      add_column  :legal_cases, :user_id, :integer
  end
end
