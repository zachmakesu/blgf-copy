class CreateCsvFiles < ActiveRecord::Migration
  def change
    create_table :csv_files do |t|
      t.attachment :csv
      t.integer    :csvable_id 
      t.string     :csvable_type
      t.integer    :total_rows, default: 0
      t.integer    :total_processed_rows, default: 0
      t.boolean    :finished, default: false
      t.timestamps null: false
    end
  end
end
