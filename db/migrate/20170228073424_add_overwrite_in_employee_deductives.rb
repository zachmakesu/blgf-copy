class AddOverwriteInEmployeeDeductives < ActiveRecord::Migration
  def change
    add_column :deductives, :overwrite, :boolean, default: false
  end
end
