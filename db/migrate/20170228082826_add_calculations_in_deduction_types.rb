class AddCalculationsInDeductionTypes < ActiveRecord::Migration
  def change
    add_column :deduction_types, :deduction_base_calculations, :string, null: false, default: "None"
  end
end
