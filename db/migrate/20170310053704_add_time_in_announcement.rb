class AddTimeInAnnouncement < ActiveRecord::Migration
  def change
    add_column :announcements, :start_time_at, :string, default: "00:00:00"
  end
end
