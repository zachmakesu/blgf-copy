class UpdateSendToInAnnouncement < ActiveRecord::Migration
  def change
    remove_column :announcements, :send_to, :integer
    add_column    :announcements, :send_to, :string, default: "all"
  end
end