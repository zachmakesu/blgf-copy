class UpdateDegreeCourseInEducation < ActiveRecord::Migration
  def change
    remove_column :educations, :degree_course, :string
    add_column    :educations, :course_id, :integer
  end
end
