class CreateTreasurerAssignments < ActiveRecord::Migration
  def change
    create_table :treasurer_assignments do |t|
      t.references  :user,  index: true, foreign_key: true
      t.string :treasurer_subrole
      t.string :region
      t.string :province
      t.string :city
      t.timestamps null: false
    end
  end
end
