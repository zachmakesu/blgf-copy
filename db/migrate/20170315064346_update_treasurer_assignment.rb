class UpdateTreasurerAssignment < ActiveRecord::Migration
  def change
    remove_column :treasurer_assignments, :province, :string
    remove_column :treasurer_assignments, :region, :string
    remove_column :treasurer_assignments, :city, :string
    add_column :treasurer_assignments, :lgu_name_id, :integer
  end
end
