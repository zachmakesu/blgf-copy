class CreateLguNames < ActiveRecord::Migration
  def change
    create_table :lgu_names do |t|
      t.string :income_class
      t.float :latitude
      t.integer :lgu_code
      t.integer :lgu_type_id
      t.float :longitude
      t.string :name, null: false
      t.integer :province_id
      t.integer :region_id


      t.timestamps null: false
    end
  end
end
