class RemoveIncomeClassFromLguName < ActiveRecord::Migration
  def change
    remove_column :lgu_names, :income_class
  end
end
