class UpdateTaxStatusInPositionDetail < ActiveRecord::Migration
  def change
    remove_column :position_details, :tax_status_id, :integer
    add_column    :position_details, :tax_status, :string, default: "Z"
  end
end
