class AddOverwriteInEmployeeSetBenefits < ActiveRecord::Migration
  def change
    add_column :set_benefits, :overwrite, :boolean, default: false
  end
end
