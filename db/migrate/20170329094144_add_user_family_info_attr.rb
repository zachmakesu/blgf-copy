class AddUserFamilyInfoAttr < ActiveRecord::Migration
  def change
    add_column :users, :spouse_first_name, :string
    add_column :users, :spouse_middle_name, :string
    add_column :users, :spouse_last_name, :string

    add_column :users, :parant_fathers_first_name, :string
    add_column :users, :parant_fathers_middle_name, :string
    add_column :users, :parant_fathers_last_name, :string

    add_column :users, :parant_mothers_first_name, :string
    add_column :users, :parant_mothers_middle_name, :string
    add_column :users, :parant_mothers_last_name, :string
  end
end