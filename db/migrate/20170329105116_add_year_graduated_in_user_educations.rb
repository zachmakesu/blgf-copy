class AddYearGraduatedInUserEducations < ActiveRecord::Migration
  def change
    add_column :educations, :year_graduated, :string
  end
end
