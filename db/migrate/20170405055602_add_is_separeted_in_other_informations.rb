class AddIsSeparetedInOtherInformations < ActiveRecord::Migration
  def change
    add_column :other_informations, :is_separated, :boolean, default: false
    add_column :other_informations, :give_details, :text
  end
end
