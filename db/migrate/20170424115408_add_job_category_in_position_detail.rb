class AddJobCategoryInPositionDetail < ActiveRecord::Migration
  def change
    add_column :position_details, :job_category, :string
  end
end
