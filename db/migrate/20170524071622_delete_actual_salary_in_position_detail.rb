class DeleteActualSalaryInPositionDetail < ActiveRecord::Migration
  def change
    remove_column :position_details, :actual_salary
  end
end
