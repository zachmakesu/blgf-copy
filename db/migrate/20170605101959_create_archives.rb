class CreateArchives < ActiveRecord::Migration
  def change
    create_table :archives do |t|
      t.references  :user,  index: true, foreign_key: true
      t.integer     :field, default: 0
      t.json        :info
      t.string      :archive_date
      t.timestamps null: false
    end
  end
end
