class IntToFloatInPlantillaGroups < ActiveRecord::Migration
  def change

    remove_column :plantilla_groups, :group_order
    add_column :plantilla_groups, :group_order, :float
  end
end
