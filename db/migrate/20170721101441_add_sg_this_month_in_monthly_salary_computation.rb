class AddSgThisMonthInMonthlySalaryComputation < ActiveRecord::Migration
  def change
    add_column :monthly_salary_computations, :sg_this_month, :integer
    add_column :monthly_salary_computations, :plantilla_position_this_month, :string
  end
end
