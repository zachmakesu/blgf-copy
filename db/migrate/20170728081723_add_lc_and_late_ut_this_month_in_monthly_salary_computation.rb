class AddLcAndLateUtThisMonthInMonthlySalaryComputation < ActiveRecord::Migration
  def change
    add_column :monthly_salary_computations, :late_this_month, :string
    add_column :monthly_salary_computations, :under_time_this_month, :string
    add_column :monthly_salary_computations, :over_time_this_month, :string
  end
end
