class CreateLeaveCreditCalculationHistories < ActiveRecord::Migration
  def change
    create_table :leave_credit_calculation_histories do |t|
      t.string      :calculation_date, null: false
      t.integer     :proccessed_by, null: false
      t.timestamps null: false
    end
  end
end
