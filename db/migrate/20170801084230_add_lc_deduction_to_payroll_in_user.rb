class AddLcDeductionToPayrollInUser < ActiveRecord::Migration
  def change
    add_column :users, :lc_deduction_to_payroll, :float, default: 0.0
  end
end
