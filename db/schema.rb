# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170801084230) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "residency"
    t.text     "location"
    t.string   "zipcode"
    t.string   "contact_number"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "agency_infos", force: :cascade do |t|
    t.string   "agency_name"
    t.string   "agency_code"
    t.string   "region"
    t.string   "tin_number"
    t.text     "address"
    t.string   "zip_code"
    t.string   "telephone_number"
    t.string   "facsimile"
    t.string   "email"
    t.string   "website"
    t.string   "salary_schedule"
    t.string   "gsis_number"
    t.float    "gsis_employee_percent_share"
    t.float    "gsis_employer_percent_share"
    t.string   "pagibig_number"
    t.float    "pagibig_employee_percent_share"
    t.float    "pagibig_employer_percent_share"
    t.float    "privident_employee_percent_share"
    t.float    "privident_employer_percent_share"
    t.float    "philhealth_employee_percent_share"
    t.float    "philhealth_employer_percent_share"
    t.float    "philhealth_percentage"
    t.string   "philhealth_number"
    t.text     "mission"
    t.text     "vision"
    t.text     "mandate"
    t.string   "bank_account_number"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "announcements", force: :cascade do |t|
    t.integer  "announcement_type"
    t.string   "posted_at"
    t.text     "message"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "start_time_at",     default: "00:00:00"
    t.string   "send_to",           default: "all"
  end

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "encrypted_access_token", default: "",   null: false
    t.boolean  "active",                 default: true, null: false
    t.datetime "expires_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "api_keys", ["user_id"], name: "index_api_keys_on_user_id", using: :btree

  create_table "appointment_statuses", force: :cascade do |t|
    t.string   "appointment_code"
    t.string   "appointment_status"
    t.boolean  "leave_entitled",     default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "archives", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "field",        default: 0
    t.json     "info"
    t.string   "archive_date"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "archives", ["user_id"], name: "index_archives_on_user_id", using: :btree

  create_table "attendance_schemes", force: :cascade do |t|
    t.string   "scheme_code",                            null: false
    t.integer  "scheme_type",       default: 0,          null: false
    t.string   "scheme_name",                            null: false
    t.string   "am_time_in_from",   default: "00:00:00"
    t.string   "am_time_in_to",     default: "00:00:00"
    t.string   "pm_time_out_from",  default: "00:00:00"
    t.string   "pm_time_out_to",    default: "00:00:00"
    t.string   "nn_time_out_from",  default: "00:00:00"
    t.string   "nn_time_out_to",    default: "00:00:00"
    t.string   "nn_time_in_from",   default: "00:00:00"
    t.string   "nn_time_in_to",     default: "00:00:00"
    t.string   "over_time_starts",  default: "00:00:00"
    t.string   "over_time_ends",    default: "00:00:00"
    t.string   "grace_period",      default: "00:00:00"
    t.boolean  "gp_leave_creadits", default: false
    t.boolean  "gp_late",           default: false
    t.string   "wrkhr_leave",       default: "00:00:00"
    t.boolean  "hlf_late_und",      default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "benefits", force: :cascade do |t|
    t.string   "code",                     null: false
    t.integer  "benefit_type", default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "children", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "middle_initial"
    t.string   "relationship"
    t.string   "birthdate"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "children", ["user_id"], name: "index_children_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "device"
    t.string   "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "contacts", ["user_id"], name: "index_contacts_on_user_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "course_code"
    t.string   "course_description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "csv_files", force: :cascade do |t|
    t.string   "csv_file_name"
    t.string   "csv_content_type"
    t.integer  "csv_file_size"
    t.datetime "csv_updated_at"
    t.integer  "csvable_id"
    t.string   "csvable_type"
    t.integer  "total_rows",           default: 0
    t.integer  "total_processed_rows", default: 0
    t.boolean  "finished",             default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "custodians", force: :cascade do |t|
    t.integer  "organization_child_id"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "deduction_histories", force: :cascade do |t|
    t.integer  "user_id",      null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "date_applied", null: false
  end

  create_table "deduction_history_children", force: :cascade do |t|
    t.integer  "deduction_history_id"
    t.integer  "deduction_type_id",                null: false
    t.integer  "amount",               default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "deduction_types", force: :cascade do |t|
    t.string   "deduction_code",                               null: false
    t.boolean  "mandatory",                   default: false
    t.string   "deduction",                                    null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "deduction_base_calculations", default: "None", null: false
  end

  create_table "deductives", force: :cascade do |t|
    t.integer  "deduction_type_id",                 null: false
    t.float    "amount",            default: 0.0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "user_id",                           null: false
    t.boolean  "overwrite",         default: false
  end

  create_table "directory_contacts", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "directory_contacts", ["user_id"], name: "index_directory_contacts_on_user_id", using: :btree

  create_table "docket_management_files", force: :cascade do |t|
    t.string   "title",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "drafts", force: :cascade do |t|
    t.string   "classification"
    t.integer  "owner_id"
    t.text     "object"
    t.integer  "status",         default: 0
    t.integer  "processor_id"
    t.datetime "processed_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "remarks"
  end

  create_table "duty_and_responsibilities", force: :cascade do |t|
    t.float    "percent_work"
    t.string   "duties"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "position_code_id", null: false
  end

  create_table "education_levels", force: :cascade do |t|
    t.string   "level_code"
    t.string   "level_description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "educations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "school_name"
    t.string   "highest_grade_level_units_earned"
    t.string   "date_of_attendance_from"
    t.string   "date_of_attendance_to"
    t.text     "honors_received"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "education_level_id"
    t.integer  "course_id"
    t.string   "year_graduated"
  end

  add_index "educations", ["user_id"], name: "index_educations_on_user_id", using: :btree

  create_table "employee_dtrs", force: :cascade do |t|
    t.string   "biometric_id",                                     null: false
    t.string   "dtr_date",                  default: "0000-00-00", null: false
    t.string   "in_am",                     default: "00:00:00",   null: false
    t.string   "out_am",                    default: "00:00:00",   null: false
    t.string   "in_pm",                     default: "00:00:00",   null: false
    t.string   "out_pm",                    default: "00:00:00",   null: false
    t.string   "in_ot",                     default: "00:00:00",   null: false
    t.string   "out_ot",                    default: "00:00:00",   null: false
    t.string   "remarks"
    t.string   "other_info"
    t.string   "name"
    t.string   "ip"
    t.string   "editdate"
    t.string   "perdiem"
    t.string   "old_value"
    t.string   "ot",                        default: "00:00:00",   null: false
    t.string   "late",                      default: "00:00:00",   null: false
    t.string   "ut",                        default: "00:00:00",   null: false
    t.integer  "attendance_scheme_id_used", default: 0,            null: false
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.boolean  "is_edited",                 default: false
    t.integer  "processed_by"
  end

  create_table "exam_types", force: :cascade do |t|
    t.string   "exam_code"
    t.string   "exam_description"
    t.boolean  "csc_eligible",     default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "examinations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "exam_type_id"
    t.float    "rating"
    t.string   "date_of_exam"
    t.string   "place_of_exam"
    t.string   "licence_number"
    t.string   "date_of_release"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "exam_description"
  end

  create_table "fts_requests", force: :cascade do |t|
    t.integer  "owner_id",                            null: false
    t.string   "biometric_id",                        null: false
    t.integer  "processor_id"
    t.string   "dtr_date",     default: "0000-00-00", null: false
    t.string   "in_am",        default: "00:00:00",   null: false
    t.string   "out_am",       default: "00:00:00",   null: false
    t.string   "in_pm",        default: "00:00:00",   null: false
    t.string   "out_pm",       default: "00:00:00",   null: false
    t.string   "in_ot",        default: "00:00:00",   null: false
    t.string   "out_ot",       default: "00:00:00",   null: false
    t.string   "remarks"
    t.integer  "status",       default: 0,            null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "knowledge_bases", force: :cascade do |t|
    t.string   "code",        null: false
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "leave_credit_calculation_histories", force: :cascade do |t|
    t.string   "calculation_date", null: false
    t.integer  "proccessed_by",    null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "leave_credit_histories", force: :cascade do |t|
    t.integer  "user_id",                        null: false
    t.integer  "credit_type",                    null: false
    t.integer  "created_by"
    t.float    "credits",          default: 0.0
    t.boolean  "deductible",                     null: false
    t.integer  "leave_request_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "leave_requests", force: :cascade do |t|
    t.integer  "owner_id"
    t.float    "applied_days"
    t.string   "date_from"
    t.string   "date_to"
    t.integer  "processor_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "remarks"
    t.integer  "status",             default: 0
    t.text     "reason"
    t.integer  "day_type"
    t.string   "half_date"
    t.integer  "leave_type"
    t.float    "deductible_credits", default: 0.0
  end

  create_table "leave_types", force: :cascade do |t|
    t.string   "leave_code"
    t.string   "leave_type"
    t.string   "specific_leave"
    t.integer  "no_of_days",     default: 0
    t.boolean  "parent",                     null: false
    t.integer  "leave_type_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "legal_cases", force: :cascade do |t|
    t.integer  "case_number",      null: false
    t.text     "title"
    t.string   "charged_offenses"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "status"
    t.integer  "user_id"
  end

  add_index "legal_cases", ["case_number"], name: "index_legal_cases_on_case_number", using: :btree

  create_table "lgu_names", force: :cascade do |t|
    t.float    "latitude"
    t.integer  "lgu_code"
    t.integer  "lgu_type_id"
    t.float    "longitude"
    t.string   "name",        null: false
    t.integer  "province_id"
    t.integer  "region_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "lgu_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mode_of_separations", force: :cascade do |t|
    t.string   "separation_mode"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "monthly_salary_computations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "this_month",                                    null: false
    t.text     "deductions_this_month",                         null: false
    t.boolean  "include_in_dtr_this_month",     default: false, null: false
    t.boolean  "include_in_payroll_this_month", default: false, null: false
    t.float    "authorized_salary_this_month",                  null: false
    t.text     "leave_credits_this_month",                      null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.text     "benefits_this_month",                           null: false
    t.integer  "sg_this_month"
    t.string   "plantilla_position_this_month"
    t.string   "late_this_month"
    t.string   "under_time_this_month"
    t.string   "over_time_this_month"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "notified_by_id"
    t.integer  "notificationable_id"
    t.string   "notificationable_type"
    t.text     "message"
    t.string   "api_slug"
    t.boolean  "read",                  default: false
    t.integer  "read_by",                               null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "notifications", ["notified_by_id"], name: "index_notifications_on_notified_by_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "organization_children", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "executive_office_id"
    t.integer  "service_id"
    t.integer  "division_id"
    t.string   "code"
    t.string   "name"
    t.string   "head_title"
    t.integer  "user_id"
    t.text     "custodian"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "organization_children", ["organization_id"], name: "index_organization_children_on_organization_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "name"
    t.integer  "level"
    t.string   "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "other_informations", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "skills_and_hobbies"
    t.text     "non_academic_destinction"
    t.string   "membership_in"
    t.boolean  "third_degree_national",    default: false
    t.text     "give_national"
    t.boolean  "fourth_degree_local",      default: false
    t.text     "give_local"
    t.boolean  "formally_charge",          default: false
    t.text     "give_charge"
    t.boolean  "administrative_offense",   default: false
    t.text     "give_offense"
    t.boolean  "any_violation",            default: false
    t.text     "give_reasons"
    t.boolean  "canditate",                default: false
    t.text     "give_date_particulars"
    t.boolean  "indigenous_member",        default: false
    t.text     "give_group"
    t.boolean  "differently_abled",        default: false
    t.text     "give_disability"
    t.boolean  "solo_parent",              default: false
    t.text     "give_status"
    t.string   "signature"
    t.string   "date_accomplished"
    t.string   "ctc_number"
    t.string   "issued_at"
    t.string   "issued_on"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "is_separated",             default: false
    t.text     "give_details"
  end

  create_table "payroll_groups", force: :cascade do |t|
    t.integer  "project_code_id"
    t.integer  "payroll_group_order"
    t.string   "payroll_group_code"
    t.string   "payroll_group_name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "pdf_files", force: :cascade do |t|
    t.string   "pdf_file_name"
    t.string   "pdf_content_type"
    t.integer  "pdf_file_size"
    t.datetime "pdf_updated_at"
    t.integer  "pdfable_id"
    t.string   "pdfable_type"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "docket_management_file_id"
  end

  create_table "phil_health_tables", force: :cascade do |t|
    t.integer  "salary_bracket",        null: false
    t.string   "salary_range"
    t.float    "salary_base"
    t.float    "total_monthly_premium", null: false
    t.float    "employee_share",        null: false
    t.float    "employer_share",        null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "default",            default: false, null: false
    t.integer  "position",                           null: false
    t.integer  "default_type",                       null: false
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  create_table "plantilla_duties", force: :cascade do |t|
    t.integer  "plantilla_id"
    t.float    "percent_work"
    t.string   "duties"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "plantilla_groups", force: :cascade do |t|
    t.string   "group_code"
    t.string   "group_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "group_order"
  end

  create_table "plantillas", force: :cascade do |t|
    t.string   "item_number"
    t.integer  "position_code_id"
    t.integer  "salary_schedule_grade"
    t.integer  "plantilla_group_id"
    t.string   "area_code"
    t.string   "area_type"
    t.boolean  "csc_eligibility",       default: false
    t.boolean  "rationalized",          default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "position_codes", force: :cascade do |t|
    t.string   "position_code"
    t.text     "position_description"
    t.string   "position_abbreviation"
    t.text     "educational_requirement"
    t.text     "experience_requirement"
    t.text     "eligibility_requirement"
    t.text     "training_requirement"
    t.string   "position_classification"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "position_details", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "service_code_id"
    t.string   "first_day_gov"
    t.string   "first_day_agency"
    t.integer  "mode_of_separation_id"
    t.string   "separation_date"
    t.integer  "appointment_status_id"
    t.integer  "executive_office_id"
    t.integer  "service_id"
    t.integer  "division_id"
    t.integer  "section_id"
    t.string   "place_of_assignment"
    t.string   "personnel_action"
    t.integer  "dependents_number",          default: 0
    t.string   "salary_effective_date"
    t.integer  "payroll_group_id"
    t.boolean  "include_dtr",                default: false
    t.boolean  "include_in_payroll",         default: false
    t.integer  "attendance_scheme_id",       default: 1
    t.float    "hazard_pay_factor",          default: 0.0
    t.integer  "plantilla_id"
    t.string   "position_date"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "step_number",                default: 1
    t.integer  "category_service",           default: 0
    t.integer  "employment_basis",           default: 0
    t.boolean  "health_insurance_exception", default: false
    t.boolean  "head_of_the_agency",         default: false
    t.string   "date_increment"
    t.string   "tax_status",                 default: "Z"
    t.string   "job_category"
  end

  create_table "project_codes", force: :cascade do |t|
    t.string   "project_code"
    t.integer  "project_order"
    t.string   "project_description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "provinces", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "geocode",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "references", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",           null: false
    t.text     "address"
    t.string   "contact_number"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "request_types", force: :cascade do |t|
    t.string   "type_of_request",     null: false
    t.integer  "applicants"
    t.integer  "first_signatory_id"
    t.integer  "second_signatory_id"
    t.integer  "third_signatory_id"
    t.integer  "fourth_signatory_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "salary_schedules", force: :cascade do |t|
    t.integer  "salary_grade", null: false
    t.float    "salary"
    t.float    "step_1"
    t.float    "step_2"
    t.float    "step_3"
    t.float    "step_4"
    t.float    "step_5"
    t.float    "step_6"
    t.float    "step_7"
    t.float    "step_8"
    t.float    "step_9"
    t.float    "step_10"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "saln_assets", force: :cascade do |t|
    t.integer  "saln_id"
    t.integer  "asset_type",           null: false
    t.text     "description"
    t.string   "kind"
    t.text     "exact_location"
    t.float    "assesed_value"
    t.float    "current_market_value"
    t.string   "acquisition_year"
    t.string   "acquisition_mode"
    t.float    "acquisition_cost"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "saln_assets", ["saln_id"], name: "index_saln_assets_on_saln_id", using: :btree

  create_table "saln_bi_and_fcs", force: :cascade do |t|
    t.integer  "saln_id"
    t.string   "business_enterprise",       null: false
    t.text     "business_address"
    t.text     "business_financial_nature"
    t.string   "acquisition_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "saln_bi_and_fcs", ["saln_id"], name: "index_saln_bi_and_fcs_on_saln_id", using: :btree

  create_table "saln_children", force: :cascade do |t|
    t.integer  "saln_id"
    t.string   "name",       null: false
    t.string   "birthdate"
    t.integer  "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "saln_children", ["saln_id"], name: "index_saln_children_on_saln_id", using: :btree

  create_table "saln_government_relatives", force: :cascade do |t|
    t.integer  "saln_id"
    t.string   "name",           null: false
    t.string   "relationship"
    t.string   "position"
    t.text     "office_address"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "saln_government_relatives", ["saln_id"], name: "index_saln_government_relatives_on_saln_id", using: :btree

  create_table "saln_issued_ids", force: :cascade do |t|
    t.integer  "saln_id"
    t.string   "id_no",       null: false
    t.string   "date_issued", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "id_type",     null: false
  end

  add_index "saln_issued_ids", ["saln_id"], name: "index_saln_issued_ids_on_saln_id", using: :btree

  create_table "saln_liabilities", force: :cascade do |t|
    t.integer  "saln_id"
    t.string   "nature",              null: false
    t.text     "creditors_name"
    t.float    "outstanding_balance", null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "saln_liabilities", ["saln_id"], name: "index_saln_liabilities_on_saln_id", using: :btree

  create_table "salns", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "statement_filing_status", default: 0, null: false
    t.string   "first_name"
    t.string   "middle_initial"
    t.string   "last_name"
    t.text     "address"
    t.string   "position"
    t.string   "office"
    t.text     "office_address"
    t.string   "spouse_first_name"
    t.string   "spouse_middle_initial"
    t.string   "spouse_last_name"
    t.string   "spouse_position"
    t.string   "spouse_office"
    t.text     "spouse_office_address"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "salns", ["user_id"], name: "index_salns_on_user_id", using: :btree

  create_table "scholarships", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "service_codes", force: :cascade do |t|
    t.string   "service_code"
    t.string   "service_description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "set_benefits", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount",     default: 0.0,   null: false
    t.integer  "benefit_id",                 null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "overwrite",  default: false
  end

  create_table "tax_ranges", force: :cascade do |t|
    t.float    "tax_base",      default: 0.0, null: false
    t.float    "tax_deduction", default: 0.0, null: false
    t.float    "factor",        default: 0.0
    t.float    "taxable_from",  default: 0.0
    t.float    "taxable_to",    default: 0.0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "tax_statuses", force: :cascade do |t|
    t.string   "tax_status",                     null: false
    t.float    "exemption_amount", default: 0.0, null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "trainings_and_seminars", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "inclusive_date_from"
    t.string   "inclusive_date_to"
    t.integer  "number_of_hours",     null: false
    t.string   "conducted_by"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "treasurer_assignments", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "treasurer_subrole"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "lgu_name_id"
  end

  add_index "treasurer_assignments", ["user_id"], name: "index_treasurer_assignments_on_user_id", using: :btree

  create_table "user_seen_notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "notification_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "user_seen_notifications", ["user_id"], name: "index_user_seen_notifications_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "",  null: false
    t.string   "encrypted_password",                default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "middle_initial"
    t.string   "name_extension"
    t.string   "birthdate"
    t.integer  "gender",                            default: 0,   null: false
    t.integer  "civil_status",                      default: 0,   null: false
    t.string   "citizenship"
    t.float    "height"
    t.float    "weight"
    t.string   "blood_type"
    t.string   "tin_number"
    t.string   "gsis_policy_number"
    t.string   "pagibig_id_number"
    t.string   "philhealth_number"
    t.string   "sss_number"
    t.string   "spouse_name"
    t.string   "spouse_occupation"
    t.string   "spouse_employer_or_business_name"
    t.string   "spouse_work_or_business_address"
    t.string   "spouse_contact_number"
    t.string   "parent_fathers_name"
    t.string   "parent_mothers_maiden_name"
    t.text     "parent_address"
    t.string   "employee_id",                                     null: false
    t.integer  "department_id"
    t.integer  "division_id"
    t.integer  "position_id"
    t.integer  "employed_date_at"
    t.boolean  "employed_status"
    t.integer  "role",                              default: 0,   null: false
    t.string   "deleted_at"
    t.integer  "office_designation",                default: 0,   null: false
    t.string   "biometric_id"
    t.integer  "failed_attempts",                   default: 0,   null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.date     "leave_credit_certified_date_month"
    t.date     "leave_credit_certified_date_year"
    t.string   "birthplace"
    t.string   "treasurer_subrole"
    t.string   "spouse_first_name"
    t.string   "spouse_middle_name"
    t.string   "spouse_last_name"
    t.string   "parant_fathers_first_name"
    t.string   "parant_fathers_middle_name"
    t.string   "parant_fathers_last_name"
    t.string   "parant_mothers_first_name"
    t.string   "parant_mothers_middle_name"
    t.string   "parant_mothers_last_name"
    t.string   "remittance_id"
    t.float    "lc_deduction_to_payroll",           default: 0.0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "voluntary_works", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name_of_organization"
    t.text     "address_of_organization"
    t.string   "inclusive_date_from"
    t.string   "inclusive_date_to"
    t.integer  "number_of_hours",         null: false
    t.string   "position_nature_of_work"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "work_experiences", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "appointment_status_id"
    t.integer  "mode_of_separation_id"
    t.string   "inclusive_date_from"
    t.string   "inclusive_date_to"
    t.string   "position_title"
    t.string   "department_agency_office"
    t.string   "monthly_salary"
    t.boolean  "government_service",       default: false
    t.string   "salary_grade_and_step"
    t.integer  "branch"
    t.string   "separation_date"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "zones", force: :cascade do |t|
    t.string   "zone_code",   null: false
    t.text     "description"
    t.string   "server_name"
    t.string   "username"
    t.string   "password"
    t.string   "database"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_foreign_key "addresses", "users"
  add_foreign_key "api_keys", "users"
  add_foreign_key "archives", "users"
  add_foreign_key "children", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "directory_contacts", "users"
  add_foreign_key "educations", "users"
  add_foreign_key "organization_children", "organizations"
  add_foreign_key "treasurer_assignments", "users"
end
