# Add seed file for all envs here
models = %w{education_levels benefits deduction_types tax_statuses tax_ranges photos attendance_schemes philhealth_premium salary_schedule project_codes payroll_groups service_codes appointment_statuses mode_of_separations organizations agencies positions plantilla_groups leave_types users children educations announcements  plantillas employee_dtr position_detail dms_file_names } 

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
