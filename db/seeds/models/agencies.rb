agency_info = [
  {
    agency_name: "BUREAU OF LOCAL GOVERNMENT AND FINANCE", 
    agency_code: "BLGF", 
    region: "Central Office", 
    tin_number: "000-111-222-333", 
    address: "Address", 
    zip_code: "0000", 
    telephone_number: "222-3333", 
    facsimile: nil, 
    email: "info@blgf.gov.ph", 
    website: "blgf.gov.ph", 
    salary_schedule: "Weekly", 
    gsis_number: "1234678", 
    gsis_employee_percent_share: 9, 
    gsis_employer_percent_share: 12, 
    pagibig_number: "123123", 
    pagibig_employee_percent_share: 2, 
    pagibig_employer_percent_share: 5, 
    privident_employee_percent_share: 2, 
    privident_employer_percent_share: 1, 
    philhealth_employee_percent_share: 2, 
    philhealth_employer_percent_share: 2, 
    philhealth_percentage: 4, mission: "BUREAU OF LOCAL GOVERNMENT FINANCE OF THE DEPARTMENT OF FINANCE IS THE FOCAL AGENCY AND AN AUTHORITY IN LOCAL FINANCE THAT AIMS TO BE AT THE FOREFRONT OF LOCAL ECONOMIC GROWTH LEADING THE WAY TOWARDS NATIONAL DEVELOPMENT.", 
    vision: "BUREAU OF LOCAL GOVERNMENT FINANCE OF THE DEPARTMENT OF FINANCE IS THE FOCAL AGENCY AND AN AUTHORITY IN LOCAL FINANCE THAT AIMS TO BE AT THE FOREFRONT OF LOCAL ECONOMIC GROWTH LEADING THE WAY TOWARDS NATIONAL DEVELOPMENT.", 
    mandate: nil, 
    bank_account_number: nil
  }
]

ActiveRecord::Base.transaction do
  if AgencyInfo.all.empty?
    agency_info.each do |info|
      new_info = AgencyInfo.new(info)
      if new_info.save
        print '✓'
      else
        puts new_info.errors.inspect
        break
      end
    end
    print "\nTotal : #{AgencyInfo.all.count}\n"
  else
    print "Skipped seeding agency_info table.\n"
  end
end
