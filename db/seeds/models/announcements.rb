announcement_list = [
  {
    announcement_type: 0,
    posted_at: DateTime.now + 3.days,
    message: "Black Saturday",
    send_to: 0
  },
  {
    announcement_type: 1,
    posted_at: DateTime.now + 3.days,
    message: "MIS department meeting at 5:00pm today",
    send_to: 0
  }
]

ActiveRecord::Base.transaction do
  if Announcement.all.empty?
    announcement_list.each do |a|
      new_announcement                    = Announcement.new
      new_announcement.announcement_type  = a[:announcement_type]
      new_announcement.posted_at          = a[:posted_at]
      new_announcement.message        = a[:message]
      new_announcement.send_to            = a[:send_to]
      if new_announcement.save
        print '✓'
      else
        puts new_announcement.errors.inspect
        break
      end
    end
    print "\nTotal : #{Announcement.all.count}"
  else
    print "Skipped seeding announcements table.\n"
  end
end
