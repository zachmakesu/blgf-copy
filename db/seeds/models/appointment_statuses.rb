appointment_statuses = [
  {
    appointment_code: "Temporary",
    appointment_status: "Temporary",
    leave_entitled: false,
  },
  {
    appointment_code: "Probationary",
    appointment_status: "Probationary",
    leave_entitled: false,
  },
  {
    appointment_code: "Permanent",
    appointment_status: "Permanent",
    leave_entitled: true,
  },
  {
    appointment_code: "EE",
    appointment_status: "Emergency Employee",
    leave_entitled: false,
  },
  {
    appointment_code: "Casual",
    appointment_status: "Casual",
    leave_entitled: true,
  },
  {
    appointment_code: "Part-time",
    appointment_status: "Part-time",
    leave_entitled: false,
  },
  {
    appointment_code: "Substitute",
    appointment_status: "Substitute",
    leave_entitled: false,
  },
  {
    appointment_code: "Phase Out",
    appointment_status: "Phase Out",
    leave_entitled: false,
  },
  {
    appointment_code: "Co-terminous",
    appointment_status: "Co-terminous",
    leave_entitled: true,
  },
  {
    appointment_code: "Suspended",
    appointment_status: "Suspended",
    leave_entitled: true,
  },
  {
    appointment_code: "Lump",
    appointment_status: "Contractual-Lumpsum",
    leave_entitled: false,
  },
  {
    appointment_code: "GIA",
    appointment_status: "Contractual-GIA",
    leave_entitled: false,
  },
  {
    appointment_code: "PRESA",
    appointment_status: "Presidential Appointee",
    leave_entitled: true,
  },
  {
    appointment_code: "JO",
    appointment_status: "Job Order",
    leave_entitled: false,
  },
  {
    appointment_code: "GOV",
    appointment_status: "Goverment Employee",
    leave_entitled: true,
  },
  {
    appointment_code: "OJT",
    appointment_status: "On The Job Training",
    leave_entitled: false,
  },
  {
    appointment_code: "CONT",
    appointment_status: "Contractual",
    leave_entitled: false,
  },
  {
    appointment_code: "OA",
    appointment_status: "Original appointment",
    leave_entitled: false,
  },
  {
    appointment_code: "PRI",
    appointment_status: "Private",
    leave_entitled: true,
  },
  {
    appointment_code: "Prom",
    appointment_status: "Promotion",
    leave_entitled: true,
  },
  {
    appointment_code: "PromTran",
    appointment_status: "PromotionTransfer",
    leave_entitled: true,
  },
  {
    appointment_code: "Proj",
    appointment_status: "Project Based",
    leave_entitled: true,
  },
  {
    appointment_code: "Trainee",
    appointment_status: "Trainee",
    leave_entitled: false,
  },
  {
    appointment_code: "Reg",
    appointment_status: "Regular",
    leave_entitled: true,
  },
  {
    appointment_code: "Dsgntd",
    appointment_status: "Designated",
    leave_entitled: true,
  },
  {
    appointment_code: "ICO",
    appointment_status: "In-Charge of Office",
    leave_entitled: true,
  },
  {
    appointment_code: "OIC",
    appointment_status: "Officer-in-Charge",
    leave_entitled: true,
  },
  {
    appointment_code: "RE-A",
    appointment_status: "Re-Appointmen",
    leave_entitled: true,
  }
]

ActiveRecord::Base.transaction do
  if AppointmentStatus.all.empty?
    appointment_statuses.each do |status|
      new_status = AppointmentStatus.new(status)
      if new_status.save
        print '✓'
      else
        puts new_status.errors.inspect
        break
      end
    end
    print "\nTotal : #{AppointmentStatus.all.count}\n"
  else
    print "Skipped seeding appointment_statuses table.\n"
  end
end
