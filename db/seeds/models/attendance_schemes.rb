attendance_schemes = [
  {
    :scheme_code => "Slide 1",
    :scheme_name => "Sliding 1",
    :am_time_in_from => "07:00:00",
    :am_time_in_to => "08:00:00",
    :pm_time_out_from => "16:00:00",
    :pm_time_out_to => "17:00:00",
    :nn_time_out_from => "12:00:00",
    :nn_time_out_to => "13:00:00",
    :nn_time_in_from => "12:00:00",
    :nn_time_in_to => "13:00:00"
  },
  {
    :scheme_code => "Slide 2",
    :scheme_name => "Sliding 2",
    :am_time_in_from => "08:00:00",
    :am_time_in_to => "09:00:00",
    :pm_time_out_from => "17:00:00",
    :pm_time_out_to => "18:00:00",
    :nn_time_out_from => "12:00:00",
    :nn_time_out_to => "13:00:00",
    :nn_time_in_from => "12:00:00",
    :nn_time_in_to => "13:00:00"
  }
]

ActiveRecord::Base.transaction do
  if AttendanceScheme.all.empty?
    attendance_schemes.each do |scheme|
      new_scheme = AttendanceScheme.new(scheme)
      if new_scheme.save
        print '✓'
      else
        puts new_scheme.errors.inspect
        break
      end
    end
    print "\nTotal : #{AttendanceScheme.all.count}\n"
  else
    print "Skipped seeding attendance_schemes table.\n"
  end
end
