benefits = [
  {
    code: "PERA",
    benefit_type: "allowance"
  },
  {
    code: "RATA",
    benefit_type: "allowance"
  },
  {
    code: "13th Month Pay",
    benefit_type: "bonus"
  },
  {
    code: "Cash Gift",
    benefit_type: "bonus"
  },
  {
    code: "Mid year",
    benefit_type: "bonus"
  },
  {
    code: "Additional Income",
    benefit_type: "additional_income"
  }
]

ActiveRecord::Base.transaction do
  if Benefit.all.empty?
    benefits.each do |benefit|
      new_benefit = Benefit.new(benefit)
      if new_benefit.save
        print '✓'
      else
        puts new_benefit.errors.inspect
        break
      end
    end
    print "\nTotal : #{Benefit.all.count}\n"
  else
    print "Skipped seeding benefits table.\n"
  end
end
