child_list = [
  {
    first_name: "Jane Dianne",
    last_name: "Gaylican",
    middle_initial: "S",
    relationship: "Daughter",
    birthdate: DateTime.now - 12.years
  },
  {
    first_name: "Jun",
    last_name: "Gaylican",
    middle_initial: "S",
    relationship: "Son",
    birthdate: DateTime.now - 15.years
  }
]

ActiveRecord::Base.transaction do
  if Child.all.empty?
    child_list.each do |child|
      new_child                 = User.first.children.build
      new_child.first_name      = child[:first_name]
      new_child.last_name       = child[:last_name]
      new_child.middle_initial  = child[:middle_initial]
      new_child.relationship    = child[:relationship]
      new_child.birthdate       = child[:birthdate]
      if new_child.save
        print '✓'
      else
        puts new_child.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.first.children.count}\n"
  else
    print "Skipped seeding child table.\n"
  end
end
