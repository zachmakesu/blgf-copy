deduction_types = [
  {
    deduction_code: "PH",
    deduction: "PhilHealth Premium",
    mandatory: true
  },
  {
    deduction_code: "GSIS",
    deduction: "GSIS Retirement",
    mandatory: true
  },
  {
    deduction_code: "PAGIBIG",
    deduction: "PAGIBIG Contribution",
    mandatory: true
  },
  {
    deduction_code: "TAX",
    deduction: "Witholding tax",
    mandatory: true
  },
  {
    deduction_code: "BLGF EA",
    deduction: "BLGF EA Dues",
    mandatory: true
  },
  {
    deduction_code: "GSIS CL",
    deduction: "GSIS Conso Loan",
    mandatory: false
  },
  {
    deduction_code: "GSIS PL",
    deduction: "GSIS Policy Loan",
    mandatory: false
  }
]

ActiveRecord::Base.transaction do
  if DeductionType.all.empty?
    deduction_types.each do |deduction|
      new_deduction = DeductionType.new(deduction)
      if new_deduction.save
        print '✓'
      else
        puts new_deduction.errors.inspect
        break
      end
    end
    print "\nTotal : #{DeductionType.all.count}\n"
  else
    print "Skipped seeding deduction_type table.\n"
  end
end
