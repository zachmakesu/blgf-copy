dms_file_names = [
  {
    title: "Appointments [CSC Form 33 (1985)]"
  },
  {
    title: "Assumption to Duty"
  },
  {
    title: "Certification of Leave Balances (for transferees)"
  },
  {
    title: "Clearance from Property and Money Accountabilities (for transferees)"
  },
  {
    title: "Contracts of Servie (if applicable)"
  },
  {
    title: "Copies of Certificates of Eligibilities"
  },
  {
    title: "Copies of Certificates of Eligibilities"
  },
  {
    title: "Copies of Diplomas, Commendation and Awards"
  },
  {
    title: "Copies of Disciplinary Actions (if any)"
  },
  {
    title: "Copy of Marriage Contract (if applicable)"
  },
  {
    title: "Designations"
  },
  {
    title: "Medical Certificate [CSC Form 211 (1977)]"
  },
  {
    title: "NBI Clearance"
  },
  {
    title: "Notices of Salary Adjustments / Step Increments"
  },
  {
    title: "Oaths of Office"
  },
  {
    title: "Personal Data Sheet [CSC Form 212 (2005)]"
  },
  {
    title: "Position Description Forms"
  },
  {
    title: "Administrative Cases"
  },
  {
    title: "Affidavits"
  },
  {
    title: "Complaints"
  },
  {
    title: "Decisions"
  },
  {
    title: "Formal Charge"
  },
  {
    title: "Investigation Reports"
  },
  {
    title: "Petitions"
  },
  {
    title: "Resolutions"
  },
  {
    title: "Subpoenas"
  },
  {
    title: "Motion for Reconsideration"
  },
  {
    title: "Show-Cause Memorandum"
  }
]

ActiveRecord::Base.transaction do
  if DocketManagementFile.all.empty?
    dms_file_names.each do |name|
      new_name = DocketManagementFile.new(name)
      if new_name.save
        print '✓'
      else
        puts new_name.errors.inspect
        break
      end
    end
    print "\nTotal : #{DocketManagementFile.all.count}\n"
  else
    print "Skipped seeding DocketManagementFile table.\n"
  end
end
