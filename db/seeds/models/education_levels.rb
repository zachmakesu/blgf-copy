education_levels = [
  {
    level_code: "ELM",
    level_description: "Elementary"
  },
  {
    level_code: "VCL",
    level_description: "Vocational"
  },
  {
    level_code: "CLG",
    level_description: "College"
  },
  {
    level_code: "MAMS",
    level_description: "Master's Degree"
  },
  {
    level_code: "Ph.D.",
    level_description: "Doctorate"
  },
  {
    level_code: "HSL",
    level_description: "High School"
  },
  {
    level_code: "TRT",
    level_description: "Tertiary"
  },
  {
    level_code: "GST",
    level_description: "Graduate Studies"
  },
  {
    level_code: "L",
    level_description: "LAW"
  },
]

ActiveRecord::Base.transaction do
  if EducationLevel.all.empty?
    education_levels.each do |level|
      new_level = EducationLevel.new(level)
      if new_level.save
        print '✓'
      else
        puts new_level.errors.inspect
        break
      end
    end
    print "\nTotal : #{EducationLevel.all.count}\n"
  else
    print "Skipped seeding education_levels table.\n"
  end
end
