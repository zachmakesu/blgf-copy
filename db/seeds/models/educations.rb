# education_list = [
#   {
#     education_type: 0,
#     school_name: "Don Bosco Manila",
#     date_of_attendance_from: DateTime.now - 20.years,
#     date_of_attendance_to: DateTime.now - 14.years,
#     honors_received: "1st Honorable Mention"
#   },
#   {
#     education_type: 1,
#     school_name: "Don Bosco Manila",
#     date_of_attendance_from: DateTime.now - 14.years,
#     date_of_attendance_to: DateTime.now - 10.years,
#     honors_received: "1st Honorable Mention"
#   },
#   {
#     education_type: 2,
#     school_name: "Tesda",
#     degree_course: "Automotive",
#     highest_grade_level_units_earned: "Basic Automotive",
#     date_of_attendance_from: DateTime.now - 10.years,
#     date_of_attendance_to: DateTime.now - 7.years,
#   },
#   {
#     education_type: 3,
#     school_name: "University of Sto Tomas",
#     degree_course: "Bachelor of Science in Fine Arts",
#     highest_grade_level_units_earned: "Graduate",
#     date_of_attendance_from: DateTime.now - 7.years,
#     date_of_attendance_to: DateTime.now - 3.years,
#   },
#   {
#     education_type: 4,
#     school_name: "University of Sto Tomas",
#     degree_course: "Masters in Computer Science",
#     highest_grade_level_units_earned: "Masters Degree",
#     date_of_attendance_from: DateTime.now - 3.years,
#     date_of_attendance_to: DateTime.now - 1.years,
#   },
#   {
#     education_type: 5,
#     school_name: "University of Sto Tomas",
#     degree_course: "PhD in Education",
#     date_of_attendance_from: DateTime.now - 1.years,
#     date_of_attendance_to: DateTime.now,
#   }
# ]

# ActiveRecord::Base.transaction do
#   education_list.each do |ed|
#     user_education                                   = User.first.educations.new()
#     user_education.education_type                    = ed[:education_type]
#     user_education.school_name                       = ed[:school_name]
#     user_education.degree_course                     = ed[:degree_course]
#     user_education.highest_grade_level_units_earned  = ed[:highest_grade_level_units_earned]
#     user_education.date_of_attendance_from           = ed[:date_of_attendance_from]
#     user_education.date_of_attendance_to             = ed[:date_of_attendance_to]
#     user_education.honors_received                   = ed[:honors_received]
#     if user_education.save
#       print '✓'
#     else
#       puts user_education.errors.inspect
#       break
#     end
#   end
#   print "\nTotal : #{User.first.educations.count}\n"
# end
