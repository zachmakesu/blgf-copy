employee_dtrs = [
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-01",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-02",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-03",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-04",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-05",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-08",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-09",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-10",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-11",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-12",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-15",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-16",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-17",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-18",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-19",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-22",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-23",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-24",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-25",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-26",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },

  {
    biometric_id: "6043",    
    dtr_date: "2016-08-29",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-30",
    in_am: "08:23:44",
    out_am: "12:24:34",
    in_pm: "12:24:35",
    out_pm: "16:24:35"
  },
  {
    biometric_id: "6043",    
    dtr_date: "2016-08-31",
    in_am: "09:23:44",
    out_am: "12:24:34",
    in_pm: "13:24:35",
    out_pm: "15:24:35"
  }
]

ActiveRecord::Base.transaction do
  if EmployeeDtr.all.empty?
    employee_dtrs.each do |dtr|
      new_dtr = EmployeeDtr.new(dtr)
      if new_dtr.save
        print '✓'
      else
        puts new_dtr.errors.inspect
        break
      end
    end
    print "\nTotal : #{EmployeeDtr.all.count}\n"
  else
    print "Skipped seeding employee_dtr table.\n"
  end
end
