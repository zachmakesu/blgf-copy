leave_types = [
  {
    leave_code: "SL",
    leave_type: "Sick Leave",
    parent: true,
    no_of_days: 15
  },
  {
    leave_code: "VL",
    leave_type: "Vacation Leave",
    parent: true,
    no_of_days: 15
  }
]

ActiveRecord::Base.transaction do
  if LeaveType.all.empty?
    leave_types.each do |leave|
      new_leave = LeaveType.new(leave)
      if new_leave.save
        print '✓'
      else
        puts new_leave.errors.inspect
        break
      end
    end
    print "\nTotal : #{LeaveType.all.count}\n"
  else
    print "Skipped seeding leavel_types table.\n"
  end
end
