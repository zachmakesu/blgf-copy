mode_of_separations = [
  {
    separation_mode: "In-Service"
  },
  {
    separation_mode: "N/A"
  },
  {
    separation_mode: "AWOL"
  },
  {
    separation_mode: "Complicated"
  },
  {
    separation_mode: "Deceased"
  },
  {
    separation_mode: "Drop-from-the-Rolls"
  },
  {
    separation_mode: "End-of-Contract"
  },
  {
    separation_mode: "Rationalization-Plan"
  },
  {
    separation_mode: "Resigned"
  },
  {
    separation_mode: "Retired"
  },
  {
    separation_mode: "Terminated"
  },
  {
    separation_mode: "Transferred"
  }

]

ActiveRecord::Base.transaction do
  if ModeOfSeparation.all.empty?
    mode_of_separations.each do |mode|
      new_mode = ModeOfSeparation.new(mode)
      if new_mode.save
        print '✓'
      else
        puts new_mode.errors.inspect
        break
      end
    end
    print "\nTotal : #{ModeOfSeparation.all.count}\n"
  else
    print "Skipped seeding mode_of_separations table.\n"
  end
end
