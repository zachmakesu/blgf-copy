organization_list = [
  {
    level: 1, 
    name: "Executive Office"
  },
  {
    level: 2, 
    name: "Service"
  },
  {
    level: 3, 
    name: "Division"
  },
  {
    level: 4, 
    name: "Section"
  },
]


ActiveRecord::Base.transaction do
  if Organization.all.empty?
    organization_list.each do |organization|
      new_organization = Organization.new(organization)
      if new_organization.save
        print '✓'
      else
        puts new_organization.errors.inspect
        break
      end
    end
    print "\nTotal : #{Organization.all.count}\n"
  else
    print "Skipped seeding Organization table.\n"
  end
end
