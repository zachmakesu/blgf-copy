payroll_groups = [
  {
    project_code_id: 1,
    payroll_group_code: "OD",
    payroll_group_name: "Office of the Executive Director",
    payroll_group_order: 1,
  },
  {
    project_code_id: 10,
    payroll_group_code: "RO III",
    payroll_group_name: "Regional Office III",
    payroll_group_order: 10,
  },
  {
    project_code_id: 11,
    payroll_group_code: "RO IV",
    payroll_group_name: "Regional Office IV",
    payroll_group_order: 11,
  },
  {
    project_code_id: 12,
    payroll_group_code: "RO V",
    payroll_group_name: "Regional Office V",
    payroll_group_order: 12,
  },
  {
    project_code_id: 13,
    payroll_group_code: "RO VI",
    payroll_group_name: "Regional Office VI",
    payroll_group_order: 13,
  },
  {
    project_code_id: 14,
    payroll_group_code: "RO VII",
    payroll_group_name: "Regional Office VII",
    payroll_group_order: 14,
  },
  {
    project_code_id: 15,
    payroll_group_code: "RO VIII",
    payroll_group_name: "Regional Office VIII",
    payroll_group_order: 15,
  },
  {
    project_code_id: 16,
    payroll_group_code: "RO IX",
    payroll_group_name: "Regional Office IX",
    payroll_group_order: 16,
  },
  {
    project_code_id: 17,
    payroll_group_code: "RO X",
    payroll_group_name: "Regional Office X",
    payroll_group_order: 17,
  },
  {
    project_code_id: 18,
    payroll_group_code: "RO XI",
    payroll_group_name: "Regional Office XI",
    payroll_group_order: 18,
  },
  {
    project_code_id: 19,
    payroll_group_code: "RO XII",
    payroll_group_name: "Regional Office XII",
    payroll_group_order: 19,
  },
  {
    project_code_id: 20,
    payroll_group_code: "CAR",
    payroll_group_name: "Cordillera Administrative Region",
    payroll_group_order: 20,
  },
  {
    project_code_id: 21,
    payroll_group_code: "CARAGA",
    payroll_group_name: "CARAGA Region",
    payroll_group_order: 21,
  },
  {
    project_code_id: 22,
    payroll_group_code: "JO",
    payroll_group_name: "Job Order",
    payroll_group_order: 22,
  },
  {
    project_code_id: 3,
    payroll_group_code: "IIO",
    payroll_group_name: "Legal Service",
    payroll_group_order: 3,
  },
  {
    project_code_id: 4,
    payroll_group_code: "IAO",
    payroll_group_name: "Administrative, Financial and Management Service",
    payroll_group_order: 4,
  },
  {
    project_code_id: 5,
    payroll_group_code: "PES",
    payroll_group_name: "Local Fiscal Policy Service",
    payroll_group_order: 5,
  },
  {
    project_code_id: 6,
    payroll_group_code: "SPMS",
    payroll_group_name: "Local Government Units Operations Service",
    payroll_group_order: 6,
  },
  {
    project_code_id: 8,
    payroll_group_code: "RO I",
    payroll_group_name: "Regional Office I",
    payroll_group_order: 8,
  },
  {
    project_code_id: 9,
    payroll_group_code: "RO II",
    payroll_group_name: "Regional Office II",
    payroll_group_order: 9,
  }
]

ActiveRecord::Base.transaction do
  if PayrollGroup.all.empty?
    payroll_groups.each do |group|
      new_group = PayrollGroup.new(group)
      if new_group.save
        print '✓'
      else
        puts new_group.errors.inspect
        break
      end
    end
    print "\nTotal : #{PayrollGroup.all.count}\n"
  else
    print "Skipped seeding payroll_groups table.\n"
  end
end
