def seed_csv(file)
  File.open(File.join(Rails.root, "/db/seeds/csv/#{file}"))
end

require 'csv' 

ActiveRecord::Base.transaction do
  if PhilHealthTable.all.empty?
    CSV.foreach(seed_csv("philhealth_premium.csv"), headers: true) do |php|
      new_php = PhilHealthTable.new(php.to_hash)
      if new_php.save
        print '✓'
      else
        puts new_php.errors.inspect
        break
      end
    end
    print "\nTotal : #{PhilHealthTable.all.count}\n"
  else
    print "Skipped seeding philhealth_premium table.\n"
  end
end
