plantilla_group = [
  {
    group_code: "OD",
    group_name: "Office of the Director",
    group_order: 2
  }
]

ActiveRecord::Base.transaction do
  if PlantillaGroup.all.empty?
    plantilla_group.each do |group|
      new_group = PlantillaGroup.new(group)
      if new_group.save
        print '✓'
      else
        puts new_group.errors.inspect
        break
      end
    end
    print "\nTotal : #{PlantillaGroup.all.count}\n"
  else
    print "Skipped seeding plantilla_group table.\n"
  end
end
