plantillas = [
  {
   area_code: "010",
   area_type: "R",
   csc_eligibility: false,
   item_number: "BLGF-LAOO3-16-1998",
   plantilla_group_id: 2,
   position_code_id: 1,
   rationalized: false,
   salary_schedule_grade: 1
  }
]

ActiveRecord::Base.transaction do
  if Plantilla.all.empty?
    plantillas.each do |plantilla|
      new_plantilla = Plantilla.new(plantilla)
      if new_plantilla.save
        print '✓'
      else
        puts new_plantilla.errors.inspect
        break
      end
    end
    print "\nTotal : #{Plantilla.all.count}\n"
  else
    print "Skipped seeding plantillas table.\n"
  end
end