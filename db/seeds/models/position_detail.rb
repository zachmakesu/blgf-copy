position_detail = 
  {
              :service_code_id => nil,
                :first_day_gov => nil,
             :first_day_agency => nil,
        :mode_of_separation_id => nil,
        :appointment_status_id => nil,
          :executive_office_id => nil,
                   :service_id => nil,
                  :division_id => nil,
                   :section_id => nil,
          :place_of_assignment => nil,
             :personnel_action => nil,
            :dependents_number => 0,
             :category_service => "career",
             :employment_basis => "full_time",
                :tax_status    => "S / ME",
             :payroll_group_id => nil,
                  :include_dtr => false,
           :include_in_payroll => false,
         :attendance_scheme_id => 1,
            :hazard_pay_factor => 0.0,
   :health_insurance_exception => false,
                 :plantilla_id => 1,
                  :step_number => 1
                
  }

ActiveRecord::Base.transaction do
  user = User.first
  if user.present?
    if user
      print '✓'
    else
      puts user.errors.full_messages.join(', ')
      break
    end
  
    print "\nTotal : #{PositionCode.all.count}\n"
  else
    print "Skipped seeding position_code table.\n"
  end
end
