position_code = [
  {
    position_code: "LAOO III",
    position_description: "Local Assessment Operations Officer III",
    position_abbreviation: nil,
    educational_requirement: "Bachelor's degree",
    experience_requirement: "2 years of relevant experience",
    eligibility_requirement: "Career Service (Professional) Second Level Eligibility",
    training_requirement: "8 hours of relevant training",
    position_classification: "Administrative"
  }
]

ActiveRecord::Base.transaction do
  if PositionCode.all.empty?
    position_code.each do |position|
      new_position = PositionCode.new(position)
      if new_position.save
        print '✓'
      else
        puts new_position.errors.inspect
        break
      end
    end
    print "\nTotal : #{PositionCode.all.count}\n"
  else
    print "Skipped seeding position_code table.\n"
  end
end
