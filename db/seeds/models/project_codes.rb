project_codes = [
  {
    project_code: "1",
    project_description: "A.01.a.01 (Office of the Director)",
    project_order: 1
  },
  {
    project_code: "2",
    project_description: "A.01.a.01 (Management and Technical Staff)",
    project_order: 2
  },
  {
    project_code: "3",
    project_description: "A.02.a",
    project_order: 3
  },
  {
    project_code: "4",
    project_description: "A.01.a.01",
    project_order: 4
  },
  {
    project_code: "5",
    project_description: "A.02.b.01",
    project_order: 5
  },
  {
    project_code: "6",
    project_description: "A.02.b.02",
    project_order: 6
  },
  {
    project_code: "7",
    project_description: "A.02.b.03",
    project_order: 7
  },
  {
    project_code: "8",
    project_description: "A.03.a.01",
    project_order: 8
  },
  {
    project_code: "9",
    project_description: "A.03.a.03",
    project_order: 9
  },
  {
    project_code: "10",
    project_description: "A.03.a.04",
    project_order: 10
  },
  {
    project_code: "11",
    project_description: "A.03.a.05",
    project_order: 11
  },
  {
    project_code: "12",
    project_description: "A.03.a.06",
    project_order: 12
  },
  {
    project_code: "13",
    project_description: "A.03.a.07",
    project_order: 13
  },
  {
    project_code: "14",
    project_description: "A.03.a.08",
    project_order: 14
  },
  {
    project_code: "15",
    project_description: "A.03.a.09",
    project_order: 15
  },
  {
    project_code: "16",
    project_description: "A.03.a.10",
    project_order: 16
  },
  {
    project_code: "17",
    project_description: "A.03.a.11",
    project_order: 17
  },
  {
    project_code: "18",
    project_description: "A.03.a.12",
    project_order: 18
  },
  {
    project_code: "19",
    project_description: "A.03.a.13",
    project_order: 19
  },
  {
    project_code: "20",
    project_description: "A.03.a.02",
    project_order: 20
  },
  {
    project_code: "21",
    project_description: "A.03.a.14",
    project_order: 21
  },
  {
    project_code: "22",
    project_description: "Job Order",
    project_order: 22
  },
  {
    project_code: "23",
    project_description: "LAMP",
    project_order: 23
  }
]

ActiveRecord::Base.transaction do
  if ProjectCode.all.empty?
    project_codes.each do |project|
      new_project = ProjectCode.new(project)
      if new_project.save
        print '✓'
      else
        puts new_project.errors.inspect
        break
      end
    end
    print "\nTotal : #{ProjectCode.all.count}\n"
  else
    print "Skipped seeding project_codes table.\n"
  end
end
