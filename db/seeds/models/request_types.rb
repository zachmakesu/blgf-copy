request_types = [
  {
    type_of_request: "Leave",
    applicants: 0,
    first_signatory_id: nil,
    second_signatory_id: nil,
    third_signatory_id: nil,
    fourth_signatory_id: nil
  },
  {
    type_of_request: "Official Business",
    applicants: 0,
    first_signatory_id: nil,
    second_signatory_id: nil,
    third_signatory_id: nil,
    fourth_signatory_id: nil
  },
  {
    type_of_request: "Update PDS",
    applicants: 0,
    first_signatory_id: nil,
    second_signatory_id: nil,
    third_signatory_id: nil,
    fourth_signatory_id: nil
  }
]

ActiveRecord::Base.transaction do
  if RequestType.all.empty?
    request_types.each do |request|
      new_request = RequestType.new(request)
      if new_request.save
        print '✓'
      else
        puts new_request.errors.inspect
        break
      end
    end
    print "\nTotal : #{RequestType.all.count}\n"
  else
    print "Skipped seeding request_types table.\n"
  end
end
