def seed_csv(file)
  File.open(File.join(Rails.root, "/db/seeds/csv/#{file}"))
end

require 'csv' 

ActiveRecord::Base.transaction do
  if SalarySchedule.all.empty?
    CSV.foreach(seed_csv("salary_grade_and_increment.csv"), headers: true) do |sg|
      new_sg = SalarySchedule.new(sg.to_hash)
      if new_sg.save
        print '✓'
      else
        puts new_sg.errors.inspect
        break
      end
    end
    print "\nTotal : #{SalarySchedule.all.count}\n"
  else
    print "Skipped seeding salary_schedules table.\n"
  end
end
