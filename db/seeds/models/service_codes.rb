service_codes = [
  {
    service_code: "GOV",
    service_description: "Government",
  },
  {
    service_code: "PRIV",
    service_description: "Private",
  },
  {
    service_code: "SPRIV",
    service_description: "Semi-Private",
  },
  {
    service_code: "LGU",
    service_description: "Local Government Unit",
  },
  {
    service_code: "GOCC",
    service_description: "Government Owned and Controlled Corporation",
  },
  {
    service_code: "SGOV",
    service_description: "Semi-Government",
  },
  {
    service_code: "GPRJ",
    service_description: "Government Project",
  },
]

ActiveRecord::Base.transaction do
  if ServiceCode.all.empty?
    service_codes.each do |service|
      new_service = ServiceCode.new(service)
      if new_service.save
        print '✓'
      else
        puts new_service.errors.inspect
        break
      end
    end
    print "\nTotal : #{ServiceCode.all.count}\n"
  else
    print "Skipped seeding service_codes table.\n"
  end
end
