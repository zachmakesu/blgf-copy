tax_ranges = [
  {
    tax_base: 0,
    tax_deduction: 0,
    factor: 0.05,
    taxable_from: 0,
    taxable_to: 10000
  },
  {
    tax_base: 10000,
    tax_deduction: 500,
    factor: 0.1,
    taxable_from: 10000,
    taxable_to: 30000
  },
  {
    tax_base: 30000,
    tax_deduction: 2500,
    factor: 0.15,
    taxable_from: 30000,
    taxable_to: 70000
  },
  {
    tax_base: 70000,
    tax_deduction: 8500,
    factor: 0.20,
    taxable_from: 70000,
    taxable_to: 140000
  },
  {
    tax_base: 140000,
    tax_deduction: 22500,
    factor: 0.25,
    taxable_from: 140000,
    taxable_to: 250000
  },
  {
    tax_base: 250000,
    tax_deduction: 50000,
    factor: 0.30,
    taxable_from: 250000,
    taxable_to: 500000
  },
  {
    tax_base: 500000,
    tax_deduction: 125000,
    factor: 0.32,
    taxable_from: 500000,
    taxable_to: 2000000
  }
]

ActiveRecord::Base.transaction do
  if TaxRange.all.empty?
    tax_ranges.each do |range|
      new_range = TaxRange.new(range)
      if new_range.save
        print '✓'
      else
        puts new_range.errors.inspect
        break
      end
    end
    print "\nTotal : #{TaxRange.all.count}\n"
  else
    print "Skipped seeding tax_ranges table.\n"
  end
end
