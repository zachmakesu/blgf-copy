tax_statuses = [
  {
    tax_status: "Single",
    exemption_amount: 50000
  },
  {
    tax_status: "Head",
    exemption_amount: 50000
  },
  {
    tax_status: "Married",
    exemption_amount: 50000
  }
]

ActiveRecord::Base.transaction do
  if TaxStatus.all.empty?
    tax_statuses.each do |status|
      new_status = TaxStatus.new(status)
      if new_status.save
        print '✓'
      else
        puts new_status.errors.inspect
        break
      end
    end
    print "\nTotal : #{TaxStatus.all.count}\n"
  else
    print "Skipped seeding tax_statuses table.\n"
  end
end
