users = [
  {
    role: 1,
    first_name: "Jenny",
    last_name: "Gaylican",
    middle_name: "Swain",
    middle_initial: "S",
    birthdate: "1969-12-03",
    gender: 1,
    civil_status: 1,
    citizenship: "Filipino",
    height: 1.40,
    weight: 60,
    blood_type: "O",
    email: "jentisay03@yahoo.com",
    employee_id: "C0001",
    gsis_policy_number: "69120900013",
    tin_number: "120-840-373",
    pagibig_id_number: "0005 018735 10",
    philhealth_number: "19-000-151571-",
    sss_number: "04-2030-4020",
    password: "password123",
    spouse_name: "Dan Allan P. Gaylican",
    spouse_occupation: "Building Administration Manager",
    spouse_employer_or_business_name: "SMX Convention Specialist Corp.",
    spouse_work_or_business_address: "Seaside Blvd., SM Mall of Asia, Macapagal Avenue, Pasay City",
    spouse_contact_number: "556-29-67",
    parent_fathers_name: "James Swain Jr.",
    parent_mothers_maiden_name: "Luzviminda Swain",
    parent_address: "Mandaluyong City",
    biometric_id: "6043",
    avatar: 'https://i.imgur.com/EZVlugw.jpg'
  },
  {
    role: 2,
    first_name: "Hazel",
    last_name: "Gampay",
    middle_name: "Lecta",
    middle_initial: "L",
    birthdate: "1971-08-04",
    gender: 1,
    civil_status: 1,
    citizenship: "Filipino",
    height: 1.54,
    weight: 50,
    blood_type: "O",
    email: "hgampay@yahoo.com.ph",
    employee_id: "C0002",
    gsis_policy_number: "71040800845",
    tin_number: "191-637-164",
    pagibig_id_number: "1050-0238-1021",
    philhealth_number: "11-000028868-3",
    sss_number: "07-2006977-4",
    password: "password123",
    spouse_name: "Raymundo G. Gampay",
    spouse_occupation: "CFO",
    spouse_employer_or_business_name: "Tara Trading/BEAMKO Shipmgt",
    spouse_work_or_business_address: "Malate, Manila",
    spouse_contact_number: nil,
    parent_fathers_name: "Romulo G. Lecta",
    parent_mothers_maiden_name: "Thelma R. Lecta",
    parent_address: "19 BLISS Project San Miguel, Jordan Guimaras",
    biometric_id: "6104",
    avatar: 'https://i.imgur.com/nh3FDXk.jpg'
  },
  {
    role: 0,
    first_name: "Dulce Cresencia",
    last_name: "Sanchez",
    middle_name: "Daleon",
    middle_initial: "D",
    birthdate: "1967-15-06",
    gender: 1,
    civil_status: 1,
    citizenship: "Filipino",
    height: 1.64,
    weight: 55,
    blood_type: "A",
    email: "dlcsnchz@yahoo.com",
    employee_id: "C0003",
    gsis_policy_number: "67061500534",
    tin_number: "120-838-723",
    pagibig_id_number: "105002377169",
    philhealth_number: "19-000151623-3",
    sss_number: nil,
    password: "password123",
    spouse_name: "Rodolfo V. Sanchez",
    biometric_id: "6084",
    avatar: ''
  }
]


ActiveRecord::Base.transaction do
  if User.all.empty?
    users.each do |user|
      new_user = User.new
      new_user.role                  = user[:role]
      new_user.first_name            = user[:first_name]
      new_user.last_name             = user[:last_name]
      new_user.middle_name           = user[:middle_name]
      new_user.gender                = user[:gender]
      new_user.civil_status          = user[:civil_status]
      new_user.citizenship           = user[:citizenship]
      new_user.height                = user[:height]
      new_user.weight                = user[:weight]
      new_user.blood_type            = user[:blood_type]
      new_user.email                 = user[:email]
      new_user.employee_id           = user[:employee_id]
      new_user.gsis_policy_number    = user[:gsis_policy_number]
      new_user.tin_number            = user[:tin_number]
      new_user.pagibig_id_number     = user[:pagibig_id_number]
      new_user.philhealth_number     = user[:philhealth_number]
      new_user.sss_number            = user[:sss_number]
      new_user.password              = user[:password]
      new_user.spouse_name           = user[:spouse_name]
      new_user.biometric_id          = user[:biometric_id]

      if new_user.save
        new_user.photos.create(image: URI.parse(user[:avatar]), position: 0, default_type: 0) if user[:avatar].present?
        print '✓'
      else
        puts new_user.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.all.count}\n"
  else
    print "Skipped seeding user table.\n"
  end
end
