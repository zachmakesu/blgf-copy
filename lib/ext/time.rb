class Time
  def is_weekday?
    (1..5).include?(wday)
  end
end