require 'rails_helper'

describe API::V1::Notifications do

  let(:admin){FactoryGirl.create(:user)}
  let(:dummy_user){FactoryGirl.create(:user)}

  before :each do
    @headers = {
      'HTTP_AUTHORIZATION' => "#{http_login(admin)}"
    }
    current_user = FactoryGirl.create(:api_key, user: admin)
    @token = "ThisIsAValidAPITokenOfValid32<Len"
  end

  context "Notifications" do
    it "should response success" do
      get "/api/v1/notifications", {}, @env

      expect(response).to be_success
    end

  end

end