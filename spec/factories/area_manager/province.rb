FactoryGirl.define do
  sequence :geocode do |n|
    "geocode_#{n}"
  end

  factory :province do
    name "First Name"
    geocode
  end
end