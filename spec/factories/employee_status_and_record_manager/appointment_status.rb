FactoryGirl.define do
  factory :appointment_status do
    appointment_code "Test Code"
    appointment_status "Test status"
  end
end
