FactoryGirl.define do
  factory :course do
    course_code "Test Code"
    course_description "Test Description"
  end
end