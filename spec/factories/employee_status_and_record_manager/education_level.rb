FactoryGirl.define do
  factory :education_level do
    level_code "Test Code"
    level_description "Test Description"
  end
end