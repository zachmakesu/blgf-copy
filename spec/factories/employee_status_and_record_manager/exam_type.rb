FactoryGirl.define do
  factory :exam_type do
    exam_code "Test Code"
    exam_description "Test Description"
  end
end
