FactoryGirl.define do
  factory :plantilla do
    area_code   "Sample Area code"
    area_type   "Sample Area type"
    csc_eligibility true
    item_number "Sample Item Numbner"
    plantilla_group_id  1
    position_code_id  1
    rationalized  true
    salary_schedule_grade 1
  end
end