FactoryGirl.define do
  factory :plantilla_group do
    group_code "Test Code"
    group_name "Test Name"
    group_order 1
  end
end