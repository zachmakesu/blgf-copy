FactoryGirl.define do
  factory :position_code do
    position_code "Test Code"
    position_description "Test Description"
  end
end
