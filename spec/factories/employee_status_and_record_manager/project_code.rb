FactoryGirl.define do
  factory :project_code do
    project_code "Test Code"
    project_description "Test Description"
    project_order 1
  end
end