FactoryGirl.define do
  sequence(:case_number) { |n| "#{n + 1}" }
  factory :legal_case do
    user_id          FFaker::PhoneNumber.area_code
    case_number      
    charged_offenses FFaker::Lorem.phrases
    status           FFaker::Lorem.word
    title            FFaker::Lorem.phrase
  end
end
