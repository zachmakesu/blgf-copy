FactoryGirl.define do
  factory :attendance_scheme do
    scheme_code "Scheme Code"
    scheme_name "Scheme Name"
    am_time_in_from "07:00:00"
    am_time_in_to "08:00:00"
    pm_time_out_from "16:00:00"
    pm_time_out_to "17:00:00"
    nn_time_out_from "12:00:00"
    nn_time_out_to "13:00:00"
    nn_time_in_from "12:00:00"
    nn_time_in_to "13:00:00"
  end
end