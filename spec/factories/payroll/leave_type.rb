FactoryGirl.define do
  factory :leave_type do
    leave_code "SL"
    leave_type "Sick Leave"
    no_of_days 15
    parent  true
  end
end