FactoryGirl.define do
  factory :payroll_group do
    project_code_id 1
    payroll_group_code "Test Code"
    payroll_group_name "Test Name"
    payroll_group_order 1
  end
end