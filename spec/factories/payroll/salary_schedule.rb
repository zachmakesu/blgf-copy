FactoryGirl.define do
  factory :salary_schedule do
    salary nil
    salary_grade 1
    step_1 9478.00
    step_2 nil
    step_3 nil
    step_4 nil
    step_5 nil
    step_6 nil
    step_7 nil
    step_8 nil
    step_9 nil
    step_10 nil
  end
end