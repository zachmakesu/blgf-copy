FactoryGirl.define do
  sequence :email do |n|
    "email#{n}@factory.com"
  end
  sequence :employee_id do |n|
    "C#{'%04d' % (User.count+1)}"
  end
  factory :user do
    email
    password "password123"
    first_name "Sample"
    last_name "Sample"
    gender "female"
    role "hr"
    employee_id

    after :create do |user|
      user.educations.create([
        {
          education_level_id: 4,
          school_name: FFaker::Education.school_name,
          course_id: 1,
          highest_grade_level_units_earned: nil,
          date_of_attendance_from: 10.years.ago,
          date_of_attendance_to: 4.years.ago,
          honors_received: "1st honors",
          year_graduated: 10.years.ago.year,
        },
        {
          education_level_id: 5,
          school_name: FFaker::Education.school_name,
          course_id: 1,
          highest_grade_level_units_earned: nil,
          date_of_attendance_from: 10.years.ago,
          date_of_attendance_to: 4.years.ago,
          honors_received: "1st honors",
          year_graduated: 10.years.ago.year,
        }
      ])
    end
  end

  factory :treasurer, class: "User" do
    email
    password "password123"
    first_name "Sample"
    last_name "Sample"
    gender "female"
    role "treasurer"
    employee_id

    after :create do |user|
      user.educations.create([
        {
          education_level_id: 4,
          school_name: FFaker::Education.school_name,
          course_id: 1,
          highest_grade_level_units_earned: nil,
          date_of_attendance_from: 10.years.ago,
          date_of_attendance_to: 4.years.ago,
          honors_received: "1st honors",
          year_graduated: 10.years.ago.year,
        },
        {
          education_level_id: 5,
          school_name: FFaker::Education.school_name,
          course_id: 1,
          highest_grade_level_units_earned: nil,
          date_of_attendance_from: 10.years.ago,
          date_of_attendance_to: 4.years.ago,
          honors_received: "1st honors",
          year_graduated: 10.years.ago.year,
        }
      ])
    end
  end
end
