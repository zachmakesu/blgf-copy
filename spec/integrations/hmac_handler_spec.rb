require 'rails_helper'

RSpec.describe HmacHandler, type: :service do


  it 'create hmac object' do
    @hmac = HmacHandler.new('endpoint', {'hello' => 'world'})

    expect(@hmac.endpoint).to be_truthy, 'should have .endpoint'
    expect(@hmac.params).to be_truthy, 'should have .endpoint'
  end

  it 'sign initialized hmac object' do
    @hmac = HmacHandler.new('endpoint', {'hello' => 'world'})
    expect(@hmac.digest).to be_truthy, 'should have .endpoint'
  end

  it 'generate signature from endpoint, params and secret' do
    expect(HmacHandler.signature_from('/test/endpoint', {})).to be_truthy
  end


  it 'securely compare signatures' do
    @hmac = HmacHandler.new('endpoint', {'hello' => 'world'})
    sig = HmacHandler.signature_from('endpoint', { 'hello' => 'world' })

    expect(@hmac.digest).to eq(sig), '.digest should equal #signature_from'
    expect(HmacHandler.secure_compare(@hmac.digest, sig)).to be_truthy, '#secure_compare should return secure comparison between signatures'
  end

  it 'compare signatures for different versions of api' do
    sig_v1 = HmacHandler.signature_from('endpoint', { 'hello' => 'world' })
    sig_v2 = HmacHandler.signature_from('endpoint', { 'hello' => 'world' }, 2) 

    expect(sig_v1).not_to eq(sig_v2), 'signature should not be equal for different api versions'
  end
end
