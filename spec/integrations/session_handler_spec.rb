require "rails_helper"

describe SessionHandler do
  let!(:user) { create(:user) }
  let!(:valid_params) { {employee_id: user.employee_id, password: user.password } }
  let!(:invalid_params) { {employee_id: user.employee_id, password: nil } }

  describe :login do

    context "with valid credentials" do
      it "return a user token and employee_id" do
        handler = SessionHandler.new(valid_params).login
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid credentials" do
      it "does not return a user token and employee_id" do
        handler = SessionHandler.new(invalid_params).login
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe :logout do
    before(:each) do
      @login_handler = SessionHandler.new(valid_params).logout
    end
    context "with valid credentials" do
      it "return success" do
        handler = SessionHandler.new({access_token: @login_handler.response[:details][:access_token]}).logout
        expect(handler.response[:success]).to be_truthy
      end
    end
  end
end
