require "rails_helper"

describe AppointmentStatusHandler do
  let!(:current_user) { create(:user) }

  let!(:valid_create_params) { {appointment_code: "Create Test Code", appointment_status: "Create Test Status" } }
  let!(:missing_create_params) { {appointment_status: "Create Test Status" } }

  let!(:appointment_status) { create(:appointment_status) }
  let!(:valid_update_params) { {id: appointment_status.id, appointment_code: "Update Test Code", appointment_status: "Update Test Status" } }
  let!(:missing_update_params) { {id: appointment_status.id, appointment_status: "Update Test Status" } }

  describe :create_appointment_status do
    context "with valid params" do
      it "returns created appointment_status" do
        handler = AppointmentStatusHandler.new(valid_create_params, current_user).create_appointment_status
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].appointment_status).to eq(valid_create_params[:appointment_status])
      end

      it "returns error message if appointment_status already exist" do
        AppointmentStatusHandler.new(valid_create_params, current_user).create_appointment_status

        handler = AppointmentStatusHandler.new(valid_create_params, current_user).create_appointment_status
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Appointment code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = AppointmentStatusHandler.new(missing_create_params, current_user).create_appointment_status
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Appointment code can't be blank")
      end
    end
  end

  describe :update_appointment_status do
    context "with valid params" do
      it "returns updated appointment_status" do
        handler = AppointmentStatusHandler.new(valid_update_params, current_user).update_appointment_status
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].appointment_status).to eq(valid_update_params[:appointment_status])
      end

      it "returns error message if appointment_status already exist" do
        AppointmentStatusHandler.new(valid_update_params, current_user).create_appointment_status
        
        handler = AppointmentStatusHandler.new(valid_update_params, current_user).update_appointment_status
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Appointment code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = AppointmentStatusHandler.new(missing_update_params, current_user).update_appointment_status
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Appointment code can't be blank")
      end
    end
  end

  describe :delete_appointment_status do
    context "with valid params" do
      it "deletes valid appointment_status id" do
        handler = AppointmentStatusHandler.new({id: appointment_status.id}, current_user).delete_appointment_status
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
