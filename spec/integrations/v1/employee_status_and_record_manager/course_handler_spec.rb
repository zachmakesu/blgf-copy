require "rails_helper"

describe CourseHandler do
  let!(:current_user) { create(:user) }

  let!(:valid_create_params) { {course_code: "Create Test Code", course_description: "Create Test Description" } }
  let!(:missing_create_params) { {course_description: "Create Test Description" } }

  let!(:course) { create(:course) }
  let!(:valid_update_params) { {id: course.id, course_code: "Update Test Code", course_description: "Update Test Description" } }
  let!(:missing_update_params) { {id: course.id, course_description: "Update Test Description" } }

  describe :create_course do
    context "with valid params" do
      it "returns created course" do
        handler = CourseHandler.new(valid_create_params, current_user).create_course
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].course_description).to eq(valid_create_params[:course_description])
      end

      it "returns error message if course code already exist" do
        CourseHandler.new(valid_create_params, current_user).create_course

        handler = CourseHandler.new(valid_create_params, current_user).create_course
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Course code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = CourseHandler.new(missing_create_params, current_user).create_course
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Course code can't be blank")
      end
    end
  end

  describe :update_course do
    context "with valid params" do
      it "returns updated course" do
        handler = CourseHandler.new(valid_update_params, current_user).update_course
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].course_description).to eq(valid_update_params[:course_description])
      end

      it "returns error message if course code already exist" do
        CourseHandler.new(valid_update_params, current_user).create_course
        
        handler = CourseHandler.new(valid_update_params, current_user).update_course
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Course code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = CourseHandler.new(missing_update_params, current_user).update_course
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Course code can't be blank")
      end
    end
  end

  describe :delete_course do
    context "with valid params" do
      it "deletes valid course id" do
        handler = CourseHandler.new({id: course.id}, current_user).delete_course
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
