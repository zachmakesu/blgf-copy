require "rails_helper"

describe DutyAndResponsibilityHandler do
  let!(:current_user) { create(:user) }
  
  let!(:position_code) { create(:position_code) }

  let!(:valid_create_params) { {position_code_id: position_code.id, percent_work: 5, duties: "Test Duties"} }
  let!(:missing_create_params) { {position_code_id: position_code.id, percent_work: 5 } }

  let!(:duty_and_responsibility) { DutyAndResponsibility.create(position_code_id: position_code.id, percent_work: 5, duties: "Test Duties") }
  let!(:valid_update_params) { {id: duty_and_responsibility.id, position_code_id: position_code.id, percent_work: 5, duties: "Update Test Duties" } }
  let!(:missing_update_params) { {id: duty_and_responsibility.id, position_code_id: position_code.id, percent_work: 5 } }

  describe :create_duty_and_responsibility do
    context "with valid params" do
      it "returns created duty_and_responsibility" do
        handler = DutyAndResponsibilityHandler.new(valid_create_params, current_user).create_duty_and_responsibility
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].duties).to eq(valid_create_params[:duties])
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = DutyAndResponsibilityHandler.new(missing_create_params, current_user).create_duty_and_responsibility
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Duties can't be blank")
      end
    end
  end

  describe :update_duty_and_responsibility do
    context "with valid params" do
      it "returns updated duty_and_responsibility" do
        handler = DutyAndResponsibilityHandler.new(valid_update_params, current_user).update_duty_and_responsibility
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].duties).to eq(valid_update_params[:duties])
      end
    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = DutyAndResponsibilityHandler.new(missing_update_params, current_user).update_duty_and_responsibility
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Duties can't be blank")
      end
    end
  end

  describe :delete_duty_and_responsibility do
    context "with valid params" do
      it "deletes valid duty_and_responsibility id" do
        handler = DutyAndResponsibilityHandler.new({id: duty_and_responsibility.id}, current_user).delete_duty_and_responsibility
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
