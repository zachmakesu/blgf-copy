require "rails_helper"

describe EducationLevelHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { {level_code: "Create Test Code", level_description: "Create Test Description" } }
  let!(:missing_create_params) { {level_description: "Create Test Description" } }

  let!(:education_level) { create(:education_level) }
  let!(:valid_update_params) { {id: education_level.id, level_code: "Update Test Code", level_description: "Update Test Description" } }
  let!(:missing_update_params) { {id: education_level.id, level_description: "Update Test Description" } }

  describe :create_education_level do
    context "with valid params" do
      it "returns created education_level" do
        handler = EducationLevelHandler.new(valid_create_params, current_user).create_education_level
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].level_description).to eq(valid_create_params[:level_description])
      end

      it "returns error message if level code already exist" do
        EducationLevelHandler.new(valid_create_params, current_user).create_education_level

        handler = EducationLevelHandler.new(valid_create_params, current_user).create_education_level
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Level code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = EducationLevelHandler.new(missing_create_params, current_user).create_education_level
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Level code can't be blank")
      end
    end
  end

  describe :update_education_level do
    context "with valid params" do
      it "returns updated education_level" do
        handler = EducationLevelHandler.new(valid_update_params, current_user).update_education_level
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].level_description).to eq(valid_update_params[:level_description])
      end

      it "returns error message if level code already exist" do
        EducationLevelHandler.new(valid_update_params, current_user).create_education_level
        
        handler = EducationLevelHandler.new(valid_update_params, current_user).update_education_level
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Level code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = EducationLevelHandler.new(missing_update_params, current_user).update_education_level
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Level code can't be blank")
      end
    end
  end

  describe :delete_education_level do
    context "with valid params" do
      it "deletes valid education_level id" do
        handler = EducationLevelHandler.new({id: education_level.id}, current_user).delete_education_level
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
