require "rails_helper"

describe ExamTypeHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { {exam_code: "Create Test Code", exam_description: "Create Test Description" } }
  let!(:missing_create_params) { {exam_description: "Create Test Description" } }

  let!(:exam_type) { create(:exam_type) }
  let!(:valid_update_params) { {id: exam_type.id, exam_code: "Update Test Code", exam_description: "Update Test Description" } }
  let!(:missing_update_params) { {id: exam_type.id, exam_description: "Update Test Description" } }

  describe :create_exam_type do
    context "with valid params" do
      it "returns created exam_type" do
        handler = ExamTypeHandler.new(valid_create_params, current_user).create_exam_type
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].exam_description).to eq(valid_create_params[:exam_description])
      end

      it "returns error message if exam_code already exist" do
        ExamTypeHandler.new(valid_create_params, current_user).create_exam_type

        handler = ExamTypeHandler.new(valid_create_params, current_user).create_exam_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Exam code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ExamTypeHandler.new(missing_create_params, current_user).create_exam_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Exam code can't be blank")
      end
    end
  end

  describe :update_exam_type do
    context "with valid params" do
      it "returns updated exam_type" do
        handler = ExamTypeHandler.new(valid_update_params, current_user).update_exam_type
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].exam_description).to eq(valid_update_params[:exam_description])
      end

      it "returns error message if exam_code already exist" do
        ExamTypeHandler.new(valid_update_params, current_user).create_exam_type
        
        handler = ExamTypeHandler.new(valid_update_params, current_user).update_exam_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Exam code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ExamTypeHandler.new(missing_update_params, current_user).update_exam_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Exam code can't be blank")
      end
    end
  end

  describe :delete_exam_type do
    context "with valid params" do
      it "deletes valid exam_type id" do
        handler = ExamTypeHandler.new({id: exam_type.id}, current_user).delete_exam_type
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
