require "rails_helper"

describe ModeOfSeparationHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { { separation_mode: "Test Mode"} }
  
  let!(:mode_of_separation) { create(:mode_of_separation) }

  let!(:valid_update_params) { {id: mode_of_separation.id, separation_mode: "Update Test Mode" } }
  let!(:missing_update_params) { {id: mode_of_separation.id } }

  describe :create_mode_of_separation do
    context "with valid params" do
      it "returns created mode_of_separation" do
        handler = ModeOfSeparationHandler.new(valid_create_params, current_user).create_mode_of_separation
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].separation_mode).to eq(valid_create_params[:separation_mode])
      end

      it "returns error message if separation_mode already exist" do
        ModeOfSeparationHandler.new(valid_create_params, current_user).create_mode_of_separation

        handler = ModeOfSeparationHandler.new(valid_create_params, current_user).create_mode_of_separation
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Separation mode has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ModeOfSeparationHandler.new({}, current_user).create_mode_of_separation
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Separation mode can't be blank")
      end
    end
  end

  describe :update_mode_of_separation do
    context "with valid params" do
      it "returns updated mode_of_separation" do
        handler = ModeOfSeparationHandler.new(valid_update_params, current_user).update_mode_of_separation
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].separation_mode).to eq(valid_update_params[:separation_mode])
      end

      it "returns error message if separation mode already exist" do
        ModeOfSeparationHandler.new(valid_update_params, current_user).create_mode_of_separation
        
        handler = ModeOfSeparationHandler.new(valid_update_params, current_user).update_mode_of_separation
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Separation mode has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ModeOfSeparationHandler.new(missing_update_params, current_user).update_mode_of_separation
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Separation mode can't be blank")
      end
    end
  end

  describe :delete_mode_of_separation do
    context "with valid params" do
      it "deletes valid mode_of_separation id" do
        handler = ModeOfSeparationHandler.new({id: mode_of_separation.id}, current_user).delete_mode_of_separation
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
