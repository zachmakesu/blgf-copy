require "rails_helper"

describe PlantillaDutyHandler do
  let!(:current_user) { create(:user) }
  
  let!(:plantilla) { create(:plantilla) }
  let!(:valid_create_params) { {plantilla_id: plantilla.id, percent_work: 5, duties: "Test Duties"} }
  let!(:missing_create_params) { {plantilla_id: plantilla.id, percent_work: 5 } }

  let!(:plantilla_duty) { PlantillaDuty.create(plantilla_id: plantilla.id, percent_work: 5, duties: "Test Duties") }
  let!(:valid_update_params) { {id: plantilla_duty.id, plantilla_id: plantilla.id, percent_work: 5, duties: "Update Test Duties" } }
  let!(:missing_update_params) { {id: plantilla_duty.id, plantilla_id: plantilla.id, percent_work: 5 } }

  describe :create_plantilla_duty do
    context "with valid params" do
      it "returns created plantilla_duty" do
        handler = PlantillaDutyHandler.new(valid_create_params, current_user).create_plantilla_duty
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].duties).to eq(valid_create_params[:duties])
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = PlantillaDutyHandler.new(missing_create_params, current_user).create_plantilla_duty
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Duties can't be blank")
      end
    end
  end

  describe :update_plantilla_duty do
    context "with valid params" do
      it "returns updated plantilla_duty" do
        handler = PlantillaDutyHandler.new(valid_update_params, current_user).update_plantilla_duty
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].duties).to eq(valid_update_params[:duties])
      end
    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = PlantillaDutyHandler.new(missing_update_params, current_user).update_plantilla_duty
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Duties can't be blank")
      end
    end
  end

  describe :delete_plantilla_duty do
    context "with valid params" do
      it "deletes valid plantilla_duty id" do
        handler = PlantillaDutyHandler.new({id: plantilla_duty.id}, current_user).delete_plantilla_duty
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
