require "rails_helper"

describe PlantillaGroupHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { { group_code: "Create Test Code", group_name: "Create Test Name", group_order: 2 } }
  let!(:missing_create_params) { {group_name: "Create Test Name" } }

  let!(:plantilla_group) { create(:plantilla_group ) }
  let!(:valid_update_params) { { id: plantilla_group.id, group_code: "Update Test Code", group_name: "Update Test Name", group_order: 2 } }
  let!(:missing_update_params) { { id: plantilla_group.id, group_name: "Update Test Name" } }

  describe :create_plantilla_group do
    context "with valid params" do
      it "returns created plantilla_group" do
        handler = PlantillaGroupHandler.new(valid_create_params, current_user).create_plantilla_group
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].group_code).to eq(valid_create_params[:group_code])
      end

      it "returns error message if group code and group order already exist" do
        PlantillaGroupHandler.new(valid_create_params, current_user).create_plantilla_group

        handler = PlantillaGroupHandler.new(valid_create_params, current_user).create_plantilla_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Group code has already been taken, Group order has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = PlantillaGroupHandler.new(missing_create_params, current_user).create_plantilla_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Group code can't be blank, Group order can't be blank, Group order is not a number")
      end
    end
  end

  describe :update_plantilla_group do
    context "with valid params" do
      it "returns updated plantilla_group" do
        handler = PlantillaGroupHandler.new(valid_update_params, current_user).update_plantilla_group
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].group_code).to eq(valid_update_params[:group_code])
      end

      it "returns error message if group code and group order already exist" do
        PlantillaGroupHandler.new(valid_update_params, current_user).create_plantilla_group
        
        handler = PlantillaGroupHandler.new(valid_update_params, current_user).update_plantilla_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Group code has already been taken, Group order has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = PlantillaGroupHandler.new(missing_update_params, current_user).update_plantilla_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Group code can't be blank, Group order can't be blank, Group order is not a number")
      end
    end
  end

  describe :delete_plantilla_group do
    context "with valid params" do
      it "deletes valid plantilla_group id" do
        handler = PlantillaGroupHandler.new({id: plantilla_group.id}, current_user).delete_plantilla_group
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
