require "rails_helper"

describe PlantillaHandler do
  let!(:current_user) { create(:user) }
  
  let!(:plantilla_group) { create(:plantilla_group) }
  let!(:position_code) { create(:position_code) }
  let!(:salary_schedule) { create(:salary_schedule) }

  let!(:valid_create_params) { { item_number: "Test Item Number", plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade } }
  let!(:missing_create_params) { { plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade } }

  let!(:plantilla) { Plantilla.create({ item_number: "Create Item Number", plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade }) }

  let!(:valid_update_params) { { id: plantilla.id, item_number: "Update Test Item Number", plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade } }
  let!(:missing_update_params) { { id: plantilla.id, plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade } }

  describe :create_plantilla_group do
    context "with valid params" do
      it "returns created plantilla" do
        handler = PlantillaHandler.new(valid_create_params, current_user).create_plantilla
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].item_number).to eq(valid_create_params[:item_number])
      end

      it "returns error message if Item number already exist" do
        PlantillaHandler.new(valid_create_params, current_user).create_plantilla

        handler = PlantillaHandler.new(valid_create_params, current_user).create_plantilla
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Item number has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = PlantillaHandler.new(missing_create_params, current_user).create_plantilla
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Item number can't be blank")
      end
    end
  end

  describe :update_plantilla_group do
    context "with valid params" do
      it "returns updated plantilla_group" do
        handler = PlantillaHandler.new(valid_update_params, current_user).update_plantilla
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].item_number).to eq(valid_update_params[:item_number])
      end

      it "returns error message if Item number already exist" do
        PlantillaHandler.new(valid_update_params, current_user).create_plantilla
        
        handler = PlantillaHandler.new(valid_update_params, current_user).update_plantilla
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Item number has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = PlantillaHandler.new(missing_update_params, current_user).update_plantilla
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Item number can't be blank")
      end
    end
  end

  describe :delete_plantilla do
    context "with valid params" do
      it "deletes valid plantilla id" do
        handler = PlantillaHandler.new({id: plantilla.id}, current_user).delete_plantilla
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
