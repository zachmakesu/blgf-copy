require "rails_helper"

describe PositionCodeHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { {position_code: "Create Test Code", position_description: "Create Test Description" } }
  let!(:missing_create_params) { {position_code: "Create Test Code" } }

  let!(:position_code) { create(:position_code) }
  let!(:valid_update_params) { {id: position_code.id, position_code: "Update Test Code", position_description: "Update Test Description" } }
  let!(:missing_update_params) { {id: position_code.id, position_code: "Update Test Code" } }

  describe :create_position_code do
    context "with valid params" do
      it "returns created position_code" do
        handler = PositionCodeHandler.new(valid_create_params, current_user).create_position_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].position_code).to eq(valid_create_params[:position_code])
      end

      it "returns error message if position_code already exist" do
        PositionCodeHandler.new(valid_create_params, current_user).create_position_code

        handler = PositionCodeHandler.new(valid_create_params, current_user).create_position_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Position code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = PositionCodeHandler.new(missing_create_params, current_user).create_position_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Position description can't be blank")
      end
    end
  end

  describe :update_position_code do
    context "with valid params" do
      it "returns updated position_code" do
        handler = PositionCodeHandler.new(valid_update_params, current_user).update_position_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].position_code).to eq(valid_update_params[:position_code])
      end

      it "returns error message if position_code already exist" do
        PositionCodeHandler.new(valid_update_params, current_user).create_position_code
        
        handler = PositionCodeHandler.new(valid_update_params, current_user).update_position_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Position code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = PositionCodeHandler.new(missing_update_params, current_user).update_position_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Position description can't be blank")
      end
    end
  end

  describe :delete_position_code do
    context "with valid params" do
      it "deletes valid position_code id" do
        handler = PositionCodeHandler.new({id: position_code.id}, current_user).delete_position_code
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
