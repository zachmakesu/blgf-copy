require "rails_helper"

describe ProjectCodeHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { { project_code: "Create Test Code", project_description: "Create Test Description", project_order: 2 } }
  let!(:missing_create_params) { {project_description: "Create Test Description" } }

  let!(:project_code) { create(:project_code ) }
  let!(:valid_update_params) { { id: project_code.id, project_code: "Update Test Code", project_description: "Update Test Description", project_order: 2 } }
  let!(:missing_update_params) { { id: project_code.id, project_description: "Update Test Description" } }

  describe :create_project_code do
    context "with valid params" do
      it "returns created project_code" do
        handler = ProjectCodeHandler.new(valid_create_params, current_user).create_project_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].project_code).to eq(valid_create_params[:project_code])
      end

      it "returns error message if project code and project order already exist" do
        ProjectCodeHandler.new(valid_create_params, current_user).create_project_code

        handler = ProjectCodeHandler.new(valid_create_params, current_user).create_project_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Project code has already been taken, Project order has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ProjectCodeHandler.new(missing_create_params, current_user).create_project_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Project code can't be blank, Project order can't be blank, Project order is not a number")
      end
    end
  end

  describe :update_project_code do
    context "with valid params" do
      it "returns updated project_code" do
        handler = ProjectCodeHandler.new(valid_update_params, current_user).update_project_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].project_code).to eq(valid_update_params[:project_code])
      end

      it "returns error message if project code and project order already exist" do
        ProjectCodeHandler.new(valid_update_params, current_user).create_project_code
        
        handler = ProjectCodeHandler.new(valid_update_params, current_user).update_project_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Project code has already been taken, Project order has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ProjectCodeHandler.new(missing_update_params, current_user).update_project_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Project code can't be blank, Project order can't be blank, Project order is not a number")
      end
    end
  end

  describe :delete_project_code do
    context "with valid params" do
      it "deletes valid project_code id" do
        handler = ProjectCodeHandler.new({id: project_code.id}, current_user).delete_project_code
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
