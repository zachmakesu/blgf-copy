require "rails_helper"

describe ScholarshipHandler do
  let!(:current_user) { create(:user) }
  
  let!(:valid_create_params) { { description: "Test Description"} }
  
  let!(:scholarship) { create(:scholarship) }

  let!(:valid_update_params) { {id: scholarship.id, description: "Update Test Description" } }
  let!(:missing_update_params) { {id: scholarship.id } }

  describe :create_scholarship do
    context "with valid params" do
      it "returns created scholarship" do
        handler = ScholarshipHandler.new(valid_create_params, current_user).create_scholarship
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].description).to eq(valid_create_params[:description])
      end

      it "returns error message if description already exist" do
        ScholarshipHandler.new(valid_create_params, current_user).create_scholarship

        handler = ScholarshipHandler.new(valid_create_params, current_user).create_scholarship
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Description has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ScholarshipHandler.new({}, current_user).create_scholarship
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Description can't be blank")
      end
    end
  end

  describe :update_scholarship do
    context "with valid params" do
      it "returns updated scholarship" do
        handler = ScholarshipHandler.new(valid_update_params, current_user).update_scholarship
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].description).to eq(valid_update_params[:description])
      end

      it "returns error message if Description already exist" do
        ScholarshipHandler.new(valid_update_params, current_user).create_scholarship
        
        handler = ScholarshipHandler.new(valid_update_params, current_user).update_scholarship
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Description has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ScholarshipHandler.new(missing_update_params, current_user).update_scholarship
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Description can't be blank")
      end
    end
  end

  describe :delete_scholarship do
    context "with valid params" do
      it "deletes valid scholarship id" do
        handler = ScholarshipHandler.new({id: scholarship.id}, current_user).delete_scholarship
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
