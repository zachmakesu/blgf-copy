require "rails_helper"

describe ServiceCodeHandler do
  let!(:current_user) { create(:user) }
  let!(:valid_create_params) { {service_code: "Create Test Code", service_description: "Create Test Description" } }
  let!(:missing_create_params) { {service_description: "Create Test Description" } }

  let!(:service_code) { create(:service_code) }
  let!(:valid_update_params) { {id: service_code.id, service_code: "Update Test Code", service_description: "Update Test Description" } }
  let!(:missing_update_params) { {id: service_code.id, service_description: "Update Test Description" } }

  describe :create_service_code do
    context "with valid params" do
      it "returns created service_code" do
        handler = ServiceCodeHandler.new(valid_create_params, current_user).create_service_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].service_description).to eq(valid_create_params[:service_description])
      end

      it "returns error message if Service code already exist" do
        ServiceCodeHandler.new(valid_create_params, current_user).create_service_code

        handler = ServiceCodeHandler.new(valid_create_params, current_user).create_service_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Service code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ServiceCodeHandler.new(missing_create_params, current_user).create_service_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Service code can't be blank")
      end
    end
  end

  describe :update_service_code do
    context "with valid params" do
      it "returns updated service_code" do
        handler = ServiceCodeHandler.new(valid_update_params, current_user).update_service_code
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].service_description).to eq(valid_update_params[:service_description])
      end

      it "returns error message if Service code already exist" do
        ServiceCodeHandler.new(valid_update_params, current_user).create_service_code
        
        handler = ServiceCodeHandler.new(valid_update_params, current_user).update_service_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Service code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ServiceCodeHandler.new(missing_update_params, current_user).update_service_code
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Service code can't be blank")
      end
    end
  end

  describe :delete_service_code do
    context "with valid params" do
      it "deletes valid service_code id" do
        handler = ServiceCodeHandler.new({id: service_code.id}, current_user).delete_service_code
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
