require "rails_helper"

describe HrUpdate::EducationHandler do

  let!(:user)     { create(:user) }

  let!(:valid_employee_params) {
    {
      employee_id:        user.employee_id,
      education_level_id: 1,
      school_name: FFaker::CompanyIT.name,
      course_id: 1,
      highest_grade_level_units_earned: "80",
      date_of_attendance_from: "2014-11-01",
      date_of_attendance_to: "2016-11-01",
      honors_received: FFaker::Lorem.word,
      year_graduated: "2014",
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id,
    }
  }

  describe ".create" do
    context "with valid parameters for education" do
      it "should create assigned parameters" do
        handler = HrUpdate::EducationHandler.new(valid_employee_params, user).create
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for education" do
      it "should not create assigned parameters" do
        handler = HrUpdate::EducationHandler.new(invalid_employee_params, user).create
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe ".update" do
    context "with valid parameters for education" do
      it "should update assigned parameters" do
        HrUpdate::EducationHandler.new(valid_employee_params, user).create
        valid_employee_params[:id] = user.educations.first.id

        handler = HrUpdate::EducationHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for education" do
      it "should not update assigned parameters" do
        HrUpdate::EducationHandler.new(valid_employee_params, user).create
        invalid_employee_params[:id] = user.educations.first.id
        
        handler = HrUpdate::EducationHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
