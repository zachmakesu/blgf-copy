require "rails_helper"

describe HrUpdate::ExaminationHandler do

  let!(:user)     { create(:user) }
  let!(:roles) { [0,1] }

  let!(:valid_employee_params) {
    {
      employee_id:   user.employee_id,
      exam_description: "test",
      place_of_exam: FFaker::AddressUS.city,
      rating: "80",
      date_of_exam: DateTime.now,
      licence_number: FFaker::PhoneNumber.imei,
      date_of_release: DateTime.now
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id,
    }
  }

  describe ".create" do
    context "with valid parameters for examination" do
      it "should create assigned parameters" do
        handler = HrUpdate::ExaminationHandler.new(valid_employee_params, user).create
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for examination" do
      it "should not create assigned parameters" do
        handler = HrUpdate::ExaminationHandler.new(invalid_employee_params, user).create
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe ".update" do
    context "with valid parameters for examination" do
      it "should update assigned parameters" do
        HrUpdate::ExaminationHandler.new(valid_employee_params, user).create
        valid_employee_params[:id] = user.examinations.first.id

        handler = HrUpdate::ExaminationHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for examination" do
      it "should not update assigned parameters" do
        HrUpdate::ExaminationHandler.new(valid_employee_params, user).create
        invalid_employee_params[:id] = user.examinations.first.id
        
        handler = HrUpdate::ExaminationHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
