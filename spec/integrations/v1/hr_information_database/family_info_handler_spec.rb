require "rails_helper"

describe HrUpdate::FamilyInfoHandler do

  let!(:user)     { create(:user) }
  let!(:roles) { [0,1] }

  let!(:valid_employee_params) {
    {
      employee_id:        user.employee_id,

      spouse_name: FFaker::Name.first_name,
      spouse_occupation: FFaker::Job.title,
      spouse_employer_or_business_name: FFaker::Job.title,
      spouse_work_or_business_address: FFaker::AddressUS.city,
      spouse_contact_number: FFaker::PhoneNumber.short_phone_number,
      parent_fathers_name: FFaker::Name.first_name,
      parent_mothers_maiden_name: FFaker::Name.first_name,
      parent_address: FFaker::AddressUS.city,

      spouse_first_name: FFaker::Name.first_name,
      spouse_middle_name: FFaker::Name.first_name,
      spouse_last_name: FFaker::Name.first_name,
      parant_fathers_first_name: FFaker::Name.first_name,
      parant_fathers_middle_name: FFaker::Name.first_name,
      parant_fathers_last_name: FFaker::Name.first_name,
      parant_mothers_first_name: FFaker::Name.first_name,
      parant_mothers_middle_name: FFaker::Name.first_name,
      parant_mothers_last_name: FFaker::Name.first_name,

      first_names: [FFaker::Name.first_name, FFaker::Name.first_name], 
      last_names: [FFaker::Name.first_name, FFaker::Name.first_name],
      middle_names: [FFaker::Name.first_name, FFaker::Name.first_name],
      middle_initials: ["M", "M"],
      relationship: ["Son", "Son"],
      birthdates: [DateTime.now, DateTime.now]
    }
  }

  describe ".update" do
    context "with valid parameters for family info" do
      it "should update assigned parameters" do
        handler = HrUpdate::FamilyInfoHandler.new(valid_employee_params, roles).update
        expect(handler.response[:success]).to be_truthy
      end
    end
  end
end
