require "rails_helper"

describe LegalCasesHandler do 
  let!(:user)     { create(:user) }

  let!(:record){create(:legal_case)}

  let!(:valid_legal_case_params) do
    {
      user_id: user.id,
      case_number: FFaker::PhoneNumber.area_code,
      charged_offenses: FFaker::Lorem.phrases,
      status: FFaker::Lorem.word,
      title: FFaker::Lorem.phrase
    }
  end

  let!(:invalid_legal_case_params) do
    {
      user_id: user.id,
      charged_offenses: FFaker::Lorem.phrases,
      status: FFaker::Lorem.word,
      title: FFaker::Lorem.phrase
    }
  end

    context "when user creates his/her own legal cases" do
      it "should create legal case with valid parameters" do
        handler = LegalCasesHandler.new(valid_legal_case_params, user).create_legal_case
        expect(handler.response[:success]).to be_truthy
      end

      it "should not create legal case with invalid parameters" do
        handler = LegalCasesHandler.new(invalid_legal_case_params, user).create_legal_case
        expect(handler.response[:success]).to be_falsey
      end
    end

    context "when user update his/her own legal cases" do
      it "should update legal case with valid parameters" do
        valid_legal_case_params[:legal_case_id] = record.id
        handler = LegalCasesHandler.new(valid_legal_case_params, user).update_legal_case
        expect(handler.response[:success]).to be_truthy
      end

      it "should not update legal case with invalid parameters" do
        handler = LegalCasesHandler.new(invalid_legal_case_params, user).create_legal_case
        expect(handler.response[:success]).to be_falsey
      end
    end
  
    context "when user delete his/her own legal cases" do
      it "should delete user's legal case " do
        valid_legal_case_params[:legal_case_id] = record.id
        handler = LegalCasesHandler.new(valid_legal_case_params, user).delete_legal_case
        expect(handler.response[:success]).to be_truthy
      end
    end
end