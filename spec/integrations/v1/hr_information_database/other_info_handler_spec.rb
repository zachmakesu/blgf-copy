require "rails_helper"

describe HrUpdate::OtherInfoHandler do

  let!(:user)     { create(:user) }
  let!(:roles) { [0,1] }

  let!(:valid_employee_params) {
    {
      employee_id:        user.employee_id,
      skills_and_hobbies: FFaker::Sport.name,
      non_academic_destinction: FFaker::Sport.name,
      membership_in: FFaker::Company.bs,
      third_degree_national: false,
      give_national: nil,
      fourth_degree_local: false,
      give_local: nil,
      formally_charge: false,
      give_charge: nil,
      administrative_offense: false,
      give_offense: nil,
      any_violation: false,
      give_reasons: nil,
      canditate: false,
      give_date_particulars: nil,
      indigenous_member: false,
      give_group: nil,
      differently_abled: false,
      give_disability: nil,
      solo_parent: false,
      give_status: "single",
      is_separated: false,
      give_details: nil,
      signature: FFaker::Name.first_name,
      date_accomplished: DateTime.now,
      ctc_number: FFaker::PhoneNumber.short_phone_number,
      issued_at: DateTime.now,
      issued_on: DateTime.now
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id,
      skills_and_hobbies: FFaker::Sport.name,
      non_academic_destinction: FFaker::Sport.name,
      membership_in: FFaker::Company.bs,
      third_degree_national: true,
      give_national: nil
    }
  }


  describe ".update" do
    context "with valid parameters for profile data" do
      it "should update assigned parameters" do
        handler = HrUpdate::OtherInfoHandler.new(valid_employee_params, roles).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for profile data" do
      it "should not update assigned parameters" do
        handler = HrUpdate::OtherInfoHandler.new(invalid_employee_params, roles).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
