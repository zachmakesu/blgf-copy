require "rails_helper"

describe HrUpdate::ProfileDataHandler do

  let!(:user)     { create(:user) }

  let!(:valid_employee_params) {
    {
      employee_id:        user.employee_id,
      email:              FFaker::Internet.free_email,
      first_name:         FFaker::Name.first_name,
      last_name:          FFaker::Name.last_name,
      middle_name:        FFaker::Name.last_name,
      middle_initial:     "O",
      name_extension:     FFaker::NameSE.suffix,
      birthdate:          20.years.ago,
      gender:             "female",
      civil_status:       "married",
      citizenship:        "Filipino",
      height:             6.5,
      weight:             65.4,
      blood_type:         "O",
      tin_number:         FFaker::PhoneNumber.imei,
      gsis_policy_number: FFaker::PhoneNumber.imei,
      pagibig_id_number:  FFaker::PhoneNumber.imei,
      philhealth_number:  FFaker::PhoneNumber.imei,
      sss_number:         FFaker::PhoneNumber.imei,
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id,
      email:              "",
      first_name:         FFaker::Name.first_name,
      last_name:          FFaker::Name.last_name,
      middle_name:        FFaker::Name.last_name,
      middle_initial:     "O",
      name_extension:     FFaker::NameSE.suffix,
      birthdate:          20.years.ago,
      gender:             "female",
      civil_status:       "married",
      citizenship:        "Filipino"
    }
  }


  describe ".update" do
    context "with valid parameters for profile data" do
      it "should update assigned parameters" do
        handler = HrUpdate::ProfileDataHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for profile data" do
      it "should not update assigned parameters" do
        handler = HrUpdate::ProfileDataHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
