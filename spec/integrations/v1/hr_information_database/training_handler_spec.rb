require "rails_helper"

describe HrUpdate::TrainingHandler do

  let!(:user)     { create(:user) }

  let!(:valid_employee_params) {
    {
      employee_id: user.employee_id,
      title: FFaker::AddressUS,
      inclusive_date_from: "2014-11-01",
      inclusive_date_to: "2016-11-01",
      number_of_hours: 80,
      conducted_by: FFaker::AddressUS.city
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id: user.employee_id,
    }
  }

  describe ".create" do
    context "with valid parameters for examination" do
      it "should create assigned parameters" do
        handler = HrUpdate::TrainingHandler.new(valid_employee_params, user).create
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for examination" do
      it "should not create assigned parameters" do
        handler = HrUpdate::TrainingHandler.new(invalid_employee_params, user).create
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe ".update" do
    context "with valid parameters for examination" do
      it "should update assigned parameters" do
        HrUpdate::TrainingHandler.new(valid_employee_params, user).create
        valid_employee_params[:id] = user.trainings_and_seminars.first.id

        handler = HrUpdate::TrainingHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for examination" do
      it "should not update assigned parameters" do
        HrUpdate::TrainingHandler.new(valid_employee_params, user).create
        invalid_employee_params[:id] = user.trainings_and_seminars.first.id
        
        handler = HrUpdate::TrainingHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
