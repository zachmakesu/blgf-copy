require "rails_helper"

describe HrUpdate::VoluntaryWorkHandler do

  let!(:user)     { create(:user) }

  let!(:valid_employee_params) {
    {
      employee_id:   user.employee_id,
      name_of_organization: FFaker::AddressUS.city,
      address_of_organization: FFaker::AddressUS.city,
      inclusive_date_from: "2014-11-01",
      inclusive_date_to: "2016-11-01",
      number_of_hours: 200,
      position_nature_of_work: FFaker::Job.title
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id,
    }
  }

  describe ".create" do
    context "with valid parameters for voluntary work" do
      it "should create assigned parameters" do
        handler = HrUpdate::VoluntaryWorkHandler.new(valid_employee_params, user).create
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for voluntary work" do
      it "should not create assigned parameters" do
        handler = HrUpdate::VoluntaryWorkHandler.new(invalid_employee_params, user).create
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe ".update" do
    context "with valid parameters for voluntary work" do
      it "should update assigned parameters" do
        HrUpdate::VoluntaryWorkHandler.new(valid_employee_params, user).create
        valid_employee_params[:id] = user.voluntary_works.first.id

        handler = HrUpdate::VoluntaryWorkHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for voluntary work" do
      it "should not update assigned parameters" do
        HrUpdate::VoluntaryWorkHandler.new(valid_employee_params, user).create
        invalid_employee_params[:id] = user.voluntary_works.first.id
        
        handler = HrUpdate::VoluntaryWorkHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
