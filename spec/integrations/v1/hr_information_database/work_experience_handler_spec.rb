require "rails_helper"

describe HrUpdate::WorkExperienceHandler do

  let!(:user)     { create(:user) }

  let!(:valid_employee_params) {
    {
      employee_id:  user.employee_id,
      position_title: FFaker::Job.title,
      inclusive_date_from: "2014-11-01",
      inclusive_date_to: "2016-11-01",
      department_agency_office: FFaker::AddressUS.city,
      monthly_salary: "12312",
      salary_grade_and_step: "09-1",
      appointment_status_id: 1,
      government_service: true
    }
  }

  let!(:invalid_employee_params) {
    {
      employee_id:        user.employee_id
    }
  }

  describe ".create" do
    context "with valid parameters for work_experience" do
      it "should create assigned parameters" do
        handler = HrUpdate::WorkExperienceHandler.new(valid_employee_params, user).create
        expect(handler.response[:success]).to be_truthy
      end
    end
    
    context "with invalid parameters for work_experience" do
      it "should not create assigned parameters" do
        handler = HrUpdate::WorkExperienceHandler.new(invalid_employee_params, user).create
        expect(handler.response[:success]).to be_falsey
      end
    end
  end

  describe ".update" do
    context "with valid parameters for work_experience" do
      it "should update assigned parameters" do
        HrUpdate::WorkExperienceHandler.new(valid_employee_params, user).create
        valid_employee_params[:id] = user.work_experiences.first.id

        handler = HrUpdate::WorkExperienceHandler.new(valid_employee_params, user).update
        expect(handler.response[:success]).to be_truthy
      end
    end

    context "with invalid parameters for work_experience" do
      it "should not update assigned parameters" do
        HrUpdate::WorkExperienceHandler.new(valid_employee_params, user).create
        invalid_employee_params[:id] = user.work_experiences.first.id

        handler = HrUpdate::WorkExperienceHandler.new(invalid_employee_params, user).update
        expect(handler.response[:success]).to be_falsey
      end
    end
  end
end
