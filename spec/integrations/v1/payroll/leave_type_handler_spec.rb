require "rails_helper"

describe LeaveTypeHandler do
  
  let!(:leave_type_valid_create_params) { { leave_code: "Test Code", leave_type: "Test Type" } }
  let!(:leave_type_missing_create_params) { { } }

  let!(:leave_type) { create(:leave_type)}
  let!(:leave_type_valid_update_params) { { id: leave_type.id, leave_code: "Update Test Code", leave_type: "Update Test Type" } }
  let!(:leave_type_missing_update_params) { { id: leave_type.id } }

  let!(:specific_leave_valid_create_params) { { leave_type_id: leave_type.id, specific_leave: "Test Specific Leave" } }
  let!(:specific_leave_missing_create_params) { { leave_type_id: leave_type.id } }

  let(:specific_leave) { create :leave_type, { leave_type_id: leave_type.id, specific_leave: "Test Specific Leave", parent: false } }
  let(:specific_leave_valid_update_params) { { id: specific_leave.id, leave_type_id: leave_type.id, specific_leave: "Update Test Specific Leave" } }
  let(:specific_leave_missing_update_params) { { id: specific_leave.id, leave_type_id: leave_type.id } }

  #FOR LEAVE TYPE
  describe :create_leave_type do
    context "with valid params" do
      it "returns created leave_type" do
        handler = LeaveTypeHandler.new(leave_type_valid_create_params, true).create_leave_type
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].leave_code).to eq(leave_type_valid_create_params[:leave_code])
      end

      it "returns error message if leave code already exist" do
        LeaveTypeHandler.new(leave_type_valid_create_params, true).create_leave_type
        handler = LeaveTypeHandler.new(leave_type_valid_create_params, true).create_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Leave code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = LeaveTypeHandler.new(leave_type_missing_create_params, true).create_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Leave code can't be blank, Leave type can't be blank")
      end
    end
  end

  describe :update_leave_type do
    context "with valid params" do
      it "returns updated leave_type" do
        handler = LeaveTypeHandler.new(leave_type_valid_update_params, true).update_leave_type
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].leave_code).to eq(leave_type_valid_update_params[:leave_code])
      end

      it "returns error message if leave code already exist" do
        LeaveTypeHandler.new(leave_type_valid_update_params, true).create_leave_type
        
        handler = LeaveTypeHandler.new(leave_type_valid_update_params, true).update_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Leave code has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = LeaveTypeHandler.new(leave_type_missing_update_params, true).update_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Leave code can't be blank, Leave type can't be blank")
      end

      it "doest not update invalid leave_type id" do
        handler = LeaveTypeHandler.new({id: 100}, true).update_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Invalid or missing leave type id")
      end
    end
  end

  describe :delete_leave_type do
    context "with valid params" do
      it "deletes valid leave_type id" do
        handler = LeaveTypeHandler.new({id: leave_type.id}, true).delete_leave_type
        expect(handler.response[:success]).to be_truthy
      end

      it "doest not deletes invalid leave_type id" do
        handler = LeaveTypeHandler.new({id: 100 }, true).delete_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Invalid or missing leave type id")
      end
    end
  end

  #FOR SPECIFIC LEAVE
  describe :create_specific_leave do
    context "with valid params" do
      it "returns created specific_leave" do
        handler = LeaveTypeHandler.new(specific_leave_valid_create_params, false).create_specific_leave
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].specific_leave).to eq(specific_leave_valid_create_params[:specific_leave])
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = LeaveTypeHandler.new(specific_leave_missing_create_params, false).create_leave_type
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Specific leave can't be blank")
      end
    end
  end

  describe :update_specific_leave do
    context "with valid params" do
      it "returns updated specific_leave" do
        handler = LeaveTypeHandler.new(specific_leave_valid_update_params, false).update_specific_leave
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].specific_leave).to eq(specific_leave_valid_update_params[:specific_leave])
      end
    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = LeaveTypeHandler.new(specific_leave_missing_update_params, false).update_specific_leave
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Specific leave can't be blank")
      end

      it "doest not update invalid specific_leave id" do
        handler = LeaveTypeHandler.new({id: 100, leave_type_id: leave_type.id }, false).update_specific_leave
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Invalid specific leave id")
      end
    end
  end


  describe :delete_specific_leave do
    context "with valid params" do
      it "deletes valid specific_leave id" do
        handler = LeaveTypeHandler.new({id: specific_leave.id}, false).delete_specific_leave
        expect(handler.response[:success]).to be_truthy
      end

      it "doest not deletes invalid specific_leave id" do
        handler = LeaveTypeHandler.new({id: 100}, false).delete_specific_leave
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Invalid specific leave id")
      end
    end
  end

end
