require "rails_helper"

describe PayrollGroupHandler do
  let!(:current_user) { create(:user) }

  let!(:project_code) { create(:project_code ) }
  let!(:valid_create_params) { { project_code_id: project_code.id, payroll_group_code: "Create Test Code", payroll_group_name: "Create Test Name", payroll_group_order: 2 } }
  let!(:missing_create_params) { {project_code_id: project_code.id, payroll_group_name: "Create Test Name" } }

  let!(:payroll_group) { create(:payroll_group ) }
  let!(:valid_update_params) { { id: payroll_group.id, project_code_id: project_code.id, payroll_group_code: "Update Test Code", payroll_group_name: "Update Test Name", payroll_group_order: 2 } }
  let!(:missing_update_params) { { id: payroll_group.id, project_code_id: project_code.id, payroll_group_name: "Update Test Name" } }

  describe :create_payroll_group do
    context "with valid params" do
      it "returns created payroll_group" do
        handler = PayrollGroupHandler.new(valid_create_params, current_user).create_payroll_group
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].payroll_group_code).to eq(valid_create_params[:payroll_group_code])
      end

      it "returns error message if group code and group order already exist" do
        PayrollGroupHandler.new(valid_create_params, current_user).create_payroll_group

        handler = PayrollGroupHandler.new(valid_create_params, current_user).create_payroll_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Payroll group code has already been taken, Payroll group order has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = PayrollGroupHandler.new(missing_create_params, current_user).create_payroll_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Payroll group code can't be blank")
      end
    end
  end

  describe :update_payroll_group do
    context "with valid params" do
      it "returns updated payroll_group" do
        handler = PayrollGroupHandler.new(valid_update_params, current_user).update_payroll_group
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].payroll_group_code).to eq(valid_update_params[:payroll_group_code])
      end

      it "returns error message if group code and group order already exist" do
        PayrollGroupHandler.new(valid_update_params, current_user).create_payroll_group
        
        handler = PayrollGroupHandler.new(valid_update_params, current_user).update_payroll_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Payroll group code has already been taken, Payroll group order has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = PayrollGroupHandler.new(missing_update_params, current_user).update_payroll_group
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Payroll group code can't be blank")
      end
    end
  end

  describe :delete_payroll_group do
    context "with valid params" do
      it "deletes valid payroll_group id" do
        handler = PayrollGroupHandler.new({id: payroll_group.id}, current_user).delete_payroll_group
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
