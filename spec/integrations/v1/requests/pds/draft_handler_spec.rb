require "rails_helper"

describe Requests::PDS::DraftHandler do

  let!(:owner)     { create(:user) }
  let!(:processor) { create(:user) }

  let!(:profile_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "update_profile_data",
      draft:{
        first_name: FFaker::Name.first_name,
        last_name: FFaker::Name.last_name,
        middle_name: "Oliva",
        middle_initial: "O",
        name_extension: FFaker::NameSE.suffix,
        birthdate: 20.years.ago,
        gender: FFaker::Gender.random,
        civil_status: "married",
        citizenship: "Filipino",
        height: 6.5,
        weight: 65.4,
        blood_type: "O",
        email: FFaker::Internet.free_email,
        # contact_devices: ["mobile","telephone"],
        # contact_numbers: [FFaker::PhoneNumber.phone_number,FFaker::PhoneNumber.short_phone_number],
        tin_number: FFaker::PhoneNumber.imei,
        gsis_policy_number: FFaker::PhoneNumber.imei,
        pagibig_id_number: FFaker::PhoneNumber.imei,
        philhealth_number: FFaker::PhoneNumber.imei,
        sss_number: FFaker::PhoneNumber.imei,
        remittance_id: FFaker::PhoneNumber.imei,
        # address_residencies: ["permanent", "residential"],
        # address_locations: [FFaker::AddressUS.city,FFaker::AddressUS.city],
        # address_zipcodes: [FFaker::AddressUS.zip_code,FFaker::AddressUS.zip_code],
        # address_contact_number: [FFaker::PhoneNumber.short_phone_number,FFaker::PhoneNumber.short_phone_number]
      }
    }
  }

  let!(:education_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "add_education",
      draft:{
        education_level_id: 1,
        school_name: FFaker::Education.school_name,
        course_id: 1,
        highest_grade_level_units_earned: nil,
        date_of_attendance_from: 10.years.ago,
        date_of_attendance_to: 1.year.ago,
        honors_received: "1st honor",
        year_graduated: 10.years.ago.year
      }
    }
  }

  let!(:examination_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "add_examination",
      draft: {
        exam_description: "testest",
        place_of_exam: FFaker::Address.city,
        rating: 92,
        date_of_exam:  DateTime.now,
        licence_number: FFaker::Identification.ssn,
        date_of_release: DateTime.now
      }
    }
  }

  let!(:work_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "add_work_experience",
      draft: {
        position_title: FFaker::Company.position,
        inclusive_date_from: "2014-11-01",
        inclusive_date_to: "2016-11-01",
        department_agency_office: FFaker::Company.bs,
        monthly_salary: "50,000",
        salary_grade_and_step: "02-5",
        appointment_status_id: 1,
        government_service: true
      }
    }
  }

  let!(:voluntary_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "add_voluntary_work",
      draft: {
        name_of_organization: FFaker::Company.name,
        address_of_organization: FFaker::Address.city,
        inclusive_date_from: DateTime.now,
        inclusive_date_to: DateTime.now,
        number_of_hours: 6,
        position_nature_of_work: FFaker::Company.position
      }
    }
  }

  let!(:family_info_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "update_family_information",
      draft: {
        spouse_name: FFaker::Name.name,
        spouse_occupation: FFaker::Company.position,
        spouse_employer_or_business_name: FFaker::Company.name,
        spouse_work_or_business_address: FFaker::Address.street_address,
        spouse_contact_number: FFaker::PhoneNumber.phone_number,
        parent_fathers_name: FFaker::Name.name,
        parent_mothers_maiden_name: FFaker::Name.name,
        parent_address: FFaker::Address.street_address,

        spouse_first_name: FFaker::Name.name,
        spouse_middle_name: FFaker::Name.name,
        spouse_last_name: FFaker::Name.name,
        parant_fathers_first_name: FFaker::Name.name,
        parant_fathers_middle_name: FFaker::Name.name,
        parant_fathers_last_name: FFaker::Name.name,
        parant_mothers_first_name: FFaker::Name.name,
        parant_mothers_middle_name: FFaker::Name.name,
        parant_mothers_last_name: FFaker::Name.name,
      }
    }
  }

  let!(:training_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "add_training_and_seminar",
      draft: {
        title: "Test1",
        inclusive_date_from: "2014-11-01",
        inclusive_date_to: "2015-11-01",
        number_of_hours: 10,
        conducted_by: "Mang Thomas"
      }
    }
  }

  let!(:other_information_params) {
    {
      owner_employee_id: owner.employee_id,
      classification: "update_other_information",
      draft: {
        skills_and_hobbies: FFaker::Sport.name,
        non_academic_destinction: FFaker::Lorem.word,
        membership_in: FFaker::Company.name,
        third_degree_national: true,
        give_national: FFaker::Lorem.phrase,
        fourth_degree_local: true,
        give_local: FFaker::Lorem.phrase,
        formally_charge: true,
        give_charge: FFaker::Lorem.phrase,
        administrative_offense: true,
        give_offense: FFaker::Lorem.phrase,
        any_violation: true,
        give_reasons: FFaker::Lorem.phrase,
        canditate: true,
        give_date_particulars: FFaker::Lorem.phrase,
        indigenous_member: true,
        give_group: FFaker::Lorem.phrase,
        differently_abled: true,
        give_disability: FFaker::Lorem.phrase,
        solo_parent: true,
        give_status: FFaker::Lorem.phrase,
        is_separated: true,
        give_details: FFaker::Lorem.phrase,
        signature: "#{FFaker::Name.first_name} #{FFaker::Name.last_name} ",
        date_accomplished: DateTime.now,
        ctc_number: FFaker::Identification.ssn,
        issued_at: DateTime.now,
        issued_on: DateTime.now
      }
    }
  }

  describe ".create" do
    context "with valid parameters" do
      it "should create drafts and assigned drafts for owner and procesor" do
        handler = Requests::PDS::DraftHandler.new(profile_params).create

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(1)
        expect(Draft.pending.count).to eq(1)
        expect(Draft.approved.count).to eq(0)
      end
    end
  end

  describe ".process_update" do
    context "with valid parameters for profile data" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(profile_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for education" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(education_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for examination" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(examination_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for work experience" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(work_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for voluntary work" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(voluntary_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for family info" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(family_info_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for training programs" do
      it "should update assigned drafts" do
        Requests::PDS::DraftHandler.new(training_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end

    context "with valid parameters for other information" do
      it "should update other information" do
        Requests::PDS::DraftHandler.new(other_information_params).create
        def process_params
          {
            processor_employee_id: processor.id,
            draft_id: owner.drafts.pending.last.id,
            roles: [0,1,2],
            status: "approved"
          }
        end

        handler = Requests::PDS::DraftHandler.new(process_params).process_update

        expect(handler.response[:success]).to be_truthy
        expect(owner.drafts.pending.count).to eq(0)
        expect(Draft.pending.count).to eq(0)
        expect(Draft.approved.count).to eq(1)
      end
    end
  end

end
