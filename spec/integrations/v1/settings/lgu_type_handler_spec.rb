require "rails_helper"

describe LguTypeHandler do
  let!(:valid_create_params) { {lgu_type: {name: "Test Name"} } }
  let!(:missing_create_params) { { lgu_type: {name: nil} }}

  let!(:lgu_type) { FactoryGirl.create(:lgu_type) }
  let!(:valid_update_params) { {id: lgu_type.id, lgu_type: {name: "Test Name"} } }
  let!(:missing_update_params) { {id: lgu_type.id, lgu_type: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created lgu_type" do
        handler = LguTypeHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:lgu_type][:name])
      end

      it "returns error message if lgu_type already exist" do
        LguTypeHandler.new(valid_create_params).create

        handler = LguTypeHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = LguTypeHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated lgu_type" do
        handler = LguTypeHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:lgu_type][:name])
      end

      it "returns error message if lgu_type already exist" do
        LguTypeHandler.new(valid_update_params).create

        handler = LguTypeHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = LguTypeHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid lgu_type id" do
        handler = LguTypeHandler.new({id: lgu_type.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
