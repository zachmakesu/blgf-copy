require "rails_helper"

describe ProvinceHandler do
  let!(:valid_create_params) { {province: {name: "Test Name", geocode: "geocode"} } }
  let!(:missing_create_params) { { province: {name: nil} }}

  let!(:province) { FactoryGirl.create(:province) }
  let!(:valid_update_params) { {id: province.id, province: {name: "Test Name", geocode: "updated_geocode"} } }
  let!(:missing_update_params) { {id: province.id, province: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created province" do
        handler = ProvinceHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:province][:name])
      end

      it "returns error message if province already exist" do
        ProvinceHandler.new(valid_create_params).create

        handler = ProvinceHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Geocode has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = ProvinceHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank, Geocode can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated province" do
        handler = ProvinceHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:province][:name])
      end

      it "returns error message if province already exist" do
        ProvinceHandler.new(valid_update_params).create

        handler = ProvinceHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Geocode has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = ProvinceHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid province id" do
        handler = ProvinceHandler.new({id: province.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
