require "rails_helper"

describe SalnHandler do
  let!(:admin) { create(:user) }
  let!(:treasurer) { create(:treasurer) }

  let!(:basic_info_params) {
    {
      saln:{
        statement_filing_status: "joint",
        last_name: FFaker::Name.last_name,
        first_name: FFaker::Name.first_name,
        middle_initial: "B",
        address: FFaker::Address.street_address,
        position: FFaker::Company.position,
        office: FFaker::Company.name,
        office_address: FFaker::Address.street_address,
        spouse_first_name: FFaker::Name.first_name,
        spouse_last_name: FFaker::Name.last_name,
        spouse_middle_initial: "P",
        spouse_position: FFaker::Company.position,
        spouse_office: FFaker::Company.name,
        spouse_office_address: FFaker::Address.street_address
      }
    }
  }

  let!(:child_params) {
    {
      saln_child:{
        name: "#{FFaker::Name.first_name} #{FFaker::Name.last_name}",
        birthdate: "#{rand(2000...2050)}-#{rand(1...12)}-#{rand(1...29)}",
        age: rand(10...42)
      }
    }
  }

  let!(:real_asset_params) {
    {
      saln_asset:{
        asset_type: "real",
        description: FFaker::BaconIpsum.paragraph, 
        kind: FFaker::BaconIpsum.word, 
        exact_location: FFaker::Address.street_address,
        assesed_value: 300,
        current_market_value: 300,
        acquisition_year: "rand(2000...2050)",
        acquisition_mode: FFaker::BaconIpsum.word,
        acquisition_cost: 300
      }
    }
  }

  let!(:personal_asset_params) {
    {
      saln_asset:{
        asset_type: "personal",
        description: FFaker::BaconIpsum.paragraph, 
        acquisition_year: "rand(2000...2050)",
        acquisition_cost: 300
      }
    }
  }

  let!(:liability_params) {
    {
      saln_liability:{
        nature: FFaker::BaconIpsum.word,
        creditors_name: "#{FFaker::Name.first_name} #{FFaker::Name.last_name}", 
        outstanding_balance: 2000
      }
    }
  }

  let!(:bi_and_fc_params) {
    {
      saln_bi_and_fc:{
        acquisition_date: "#{rand(2000...2050)}-#{rand(1...12)}-#{rand(1...29)}",
        business_address: FFaker::Address.street_address,
        business_enterprise: FFaker::BaconIpsum.word,
        business_financial_nature: FFaker::BaconIpsum.word
      }
    }
  }

  let!(:government_relative_params) {
    {
      saln_government_relative:{
        name: "#{rand(2000...2050)}-#{rand(1...12)}-#{rand(1...29)}",
        office_address: FFaker::Address.street_address,
        position: FFaker::BaconIpsum.word,
        relationship: FFaker::BaconIpsum.word
      }
    }
  }

  let!(:issued_id_params) {
    {
      saln_issued_id:{
        date_issued: "#{rand(2000...2050)}-#{rand(1...12)}-#{rand(1...29)}",
        id_type: "declarant",
        id_no: FFaker::BaconIpsum.word
      }
    }
  }

  describe :update_basic do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, basic_info_params, admin).update_basic
        expect(handler.response[:success]).to be_truthy
        basic_info_params[:saln].each{ |k,v| 
          v = 0 if k == :statement_filing_status  
          expect(handler.response[:details][k]).to eq(v)
        }
      end
    end
  end

  describe :add_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        expect(handler.response[:success]).to be_truthy
        child_params[:saln_child].each{ |k,v| 
          expect(handler.response[:details].saln_children.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        child_params[:id] = handler.response[:details].saln_children.first.id
        handler = SalnHandler.new(treasurer, child_params, admin).update_child
        expect(handler.response[:success]).to be_truthy
        child_params[:saln_child].each{ |k,v| 
          expect(handler.response[:details].saln_children.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        child_params[:id] = handler.response[:details].saln_children.first.id
        handler = SalnHandler.new(treasurer, child_params, admin).delete_child
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_real_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, real_asset_params, admin).add_real_asset
        expect(handler.response[:success]).to be_truthy
        real_asset_params[:saln_asset].each{ |k,v| 
          v = 0 if k == :asset_type 
          expect(handler.response[:details].saln_assets.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_real_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, real_asset_params, admin).add_real_asset
        real_asset_params[:id] = handler.response[:details].saln_assets.first.id
        handler = SalnHandler.new(treasurer, real_asset_params, admin).update_real_asset
        expect(handler.response[:success]).to be_truthy
        real_asset_params[:saln_asset].each{ |k,v| 
          v = 0 if k == :asset_type 
          expect(handler.response[:details].saln_assets.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_real_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, real_asset_params, admin).add_real_asset
        real_asset_params[:id] = handler.response[:details].saln_assets.first.id
        handler = SalnHandler.new(treasurer, real_asset_params, admin).delete_real_asset
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_personal_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, personal_asset_params, admin).add_personal_asset
        expect(handler.response[:success]).to be_truthy
        personal_asset_params[:saln_asset].each{ |k,v| 
          v = 1 if k == :asset_type 
          expect(handler.response[:details].saln_assets.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_personal_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, personal_asset_params, admin).add_personal_asset
        personal_asset_params[:id] = handler.response[:details].saln_assets.first.id
        handler = SalnHandler.new(treasurer, personal_asset_params, admin).update_personal_asset
        expect(handler.response[:success]).to be_truthy
        personal_asset_params[:saln_asset].each{ |k,v| 
          v = 1 if k == :asset_type 
          expect(handler.response[:details].saln_assets.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_personal_asset do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, personal_asset_params, admin).add_personal_asset
        personal_asset_params[:id] = handler.response[:details].saln_assets.first.id
        handler = SalnHandler.new(treasurer, personal_asset_params, admin).delete_personal_asset
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        expect(handler.response[:success]).to be_truthy
        child_params[:saln_child].each{ |k,v| 
          expect(handler.response[:details].saln_children.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        child_params[:id] = handler.response[:details].saln_children.first.id
        handler = SalnHandler.new(treasurer, child_params, admin).update_child
        expect(handler.response[:success]).to be_truthy
        child_params[:saln_child].each{ |k,v| 
          expect(handler.response[:details].saln_children.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_child do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, child_params, admin).add_child
        child_params[:id] = handler.response[:details].saln_children.first.id
        handler = SalnHandler.new(treasurer, child_params, admin).delete_child
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_liability do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, liability_params, admin).add_liability
        expect(handler.response[:success]).to be_truthy
        liability_params[:saln_liability].each{ |k,v| 
          expect(handler.response[:details].saln_liabilities.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_liability do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, liability_params, admin).add_liability
        liability_params[:id] = handler.response[:details].saln_liabilities.first.id
        handler = SalnHandler.new(treasurer, liability_params, admin).update_liability
        expect(handler.response[:success]).to be_truthy
        liability_params[:saln_liability].each{ |k,v| 
          expect(handler.response[:details].saln_liabilities.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_liability do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, liability_params, admin).add_liability
        liability_params[:id] = handler.response[:details].saln_liabilities.first.id
        handler = SalnHandler.new(treasurer, liability_params, admin).delete_liability
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_bi_and_fc do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, bi_and_fc_params, admin).add_bi_and_fc
        expect(handler.response[:success]).to be_truthy
        bi_and_fc_params[:saln_bi_and_fc].each{ |k,v| 
          expect(handler.response[:details].saln_bi_and_fcs.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_bi_and_fc do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, bi_and_fc_params, admin).add_bi_and_fc
        bi_and_fc_params[:id] = handler.response[:details].saln_bi_and_fcs.first.id
        handler = SalnHandler.new(treasurer, bi_and_fc_params, admin).update_bi_and_fc
        expect(handler.response[:success]).to be_truthy
        bi_and_fc_params[:saln_bi_and_fc].each{ |k,v| 
          expect(handler.response[:details].saln_bi_and_fcs.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_bi_and_fc do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, bi_and_fc_params, admin).add_bi_and_fc
        bi_and_fc_params[:id] = handler.response[:details].saln_bi_and_fcs.first.id
        handler = SalnHandler.new(treasurer, bi_and_fc_params, admin).delete_bi_and_fc
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_government_relative do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, government_relative_params, admin).add_government_relative
        expect(handler.response[:success]).to be_truthy
        government_relative_params[:saln_government_relative].each{ |k,v| 
          expect(handler.response[:details].saln_government_relatives.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_government_relative do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, government_relative_params, admin).add_government_relative
        government_relative_params[:id] = handler.response[:details].saln_government_relatives.first.id
        handler = SalnHandler.new(treasurer, government_relative_params, admin).update_government_relative
        expect(handler.response[:success]).to be_truthy
        government_relative_params[:saln_government_relative].each{ |k,v| 
          expect(handler.response[:details].saln_government_relatives.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_government_relative do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, government_relative_params, admin).add_government_relative
        government_relative_params[:id] = handler.response[:details].saln_government_relatives.first.id
        handler = SalnHandler.new(treasurer, government_relative_params, admin).delete_government_relative
        expect(handler.response[:success]).to be_truthy
      end
    end
  end

  describe :add_issued_id do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, issued_id_params, admin).add_issued_id
        expect(handler.response[:success]).to be_truthy
        issued_id_params[:saln_issued_id].each{ |k,v| 
          v = 0 if k == :id_type  
          expect(handler.response[:details].saln_issued_ids.first[k]).to eq(v)
        }
      end
    end
  end

  describe :update_issued_id do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, issued_id_params, admin).add_issued_id
        issued_id_params[:id] = handler.response[:details].saln_issued_ids.first.id
        handler = SalnHandler.new(treasurer, issued_id_params, admin).update_issued_id
        expect(handler.response[:success]).to be_truthy
        issued_id_params[:saln_issued_id].each{ |k,v| 
          v = 0 if k == :id_type 
          expect(handler.response[:details].saln_issued_ids.first[k]).to eq(v)
        }
      end
    end
  end

  describe :delete_issued_id do
    context "with valid params" do
      it "returns treasurer's saln" do
        handler = SalnHandler.new(treasurer, issued_id_params, admin).add_issued_id
        issued_id_params[:id] = handler.response[:details].saln_issued_ids.first.id
        handler = SalnHandler.new(treasurer, issued_id_params, admin).delete_issued_id
        expect(handler.response[:success]).to be_truthy
      end
    end
  end
end
