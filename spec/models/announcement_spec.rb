require 'rails_helper'

RSpec.describe Announcement, type: :model do
  describe "Announcement" do
    context "with valid params" do
      it "should create Announcement in Calendar" do
        expect{Announcement.create(announcement_type: 0, message: "Test Message", posted_at: DateTime.now + 10.days )}.to change{Announcement.count}.from(0).to(1)
      end


      it "should create Announcement in Web" do
        expect{Announcement.create(announcement_type: 1, message: "Test Message", posted_at: DateTime.now)}.to change{Announcement.count}.from(0).to(1)
      end
    end

    context "with missing params" do
      it "should not create Announcement" do
        expect{Announcement.create(announcement_type: 1,  message: "Test Message")}.not_to change{Announcement.count}
      end
    end
  
  end
end
