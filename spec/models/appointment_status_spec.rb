require 'rails_helper'

RSpec.describe AppointmentStatus, type: :model do
  describe "AppointmentStatus" do
    context "with valid params" do
      it "should create AppointmentStatus" do
        expect{AppointmentStatus.create(appointment_code: "Test Code", appointment_status: "Test Status")}.to change{AppointmentStatus.count}.from(0).to(1)
      end

      it "should not create AppointmentStatus if position_code already taken" do
        AppointmentStatus.create(appointment_code: "Test Code", appointment_status: "Test Status")
        expect{AppointmentStatus.create(appointment_code: "Test Code", appointment_status: "Test Status")}.not_to change{AppointmentStatus.count}
      end
    end

    context "with missing params" do
      it "should not create AppointmentStatus" do
        expect{AppointmentStatus.create(appointment_code: "Test Code")}.not_to change{AppointmentStatus.count}
      end
    end
  
  end
end
