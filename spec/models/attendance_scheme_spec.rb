require 'rails_helper'

RSpec.describe AttendanceScheme, type: :model do

  let!(:valid_params) { 
    { 
      :scheme_code => "Slide 1",
      :scheme_name => "Sliding 1",
      :am_time_in_from => "07:00:00",
      :am_time_in_to => "08:00:00",
      :pm_time_out_from => "16:00:00",
      :pm_time_out_to => "17:00:00",
      :nn_time_out_from => "12:00:00",
      :nn_time_out_to => "13:00:00",
      :nn_time_in_from => "12:00:00",
      :nn_time_in_to => "13:00:00"
    } 
  }

  let!(:invalid_params) { 
    { 
      :scheme_code => "Slide 1",
      :scheme_name => "Sliding 1",
      :am_time_in_from => "asd",
      :am_time_in_to => "08:00:00",
      :pm_time_out_from => "16:00:00",
      :pm_time_out_to => "17:00:00",
      :nn_time_out_from => "12:00:00",
      :nn_time_out_to => "13:00:00",
      :nn_time_in_from => "12:00:00",
      :nn_time_in_to => "13:00:00"
    } 
  }

  describe "AttendanceScheme" do
    context "with valid params" do
      it "should create AttendanceScheme" do
        expect{AttendanceScheme.create(valid_params)}.to change{AttendanceScheme.count}.from(0).to(1)
      end

      it "should not create AttendanceScheme if scheme_code and scheme_name already taken" do
        AttendanceScheme.create(valid_params)
        expect{AttendanceScheme.create(valid_params)}.not_to change{AttendanceScheme.count}
      end
    end

    context "with invalid params" do
      it "should not create AttendanceScheme" do
        expect{AttendanceScheme.create(invalid_params)}.not_to change{AttendanceScheme.count}
      end
    end
  
  end

end
