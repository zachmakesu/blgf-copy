require 'rails_helper'

RSpec.describe Course, type: :model do
  describe "Course" do
    context "with valid params" do
      it "should create Course" do
        expect{Course.create(course_code: "Test Code", course_description: "Test Description")}.to change{Course.count}.from(0).to(1)
      end

      it "should not create Course if position_code already taken" do
        Course.create(course_code: "Test Code", course_description: "Test Description")
        expect{Course.create(course_code: "Test Code", course_description: "Test Description")}.not_to change{Course.count}
      end
    end

    context "with missing params" do
      it "should not create Course" do
        expect{Course.create(course_code: "Test Code")}.not_to change{Course.count}
      end
    end
  
  end
end
