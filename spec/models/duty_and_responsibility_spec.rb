require 'rails_helper'

RSpec.describe DutyAndResponsibility, type: :model do
  let!(:position_code) { create(:position_code) }

  describe "DutyAndResponsibility" do
    context "with valid params" do
      it "should create Duty And Responsibility" do
        expect{DutyAndResponsibility.create(position_code: position_code, percent_work: 2, duties: "Test Duties")}.to change{DutyAndResponsibility.count}.from(0).to(1)
      end
    end

    context "with missing params" do
      it "should not create Duty And Responsibility" do
        expect{DutyAndResponsibility.create(position_code: position_code)}.not_to change{DutyAndResponsibility.count}
      end
    end
  
  end
end
