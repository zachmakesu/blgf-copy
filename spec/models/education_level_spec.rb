require 'rails_helper'

RSpec.describe EducationLevel, type: :model do
  describe "EducationLevel" do
    context "with valid params" do
      it "should create EducationLevel" do
        expect{EducationLevel.create(level_code: "Test Code", level_description: "Test Description")}.to change{EducationLevel.count}.from(0).to(1)
      end

      it "should not create EducationLevel if level_code already taken" do
        EducationLevel.create(level_code: "Test Code", level_description: "Test Description")
        expect{EducationLevel.create(level_code: "Test Code", level_description: "Test Description")}.not_to change{EducationLevel.count}
      end
    end

    context "with missing params" do
      it "should not create EducationLevel" do
        expect{EducationLevel.create(level_code: "Test Code")}.not_to change{EducationLevel.count}
      end
    end
  
  end
end
