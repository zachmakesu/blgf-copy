require 'rails_helper'

RSpec.describe ExamType, type: :model do
  describe "ExamType" do
    context "with valid params" do
      it "should create ExamType" do
        expect{ExamType.create(exam_code: "Test Code", exam_description: "Test Description")}.to change{ExamType.count}.from(0).to(1)
      end

      it "should not create ExamType if position_code already taken" do
        ExamType.create(exam_code: "Test Code", exam_description: "Test Description")
        expect{ExamType.create(exam_code: "Test Code", exam_description: "Test Description")}.not_to change{ExamType.count}
      end
    end

    context "with missing params" do
      it "should not create ExamType" do
        expect{ExamType.create(exam_code: "Test Code")}.not_to change{ExamType.count}
      end
    end
  
  end
end
