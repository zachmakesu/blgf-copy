require 'rails_helper'

RSpec.describe LeaveType, type: :model do
  let!(:leave_type) { create(:leave_type) }

  describe "LeaveType" do
    context "with valid params" do
      it "should create Parent Leave Type" do
        expect{LeaveType.create(leave_code: "test Code", leave_type: "test Type", parent: true)}.to change{LeaveType.count}.from(1).to(2)
      end

      it "should not create Parent Leave Type if leave_code already exist " do
        expect{LeaveType.create(leave_code: leave_type.leave_code, leave_type: "test Type", parent: true)}.not_to change{LeaveType.count}
      end
    end

    context "with missing params" do
      it "should not create Parent Leave Type if parent missing" do
        expect{LeaveType.create(leave_code: "test Code", leave_type: "test Type")}.not_to change{LeaveType.count}
      end

      it "should not create Parent Leave Type if leave_type missing" do
        expect{LeaveType.create(leave_code: "test Code", parent: true)}.not_to change{LeaveType.count}
      end

      it "should not create Parent Leave Type if leave_code missing" do
        expect{LeaveType.create(leave_type: "test Type", parent: true)}.not_to change{LeaveType.count}
      end
    end
  end

  describe "Specific Leave" do
    context "with valid params" do
      it "should create Specific Leave" do
        expect{LeaveType.create(leave_type_id: leave_type.id, specific_leave: "test Specific Leave", parent: false)}.to change{LeaveType.count}.from(1).to(2)
      end
    end

    context "with missing params" do
      it "should not create Specific Leave parent missing" do
        expect{LeaveType.create(leave_type_id: leave_type.id, specific_leave: "test Specific Leave")}.not_to change{LeaveType.count}
      end

      it "should not create Parent Leave Type if specific_leave missing" do
        expect{LeaveType.create(leave_type_id: leave_type.id, parent: false)}.not_to change{LeaveType.count}
      end

      it "should not create Parent Leave Type if leave_type_id missing" do
        expect{LeaveType.create(specific_leave: "test Specific Leave", parent: false)}.not_to change{LeaveType.count}
      end
    end
  end
end
