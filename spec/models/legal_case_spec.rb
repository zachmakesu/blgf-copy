require 'rails_helper'

RSpec.describe LegalCase, type: :model do
  let(:legal_case){ FactoryGirl.build(:legal_case) }
  let(:dummy_record){ FactoryGirl.create(:legal_case) }
  let(:user){ FactoryGirl.create(:user) }
  
  context "with valid params" do
    it "is valid" do
      expect(legal_case).to be_valid
    end
  
    it "creates legal case" do
      expect{LegalCase.create(legal_case.attributes)}.to change{LegalCase.count}.from(0).to(1)
    end      
  end

  context "when case number is not present" do
    it "is not valid" do
      legal_case.case_number = nil
      expect(legal_case).to_not be_valid
    end

    it "does not create legal case" do
      legal_case.case_number = nil
      expect{LegalCase.create(legal_case.attributes)}.not_to change(LegalCase, :count)
    end
  end

  describe ".of_user(user_id)" do
    context "when viewing specific employee's legal cases" do
      it "return legal case" do
        record = described_class.of_user(user.id)
        expect(record).to match_array(LegalCase.of_user(user.id))
      end

      it "does not return legal case" do
        user_id = nil
        record = described_class.of_user(user_id)
        expect(record).to be_empty
      end
    end
  end
end
