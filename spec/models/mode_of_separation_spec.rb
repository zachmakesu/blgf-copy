require 'rails_helper'

RSpec.describe ModeOfSeparation, type: :model do
  describe "ModeOfSeparation" do
    context "with valid params" do
      it "should create ModeOfSeparation" do
        expect{ModeOfSeparation.create(separation_mode: "AWOL")}.to change{ModeOfSeparation.count}.from(0).to(1)
      end

      it "should not create ModeOfSeparation if separation_mode already taken" do
        ModeOfSeparation.create(separation_mode: "AWOL")
        expect{ModeOfSeparation.create(separation_mode: "AWOL")}.not_to change{ModeOfSeparation.count}
      end
    end

    context "with missing params" do
      it "should not create ModeOfSeparation" do
        expect{ModeOfSeparation.create()}.not_to change{ModeOfSeparation.count}
      end
    end
  
  end
end
