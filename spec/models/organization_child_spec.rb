require 'rails_helper'

RSpec.describe OrganizationChild, type: :model do
  let!(:executive_office) { Organization.create(level: 1, name: "Executive Office") }
  let!(:service)          { Organization.create(level: 2, name: "Service") }
  let!(:division)         { Organization.create(level: 3, name: "Division") }
  let!(:section)          { Organization.create(level: 4, name: "Section") }

  let!(:user)              { create(:user) }

  describe "Executive Office" do
    context "with valid_params" do
      it "should create executive_office child" do
        expect{executive_office.organization_children.create(head_title: "test head_title", code: "test code", user: user, name: "test name")}.to change{executive_office.organization_children.count}.from(0).to(1)
      end
    end
    context "with missing params" do
      it "should not create executive_office child" do
        expect{executive_office.organization_children.create(head_title: "test head_title")}.not_to change{executive_office.organization_children.count}
      end
    end
  end

  describe "Service" do
    context "with valid_params" do
      it "should create service child" do
        expect{service.organization_children.create(executive_office_id: executive_office.id, head_title: "test head_title", code: "test code", user: user, name: "test name")}.to change{service.organization_children.count}.from(0).to(1)
      end
    end
    context "with missing params" do
      it "should not create service child" do
        expect{service.organization_children.create(head_title: "test head_title")}.not_to change{service.organization_children.count}
      end
    end
  end

  describe "Division" do
    context "with valid_params" do
      it "should create division child" do
        expect{division.organization_children.create(executive_office_id: executive_office.id, service_id: service.id ,head_title: "test head_title", code: "test code", user: user, name: "test name")}.to change{division.organization_children.count}.from(0).to(1)
      end
    end
    context "with missing params" do
      it "should not create division child" do
        expect{division.organization_children.create(head_title: "test head_title")}.not_to change{division.organization_children.count}
      end
    end
  end

  describe "Section" do
    context "with valid_params" do
      it "should create section child" do
        expect{section.organization_children.create(executive_office_id: executive_office.id, service_id: service.id, division_id: division.id, head_title: "test head_title", code: "test code", user: user, name: "test name")}.to change{section.organization_children.count}.from(0).to(1)
      end
    end
    context "with missing params" do
      it "should not create section child" do
        expect{section.organization_children.create(head_title: "test head_title")}.not_to change{section.organization_children.count}
      end
    end
  end
end
