require 'rails_helper'

RSpec.describe Organization, type: :model do
  
  describe "Organization" do
    context "with valid params" do
      it "should create Organization" do
        expect{Organization.create(name: "Executive Office", level: 1)}.to change{Organization.count}.from(0).to(1)
      end

      it "should not create Organization if level already taken" do
        Organization.create(name: "Executive Office", level: 1)
        expect{Organization.create(name: "Executive Office", level: 1)}.not_to change{Organization.count}
      end
    end

    context "with missing params" do
      it "should not create Organization" do
        expect{Organization.create(name: "Executive Office")}.not_to change{Organization.count}
      end
    end
  
  end
end
