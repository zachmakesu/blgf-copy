require 'rails_helper'

RSpec.describe OtherInformation, type: :model do
  let!(:user) { create(:user) }

  describe "OtherInformation" do
    context "with valid params" do
      it "should create OtherInformation" do
        expect{OtherInformation.create(user_id: 2)}.to change{OtherInformation.count}.from(1).to(2)
      end

      it "should update user OtherInformation" do
        expect(user.other_information.update(indigenous_member: true, give_group: "Ita")).to be_truthy
      end
    end

    context "with valid true params" do
      it "should not update user OtherInformation if missing designated fields " do
        expect(user.other_information.update(third_degree_national: true)).to be_falsey
        expect(user.other_information.update(fourth_degree_local: true)).to be_falsey
        expect(user.other_information.update(formally_charge: true)).to be_falsey
        expect(user.other_information.update(administrative_offense: true)).to be_falsey
        expect(user.other_information.update(any_violation: true)).to be_falsey
        expect(user.other_information.update(canditate: true)).to be_falsey
        expect(user.other_information.update(indigenous_member: true)).to be_falsey
        expect(user.other_information.update(differently_abled: true)).to be_falsey
        expect(user.other_information.update(solo_parent: true)).to be_falsey
        expect(user.other_information.update(is_separated: true)).to be_falsey
      end
    end
  
  end
end
