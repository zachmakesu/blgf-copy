require 'rails_helper'

RSpec.describe PayrollGroup, type: :model do
  let!(:project_code) { create(:project_code) }
  describe "PayrollGroup" do
    context "with valid params" do
      it "should create PayrollGroup" do
        expect{PayrollGroup.create(project_code_id: project_code.id ,payroll_group_code: "Test Code", payroll_group_name: "Test Name", payroll_group_order: 1)}.to change{PayrollGroup.count}.from(0).to(1)
      end

      it "should not create PayrollGroup if group_code and group_order already taken" do
        PayrollGroup.create(project_code_id: project_code.id, payroll_group_code: "Test Code", payroll_group_name: "Test Name", payroll_group_order: 1)
        expect{PayrollGroup.create(project_code_id: project_code.id, payroll_group_code: "Test Code", payroll_group_name: "Test Name", payroll_group_order: 1)}.not_to change{PayrollGroup.count}
      end

    end

    context "with missing params" do
      it "should not create PayrollGroup" do
        expect{PayrollGroup.create(payroll_group_code: "Test Code")}.not_to change{PayrollGroup.count}
      end
    end
  
  end
end
