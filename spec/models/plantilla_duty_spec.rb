require 'rails_helper'

RSpec.describe PlantillaDuty, type: :model do
  let!(:plantilla) { create(:plantilla) }

  describe "PlantillaDuty" do
    context "with valid params" do
      it "should create Plantilla Duty And Responsibility" do
        expect{PlantillaDuty.create(plantilla: plantilla, percent_work: 2, duties: "Test Duties")}.to change{PlantillaDuty.count}.from(0).to(1)
      end
    end

    context "with missing params" do
      it "should not create Plantilla Duty And Responsibility" do
        expect{PlantillaDuty.create(plantilla: plantilla)}.not_to change{PlantillaDuty.count}
      end
    end
  
  end
end
