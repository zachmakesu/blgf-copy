require 'rails_helper'

RSpec.describe PlantillaGroup, type: :model do
  describe "PlantillaGroup" do
    context "with valid params" do
      it "should create PlantillaGroup" do
        expect{PlantillaGroup.create(group_code: "Test Code", group_name: "Test Name", group_order: 1)}.to change{PlantillaGroup.count}.from(0).to(1)
      end

      it "should not create PlantillaGroup if group_code and group_order already taken" do
        PlantillaGroup.create(group_code: "Test Code", group_name: "Test Name", group_order: 1)
        expect{PlantillaGroup.create(group_code: "Test Code", group_name: "Test Name", group_order: 1)}.not_to change{PlantillaGroup.count}
      end

    end

    context "with missing params" do
      it "should not create PlantillaGroup" do
        expect{PlantillaGroup.create(group_code: "Test Code")}.not_to change{PlantillaGroup.count}
      end
    end
  
  end
end
