require 'rails_helper'

RSpec.describe Plantilla, type: :model do

  let!(:plantilla_group) { create(:plantilla_group) }
  let!(:position_code) { create(:position_code) }
  let!(:salary_schedule) { create(:salary_schedule) }

  let!(:valid_params) { { item_number: "Test Item Number", plantilla_group_id: plantilla_group.id, position_code_id: position_code.id, salary_schedule_grade: salary_schedule.salary_grade } }
  let!(:missing_params) { { item_number: "Test Item Number", salary_schedule: salary_schedule } }

  describe "Plantilla" do
    context "with valid params" do
      it "should create Plantilla" do
        expect{Plantilla.create(valid_params)}.to change{Plantilla.count}.from(0).to(1)
      end

      it "should not create Plantilla if item_number already taken" do
        Plantilla.create(valid_params)
        expect{Plantilla.create(valid_params)}.not_to change{Plantilla.count}
      end

    end

    context "with missing params" do
      it "should not create Plantilla" do
        expect{Plantilla.create(missing_params)}.not_to change{Plantilla.count}
      end
    end
  
  end
end
