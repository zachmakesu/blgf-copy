require 'rails_helper'

RSpec.describe PositionCode, type: :model do
  describe "PositionCode" do
    context "with valid params" do
      it "should create PositionCode" do
        expect{PositionCode.create(position_code: "Test Code", position_description: "Test Description")}.to change{PositionCode.count}.from(0).to(1)
      end

      it "should not create PositionCode if position_code already taken" do
        PositionCode.create(position_code: "Test Code", position_description: "Test Description")
        expect{PositionCode.create(position_code: "Test Code", position_description: "Test Description")}.not_to change{PositionCode.count}
      end
    end

    context "with missing params" do
      it "should not create PositionCode" do
        expect{PositionCode.create(position_code: "Test Code")}.not_to change{PositionCode.count}
      end
    end
  
  end
end
