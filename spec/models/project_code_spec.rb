require 'rails_helper'

RSpec.describe ProjectCode, type: :model do
  describe "ProjectCode" do
    context "with valid params" do
      it "should create ProjectCode" do
        expect{ProjectCode.create(project_code: "Test Code", project_description: "Test Description", project_order: 1)}.to change{ProjectCode.count}.from(0).to(1)
      end

      it "should not create ProjectCode if project_code and project_order already taken" do
        ProjectCode.create(project_code: "Test Code", project_description: "Test Description", project_order: 1)
        expect{ProjectCode.create(project_code: "Test Code", project_description: "Test Description", project_order: 1)}.not_to change{ProjectCode.count}
      end

    end

    context "with missing params" do
      it "should not create ProjectCode" do
        expect{ProjectCode.create(project_code: "Test Code")}.not_to change{ProjectCode.count}
      end
    end
  
  end
end
