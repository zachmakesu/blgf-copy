require 'rails_helper'

RSpec.describe Province, type: :model do
  let!(:province) { FactoryGirl.create(:province) }

  describe "Province" do
    context "with valid params" do
      it "should create Province" do
        expect{Province.create(name: "Test Name", geocode: "geocode")}.to change{Province.count}.from(1).to(2)
      end

      it "should not create Province on geocode already exist" do
        Province.create(name: "Test Name", geocode: "geocode")
        expect{Province.create(name: "First Name", geocode: "geocode")}.not_to change{Province.count}
      end

    end

    context "with invalid params" do
      it "should not create Province" do
        expect{Province.create(name: nil)}.not_to change{Province.count}
      end
    end

    context "with valid search text" do
      it "should fetch single data" do
        expect( Province.search("F").first ).to eq(province)
      end
    end

  end
end
