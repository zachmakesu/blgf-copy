require 'rails_helper'

RSpec.describe Region, type: :model do
  let!(:region) { FactoryGirl.create(:region) }

  describe "Region" do
    context "with valid params" do
      it "should create Region" do
        expect{Region.create(name: "Test Name")}.to change{Region.count}.from(1).to(2)
      end

      it "should not create Region on name already exist" do
        expect{Region.create(name: "First Name")}.not_to change{Region.count}
      end

    end

    context "with invalid params" do
      it "should not create Region" do
        expect{Region.create(name: nil)}.not_to change{Region.count}
      end
    end

    context "with valid search text" do
      it "should fetch single data" do
        expect( Region.search("F").first ).to eq(region)
      end
    end

  end
end
