require 'rails_helper'

RSpec.describe RequestType, type: :model do
  describe "RequestType" do
    context "with valid params" do
      it "should create RequestType" do
        expect{RequestType.create(type_of_request: "Test Request", applicants: 0)}.to change{RequestType.count}.from(0).to(1)
      end

      it "should not create RequestType if type_of_request already taken" do
        RequestType.create(type_of_request: "Test Request", applicants: 0)
        expect{RequestType.create(type_of_request: "Test Request", applicants: 0)}.not_to change{RequestType.count}
      end
    end

    context "with missing params" do
      it "should not create RequestType" do
        expect{RequestType.create(type_of_request: "Test Request")}.not_to change{RequestType.count}
      end
    end
  
  end
end
