require 'rails_helper'

RSpec.describe Scholarship, type: :model do
  describe "Scholarship" do
    context "with valid params" do
      it "should create Scholarship" do
        expect{Scholarship.create(description: "DOST-SEI Scholarship")}.to change{Scholarship.count}.from(0).to(1)
      end

      it "should not create Scholarship if description already taken" do
        Scholarship.create(description: "DOST-SEI Scholarship")
        expect{Scholarship.create(description: "DOST-SEI Scholarship")}.not_to change{Scholarship.count}
      end
    end

    context "with missing params" do
      it "should not create Scholarship" do
        expect{Scholarship.create()}.not_to change{Scholarship.count}
      end
    end
  
  end
end
