require 'rails_helper'

RSpec.describe ServiceCode, type: :model do
  describe "ServiceCode" do
    context "with valid params" do
      it "should create ServiceCode" do
        expect{ServiceCode.create(service_code: "Test Code", service_description: "Test Description")}.to change{ServiceCode.count}.from(0).to(1)
      end

      it "should not create ServiceCode if service_code already taken" do
        ServiceCode.create(service_code: "Test Code", service_description: "Test Description")
        expect{ServiceCode.create(service_code: "Test Code", service_description: "Test Description")}.not_to change{ServiceCode.count}
      end
    end

    context "with missing params" do
      it "should not create ServiceCode" do
        expect{ServiceCode.create(service_code: "Test Code")}.not_to change{ServiceCode.count}
      end
    end
  
  end
end
