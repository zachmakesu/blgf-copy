# == Schema Information
#
# Table name: users
#
#  id                                :integer          not null, primary key
#  email                             :string           default(""), not null
#  encrypted_password                :string           default(""), not null
#  reset_password_token              :string
#  reset_password_sent_at            :datetime
#  remember_created_at               :datetime
#  sign_in_count                     :integer          default(0), not null
#  current_sign_in_at                :datetime
#  last_sign_in_at                   :datetime
#  current_sign_in_ip                :inet
#  last_sign_in_ip                   :inet
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  first_name                        :string
#  last_name                         :string
#  middle_name                       :string
#  middle_initial                    :string
#  name_extension                    :string
#  birthdate                         :string
#  gender                            :integer          default(0), not null
#  civil_status                      :integer          default(0), not null
#  citizenship                       :string
#  height                            :float
#  weight                            :float
#  blood_type                        :string
#  cell_phone_number                 :string
#  agency_employee_number            :string
#  tin_number                        :string
#  gsis_policy_number                :string
#  pagibig_id_number                 :string
#  philhealth_number                 :string
#  sss_number                        :string
#  residential_address               :text
#  residential_zip_code              :string
#  residential_landline_number       :string
#  permanent_address                 :text
#  permanent_zip_code                :string
#  permanent_landline_number         :string
#  spouse_name                       :string
#  spouse_occupation                 :string
#  spouse_employer_or_business_name  :string
#  spouse_work_or_business_address   :string
#  spouse_mobile_or_telephone_number :string
#  parent_fathers_name               :string
#  parent_mothers_maiden_name        :string
#  parent_address                    :text
#  role                              :integer          default(0), not null
#  deleted_at                        :string
#

require 'rails_helper'

RSpec.describe User, type: :model do
end
