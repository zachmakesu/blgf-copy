require "rails_helper"

RSpec.feature "MyLegalCase", type: :request do
  let(:admin){FactoryGirl.create(:user)}
  let(:dummy_user){FactoryGirl.create(:user)}
  let(:valid_legal_case_params){{         
    case_number: FFaker::AddressCA.building_number,
    charged_offenses: FFaker::Lorem.phrases,
    status:           FFaker::Lorem.word,
    title:  FFaker::Lorem.word
  }}

  before :each do
    @headers = {
      'HTTP_AUTHORIZATION' => "#{http_login(admin)}"
    }
    current_user = FactoryGirl.create(:api_key, user: admin)
    @token = "ThisIsAValidAPITokenOfValid32<Len"
  end
  
  describe "LegalCaseManager" do
    context "when admin view users legal cases" do
      it "response with success status code if params are valid" do
        get "/api/v1/legal_case_manager/legal_cases", { access_token: @token }, @headers #url, access_token, params, headers
        expect(response).to have_http_status(200)
      end

      it "response with unauthorized status code if headers are missing" do
        get "/api/v1/legal_case_manager/legal_cases", { access_token: @token }
        expect(response).to have_http_status(401)
      end
    end

    context "when admin create employee's legal cases" do
      it "response with successfully created status code if params are valid" do
        @params = valid_legal_case_params
        @params[:access_token]  = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}", @params, @headers #url, access_token, params, headers
        expect(response).to have_http_status(201)
      end

      it "response with unsuccessful status code if params are invalid" do
        @params = valid_legal_case_params
        @params[:case_number] = nil
        @params[:access_token] = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}", @params, @headers
        expect(response).to have_http_status(400)
      end

      it "response with unauthorized status code if headers are missing" do
        @invalid_legal_case_params = valid_legal_case_params
        @invalid_legal_case_params[:case_number] = nil
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}", @invalid_legal_case_params
        expect(response).to have_http_status(401)
      end
    end

    context "when admin update employee's legal cases" do
      before :each do
        @params = valid_legal_case_params
        @params[:access_token]  = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}", @params, @headers #url, access_token, params, headers
      end

      it "response with successfully updated status code if params are valid" do
        @params = valid_legal_case_params
        @params[:access_token]  = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}/#{dummy_user.legal_cases.first.id}/update", @params, @headers #url, access_token, params, headers
        expect(response).to have_http_status(201)
      end

      it "response with unsuccessful status code if params are invalid" do
        @params = valid_legal_case_params
        @params[:case_number] = nil
        @params[:access_token] = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}/#{dummy_user.legal_cases.first.id}/update", @params, @headers
        expect(response).to have_http_status(400)
      end

      it "response with unauthorized status code if headers are missing" do
        @invalid_legal_case_params = valid_legal_case_params
        @invalid_legal_case_params[:case_number] = nil
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}/#{dummy_user.legal_cases.first.id}/update", @invalid_legal_case_params
        expect(response).to have_http_status(401)
      end
    end

    context "when admin delete employee's legal cases" do
       before :each do
        @params = valid_legal_case_params
        @params[:access_token]  = @token
        post "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}", @params, @headers #url, access_token, params, headers
      end
      it "response with successfull status code if params are valid" do
        delete "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}/#{dummy_user.legal_cases.first.id}/delete", { access_token: @token }, @headers #url, access_token, params, headers
        expect(response).to have_http_status(200)
      end

      it "response with unauthorized status code if headers are missing" do
        delete "/api/v1/legal_case_manager/legal_cases/#{dummy_user.employee_id}/#{dummy_user.legal_cases.first.id}/delete"
        expect(response).to have_http_status(401)
      end
    end
  end
end
